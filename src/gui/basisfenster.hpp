#pragma once

#include "../welt/zeit.hpp"
#include "../welt/spieler/dynastie.hpp"
#include "gui.hpp"
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

/**
 * Von diesem Fenster kann geerbt werden,
 * um einige grundlegende Funktionen wegzuabstrahieren.
 * Dazu gehört:
 * + das verwalten und synchronisieren von Zeit + Dynastie und
 * + das Anzeigen des Rahmens.
 * Wird beispielsweise von `Gui_Lager` verwendet.
 */
class Basisfenster {

public:

    /// Zu verwendender Konstruktor.
    explicit Basisfenster(sf::RenderWindow& fenster = *Gui::fenster);

    /// Zeigt das Fenster an.
    virtual void start() final;

protected:

    /**
     *  Zu überschreibender Schleifenkörpper.
     *  Wird jedes Frame ausgeführt von `start()`.
     *  @note Am Ende des Aufrufs _sollte_ `fenster.clear()` ausgeführt werden.
     *  @note Zu beginn _muss_ um Bugs vorzubeugen die Event-Schleife abgefragt werden:
     *
     *        sf::Event event;
     *        while (fenster.pollEvent(event)) {
     *            ImGui::SFML::ProcessEvent(event);
     *            // ...
     */
    virtual void loop_body() = 0;

    /// Schließt den Renderprozess ab.
    virtual void draw() final;

    /// False wird die Schleife des Anzeigens Beenden.
    bool loop;

    /// In dieses Fenster wird gerendert.
    sf::RenderWindow& fenster;

    /// Spielerdynastie. Wird automatisch synchronisiert.
    Dynastie dynastie;

    /// Zeit. Wird automatisch synchronisiert.
    Zeit zeit;

    /// Verwendbarer Timer für Netzwerksynchros. Darf frei von Kindklassen verwendet werden.
    sf::Clock net_clock;

    /// Verwendeter Timer für Grafikrendering. Von dieser Klasse reserviert.
    sf::Clock fps_clock;

private:

    /// Synchroniert Zeit + Dynastie.
    void basis_sync();

    /// Verwendeter Timer für `basis_sync()`.
    sf::Clock basis_sync_clock;

};
