#include <imgui-SFML.h>
#include "basisfenster.hpp"
#include "../gfx/rahmen.hpp"
#include "../netzwerk/klient.hpp"

Basisfenster::Basisfenster(sf::RenderWindow& fenster) :
    loop(true), fenster(fenster), dynastie(Klient::id())
{
    dynastie = Klient::request(Netzwerk::Anfrage::DYNASTIE, dynastie.get_id(), dynastie);
    zeit = Klient::request(Netzwerk::Anfrage::ZEIT, 0, zeit);
}

void Basisfenster::start() {
    net_clock.restart();
    basis_sync_clock.restart();
    while (loop) {
        basis_sync();
        ImGui::SFML::Update(fenster, fps_clock.restart());
        loop_body(); // override
        draw();
    }
}

void Basisfenster::draw() {
    ImGui::SFML::Render(fenster);
    Rahmen::set(zeit);
    Rahmen::set(dynastie);
    Rahmen::draw();
    fenster.display();
}

void Basisfenster::basis_sync() {
    if (basis_sync_clock.getElapsedTime() >= sf::milliseconds(500)) {
        dynastie = Klient::request(Netzwerk::Anfrage::DYNASTIE, dynastie.get_id(), dynastie);
        zeit = Klient::request(Netzwerk::Anfrage::ZEIT, 0, zeit);
        basis_sync_clock.restart();
    }
}
