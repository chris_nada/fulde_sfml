#include <implot.h>
#include <misc/cpp/imgui_stdlib.h>
#include <imgui_internal.h>

#include "gui_heim.hpp"
#include "../gfx/rahmen.hpp"
#include "../netzwerk/klient.hpp"
#include "../lingua/lingua.hpp"
#include "../welt/produktion/werkstatt.hpp"
#include "zusatz/imgui_zusatz.h"
#include <nada/log.hpp>

using nada::Log;

Gui_Heim::Gui_Heim(const Gebaeude& gebaeude) : dynastie(gebaeude.get_besitzer()) {
    sync();
}

void Gui_Heim::sync() {
    dynastie = Klient::request(Netzwerk::Anfrage::DYNASTIE, dynastie.get_id(), dynastie);
    charakter = Klient::request(Netzwerk::Anfrage::CHARAKTER, dynastie.get_oberhaupt(), charakter);
    sync_dynastien();
}

void Gui_Heim::loop_body() {
    // Events
    sf::Event event;
    while (fenster.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(event);
        if (event.type == sf::Event::Closed) fenster.close(); //macht Fenster schließen mögich
        if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Escape: loop = false; break;
                default: break;
            }
        }
    }

    ImGui::SetNextWindowPos({0, Rahmen::ABSTAND_TOP});
    ImGui::SetNextWindowSize({static_cast<float>(fenster.getSize().x), fenster.getSize().y - Rahmen::ABSTAND_TOP});
    ImGui::Begin("Heim", nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

    // Tabs
    if (ImGui::BeginTabBar("heimtabs", ImGuiTabBarFlags_NoCloseWithMiddleMouseButton)) {
        if (imgui_zusatz::Tab("nachrichten")) {
            draw_nachrichten();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("dynastie")) {
            draw_dynastie();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("infrastruktur")) {
            draw_infrastruktur();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("finanzen")) {
            draw_finanzen();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("aemter")) {
            draw_aemter();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("kontrahenten")) {
            draw_kontrahenten();
            ImGui::EndTabItem();
        }
        ImGui::EndTabBar();
    }

    ImGui::End();
    fenster.clear();
}

void Gui_Heim::draw_dynastie() {
    ImGui::Image(*charakter.get_grafik().data());
    ImGui::Text("%s %s", charakter.get_vorname().c_str(), dynastie.get_name().c_str());

}

void Gui_Heim::draw_infrastruktur() {
    static std::vector<Werkstatt> werke = Klient::get_werke(dynastie);
    static sf::Clock timer;
    if (timer.getElapsedTime().asSeconds() > 3) {
        werke = Klient::get_werke(dynastie);
        timer.restart();
    }

    // Überschriften
    ImGui::Columns(4, "infrastruktur", true);
    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("anlage").c_str()); ImGui::NextColumn();
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("fortschritt").c_str()); ImGui::NextColumn();
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("arbeiter").c_str()); ImGui::NextColumn();
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("taegliche_kosten").c_str()); ImGui::NextColumn();
    ImGui::PopFont();

    // Daten eintragen
    for (const Werkstatt& werk : werke) { // TODO Werkstandort
        unsigned anlagen = 0;
        for (const Anlage& anlage : werk.get_anlagen()) {
            ImGui::TextUnformatted(TXT::get(anlage.get_prozess().get_key()).c_str()); ImGui::NextColumn();
            imgui_zusatz::Fortschritt(anlage.get_fortschritt(), anlage.is_in_betrieb()); ImGui::NextColumn();
            ImGui::Text("%u/%d", static_cast<unsigned>(anlage.get_arbeiter().size()), anlage.get_prozess().get_arbeiter()); ImGui::NextColumn();
            //ImGui::Text("%u", anlage.get_taegliche_kosten());
            ImGui::NextColumn();
            ++anlagen;
        }
        if (anlagen == 0) ImGui::TextUnformatted(TXT::get("keine_anlagen").c_str());
    }
    ImGui::Columns(); // Columns Ende
}

void Gui_Heim::draw_finanzen() {

}

void Gui_Heim::draw_aemter() {

}

void Gui_Heim::draw_kontrahenten() {
    static int combo_wahl = 0;
    static Statistik statistik = Klient::request(Netzwerk::Anfrage::DYNASTIESTATISTIK, combo_wahl, statistik);
    static Zeit zeit;
    static std::vector<const char*> dynastien_namen;

    // Sync
    if (static sf::Clock timer; timer.getElapsedTime().asSeconds() > 5 || dynastien_namen.empty()) {
        zeit      = Klient::request(Netzwerk::Anfrage::ZEIT, 0, zeit);
        dynastien_namen.clear();
        sync_dynastien();
        for (const auto& dyn : dynastien) dynastien_namen.push_back(dyn.get_name().c_str());
        timer.restart();
    }

    // Anzuzeigende Dynastie auswählen
    if (ImGui::Combo("Dynastie", &combo_wahl, dynastien_namen.data(), dynastien_namen.size())) {
        statistik = Klient::request(Netzwerk::Anfrage::DYNASTIESTATISTIK, combo_wahl, statistik);
    }

    /// Funktion erstellt einen Plot; Anwendungsbeispiele siehe darunter
    auto plot = [&](const std::string& titel, const auto& datentypen, auto statistik_getter) {
        using plot_t = unsigned; // Datentyp für die geplotteten Werte
        ImPlot::SetNextAxesToFit();
        ImPlot::PushStyleVar(ImPlotStyleVar_LabelPadding, {50, 0});
        ImPlot::PushStyleVar(ImPlotStyleVar_PlotPadding, {25, 5});

        if (ImPlot::BeginPlot(TXT::get(titel).c_str(), ImVec2{-1, fenster.getSize().y/3.f},
            ImPlotFlags_Crosshairs | ImPlotFlags_NoMenus | ImPlotFlags_NoBoxSelect | ImPlotFlags_NoMouseText))
        {
            ImPlot::SetupAxis(ImAxis_X1, "jahr"_txtc, ImPlotAxisFlags_LockMin | ImPlotAxisFlags_LockMax);
            ImPlot::SetupAxis(ImAxis_Y1, "summe"_txtc, ImPlotAxisFlags_LockMin);
            ImPlot::SetupAxisLimits(ImAxis_X1,
                                    static_cast<plot_t>(zeit.get_jahr() - Statistik::MAX),
                                    static_cast<plot_t>(zeit.get_jahr()), ImGuiCond_Always);

            for (auto e : datentypen) {
                std::array<plot_t, Statistik::MAX> data_x;
                std::array<plot_t, Statistik::MAX> data_y;
                for (unsigned i = 0; i < data_x.size(); ++i) data_x[i] = zeit.get_jahr() - i;
                for (unsigned i = 0; i < data_y.size(); ++i) data_y[i] = statistik_getter(e, i);
                ImPlot::PlotLine<plot_t>(Statistik::get_txt(e).c_str(), data_x.data(), data_y.data(), data_x.size());
            }
            ImPlot::EndPlot();
        }
        ImPlot::PopStyleVar(2);
    };
    plot("einnahmen", Statistik::EINNAHMEN, [&](Statistik::Einnahme e, unsigned i) { return statistik.get_einnahme(e, i); } );
    plot("ausgaben", Statistik::AUSGABEN, [&](Statistik::Ausgabe  a, unsigned i) { return statistik.get_ausgabe(a, i); } );
}

void Gui_Heim::draw_nachrichten() {
    // Nachrichten
    static std::optional<Nachrichten> nachrichten;
    auto sync = [&]() {
        nachrichten = Klient::request(Netzwerk::Anfrage::NACHRICHTEN, dynastie.get_id(), nachrichten.has_value() ? nachrichten.value() : Nachrichten{});
    };
    if (static sf::Clock sync_timer; sync_timer.getElapsedTime().asSeconds() > 2 || !nachrichten.has_value()) {
        sync();
        sync_dynastien();
        sync_timer.restart();
    }

    // Empfangene Nachrichten
    for (const auto& n : nachrichten->nachrichten) {
        std::string imgui_id = "nnr_" + n.get_hash();

        // Nachricht: Inhalt im Pop-Up
        if (ImGui::BeginPopup(imgui_id.c_str())) {
            ImGui::TextUnformatted(n.get_text().c_str());
            if (n.beantwortbar()) {
                if (ImGui::Button(TXT::get("akzeptieren").c_str())) {
                    Log::debug() << "interaktion1\n";
                    ImGui::CloseCurrentPopup();
                }
                if (ImGui::Button(TXT::get("ablehnen").c_str())) {
                    Log::debug() << "interaktion2\n";
                    ImGui::CloseCurrentPopup();
                }
            }
            if (ImGui::Button(TXT::get("schliessen").c_str())) ImGui::CloseCurrentPopup();
            ImGui::EndPopup();
        }

        // Nachricht: Metadaten
        ImGui::TextUnformatted("Platzhalter für Absender");
        ImGui::SameLine();
        ImGui::TextUnformatted("Platzhalter für Betreff");

        // Nachricht: Interaktionen
        if (ImGui::SameLine(); ImGui::Button((TXT::get("lesen")+"##read"+imgui_id).c_str())) {
            ImGui::OpenPopup(imgui_id.c_str());
        }
        if (ImGui::SameLine(); ImGui::Button((TXT::get("loeschen")+"##del"+imgui_id).c_str())) {
            Klient::a_nachricht_loeschen(n.get_hash());
            sync();
            break;
        }
        // Datum
        ImGui::SameLine(); ImGui::Text("%d:%d %s %d",
                n.get_zeit().get_h(), n.get_zeit().get_min(),
                n.get_zeit().get_jahreszeit_name().c_str(),
                n.get_zeit().get_jahr()
        );
    }

    // Nachricht verfassen an Mitspieler
    static std::string nachricht_text;
    ImGui::GetIO().KeyMods = ImGui::GetMergedKeyModFlags(); // workaround https://github.com/ocornut/imgui/issues/3575
    ImGui::InputTextMultiline("nachricht"_txtc, &nachricht_text);

    // Typ der Nachricht
    static int nachricht_typ = (int) Nachricht::TEXTNACHRICHT;
    ImGui::RadioButton("einfache_nachricht"_txtc, &nachricht_typ, Nachricht::TEXTNACHRICHT);
    ImGui::RadioButton("handelsvorschlag"_txtc,   &nachricht_typ, Nachricht::HANDEL);

    // TODO Empfänger
    if (nachricht_typ == Nachricht::HANDEL) {
        // TODO Liste: Geld+Waren angeboten
        // TODO Liste: Geld+Waren angefragt
    }
    if (ImGui::Button("absenden"_txtc)) {

    }
}

void Gui_Heim::sync_dynastien() {
    dynastien = Klient::request_alle<Dynastie>(Netzwerk::Anfrage::DYNASTIE);
}
