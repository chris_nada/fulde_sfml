#include "imgui_zusatz.h"
#include "../gui.hpp"
#include "../../lingua/lingua.hpp"

#include <nada/time.hpp>
#include <nada/misc.hpp>
#include <imgui_internal.h>

bool imgui_zusatz::Button(const char *label, bool aktiv, const ImVec2& groesse) {
    if (aktiv) return ImGui::Button(label, groesse); // Regulärer Button
    else {
        // Ausgrauen
        static constexpr ImU32 ausgedunkelt = 0x20'20'20'FF;
        static constexpr ImU32 schrift = 0x60'60'60'FF;
        ImGui::PushStyleColor(ImGuiCol_Text, schrift);
        ImGui::PushStyleColor(ImGuiCol_Button,        ausgedunkelt);
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ausgedunkelt);
        ImGui::PushStyleColor(ImGuiCol_ButtonActive,  ausgedunkelt);
        ImGui::Button(label, groesse);
        ImGui::PopStyleColor(4);
    }
    return false;
}

void imgui_zusatz::Fortschritt(float fortschritt, bool animieren, const ImVec2& size_arg) {
    const ImGuiWindow* window = ImGui::GetCurrentWindow();
    if (window->SkipItems) return;

    const ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;

    const ImVec2 pos = window->DC.CursorPos;
    const ImVec2 size = ImGui::CalcItemSize(size_arg, ImGui::CalcItemWidth(), g.FontSize + style.FramePadding.y*2.0f);
    ImRect bb(pos.x, pos.y, pos.x + size.x, pos.y + size.y);
    ImGui::ItemSize(size, style.FramePadding.y);
    if (!ImGui::ItemAdd(bb, 0)) return;

    // Hintergrund
    ImGui::RenderFrame(bb.Min, bb.Max, ImGui::GetColorU32(ImGuiCol_FrameBg), true, style.FrameRounding);

    // Balken
    bb.Expand(-2);
    const auto balken_farbe = ImGui::GetColorU32(ImGuiCol_PlotHistogram);
    ImGui::RenderRectFilledRangeH(window->DrawList, bb, balken_farbe, 0.0f, fortschritt, style.FrameRounding);

    // Animierte Kreise
    if (!animieren) return;
    const unsigned r = bb.GetHeight()/2; // Radius
    const unsigned w = bb.GetWidth();    // Breite des Hintergrunds
    const unsigned n = bb.GetWidth()/80; // Anzahl Kreise
    for (unsigned i = 0; i < n; ++i) {
        window->DrawList->AddCircleFilled(
                {bb.Max.x - r -
                ((nada::time::millis() / 40u + ((i * w) / n)) % (w - (2 * r))),
                bb.GetCenter().y},
                static_cast<float>(r),
                balken_farbe
        );
    }

}

void imgui_zusatz::Farbtext(const char* text, uint32_t farbe) {
    ImGui::PushStyleColor(ImGuiCol_Text, sf::Color(farbe));
    ImGui::TextUnformatted(text);
    ImGui::PopStyleColor();
}

void imgui_zusatz::Ueberschrift(const char* text, uint32_t farbe) {
    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
    ImGui::TextColored(sf::Color(farbe), "%s", text);
    ImGui::PopFont();
}


void imgui_zusatz::Hilfe(const char* desc) {
    ImGui::SameLine();
    ImGui::TextDisabled("(?)");
    Tooltip(desc);
}

void imgui_zusatz::Tooltip(const char* desc) {
    if (ImGui::IsItemHovered()) {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

void imgui_zusatz::Align_right(const char* text) {
    ImGui::SetCursorPosX(
            ImGui::GetCursorPosX() + ImGui::GetColumnWidth() -
            ImGui::CalcTextSize(text).x - ImGui::GetScrollX() -
            2 * ImGui::GetStyle().ItemSpacing.x
    );
}

bool imgui_zusatz::Tab(const std::string& text_key) {
    thread_local std::unordered_map<std::string, std::string> btn_ids;
    try { return ImGui::BeginTabItem(btn_ids.at(text_key).c_str()); }
    catch (const std::exception& e) {
        btn_ids[text_key] = TXT::get(text_key) + "##tab" + text_key;
        return ImGui::BeginTabItem(btn_ids.at(text_key).c_str());
    }
}
