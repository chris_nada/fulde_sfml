#pragma once

#include <imgui.h>
#include <cstdint>

#include "../gui.hpp"

/**
 * Funktionen zur Erweiterung von ImGui, die hier verwendet werden.
 * Funktionen beginnen ImGui-Analog mit Großbuchstaben.
 */
namespace imgui_zusatz {

    /// `ImGui::Button`, der deaktivierbar ist.
    bool Button(const char* label, bool aktiv = true, const ImVec2& size = ImVec2(0, 0));

    /// Vereinfachte Erzeugung eines Tabs (vgl. ImGui::BeginTabItem).
    bool Tab(const std::string& text_key);

    /**
     * Animierter Fortschrittsbalken.
     * @note Fortschritt bitte angeben als 0.0 bis 1.0.
     * @param animieren Animation aktiviert?
     */
    void Fortschritt(float fortschritt, bool animieren = true, const ImVec2& size = ImVec2(-1, 8));

    /// Rendert einfachen farbigen Text via `ImGui::TextUnformatted`.
    void Farbtext(const char* text, uint32_t farbe);

    /// Rendert gegebenen Text als Standard-Überschrift.
    void Ueberschrift(const char* text, uint32_t farbe = Gui::FARBE1);

    /// Erzeugt einen Tooltip mit Icon (?).
    void Hilfe(const char* desc);

    /// Erzeugt einen Tooltip für das vorige Item.
    void Tooltip(const char* desc);

    /// Richtet den nächsten Text rechts aus.
    void Align_right(const char* text);

    /// ImGui Farben vom Typ RGBA/32.
    constexpr uint32_t GELB    = 0xFF'FF'00'FF;
    constexpr uint32_t GRUEN   = 0x00'FF'00'FF;
    constexpr uint32_t GOLD    = 0xFF'CC'00'FF;
    constexpr uint32_t ROT     = 0xFF'00'00'FF;
    constexpr uint32_t SCHWARZ = 0x00'00'00'FF;
    constexpr uint32_t WEISS   = 0xFF'FF'FF'FF;

}
