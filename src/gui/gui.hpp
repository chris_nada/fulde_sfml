#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Font.hpp>
#include <imgui-SFML.h>
#include <imgui.h>
#include <iostream>
#include "gui_hauptmenu.hpp"

/**
 * Hier wird das Fenster initialisiert, Schriften gespeichert, globale Fenstervariablen zur Verfügung gestellt.
 */
class Gui final {

public:

    /// Limit für die Anzahl Bilder pro Sekunde.
    static constexpr const uint8_t  FPS = 50;

    /// Absolute Größe des Views
    static constexpr const float VIEW_SIZE_X = 1600;
    static constexpr const float VIEW_SIZE_Y =  900;

    /* Farben */
    static constexpr const uint32_t FARBE1         = 0xC7'90'4D'FF;
    static constexpr const uint32_t FARBE_DUNKEL   = 0x33'22'11'FF;
    static constexpr const uint32_t FARBE_STANDARD = 0x66'44'22'FF;
    static constexpr const uint32_t FARBE_HOVERED  = 0x77'55'33'FF;
    static constexpr const uint32_t FARBE_ACTIVE   = 0x88'66'44'FF;

    /// Startet die Gui (mit dem Hauptmenü).
    static int start();

    /// Führt ImGui Renderbefehle aus. Muss beispielsweise vor Fensterwechsel ausgeführt werden.
    static void draw();

    /// Das Fenster, das zu Beginn erstellt wird. Global gültig.
    static inline sf::RenderWindow* fenster;

    /*
     * Schriftart ändern:
     *
     * ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[i]);
     * // Ab hier neue Schriftart
     * ImGui::PopFont(); // setzt wieder die alte Schrift
     *
     * Schriften (i):
     * 0 = Serif 18
     * 1 = Serif 22
     * 2 = Serif 26
     * 3 = Mono  18
     */

    /// Standardschrift Serif
    static inline sf::Font font_standard;

    /// Standardschrift Mono
    static inline sf::Font font_mono;

    /// Standardschrift Sans-serif
    static inline sf::Font font_sans;

private:

    /**
     * Erzeugt das Fenser für die Anwendung und führt weitere Initialisierungen durch.
     * Initialisiert die GUI. Muss vor Fensteranzeigen aufgerufen werden. Lädt Schriften etc.
     **/
    static void init();

};
