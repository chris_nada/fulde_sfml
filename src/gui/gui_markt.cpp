#include "gui_markt.hpp"
#include "gui_hauptmenu.hpp"
#include "../netzwerk/klient.hpp"
#include "../gfx/rahmen.hpp"
#include "../welt/welt.hpp"
#include "../lingua/lingua.hpp"
#include "zusatz/imgui_zusatz.h"

#include <iomanip>
#include <cmath>

Gui_Markt::Gui_Markt(Netzwerk::id_t markt_id, Netzwerk::id_t stadt_id, int lager_id, sf::RenderWindow& window) :
    lager_id(lager_id),
    stadt(stadt_id, markt_id, 0, "?"),
    markt(markt_id),
    dynastie(Klient::id()),
    window(window)
{
    stadt = Klient::request(Netzwerk::Anfrage::STADT, stadt_id, stadt);
    land  = Klient::request(Netzwerk::Anfrage::LAND, stadt.get_id_land(), land);
    sync();
    if (auswahl_ware.empty()) auswahl_ware = Ware::alle().begin()->first;
}

void Gui_Markt::start() {
    net_clock.restart();
    while (loop) {
        ImGui::SFML::Update(window, fps_clock.restart());

        // Daten vom Netzwerk synchronisieren
        if (net_clock.getElapsedTime() >= sf::milliseconds(1000)) {
            sync();
            net_clock.restart();
        }

        // Events
        sf::Event event;
        while (window.pollEvent(event)) {
            ImGui::SFML::ProcessEvent(event);
            if (event.type == sf::Event::Closed) window.close(); //macht Fenster schließen mögich
            if (event.type == sf::Event::KeyReleased) {
                switch (event.key.code) {
                    case sf::Keyboard::Escape: loop = false; break;
                    default: break;
                }
            }
        }

        /// Linke Fensterseite zeichnen (Abbrechen => false)
        if (loop) loop = draw_linke_seite();

        /// Rechtes Fenster
        ImGui::SetNextWindowPos({window.getSize().x * 0.67f, 80.f});
        ImGui::SetNextWindowSize({window.getSize().x * 0.33f, (float)window.getSize().y - 80.f});
        ImGui::Begin("recht_fenster", nullptr,
                ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);
        ImGui::Dummy({0, 12});

        // Kauf / Verkauf, wenn Lager vorhanden
        if (lager_id >= 0) {
            draw_kauf();
            draw_verkauf();
        }

        // Rendern
        ImGui::End();
        window.clear();
        ImGui::SFML::Render(window);
        Rahmen::set(zeit);
        Rahmen::set(dynastie);
        Rahmen::draw();
        window.display();
    }
}

void Gui_Markt::sync() {
    using namespace Netzwerk;
    markt    = Klient::request<Markt>(Anfrage::MARKT, markt.get_id(), markt);
    dynastie = Klient::request<Dynastie>(Anfrage::DYNASTIE, Klient::id(), dynastie);
    zeit     = Klient::request<Zeit>(Anfrage::ZEIT, 0, zeit);
    if (lager_id >= 0) {
        lager = Klient::request<Lager>(Anfrage::LAGER, lager_id, lager);
    }
}

void Gui_Markt::draw_kauf() {

    // Kaufen
    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s: %s", TXT::get("auswahl").c_str(), Lager::get_warenname(auswahl_ware).c_str());
    ImGui::NewLine();
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("kaufen").c_str());
    ImGui::PopFont();

    static int anzahl = 10;
    const int max_kauf = 100; // Maximaler Kauf auf 1-Mal.
    const float quali = markt.get_vorrat(auswahl_ware).quali;
    const int verfuegbar = markt.get_vorrat(auswahl_ware).menge;
    if (anzahl < 1) anzahl = 1;
    if (anzahl > verfuegbar) anzahl = verfuegbar;
    if (anzahl > max_kauf) anzahl = max_kauf;

    // Kaufen erlaubt?
    if (verfuegbar > 0) {
        ImGui::DragInt("##n_k", &anzahl, 1, 1, std::min(verfuegbar, max_kauf), (TXT::get("anzahl")+": %d").c_str());
        //ImGui::SliderInt((TXT::get("anzahl")+"##n_k").c_str(),  &anzahl,  1, std::min(verfuegbar, max_kauf));
        const uint32_t gesamtpreis = markt.get_preis(auswahl_ware, anzahl);
        const uint32_t einzelpreis = gesamtpreis / anzahl;
        const uint32_t steuern = std::round((float)gesamtpreis * (((float)land.get_steuern(Steuertyp::GRUND)) / 100.f));

        if (imgui_zusatz::Button(TXT::get("kaufen").c_str(), dynastie.get_geld() >= gesamtpreis + steuern)
                && anzahl <= (int) markt.get_vorrat(auswahl_ware).menge) {
            Klient::a_markt_kauf(
                    stadt.get_id(), markt.get_id(), lager_id,
                    auswahl_ware, anzahl, gesamtpreis + steuern, steuern, quali
            );
            sync();
        }
        ImGui::Text("%s: %.1f", TXT::get("qualitaet").c_str(), quali);
        ImGui::Text("%s: %d",
                TXT::get("stueckpreis_brutto").c_str(), einzelpreis);
        ImGui::Text("%s: %.1f",
                    TXT::get("stueckpreis_netto").c_str(),
                    (float)(gesamtpreis + steuern) / (float)anzahl);
        ImGui::Text("%s x %d", TXT::get("anzahl").c_str(), anzahl);
        ImGui::Text("%s: %d", TXT::get("bruttopreis").c_str(), gesamtpreis);
        ImGui::Text(" + %d%% %s", land.get_steuern(Steuertyp::MEHRWERT),
                    TXT::get("mwst").c_str());
        ImGui::Text("%s: %d", TXT::get("nettopreis").c_str(),
                gesamtpreis + steuern);
    }
    else ImGui::TextColored(sf::Color(0xAA0000FF), "%s",
            TXT::get("nicht_auf_lager").c_str()
         );
}

void Gui_Markt::draw_verkauf() {
    static int anzahl_vk = 10; // Anzahl zu verkaufen
    ImGui::NewLine();
    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("verkaufen").c_str());
    ImGui::PopFont();

    // Mengen
    const int n_lager = lager.get_vorrat(auswahl_ware).menge;
    const int n_max   = std::min(n_lager, static_cast<int>(markt.get_bedarf(auswahl_ware)));
    if (n_lager > 0) {
        if (n_max > 0) {
            // Anzahl einstellen
            const float quali_vk = lager.get_vorrat(auswahl_ware).quali;
            ImGui::SliderInt((TXT::get("anzahl")+"##n_vk").c_str(), &anzahl_vk, 1, n_max);
            anzahl_vk = std::min(anzahl_vk, n_max);
            const uint32_t gesamtpreis_vk = markt.get_vorrat(auswahl_ware).get_preis(auswahl_ware, anzahl_vk, quali_vk);
            ImGui::Text("%s: %.1f", TXT::get("qualitaet").c_str(), quali_vk);
            ImGui::Text("%s: %.1f", TXT::get("stueckpreis").c_str(), (float)gesamtpreis_vk / (float)anzahl_vk);
            ImGui::Text("%s: %d", TXT::get("gesamtpreis").c_str(), gesamtpreis_vk);

            // Verkaufsknopf
            if (ImGui::Button(TXT::get("verkaufen").c_str()) && anzahl_vk > 0 && anzahl_vk <= n_max) {
                Klient::a_markt_verkauf(
                        markt.get_id(), lager.get_id(), auswahl_ware,
                        anzahl_vk, gesamtpreis_vk, quali_vk
                );
                sync();
            };
        }
        else ImGui::TextColored(sf::Color::Yellow, "%s", TXT::get("kein_bedarf").c_str());
    }
    else ImGui::TextColored(sf::Color(0xAA0000FF), "%s", TXT::get("nicht_auf_lager").c_str());
}

bool Gui_Markt::draw_linke_seite() {
    const float padding_y = 40;
    ImGui::SetNextWindowPos({0,padding_y});
    ImGui::SetNextWindowSize({window.getSize().x * 0.67f, (float)window.getSize().y - padding_y});
    ImGui::Begin(TXT::get("kaufen").c_str(), nullptr,
            ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove |
            ImGuiWindowFlags_NoResize   | ImGuiWindowFlags_AlwaysVerticalScrollbar);
    ImGui::Indent(16);
    ImGui::Dummy({0, 12});

    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
    const std::string name_markt = TXT::get("markt") + " " + stadt.get_name();
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", name_markt.c_str()); ImGui::SameLine();
    ImGui::PopFont();

    bool weiter = true; // Loop fortsetzen?
    if (ImGui::Button(TXT::get("zurueck").c_str())) weiter = false;

    // Plot Dimensionen
    static const float PLOT_BREITE = 160;
    static const float PLOT_HOEHE  =  40;
    static const auto  PLOT_SIZE   = ImVec2(PLOT_BREITE, PLOT_HOEHE);

    // Überschriften
    ImGui::Text("%s", TXT::get("ware").c_str());
    ImGui::Dummy({0, 0});
    ImGui::SameLine(PLOT_BREITE * 0.4f);
    ImGui::Text("%s", TXT::get("vorrat").c_str());
    ImGui::SameLine(PLOT_BREITE * 1.5f);
    ImGui::Text("%s", TXT::get("qualitaet").c_str());
    ImGui::SameLine(PLOT_BREITE * 2.6f);
    ImGui::Text("%s", TXT::get("preis").c_str());
    if (lager_id >= 0) {
        ImGui::SameLine(700);
        ImGui::Text("%s", TXT::get("vorrat_lager").c_str());
    }

    // Alle Waren auflisten
    ImGui::PushStyleColor(ImGuiCol_FrameBg, sf::Color(0x20, 0x20, 0x20, 0xFF)); // Hintergrund Plots
    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[3]);
    for (const std::string& ware : Markt::alle_waren_keys_sortiert()) {
        const auto& vorrat = markt.get_vorrat(ware);
        const auto& temp_ware = Ware::get(ware);
        // Plots
        std::stringstream ss;
        ss << std::setprecision(1) << std::fixed << vorrat.quali;
        const std::string s_quali(ss.str());

        ImGui::PlotLines("", vorrat.menge_v.data(), vorrat.menge_v.size(), 0,
                std::to_string(vorrat.menge).c_str(),   FLT_MAX, FLT_MAX, PLOT_SIZE);

        ImGui::SameLine();
        ImGui::PlotLines("", vorrat.quali_v.data(), vorrat.quali_v.size(), 0,
                s_quali.c_str(), 0.f, 10.f, PLOT_SIZE);

        ImGui::SameLine();
        ImGui::PushStyleColor(ImGuiCol_Text, nada::misc::get_color_from_ratio((float)temp_ware.get_basispreis() / (float)vorrat.get_preis(ware, 1)));
        ImGui::PlotLines("", vorrat.preis_v.data(), vorrat.preis_v.size(), 0,
                std::to_string(vorrat.get_preis(ware, 1)).c_str(),    FLT_MAX, FLT_MAX, PLOT_SIZE);
        ImGui::PopStyleColor();
        ImGui::SameLine();

        // Warenauswahl
        if (ImGui::Button(Lager::get_warenname(ware).c_str(), {120, 40})) auswahl_ware = ware;
        ImGui::SameLine();
        ImGui::Image(*temp_ware.get_icon().data(), temp_ware.get_icon().size_f());
        if (lager_id >= 0) {
            ImGui::SameLine(770);
            ImGui::Text("%d", lager.get_vorrat(ware).menge);
        }
    }
    ImGui::PopStyleColor();
    ImGui::PopFont();

    ImGui::End();
    return weiter;
}

