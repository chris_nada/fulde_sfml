#include "gui_rathaus.hpp"
#include "../netzwerk/klient.hpp"
#include "../gfx/rahmen.hpp"
#include "../lingua/lingua.hpp"
#include "../welt/spieler/charakter.hpp"
#include "zusatz/imgui_zusatz.h"

#include <nada/log.hpp>

using nada::Log;

Gui_Rathaus::Gui_Rathaus(Netzwerk::id_t stadt_id) :
    stadt(stadt_id, 0, 0, "?!")
{
    sync(true);
    steuern = std::vector<Steuer_UI>{
            {Steuertyp::ERTRAG,   land.get_steuern(Steuertyp::ERTRAG),   "ertragssteuer"_txtc},
            {Steuertyp::MEHRWERT, land.get_steuern(Steuertyp::MEHRWERT), "mehrwertsteuer"_txtc},
            {Steuertyp::GRUND,    land.get_steuern(Steuertyp::GRUND),    "grundsteuer"_txtc}
    };
}

void Gui_Rathaus::sync(bool force) {
    auto is_bewerber_in_anderer_stadt = [](const Stadt& stadt, const Land& land) {
        for (auto andere_stadt : land.get_staedte()) {
            if (andere_stadt == stadt.get_id()) continue;
            std::vector<Amt> andere_aemter;
            for (const auto& anderes_amt : Klient::request(Netzwerk::Anfrage::AEMTER_STADT, stadt.get_id(), andere_aemter)) {
                if (anderes_amt.is_bewerber(Klient::id_char())) return true;
            }
        }
        return false;
    };

    auto sync_alles = [this, &is_bewerber_in_anderer_stadt]() {
        // Sync Stadt, Land, Ämter
        stadt = Klient::request(Netzwerk::Anfrage::STADT, stadt.get_id(), stadt);
        land  = Klient::request(Netzwerk::Anfrage::LAND, stadt.get_id_land(), land);
        aemter_stadt = Klient::request(Netzwerk::Anfrage::AEMTER_STADT, stadt.get_id(), aemter_stadt);
        aemter_land  = Klient::request(Netzwerk::Anfrage::AEMTER_LAND, land.get_id(), aemter_land);
        std::sort(aemter_stadt.begin(), aemter_stadt.end());
        std::sort(aemter_land.begin(), aemter_land.end());

        eigenes_amt.reset(); // Eigenes Amt neu bestimmen
        darf_bewerben = dynastie.get_id_land() == land.get_id(); // Darf der PC sich bewerben?
        std::unordered_set<Netzwerk::big_id_t> char_id_set;
        for (const auto& aemter : {aemter_stadt, aemter_land}) for (const auto& amt : aemter) {
            if (amt.is_besetzt()) char_id_set.insert(amt.get_inhaber()); // Inhaber holen
            for (const auto& paar : amt.get_bewerber()) char_id_set.insert(paar.first); // Bewerber holen
            if (amt.is_bewerber(Klient::id_char())) darf_bewerben = false; // nur 1 Bewerbung erlaubt
            if (amt.is_besetzt() && amt.get_inhaber() == Klient::id_char()) eigenes_amt = amt;
        }
        // nur 1 Bewerbung erlaubt: in anderen Städten checken
        if (darf_bewerben) darf_bewerben = !is_bewerber_in_anderer_stadt(stadt, land);

        // Alle relevanten Charaktere holen
        const auto& char_request = Klient::request_alle<Charakter>(Netzwerk::CHARAKTER, char_id_set);
        for (const auto& c : char_request) charmap[c.get_id()] = std::make_unique<Charakter>(c);

        // Steuern aktualisieren
        for (auto& steuer : steuern) steuer.alter_satz = land.get_steuern(steuer.steuertyp);
    };
    if (static sf::Clock clock; clock.getElapsedTime().asSeconds() > 5 || force) {
        sync_alles();
        clock.restart();
    }
}

void Gui_Rathaus::loop_body() {
    sync();

    // Events
    sf::Event event;
    while (fenster.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(event);
        if (event.type == sf::Event::Closed) fenster.close(); //macht Fenster schließen mögich
        if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Escape: loop = false; break;
                default: break;
            }
        }
    }

    // Hintergrundfenster
    ImGui::SetNextWindowPos({0, Rahmen::ABSTAND_TOP});
    ImGui::SetNextWindowSize({static_cast<float>(fenster.getSize().x), fenster.getSize().y - Rahmen::ABSTAND_TOP});
    ImGui::Begin(TXT::get("rathaus").c_str(), nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

    // Tabs
    if (ImGui::BeginTabBar("##tabbar_gui_rathaus", ImGuiTabBarFlags_NoCloseWithMiddleMouseButton)) {
        if (imgui_zusatz::Tab("stadt")) {
            draw_tab_stadt();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("infrastruktur")) {
            draw_tab_infrastruktur();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("land")) {
            draw_tab_land();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("aemter")) {
            draw_tab_aemter();
            ImGui::EndTabItem();
        }
    } ImGui::EndTabItem();
    ImGui::End();
    fenster.clear();
}

void Gui_Rathaus::draw_tab_stadt() {
    imgui_zusatz::Ueberschrift(stadt.get_name().c_str());
    ImGui::Text("%s: %u",  "einwohner"_txtc,  stadt.get_einwohnerzahl());
    ImGui::Text("%s: %lld", "stadtkasse"_txtc, (long long)stadt.get_geld());
    // TODO Stadt-Übersicht: Wirtschaft, Gebäude, Militär
}

void Gui_Rathaus::draw_tab_land() {
    imgui_zusatz::Ueberschrift(land.get_name().c_str());
    imgui_zusatz::Ueberschrift("steuern"_txtc);

    /// Anzeigefunktion für Steuern
    for (auto& steuer : steuern) {
        ImGui::NewLine();
        const std::string str_neuer_satz("neuer_satz"_txt  + "##neuer_satz" + std::string(steuer.name));
        const std::string str_neu_festlegen("festlegen"_txt + "##festlegen" + std::string(steuer.name));
        ImGui::Text("%s: %d%%", steuer.name, steuer.alter_satz);
        ImGui::SetNextItemWidth(ImGui::GetWindowWidth() / 4.f);
        if (ImGui::InputInt(str_neuer_satz.c_str(), &steuer.neuer_satz, 1)) steuer.neuer_satz = std::clamp(steuer.neuer_satz, 0, 50);
        if (ImGui::Button(str_neu_festlegen.c_str())) {
            Klient::a_rathaus_steuer_anpassen(land.get_id(), steuer.steuertyp, steuer.neuer_satz);
            sync(true);
        }
    }
}

void Gui_Rathaus::draw_tab_infrastruktur() {
    /// Fertige Infrastruktur auflisten:
    imgui_zusatz::Ueberschrift("bestehende_infrastruktur"_txtc);
    if (ImGui::BeginTable("tabelle_infra_fertig", 7)) {
        ImGui::TableSetupColumn("infrastruktur"_txtc, ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn("anzahl"_txtc, ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn("unterhalt_pro_jahr"_txtc, ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn("versorgt"_txtc, ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn("trainingswert"_txtc, ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn("ausruestwert"_txtc, ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn("reichtumswert"_txtc, ImGuiTableColumnFlags_NoHide);
        ImGui::TableHeadersRow();
        size_t gesamt_unterhalt = 0;
        for (const auto& infra : stadt.get_infrastruktur()) {
            if (infra.second == 0) continue; // keine vorhanden
            const Infrastruktur& infrastruktur = Infrastruktur::get(infra.first);
            ImGui::TableNextRow();
            ImGui::TableNextColumn();
            ImGui::TextUnformatted(TXT::get(infrastruktur.get_key()).c_str());
            ImGui::TableNextColumn();
            ImGui::Text("%d", infra.second);
            ImGui::TableNextColumn();
            const auto unterhalt = infrastruktur.get_unterhalt() * infra.second;
            ImGui::Text("%d", unterhalt);
            gesamt_unterhalt += unterhalt;
            ImGui::TableNextColumn();
            ImGui::Text("%d", infrastruktur.get_versorgt() * infra.second);
            ImGui::TableNextColumn();
            ImGui::Text("%.2f", infrastruktur.get_garnision_training() * static_cast<float>(infra.second));
            ImGui::TableNextColumn();
            ImGui::Text("%.2f", infrastruktur.get_garnision_ausruestung() * static_cast<float>(infra.second));
            ImGui::TableNextColumn();
            ImGui::Text("%d", infrastruktur.get_reichtum() * infra.second);
        }
        // Gesamtsumme
        ImGui::TableNextRow();    // eine
        ImGui::TableNextColumn(); // Zeile
        ImGui::NewLine();         // frei
        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        ImGui::TextUnformatted("Gesamt");
        ImGui::TableNextColumn();
        ImGui::TableNextColumn();
        ImGui::Text("%zu", gesamt_unterhalt);
        ImGui::EndTable();
    }

    /// Infrastruktur Bauen
    ImGui::NewLine();
    ImGui::Separator();
    ImGui::NewLine();
    imgui_zusatz::Ueberschrift("bauprojekt"_txtc);
    static std::vector<std::string> infra_keys = std::invoke([]() {
        decltype(infra_keys) keys; // TODO check Bauerlaubnis für PC
        for (const auto& paar : Infrastruktur::alle()) keys.push_back(paar.first);
        return keys;
    });
    static std::vector<const char*> infra_namen = std::invoke([&]() {
        decltype(infra_namen) namen;
        for (const auto& key : infra_keys) namen.push_back(TXT::get(key).c_str());
        return namen;
    });
    static int infra_bau_auswahl = 0;
    static int infra_bau_anzahl  = 1;
    const auto input_width = ImGui::GetWindowWidth() / 4.f;
    ImGui::SetNextItemWidth(input_width);
    ImGui::Combo("##combo_neues_bauprojekt", &infra_bau_auswahl, infra_namen.data(), infra_namen.size());
    const Infrastruktur& infra = Infrastruktur::get(infra_keys.at(infra_bau_auswahl));
    ImGui::SetNextItemWidth(input_width);
    ImGui::InputInt("anzahl"_txtc, &infra_bau_anzahl, 1, 10);
    infra_bau_anzahl = std::clamp(infra_bau_anzahl, 1, 10);
    const auto gesamtkosten = infra.get_baukosten() * infra_bau_anzahl;

    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[3]);
    ImGui::Text("%s: %s", "auswahl"_txtc, TXT::get(infra.get_key()).c_str());
    ImGui::Text("%s: %.2f %s", "bauzeit"_txtc, (float) infra.get_bauzeit() / 4.0f, "jahr_evtl_pl"_txtc);
    ImGui::Text("%s: %d %s", "gesamtunterhalt"_txtc, infra.get_unterhalt() * infra_bau_anzahl, "pro_jahr"_txtc);
    ImGui::Text("%s: %d", "gesamtkosten"_txtc, gesamtkosten);
    ImGui::Text("(%s: %lld)", "stadtkasse"_txtc, (long long)stadt.get_geld());
    ImGui::PopFont();
    if (gesamtkosten > stadt.get_geld()) ImGui::TextUnformatted("zu_wenig_geld"_txtc);
    else if (ImGui::Button("Projekt Starten")) {
        Klient::a_rathaus_neues_projekt(stadt.get_id(), infra.get_key(), infra_bau_anzahl);
        sync(true);
    }

    /// Infrastruktur in Bau
    ImGui::NewLine();
    ImGui::Separator();
    ImGui::NewLine();
    imgui_zusatz::Ueberschrift("infrastruktur_in_bau"_txtc);
    if (stadt.get_infrastruktur_in_bau().empty()) ImGui::TextUnformatted("derzeit_keine_nichts_in_bau"_txtc);
    else if (ImGui::BeginTable("tabelle_infra_in_bau", 4)) {
        ImGui::TableSetupColumn("infrastruktur"_txtc, ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn("anzahl"_txtc, ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn("gesamtunterhalt"_txtc,   ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn("baufortschritt"_txtc,   ImGuiTableColumnFlags_NoHide);
        ImGui::TableHeadersRow();
        for (const auto& bauprojekt: stadt.get_infrastruktur_in_bau()) {
            const Infrastruktur& infra_in_bau = Infrastruktur::get(bauprojekt.key);
            ImGui::TableNextRow();
            ImGui::TableNextColumn();
            ImGui::TextUnformatted(TXT::get(infra_in_bau.get_key()).c_str());
            ImGui::TableNextColumn();
            ImGui::Text("%d", bauprojekt.anzahl);
            ImGui::TableNextColumn();
            ImGui::Text("%d", infra_in_bau.get_unterhalt() * bauprojekt.anzahl);
            ImGui::TableNextColumn();
            imgui_zusatz::Fortschritt(1.f - (float)bauprojekt.restbauzeit / (float)infra_in_bau.get_bauzeit(),
                                      true, {-1, 12});
        }
        ImGui::EndTable();
    }
}

void Gui_Rathaus::draw_tab_aemter() {
    // Darstellung eines Amtes
    const auto draw_amt = [&](const Amt& amt, unsigned i) { // i muss je Amt einmalig sein für Button-IDs
        ImGui::TableNextRow();

        // Spalte: Amt
        ImGui::TableNextColumn();
        ImGui::TextUnformatted(TXT::get(amt.get_key()).c_str());

        // Spalte: Ebene
        ImGui::TableNextColumn();
        ImGui::TextUnformatted(TXT::get(amt.get_ebene() == Amt::Ebene::LAND ? "ebene_land" : "ebene_stadt").c_str());

        // Spalte: Stufe
        ImGui::TableNextColumn();
        ImGui::Text("%c", amt.get_stufe_as_char());

        // Spalte: Gehalt
        ImGui::TableNextColumn();
        ImGui::Text("%hu / %s", amt.get_jaehrliches_einkommen(), "jahr"_txtc);

        // Spalte: Inhaber
        ImGui::TableNextColumn();
        const std::string inhaber = (amt.is_besetzt() && amt.get_inhaber() == Klient::id_char()) ? "inhaber_du"_txt : // Spieler selbst
                    amt.is_besetzt() ? charmap.at(amt.get_inhaber())->get_voller_name() : TXT::get("unbesetzt");
        ImGui::TextUnformatted(inhaber.c_str());

        // Spalte: Bewerben
        ImGui::TableNextColumn();
        const uint8_t alte_stufe = eigenes_amt.has_value() ? eigenes_amt->get_stufe() : 0;
        const bool check_altes_amt = amt.darf_bewerben(Klient::id_char(), alte_stufe, true);
        const std::string bewerben_id(TXT::get("bewerben") + "##amt_bew_" + std::to_string(i));
        if (darf_bewerben && check_altes_amt) {
            if (ImGui::Button(bewerben_id.c_str())) {
                Log::debug() << "\tBewerben: " << amt.get_key() << '\n';
                Klient::a_amt_bewerben(amt.get_ebene(), amt.get_ebene() == Amt::STADT ? stadt.get_id() : land.get_id(), amt.get_key());
                sync(true);
                return;
            }
        } else if (amt.is_bewerber(Klient::id_char())) imgui_zusatz::Farbtext("selbst_beworben"_txtc, imgui_zusatz::GRUEN);

        // Spalte: Wahl
        ImGui::TableNextColumn();
        if (const std::string tree_id = "bewerber_pl"_txt + ": " + std::to_string(amt.get_bewerber().size()) + " " + "##treebew_" + amt.get_key();
            ImGui::TreeNode(tree_id.c_str()))
        {
            // Bewerber nach n Stimmen abwärts sortieren
            std::vector<std::pair<Netzwerk::big_id_t, std::unordered_set<Netzwerk::big_id_t>>> bewerber(amt.get_bewerber().begin(), amt.get_bewerber().end());
            std::sort(bewerber.begin(), bewerber.end(), [](const auto& lhs, const auto& rhs) { return lhs.second.size() > rhs.second.size(); });

            // Bewerber auflisten
            for (const auto& bewerber_paar : bewerber) {
                const Charakter& c = *charmap.at(bewerber_paar.first);
                // darf wählen?
                if (!amt.is_waehler(Klient::id_char())) {
                    ImGui::SameLine();
                    if (const std::string waehlen_id(TXT::get("waehlen") + "##amt_wahl_" + amt.get_key() + std::to_string(c.get_id())); ImGui::Button(waehlen_id.c_str())) {
                        Log::debug() << "\tWaehlen: " << amt.get_key() << '=' << c.get_voller_name() << '\n';
                        Klient::a_amt_waehlen(c.get_id());
                        sync(true);
                        break;
                    }
                }
                ImGui::Text("%llu %s %s %s (%hu)",
                            (unsigned long long) bewerber_paar.second.size(),
                            bewerber_paar.second.size() == 1 ? "stimme"_txtc : "stimmen"_txtc, "stimmen_fuer"_txtc,
                            c.get_voller_name().c_str(), c.get_alter(zeit));
            }
            ImGui::TreePop();
        }
    };

    const auto auflisten = [&draw_amt = std::as_const(draw_amt)](const std::vector<Amt>& aemter) {
        for (unsigned i = 0; i < aemter.size(); ++i) {
            const Amt& amt = aemter[i];
            draw_amt(amt, i);
        }
        ImGui::NewLine();
    };

    if (ImGui::BeginTable("tabelle_aemter", 7,
        ImGuiTableFlags_Resizable | ImGuiTableFlags_Borders | ImGuiTableFlags_SizingFixedFit | ImGuiTableFlags_PadOuterX))
    {
        ImGui::TableSetupColumn(TXT::get("amt").c_str(),      ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn(TXT::get("ebene").c_str(),    ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn(TXT::get("stufe").c_str(),    ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn(TXT::get("gehalt").c_str(),   ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn(TXT::get("inhaber").c_str(),  ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn(TXT::get("bewerben").c_str(), ImGuiTableColumnFlags_NoHide);
        ImGui::TableSetupColumn(TXT::get("wahl").c_str(),     ImGuiTableColumnFlags_NoHide | ImGuiTableColumnFlags_WidthStretch);
        ImGui::TableHeadersRow();
        auflisten(aemter_stadt);
        auflisten(aemter_land);
        ImGui::EndTable();
    }
}
