#include "gui_hauptmenu.hpp"
#include "gui_markt.hpp"
#include "../netzwerk/klient.hpp"
#include "gui.hpp"
#include "gui_viertel.hpp"
#include "selektoren/selektor_viertel.hpp"
#include "../gfx/partikel.hpp"
#include "../lingua/lingua.hpp"
#include "../welt/waren/prozess.hpp"
#include "zusatz/imgui_zusatz.h"

#include <SFML/Graphics/RectangleShape.hpp>
#include <filesystem>
#include <imgui.h>
#include <misc/cpp/imgui_stdlib.h>
#include <thread>
#include <nada/log.hpp>

using nada::Log;

int Gui_hauptmenu::start() {
    Log::debug() << "Gui_hauptmenu::start()" << Log::endl;

    Log::benchmark_debug(init, "Gui_hauptmenu::init"); // Ressourcen laden

    // Felder
    std::string host_ip = "127.0.0.1";
    std::string spieler_vorname = "John";
    std::string spieler_name    = "Doe";
    int spieler_id = 1;
    bool maennlich = true;

    // Debug Felder
    #ifndef NDEBUG
        //
    #endif

    //sf::Color const color_bg{61, 55, 42, 0xFF};
    sf::Clock deltaClock;
    ImGui::SetNextWindowPos({0,0});
    while (Gui::fenster->isOpen()) {

        // Events
        sf::Event event;
        while (Gui::fenster->pollEvent(event)) {
            ImGui::SFML::ProcessEvent(event);
            if (event.type == sf::Event::Closed) Gui::fenster->close();
        }
        ImGui::SFML::Update(*Gui::fenster, deltaClock.restart());

        /// Hauptmenü Elemente
        ImGui::SetNextWindowSize({800, (float)Gui::fenster->getSize().y});
        ImGui::Begin("Hauptmenü", nullptr,
                ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);
        ImGui::Indent(100);
        ImGui::Dummy({0, 100});
        ImGui::NewLine();

        ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
        ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("an_partie_teilnehmen").c_str());
        ImGui::PopFont();

        ImGui::InputText(TXT::get("host_ip").c_str(), &host_ip);
        ImGui::InputInt(TXT::get("spieler_id").c_str(), &spieler_id);
        imgui_zusatz::Hilfe(TXT::get("tooltip_nur_bei_spieler_id_beitritt").c_str());
        ImGui::InputText(TXT::get("vorname").c_str(), &spieler_vorname);
        ImGui::InputText(TXT::get("nachname").c_str(), &spieler_name);
        if (ImGui::RadioButton(TXT::get("maennlich").c_str(), maennlich)) maennlich = true;
        ImGui::SameLine();
        if (ImGui::RadioButton(TXT::get("weiblich").c_str(), !maennlich)) maennlich = false;

        // Viertelauswahl
        static int join_viertel_id = -1;
        ImGui::Text("%s %d", TXT::get("viertel").c_str(), join_viertel_id);
        ImGui::SameLine(0, 20);
        if (ImGui::Button(TXT::get("auswaehlen").c_str())) {
            Gui::draw();
            Klient::verbinde(host_ip);
            Selektor_Viertel sv;
            join_viertel_id = sv.start();
            continue;
        }
        ImGui::NewLine();

        // Beitreten
        if (ImGui::Button(TXT::get("beitreten_neuer_spieler").c_str())) {
            if (Klient::verbinde(host_ip)) {
                const auto n1 = sf::String::fromUtf8(std::begin(spieler_vorname), std::end(spieler_vorname));
                const auto n2 = sf::String::fromUtf8(std::begin(spieler_name), std::end(spieler_name));
                const auto id = Klient::a_neuer_spieler(n1, n2, maennlich, join_viertel_id);
                Gui::draw(); // Abschluss aus Aufruf aus start()
                join(id); // loopt
                Klient::trennen();
                continue;
            }
        }
        ImGui::SameLine();
        if (ImGui::Button(TXT::get("beitreten_mit_id").c_str())) {
            if (Klient::verbinde(host_ip)) {
                Gui::draw();
                join(Klient::id());
                Klient::trennen();
                continue;
            }
        }
        ImGui::NewLine(); ImGui::NewLine();

        /// Beenden
        if (ImGui::Button(TXT::get("beenden").c_str())) Gui::fenster->close();

        // DEBUG draws
        #ifndef NDEBUG
        #endif

        // Rendern
        ImGui::End();
        Gui::fenster->clear();
        ImGui::SFML::Render(*Gui::fenster);
        Gui::fenster->display();
    }
    return 0;
}

int Gui_hauptmenu::join(Netzwerk::id_t id_dynastie) {
    Log::debug() << "Gui_hauptmenu::join()" << Log::endl;
    Dynastie dynastie = Klient::request<Dynastie>(Netzwerk::Anfrage::DYNASTIE, id_dynastie, dynastie);
    Charakter charakter = Klient::request<Charakter>(Netzwerk::Anfrage::CHARAKTER, dynastie.get_oberhaupt(), charakter);
    Klient::id(id_dynastie);
    Klient::id_char(charakter.get_id());
    const auto start = std::chrono::system_clock::now();
    Gui_Viertel gui_viertel(charakter.get_pos().viertel, charakter.get_id());
    std::chrono::duration<double> dauer = std::chrono::system_clock::now() - start;
    Log::debug() << "\tDauer Gui_Viertel() ctor: " << dauer.count() << 's' << '\n';
    gui_viertel.start();
    return 0;
}

void Gui_hauptmenu::init() {
    Log::debug() << "Gui_hauptmenu::init()" << Log::endl;

    auto get_text = [](const std::string& key) {
        sf::Text text(TXT::get(key), Gui::font_standard);
        text.setPosition(8.f, Gui::fenster->getSize().y / 2.f + 30.f);
        text.setFillColor(sf::Color::White);
        text.setOutlineColor(sf::Color::White);
        text.setCharacterSize(16);
        return text;
    };

    // Prozesse laden
    Gui::fenster->clear();
    Gui::fenster->draw(get_text("lade_prozesse"));
    Gui::fenster->display();
    auto alle_prozesse = Prozess::alle();
    //std::this_thread::sleep_for(std::chrono::milliseconds(500)); // Debug Ladeanimation

    // Waren laden
    Gui::fenster->clear();
    Gui::fenster->draw(get_text("lade_waren"));
    Gui::fenster->display();
    auto alle_waren = Ware::alle();
    //std::this_thread::sleep_for(std::chrono::milliseconds(500)); // Debug Ladeanimation

    // Alle zu ladenden Grafiken als Dateipfad sammeln
    std::vector<std::string> grafikpfade;
    std::vector<std::string> ordner_precache = { // Grafiken dieser Ordner werden beim Start geladen
            "data/gfx/viertel"
    };
    for (const auto& pfad : ordner_precache) {
        for (const auto& entry : std::filesystem::recursive_directory_iterator(pfad)) {
            if (entry.is_regular_file()) {
                const auto& dateiname = entry.path().string();
                if (dateiname.find(".png") != std::string::npos &&
                dateiname.substr(dateiname.find(".png")).size() == 4)
                {
                    grafikpfade.push_back(dateiname);
                }
            }
        }
    }

    // Ladeanimation
    const auto& text_lade_gfx = get_text("lade_grafiken");
    for (size_t i = 0; i < grafikpfade.size(); ++i) {
        Gui::fenster->clear();
        const std::string& dateiname = grafikpfade[i];
        Grafik temp(dateiname, true);
        Log::debug() << "\tGrafik geladen: " << dateiname << ' ' << temp.size().x << 'x' << temp.size().y << '\n';

        // Ladebalken
        if (i % 10) continue;
        static float x_multi = Gui::fenster->getSize().x / (float) grafikpfade.size();
        sf::RectangleShape rect({x_multi * (1.f+i), 40.f});
        rect.setFillColor({0x80, 0xFF, 0x80, 0xFF});
        rect.setPosition(0, (Gui::fenster->getSize().y / 2.f) - (rect.getSize().y/2.f));
        Gui::fenster->draw(text_lade_gfx);
        Gui::fenster->draw(rect);
        Gui::fenster->display();
        //std::this_thread::sleep_for(std::chrono::milliseconds(500)); // Debug Ladeanimation
    }
}
