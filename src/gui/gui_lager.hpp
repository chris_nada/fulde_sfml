#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include "../netzwerk/netzwerk.hpp"
#include "../welt/stadt/stadt.hpp"
#include "../welt/spieler/dynastie.hpp"
#include "../welt/zeit.hpp"
#include "basisfenster.hpp"

/// Nutzeroberfläche für die Verwaltung eines Lagers.
class Gui_Lager final : public Basisfenster {

public:

    /// Ctor.
    Gui_Lager(Netzwerk::id_t stadt_id, Netzwerk::id_t lager_id, sf::RenderWindow& window);

private:

    /// Lädt benötigte Daten aus dem Netzwerk.
    void sync();

    /// Rendering-Schleife.
    void loop_body() override;

    /// Rendert das Fenster "Übersicht".
    void draw_uebersicht();

    /// Rendert das Fenster "Transport" (Import/Export).
    bool draw_transport();

    /// Rendert das Fenster zur Lagerverwaltung (automatischer Einkauf/Verkauf).
    bool draw_verwaltung();

    /// Vorzeitiges Abschließen des Fensterrenderings zum Springen in fremde Fenster.
    void tab_abbruch();

    /// Stadt, in der sich das Lager befindet.
    Stadt stadt;

    /// Zielstadt für einen Transport.
    Stadt zielstadt;

    /// Zu verwaltendes Lager von dieser Ansicht.
    Lager lager;

};
