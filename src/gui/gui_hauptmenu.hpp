#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Font.hpp>

#include "imgui.h"
#include "imgui-SFML.h"
#include "../netzwerk/netzwerk.hpp"

/**
 * @brief Hauptmenü.
 */
class Gui_hauptmenu final {

public:

    /// Startet das Hauptmenü.
    int start();

private:

    /**
     * Pre-Initialisierung von Ressourcen, z.B. Grafiken.
     * + Spielt Ladeanimation ab.
     * + Bei Fehlern wird eine Exception geworfen.
     */
    static void init();

    /**
     * Eintrittspunkt ins Spiel für die Nutzeroberfläche.
     * @param id_dynastie Dynastie-ID (Server-seitig).
     * @return Fehlerkode.
     */
    int join(Netzwerk::id_t id_dynastie);

};
