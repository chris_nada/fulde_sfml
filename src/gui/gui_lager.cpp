#include "gui_lager.hpp"
#include "../gfx/rahmen.hpp"
#include "../netzwerk/klient.hpp"
#include "../lingua/lingua.hpp"
#include "gui.hpp"
#include "selektoren/selektor_stadt.hpp"
#include "selektoren/selektor_generic.hpp"
#include "selektoren/selektor_ware.hpp"
#include "zusatz/imgui_zusatz.h"

#include <nada/log.hpp>
#include <imgui-SFML.h>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Event.hpp>
#include <imgui.h>
#include <iomanip>

using nada::Log;

Gui_Lager::Gui_Lager(Netzwerk::id_t stadt_id, Netzwerk::id_t lager_id, sf::RenderWindow& window) :
    Basisfenster(window)
{
    stadt = Klient::request(Netzwerk::Anfrage::STADT, stadt_id, stadt);
    lager = Klient::request(Netzwerk::Anfrage::LAGER, lager_id, lager);
    zielstadt = Klient::request(Netzwerk::Anfrage::STADT, stadt.get_id() == 0 ? 1 : 0, zielstadt);
    // TODO laender wg. Transportkosten, Zoll (?)
}

void Gui_Lager::sync() {
    // Daten vom Netzwerk synchronisieren
    if (static unsigned sekunden = 0; net_clock.getElapsedTime() >= sf::milliseconds(1000)) {
        lager = Klient::request(Netzwerk::Anfrage::LAGER, lager.get_id(), lager);
        net_clock.restart();
        if (++sekunden > 10) {
            // ungenutzt
            sekunden = 0;
        }
    }
}

void Gui_Lager::loop_body() {
    sync();

    // Events
    sf::Event event;
    while (fenster.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(event);
        if (event.type == sf::Event::Closed) fenster.close(); //macht Fenster schließen mögich
        if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Escape:
                    loop = false;
                    break;
                default:
                    break;
            }
        }
    }

    // Hintergrundfenster
    ImGui::SetNextWindowPos({0, Rahmen::ABSTAND_TOP});
    ImGui::SetNextWindowSize({static_cast<float>(fenster.getSize().x), fenster.getSize().y - Rahmen::ABSTAND_TOP});
    ImGui::Begin(TXT::get("lager").c_str(), nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

    // Tabs
    if (ImGui::BeginTabBar("##tabbar_gui_lager", ImGuiTabBarFlags_NoCloseWithMiddleMouseButton)) {
        if (imgui_zusatz::Tab("uebersicht")) {
            draw_uebersicht();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("verwaltung")) {
            if (draw_verwaltung()) return;
            else ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("transport")) {
            if (draw_transport()) return;
            else ImGui::EndTabItem();
        } ImGui::EndTabBar();
    }
    ImGui::End(); // Hintergrund/Hauptfenster mit Tabs
    fenster.clear();
}

void Gui_Lager::draw_uebersicht() {
    // Titel
    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("warenbestand").c_str());
    ImGui::PopFont();

    if (ImGui::Button(TXT::get("zurueck").c_str())) loop = false;
    static bool nur_vorhandene_zeigen = true;
    ImGui::Checkbox(TXT::get("nur_vorhandene_filter").c_str(), &nur_vorhandene_zeigen);

    ImGui::Columns(3, "lager_uebersicht", false);
    ImGui::SetColumnWidth(0, 150.f);

    // Überschriften
    static const char* ware_header_str = TXT::get("ware").c_str();
    imgui_zusatz::Align_right(ware_header_str);
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", ware_header_str);
    ImGui::NextColumn();
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("vorrat").c_str());
    ImGui::NextColumn();
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", TXT::get("qualitaet").c_str());
    ImGui::NextColumn();

    // Vorräte anzeigen
    for (const auto& warenkey : Lager::alle_waren_keys_sortiert()) {
        const auto& vorrat = lager.get_vorrat(warenkey);
        if (nur_vorhandene_zeigen && vorrat.menge == 0) continue;

        // Icon
        const auto& ware = Ware::get(warenkey);
        ImGui::Image(*ware.get_icon().data(), ware.get_icon().size_f());
        ImGui::SameLine();

        // Text
        const char* ware_str = TXT::get(warenkey).c_str();
        //Gui::align_right(ware_str);
        ImGui::TextUnformatted(ware_str);
        ImGui::NextColumn();

        // Menge
        ImGui::TextUnformatted(std::to_string(vorrat.menge).c_str());
        ImGui::SameLine(60.f);
        ImGui::PlotLines("", vorrat.menge_v.data(), vorrat.menge_v.size(), 0,
                "",   FLT_MAX, FLT_MAX,
                ImVec2(0.9f * ImGui::GetColumnWidth(ImGui::GetColumnIndex()), 40));
        ImGui::NextColumn();

        // Qualität
        std::stringstream ss;
        ss << std::setprecision(1) << std::fixed << vorrat.quali;
        const std::string s_quali(ss.str());
        ImGui::PlotLines("", vorrat.quali_v.data(), vorrat.quali_v.size(), 0,
                         s_quali.c_str(), 0.f, 10.f,
                ImVec2(0.9f * ImGui::GetColumnWidth(ImGui::GetColumnIndex()), 40));
        ImGui::NextColumn();
    }
    ImGui::Columns();
}

bool Gui_Lager::draw_transport() {
    imgui_zusatz::Ueberschrift(TXT::get("lagertransport_header").c_str());
    ImGui::NewLine();

    // Lagertransport unterwegs?
    if (lager.get_transport().unterwegs()) {
        if (lager.get_transport().is_hinfahrt()) ImGui::Text("%s", TXT::get("hinreise").c_str());
        else ImGui::Text("%s", TXT::get("rueckreise").c_str());
        ImGui::ProgressBar(lager.get_transport().fortschritt());
        return false;
    }

    // Attribute
    using auswahl_t = std::vector<std::pair<std::string, int>>;
    static auswahl_t waren_hin_anzahl;
    static auswahl_t waren_zurueck_anzahl;

    // Ziel
    ImGui::TextColored(sf::Color(Gui::FARBE1),"%s: ", TXT::get("ziel").c_str());
    ImGui::SameLine();
    ImGui::TextUnformatted(zielstadt.get_name().c_str());
    ImGui::SameLine();
    if (ImGui::Button(TXT::get("auswaehlen").c_str())) {
        tab_abbruch();
        Selektor_Stadt ss;
        ss.erase(stadt.get_id());
        ss.start();
        if (ss.get_auswahl() >= 0) {
            zielstadt = Klient::request(Netzwerk::Anfrage::STADT, ss.get_auswahl(), zielstadt);
        }
        return true;
    }

    // Lagertransport oder Verkauft
    static bool transport = false; // TODO zweites Lager
    if (dynastie.hat_lager(zielstadt.get_id())) {
        ImGui::SameLine();
        ImGui::Checkbox(TXT::get("vom_lager").c_str(), &transport); // TODO Tooltip
    }
    else transport = false;

    // Versenden
    ImGui::SameLine();
    if (ImGui::Button(TXT::get("los").c_str())) {
        Log::debug() << "\tLagertransport Los" << Log::endl;
        const std::unordered_map<std::string, uint16_t> waren_hin(waren_hin_anzahl.begin(), waren_hin_anzahl.end());
        const std::unordered_map<std::string, uint16_t> waren_zurueck(waren_zurueck_anzahl.begin(), waren_zurueck_anzahl.end());
        Klient::a_lager_transport(lager.get_id(), zielstadt.get_id(), transport, waren_hin, waren_zurueck);
        lager = Klient::request(Netzwerk::Anfrage::LAGER, lager.get_id(), lager);
    }

    // TODO Profit abschätzen

    // TODO Waren vorhanden?

    ImGui::NewLine();
    ImGui::Columns(2, "lager_kauf_verkauf", false);

    // Liefert true, wenn ein separates Fenster aufgerufen wurde.
    auto spalte_erzeugen = [&] (auswahl_t& auswahl_liste, const std::string& txt_markt, const std::string& txt_lager) {
        ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[1]);
        if (transport) ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", txt_lager.c_str());
        else ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", txt_markt.c_str());
        ImGui::PopFont();

        // Gewählte Waren
        for (size_t i = 0; i < auswahl_liste.size(); ++i) {
            auto& paar = auswahl_liste[i];
            const Ware& temp_ware = Ware::get(paar.first);
            ImGui::SetNextItemWidth(200.f);
            ImGui::SliderInt(TXT::get(paar.first).c_str(), &paar.second, 1, 100);
            ImGui::SameLine();
            ImGui::Image(*temp_ware.get_icon().data(), temp_ware.get_icon().size_f());
            ImGui::SameLine();
            std::string id_btn(TXT::get("entfernen") + "##ware_entfernen");
            id_btn.append(txt_markt);
            id_btn.append(std::to_string(i));
            if (ImGui::Button(id_btn.c_str())) auswahl_liste.erase(auswahl_liste.begin() + i);
        }

        // Ware hinzufügen
        std::string id_btn(TXT::get("hinzufuegen") + "##ware_hinzufuegen");
        id_btn.append(txt_markt);
        if (ImGui::Button(id_btn.c_str())) {
            ImGui::Columns();
            tab_abbruch();
            std::vector<std::string> blacklist;
            blacklist.reserve(auswahl_liste.size());
            for (const auto& auswahl : auswahl_liste) blacklist.push_back(auswahl.first);
            Selektor_Ware selektor = get_selektor_ware(blacklist);
            selektor.start();
            if (!selektor.is_abgebrochen()) auswahl_liste.emplace_back(selektor.get_auswahl(), 1);
            return true;
        }
        ImGui::NextColumn();
        return false;
    };

    // Kauf, Verkauf // TODO Verkauf/Bringen: Check Verfügbarkeit Lager
    if (spalte_erzeugen(waren_hin_anzahl, TXT::get("verkauf"),TXT::get("bringen"))) return true;
    if (spalte_erzeugen(waren_zurueck_anzahl,TXT::get("kauf"), TXT::get("holen"))) return true;
    return false;
}

bool Gui_Lager::draw_verwaltung() {
    static Lager::Automatik_Kauf auto_kauf;
    static Lager::Automatik_Kauf auto_verkauf;
    static bool zeige_auto_kauf = false;
    static bool zeige_auto_verkauf = false;

    // Autokauf/Autoverkauf-Fenster aufbauen
    auto auto_kauf_fenster = [&](
            bool& show,
            Lager::Automatik_Kauf& auto_verwaltung,
            const char* titel,
            int mindestmenge
            )
    {
        ImGui::SetNextWindowFocus();
        ImGui::Begin("##auto_verwaltung", &show, ImGuiWindowFlags_NoTitleBar);
        imgui_zusatz::Ueberschrift(titel);
        ImGui::SameLine();
        ImGui::Checkbox(TXT::get("aktiviert").c_str(), &auto_verwaltung.aktiv);
        ImGui::NewLine();

        // Waren konfigurieren
        if (!auto_verwaltung.ware_menge_preis.empty()) {
            ImGui::Columns(4);
            ImGui::TextUnformatted(TXT::get("ware").c_str());        ImGui::NextColumn();
            ImGui::TextUnformatted(TXT::get("anzahl").c_str());      ImGui::NextColumn();
            ImGui::TextUnformatted(TXT::get("stueckpreis").c_str()); ImGui::NextColumn();
            ImGui::TextUnformatted(TXT::get("entfernen").c_str());   ImGui::NextColumn();
        }
        for (auto& ware_menge_preis : auto_verwaltung.ware_menge_preis) {
            int menge = std::get<0>(ware_menge_preis.second);
            int preis = std::get<1>(ware_menge_preis.second);

            // Ware + Icon
            ImGui::Image(*Ware::get(ware_menge_preis.first).get_icon().data());
            ImGui::SameLine();
            ImGui::TextUnformatted(TXT::get(ware_menge_preis.first).c_str());
            ImGui::NextColumn();

            // Input: Menge
            if (const std::string btn_code("##input_menge_" + ware_menge_preis.first);
                ImGui::InputInt(btn_code.c_str(), &menge, 1) && menge >= mindestmenge)
            {
                ware_menge_preis.second = std::make_tuple(
                        static_cast<uint16_t>(menge),
                        static_cast<uint16_t>(preis)
                );
            } ImGui::NextColumn();

            // Input: Preis
            if (const std::string btn_code("##input_preis_" + ware_menge_preis.first);
                ImGui::InputInt(btn_code.c_str(), &preis, 1, 1) && preis >= 1)
            {
                ware_menge_preis.second = std::make_tuple(
                        static_cast<uint16_t>(menge),
                        static_cast<uint16_t>(preis)
                );
            } ImGui::NextColumn();

            // Ware Löschen
            if (const std::string btn_code("X##del_" + ware_menge_preis.first); ImGui::Button(btn_code.c_str())) {
                auto_kauf.ware_menge_preis.erase(ware_menge_preis.first);
                break;
            }
            ImGui::NextColumn();
        }
        ImGui::Columns();

        // Ware hinzufügen
        if (ImGui::Button(TXT::get("hinzufuegen").c_str())) {
            ImGui::End();
            tab_abbruch();
            std::vector<std::string> blacklist;
            blacklist.reserve(auto_verwaltung.ware_menge_preis.size());
            for (const auto& e : auto_verwaltung.ware_menge_preis) blacklist.push_back(e.first);
            Selektor_Ware selektor = get_selektor_ware(blacklist);
            selektor.start();
            if (!selektor.is_abgebrochen()) {
                const std::string& auswahl = selektor.get_auswahl();
                auto_verwaltung.ware_menge_preis[auswahl] = std::make_tuple<uint16_t, uint16_t>(mindestmenge, 10);
            }
            return true;
        }

        ImGui::NewLine();
        if (ImGui::Button(TXT::get("speichern").c_str())) {
            lager.auto_einkauf = auto_kauf;
            lager.auto_verkauf = auto_verkauf;
            Klient::a_lager_verwaltung(lager);
            sync();
            show = false;
        }
        ImGui::SameLine();
        if (ImGui::Button(TXT::get("abbrechen").c_str())) show = false;
        ImGui::End();
        return false;
    };

    if (zeige_auto_kauf    && auto_kauf_fenster(zeige_auto_kauf,    auto_kauf,    TXT::get("auto_kauf").c_str(), 1)) return true;
    if (zeige_auto_verkauf && auto_kauf_fenster(zeige_auto_verkauf, auto_verkauf, TXT::get("auto_verkauf").c_str(), 0)) return true;

    imgui_zusatz::Ueberschrift(TXT::get("lagerverwaltung_header").c_str());
    ImGui::NewLine();

    if (ImGui::Button(TXT::get("auto_kauf").c_str())) { auto_kauf = lager.auto_einkauf; zeige_auto_kauf = true; }
    ImGui::SameLine();
    if (ImGui::Button(TXT::get("auto_verkauf").c_str())) { auto_verkauf = lager.auto_verkauf; zeige_auto_verkauf = true; }
    return false;
}

void Gui_Lager::tab_abbruch() {
    ImGui::EndTabItem();
    ImGui::EndTabBar();
    ImGui::End();
    this->draw();
}
