#include "gui_werkstatt.hpp"
#include "../netzwerk/klient.hpp"
#include "../gfx/rahmen.hpp"
#include "selektoren/selektor_arbeiter.hpp"
#include "selektoren/selektor_prozess.hpp"
#include "selektoren/selektor_generic.hpp"
#include "../lingua/lingua.hpp"
#include "zusatz/imgui_zusatz.h"

#include <nada/log.hpp>

using nada::Log;

Gui_Werkstatt::Gui_Werkstatt(Netzwerk::id_t id_werk, Netzwerk::id_t id_stadt) :
    Basisfenster(*Gui::fenster)
{
    Log::debug() << "Gui_Werkstatt(" << id_werk << ", " << id_stadt << ')' << Log::endl;
    werk  = Klient::request(Netzwerk::Anfrage::WERKSTATT, id_werk, werk);
    lager = Klient::request(Netzwerk::Anfrage::LAGER, dynastie.get_lager(id_stadt), lager);
    stadt = Klient::request(Netzwerk::Anfrage::STADT, id_stadt, stadt);
    markt = Klient::request(Netzwerk::Anfrage::MARKT, stadt.get_id_markt(), markt);
    sync_arbeiter();
    update_filter();
}

void Gui_Werkstatt::sync() {
    static unsigned sekunden = 0;
    static unsigned last_sync_tag = 0;

    // Daten vom Netzwerk synchronisieren
    if (net_clock.getElapsedTime() >= sf::milliseconds(1000)) {
        werk  = Klient::request(Netzwerk::Anfrage::WERKSTATT, werk.get_id(), werk);
        lager = Klient::request(Netzwerk::Anfrage::LAGER, lager.get_id(), lager);
        net_clock.restart();
        sekunden++;
        update_filter();
        sync_arbeiter();
    }

    // Jeden Tag
    if (last_sync_tag != zeit.get_jahreszeit()) {
        stadt = Klient::request(Netzwerk::Anfrage::STADT, stadt.get_id(), stadt);
        markt = Klient::request(Netzwerk::Anfrage::MARKT, stadt.get_id_markt(), markt);
        last_sync_tag = zeit.get_jahreszeit();
        sync_arbeiter();
    }
}

void Gui_Werkstatt::loop_body() {
    sync();

    // Events
    sf::Event event;
    while (fenster.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(event);
        if (event.type == sf::Event::Closed) fenster.close(); //macht Fenster schließen mögich
        if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Escape: loop = false; break;
                default: break;
            }
        }
    }

    /// Wenn ImGui/Rendern dieses Fensters vorzeitig abgebrochen werden muss
    const std::function f = [this]() {
        ImGui::EndTabItem();
        ImGui::EndTabBar();
        ImGui::End();
        this->draw();
    };

    ImGui::SetNextWindowPos({0, Rahmen::ABSTAND_TOP});
    ImGui::SetNextWindowSize({static_cast<float>(fenster.getSize().x), fenster.getSize().y - Rahmen::ABSTAND_TOP});
    ImGui::Begin("Werkstatt", nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

    // const std::string btn_verlassen(TXT::get("verlassen") + "##WerkstattVerlassen");
    // if (ImGui::Button(btn_verlassen.c_str())) loop = false; TODO: Verlassen Button irgendwo besser positionieren, hier doof

    if (ImGui::BeginTabBar("werkstatttabs", ImGuiTabBarFlags_NoCloseWithMiddleMouseButton)) {
        if (imgui_zusatz::Tab("umbau")) {
            if (draw_ausbau(f)) return;
            else ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("lager")) {
            draw_lager();
            ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("anlagen")) {
            if (draw_werk()) return;
            else ImGui::EndTabItem();
        }
        if (imgui_zusatz::Tab("arbeiter")) {
            if (draw_arbeiter(f)) return;
            else ImGui::EndTabItem();
        }
        ImGui::EndTabBar();
    }
    ImGui::End();
    fenster.clear();
}

bool Gui_Werkstatt::draw_arbeiter(const std::function<void(void)>& f) {
    // Arbeiter einstellen
    if (ImGui::Button("rekrutieren"_txtc)) {
        f();

        Selektor_Arbeiter selektor_arbeiter(stadt.get_id());
        selektor_arbeiter.start();
        if (!selektor_arbeiter.is_abgebrochen()) {
            const auto& auswahl = selektor_arbeiter.get_auswahl();
            for (const auto& a : auswahl) {
                Log::debug() << "Arbeiter " << a << " wird rekrutiert." << Log::endl;
                Klient::a_werk_stadt_arbeitertransfer(werk.get_id(), stadt.get_id(), a, true);
            }
        }
        return true;
    }
    ImGui::Separator();
    ImGui::NewLine();

    // Freie Arbeiter auflisten
    imgui_zusatz::Ueberschrift("unzugewiesene_arbeiter"_txtc);
    imgui_zusatz::Hilfe("untaetig_tooltip"_txtc);
    ImGui::Columns(2, nullptr, false);
    for (const auto& arbeiter : werk.get_freie_arbeiter()) {
        const Selektor_Arbeiter::Auswahl selektiert = Selektor_Arbeiter::arbeiter_info(arbeiterchars.at(arbeiter), nullptr, true);
        if (selektiert == Selektor_Arbeiter::Auswahl::NAME && !werk.get_anlagen().empty()) { // TODO Rückmeldung "keine Anlage vorhanden"
            ImGui::Columns();
            f();

            // Anlagen stehen zur Auswahl
            std::vector<std::tuple<std::string, uint8_t, const Grafik*>> liste;
            liste.reserve(werk.get_anlagen().size());
            for (uint8_t i = 0; i < static_cast<uint8_t>(werk.get_anlagen().size()); ++i) {
                const auto& anlage = werk.get_anlagen()[i];
                liste.emplace_back(
                        "anlage"_txt + " " + std::to_string(i+1) + " " + TXT::get(anlage.get_prozess().get_key()),
                        i, nullptr
                );
            }

            // Arbeiter zuweisen
            Selektor_Generic<uint8_t> selektor(liste); // Liefert Anlagen ID
            selektor.start();
            if (!selektor.is_abgebrochen()) {
                Klient::a_werk_werk_arbeitertransfer(werk.get_id(), arbeiter, selektor.get_auswahl());
                werk = Klient::request(Netzwerk::Anfrage::WERKSTATT, werk.get_id(), werk);
            }
            return true;
        }
        // Entlassen Option
        if (selektiert == Selektor_Arbeiter::Auswahl::ENTLASSEN) {
            Klient::a_werk_stadt_arbeitertransfer(werk.get_id(), stadt.get_id(), arbeiter, false);
            werk = Klient::request(Netzwerk::Anfrage::WERKSTATT, werk.get_id(), werk);
            break;
        }
        ImGui::NextColumn();
    } ImGui::Columns();
    return false;
}

void Gui_Werkstatt::draw_lager() {
    // Sollen nur verwendete Waren angezeigt werden?
    static bool nur_verwendete_zeigen = false;
    ImGui::Checkbox("nur_verwendete"_txtc, &nur_verwendete_zeigen);

    // Vorräte anzeigen
    ImGui::Columns(4, "Werklager", false);

    // Zeigt Infos zu einem Vorrat
    auto vorratinfo = [] (const std::string& warenkey, const Vorrat& vorrat) {
        const auto& ware = Ware::get(warenkey);
        ImGui::Text("%d ", vorrat.menge);
        ImGui::SameLine();
        ImGui::Image(*ware.get_icon().data(), ware.get_icon().size_f());
        ImGui::SameLine();
        ImGui::Text("%s", TXT::get(warenkey).c_str());
        ImGui::NextColumn();
    };

    // Alle / Verwendete iterieren
    if (nur_verwendete_zeigen) for (const auto& key_ware : warenkeys) { // eisen nicht vorhanden TODO BUG
        const Vorrat& vorrat = lager.get_vorrat(key_ware);
        vorratinfo(key_ware, vorrat);
    }
    else for (const auto& key_ware : Ware::alle()) {
        const Vorrat& vorrat = lager.get_vorrat(key_ware.first);
        vorratinfo(key_ware.first, vorrat);
    }
    ImGui::Columns();
}

bool Gui_Werkstatt::draw_werk() {
    // Anlage auswählen
    static int i_anlage = 0;
    auto get_anlage_text = [&](unsigned i) {
        return std::string("anlage"_txt + " " + std::to_string(i+1) + " " + TXT::get(werk.get_anlagen()[i].get_prozess().get_key()));
    };
    if (werk.get_anlagen().empty()) return false;
    std::string preview(get_anlage_text(i_anlage));
    if (ImGui::BeginCombo("##anlage_waehlen", preview.c_str())) {
        for (unsigned i = 0; i < werk.get_anlagen().size(); ++i) {
            std::string select_id(get_anlage_text(i));
            if (ImGui::Selectable(select_id.c_str())) i_anlage = i;
            if (i_anlage == (int)i) ImGui::SetItemDefaultFocus();
        }
        ImGui::EndCombo();
    }
    if (i_anlage > (int)werk.get_anlagen().size() - 1) i_anlage = (int)werk.get_anlagen().size() - 1; // Safety

    // Anlageninfo
    ImGui::NewLine();
    imgui_zusatz::Ueberschrift(TXT::get("basisinfo").c_str());
    const auto& anlage = werk.get_anlagen()[i_anlage];
    ImGui::Columns(2, nullptr, false);
    ImGui::NewLine();
    Selektor_Prozess::prozessinfo(anlage.get_prozess());
    ImGui::NewLine();

    // Produktion An/Aus
    ImGui::NewLine();
    imgui_zusatz::Ueberschrift("produktion"_txtc);
    bool aktiv = anlage.is_aktiviert();
    if (ImGui::Checkbox("produktion_aktiv"_txtc, &aktiv)) {
        Klient::a_werk_anlage_start_stop(werk.get_id(), i_anlage);
        werk = Klient::request(Netzwerk::Anfrage::WERKSTATT, werk.get_id(), werk);
        ImGui::TreePop();
        return false;
    }

    // Anlageninfos
    ImGui::NewLine();
    imgui_zusatz::Ueberschrift(TXT::get("effizienz").c_str());
    imgui_zusatz::Fortschritt(anlage.get_fortschritt(), anlage.get_fortschritt() > 0 && !anlage.get_arbeiter().empty());
    ImGui::Text("%s: %.1f%%", TXT::get("produktivitaet").c_str(), 100.f * anlage.get_produktivitaet());
    ImGui::ProgressBar(anlage.get_bonus_arbeiter(), {-1,0}, TXT::get("bonus_arbeiter").c_str());
    ImGui::ProgressBar(anlage.get_bonus_platz(), {-1,0}, TXT::get("bonus_platz").c_str());
    ImGui::ProgressBar(anlage.get_bonus_massenproduktion(), {-1,0}, TXT::get("bonus_masse").c_str());

    ImGui::Text("%s: %d", "taegliche_kosten"_txtc, anlage.get_taegliche_kosten());
    ImGui::Text("%s: %d", "groesse"_txtc, anlage.get_groesse());
    ImGui::Text("%s: %d", "produziert3sg"_txtc, anlage.get_zaehler());

    // Anlage vergrößern
    ImGui::NewLine();
    imgui_zusatz::Ueberschrift(TXT::get("umbau").c_str());
    const int32_t preis_anlage_erweitern = werk.get_preis_erweitern() + anlage.get_groesse() * 100;
    const std::string tooltip_k (TXT::get("tooltip_anlage_vergroessern") + std::to_string(preis_anlage_erweitern));
    const std::string button_k_text (TXT::get("erweitern") + "##AnlageErweitern" + std::to_string(i_anlage));
    if (ImGui::NewLine(); ImGui::Button(button_k_text.c_str())) {
        Klient::a_werk_anlage_erweitern_verkleinern(dynastie.get_id(), werk.get_id(), i_anlage,
                                                    +1, -preis_anlage_erweitern
        );
    }
    imgui_zusatz::Tooltip(tooltip_k.c_str());

    // Anlage verkleinern
    const std::string tooltip_verkleinern (TXT::get("tooltip_anlage_verkleinern") + std::to_string(preis_anlage_erweitern / 2));
    const std::string button_verkl_text (TXT::get("verkleinern") + "##AnlageVerkleinern" + std::to_string(i_anlage));
    if (ImGui::SameLine(); ImGui::Button(button_verkl_text.c_str())) {
        Klient::a_werk_anlage_erweitern_verkleinern(dynastie.get_id(), werk.get_id(), i_anlage,
                                                    -1, (-preis_anlage_erweitern) / 2
        );
    }
    imgui_zusatz::Tooltip(tooltip_verkleinern.c_str());

    // Anlage verkaufen
    const int32_t vk_erloes = preis_anlage_erweitern / 4;
    const std::string tooltip_verkaufen (TXT::get("tooltip_anlage_verkaufen") + std::to_string(vk_erloes));
    const std::string button_verkaufen_text (TXT::get("verkaufen") + "##AnlageVerkaufen" + std::to_string(i_anlage));
    if (ImGui::SameLine(); ImGui::Button(button_verkaufen_text.c_str())) {
        Klient::a_werk_anlage_verkaufen(
                dynastie.get_id(), werk.get_id(), i_anlage, vk_erloes
        );
    }
    imgui_zusatz::Tooltip(tooltip_verkaufen.c_str());
    ImGui::NextColumn();

    // Arbeiter in Anlage (+ Transfer)
    ImGui::BeginChild(TXT::get("arbeiter_in_anlage").c_str(), {0,0}, true);
    imgui_zusatz::Ueberschrift(TXT::get("arbeiter_in_anlage").c_str());
    ImGui::SameLine();
    ImGui::TextDisabled("(?)");
    imgui_zusatz::Tooltip(TXT::get("tooltip_arbeiter_freistellen").c_str());
    for (const auto& arbeiter : anlage.get_arbeiter()) {
        const auto klick = Selektor_Arbeiter::arbeiter_info(arbeiterchars.at(arbeiter), nullptr);
        if (klick == Selektor_Arbeiter::Auswahl::NAME) {
            Klient::a_werk_werk_arbeitertransfer(werk.get_id(), arbeiter, i_anlage);
            werk = Klient::request(Netzwerk::Anfrage::WERKSTATT, werk.get_id(), werk);
            break;
        }
    }
    ImGui::EndChild();
    ImGui::Columns();
    return false;
}

void Gui_Werkstatt::update_filter() {
    for (const auto& anlage : werk.get_anlagen()) {
        for (const auto& input  : anlage.get_prozess().get_inputs())  warenkeys.insert(input.first);
        for (const auto& output : anlage.get_prozess().get_outputs()) warenkeys.insert(output.first);
    }
}

bool Gui_Werkstatt::draw_ausbau(const std::function<void(void)>& f) {
    // Tägliche Gesamtkosten
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s: %d", TXT::get("taegliche_gesamtkosten").c_str(), werk.get_taegliche_kosten());
    ImGui::TextColored(sf::Color(Gui::FARBE1), "%s: %d/%d", TXT::get("platz_verfuegbar").c_str(),
                       werk.get_freier_platz(), werk.get_freier_platz() + werk.get_platz_anlagen());

    // Neue Anlage einrichten
    if (werk.get_anlagen().size() < Werkstatt::MAX_ANLAGEN && ImGui::Button(TXT::get("neue_anlage_einrichten").c_str())) {
        f();
        // Nur Prozesse derselben Gruppe erlaubt
        Selektor_Prozess selektor(werk.get_prozessgruppe());
        selektor.start();

        if (!selektor.get_prozess_key().empty()) { // Wurde etwas ausgewählt?
            Log::debug() << '\t' << selektor.get_prozess_key() << " wird eingerichtet" << Log::endl;
            const Prozess& auswahl = Prozess::get(selektor.get_prozess_key());
            Klient::a_werk_neue_anlage(
                    dynastie.get_id(), werk.get_id(), auswahl.get_kosten(), auswahl.get_key()
            );
        }
        return true;
    }
    // Werk verkleinern
    if (werk.get_freier_platz() > 1) {
        const int32_t erloes = werk.get_preis_erweitern() / 2;
        const std::string tooltip_k(TXT::get("tooltip_werk_verkleinern") + std::to_string(erloes));
        const std::string button_k_text(TXT::get("verkleinern") + "##WerkVerkleinern");
        ImGui::SameLine(); if (ImGui::Button(button_k_text.c_str())) {
            Klient::a_werk_erweitern_verkleinern(dynastie.get_id(), werk.get_id(), -1, erloes);
            werk = Klient::request(Netzwerk::Anfrage::WERKSTATT, werk.get_id(), werk);
        } imgui_zusatz::Tooltip(tooltip_k.c_str());
    }
    {   // Werk Vergrößern
        const std::string tooltip_k(TXT::get("tooltip_werk_vergroessern") + std::to_string(werk.get_preis_erweitern()));
        const std::string button_k_text(TXT::get("erweitern") + "##WerkErweitern");
        ImGui::SameLine(); if (ImGui::Button(button_k_text.c_str())) {
            Klient::a_werk_erweitern_verkleinern(dynastie.get_id(), werk.get_id(), +1, -werk.get_preis_erweitern());
            werk = Klient::request(Netzwerk::Anfrage::WERKSTATT, werk.get_id(), werk);
        } imgui_zusatz::Tooltip(tooltip_k.c_str());
    }
    return false;
}

void Gui_Werkstatt::sync_arbeiter() {
    // Holt alle Arbeiter in Stadt und Werk (inkl. Anlagen)
    std::unordered_set<Netzwerk::big_id_t> arbeiter_ids;
    arbeiter_ids.reserve(stadt.get_arbeiter().size() + werk.get_freie_arbeiter().size());
    arbeiter_ids.insert(stadt.get_arbeiter().begin(), stadt.get_arbeiter().end());
    arbeiter_ids.insert(werk.get_freie_arbeiter().begin(), werk.get_freie_arbeiter().end());
    for (const auto& anlage : werk.get_anlagen()) arbeiter_ids.insert(anlage.get_arbeiter().begin(), anlage.get_arbeiter().end());
    const auto& antwort = Klient::request_alle<Charakter>(Netzwerk::Anfrage::CHARAKTER, arbeiter_ids);
    for (const auto& c : antwort) arbeiterchars[c.get_id()] = c;
}
