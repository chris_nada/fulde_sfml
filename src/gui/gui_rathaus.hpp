#pragma once

#include "basisfenster.hpp"
#include "../welt/stadt/stadt.hpp"
#include "../welt/land.hpp"
#include "../welt/politik/amt.hpp"

class Charakter;

/**
 * Nutzeroberfläche, die erscheint, wenn das Rathaus aus einem
 * Viertel heraus (vgl. `Gui_Viertel`) betreten wird.
 */
class Gui_Rathaus final : public Basisfenster {

    /// Hilfs-Struct für die Daten der Steuerarten. Nur für die GUI.
    struct Steuer_UI {
        Steuer_UI(Steuertyp steuertyp, int alter_satz, const char* name) :
                steuertyp(steuertyp), alter_satz(alter_satz), neuer_satz(alter_satz), name(name) {}
        Steuertyp steuertyp;
        int alter_satz, neuer_satz;
        const char* name;
    };

public:

    /// Ctor.
    explicit Gui_Rathaus(Netzwerk::id_t stadt_id);

private:

    /// Draw-Schleife
    void loop_body() override;

    /// Netzwerksynchronisierung.
    void sync(bool force = false);

    /// Tabs
    void draw_tab_stadt();
    void draw_tab_land();
    void draw_tab_infrastruktur();
    void draw_tab_aemter();

private:

    /// Land, in dem das abgebildete Rathaus steht.
    Land land;

    /// Stadt, in der das abgebildete Rathaus steht.
    Stadt stadt;

    /// Ämter dieser Stadt.
    std::vector<Amt> aemter_stadt;

    /// Ämter dieses Landes.
    std::vector<Amt> aemter_land;

    /// Alle relevanten Charaktere: id - Charakter
    std::unordered_map<Netzwerk::id_t, std::unique_ptr<Charakter>> charmap;

    /// Darf bewerben? Muss synchronisiert werden, da in anderer Stadt möglicherweise schon beworben.
    bool darf_bewerben;

    /// Amt des Spielers.
    std::optional<Amt> eigenes_amt;

    /// Speicher für alle anzuzeigenden + änderbaren Steuern
    std::vector<Steuer_UI> steuern;

};
