#pragma once

#include "basisfenster.hpp"
#include "../welt/bauten/gebaeude.hpp"
#include "../welt/spieler/charakter.hpp"

/**
 *
 */
class Gui_Heim final : public Basisfenster {

public:

    /// Ctor.
    explicit Gui_Heim(const Gebaeude& gebaeude);

private:

    /// @inherit
    void loop_body() override;

    /// Zeichnet das Tab "Dynastie".
    void draw_dynastie();

    /// Zeichnet das Tab "Infrastruktur".
    void draw_infrastruktur();

    /// Zeichnet das Tab "Finanzen".
    void draw_finanzen();

    /// Zeichnet das Tab "Ämter".
    void draw_aemter();

    /// Zeichnet das Tab "Kontrahenten".
    void draw_kontrahenten();

    /// Zeichnet das Tab "Nachrichten".
    void draw_nachrichten();

    /// Netzwerksynchronisation.
    void sync();

    /// Netzwerksynchronisation: Dynastien.
    void sync_dynastien();

private:

    /// Spielerdynastie. Wird sync()-hronisiert.
    Dynastie dynastie;

    /// Spielercharakter. Wird sync()-hronisiert.
    Charakter charakter;

    /// Alle Dynastien. Wird synchronisiert, wo gebraucht.
    std::vector<Dynastie> dynastien;

};
