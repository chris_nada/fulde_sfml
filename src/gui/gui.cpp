#include "gui.hpp"
#include "../netzwerk/klient.hpp"
#include "../werkzeuge/assets.hpp"

#include <nada/log.hpp>
#include <implot.h>

using nada::Log;

int Gui::start() {
    Log::debug() << "Gui::start()" << Log::endl;
    ImGui::CreateContext(); //neu ab [IMGUI160]
    ImPlot::CreateContext();
    try { Log::benchmark_debug(Gui::init, "Gui::init()"); }
    catch (std::exception& e) {
        Log::err() << "\tGui::init() fehlgeschlagen: " << e.what() << Log::endl;
        return 1;
    }

    // Hauptmenü
    {
        Gui_hauptmenu hauptmenu;
        hauptmenu.start();
    }
    delete fenster;
    ImPlot::DestroyContext();
    ImGui::DestroyContext(); //neu ab [IMGUI160]
    return 0;
}

void Gui::draw() {
    // Rendern
    ImGui::End();
    fenster->clear();
    ImGui::SFML::Render(*fenster);
    fenster->display();
}

void Gui::init() {
    Log::debug() << "Gui::init()" << Log::endl;
    nada::Ini cfg{"config.ini"};

    // Mit Keyboard UI navigieren erlaubt
    ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    ImGui::GetIO().IniFilename = "data/imgui.ini"; // TODO notwendig?
    ImGui::GetIO().FontGlobalScale = cfg.get_float<float>("ui_scale", 1.0f); // TODO einstellbar
    const float font_scale = cfg.get_float<float>("font_scale", 1.0f);

    // Schriften laden
    Log::debug() << "Gui::init() Schriften laden..." << Log::endl;
    static auto font_mono_data     = Assets::get("data/gfx/font/font_mono.ttf");
    static auto font_sans_data     = Assets::get("data/gfx/font/font_sans.ttf");
    static auto font_standard_data = Assets::get("data/gfx/font/font.ttf");
    if (font_mono.loadFromMemory(font_mono_data.data(), font_mono_data.size()) &&
        font_sans.loadFromMemory(font_sans_data.data(), font_sans_data.size()) &&
        font_standard.loadFromMemory(font_standard_data.data(), font_standard_data.size())) {}
    else throw std::runtime_error("Schriften konnten nicht geladen werden");

    // ImGui Stil einstellen
    Log::debug() << "Gui::init() ImGui konfigurieren..." << Log::endl;
    ImGui::PushStyleVar(ImGuiStyleVar_ScrollbarSize, 32.f);
    ImGui::StyleColorsClassic();

    // Rundungen gleichmäßig setzen bzw. entfernen
    for (const auto& style_var : {
            ImGuiStyleVar_FrameRounding, ImGuiStyleVar_ScrollbarRounding, ImGuiStyleVar_WindowRounding,
            ImGuiStyleVar_TabRounding, ImGuiStyleVar_GrabRounding, ImGuiStyleVar_ChildRounding, ImGuiStyleVar_PopupRounding
    }) ImGui::PushStyleVar(style_var, 0.f);

    ImGui::PushStyleColor(ImGuiCol_Button,        sf::Color(FARBE_STANDARD));
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, sf::Color(FARBE_HOVERED));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive,  sf::Color(FARBE_ACTIVE));

    ImGui::PushStyleColor(ImGuiCol_FrameBg,        sf::Color(FARBE_STANDARD));
    ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, sf::Color(FARBE_HOVERED));
    ImGui::PushStyleColor(ImGuiCol_FrameBgActive,  sf::Color(FARBE_ACTIVE));

    ImGui::PushStyleColor(ImGuiCol_Header,        sf::Color(FARBE_STANDARD));
    ImGui::PushStyleColor(ImGuiCol_HeaderHovered, sf::Color(FARBE_HOVERED));
    ImGui::PushStyleColor(ImGuiCol_HeaderActive,  sf::Color(FARBE_ACTIVE));

    ImGui::PushStyleColor(ImGuiCol_Tab,                sf::Color(FARBE_STANDARD));
    ImGui::PushStyleColor(ImGuiCol_TabHovered,         sf::Color(FARBE_HOVERED));
    ImGui::PushStyleColor(ImGuiCol_TabActive,          sf::Color(FARBE_ACTIVE));
    ImGui::PushStyleColor(ImGuiCol_TabUnfocused,       sf::Color(FARBE_DUNKEL));
    ImGui::PushStyleColor(ImGuiCol_TabUnfocusedActive, sf::Color(FARBE_DUNKEL));

    ImGui::PushStyleColor(ImGuiCol_ScrollbarBg,          sf::Color(FARBE_DUNKEL));
    ImGui::PushStyleColor(ImGuiCol_ScrollbarGrab,        sf::Color(FARBE_STANDARD));
    ImGui::PushStyleColor(ImGuiCol_ScrollbarGrabHovered, sf::Color(FARBE_HOVERED));
    ImGui::PushStyleColor(ImGuiCol_ScrollbarGrabActive,  sf::Color(FARBE_ACTIVE));

    ImGui::PushStyleColor(ImGuiCol_NavHighlight,          sf::Color(FARBE_ACTIVE));
    ImGui::PushStyleColor(ImGuiCol_NavWindowingHighlight, sf::Color(FARBE_ACTIVE));
    ImGui::PushStyleColor(ImGuiCol_NavWindowingDimBg,     sf::Color(FARBE_DUNKEL));
    ImGui::PushStyleColor(ImGuiCol_ModalWindowDimBg,      sf::Color(FARBE_DUNKEL));

    ImGui::PushStyleColor(ImGuiCol_TableRowBg,    sf::Color(FARBE_STANDARD));
    ImGui::PushStyleColor(ImGuiCol_TableRowBgAlt, sf::Color(FARBE_DUNKEL));

    // Fenster init
    Log::debug() << "Gui::init() Fenster initialisieren..." << Log::endl;
    const bool vollbild = cfg.get_int<int>("fullscreen") == 1;
    const sf::Uint32 fenster_flags = vollbild ? sf::Style::None : sf::Style::Close;
    const sf::VideoMode video_mode = vollbild ? sf::VideoMode::getDesktopMode() : sf::VideoMode(
            cfg.get_int<unsigned>("resolution_x"), cfg.get_int<unsigned>("resolution_y"));
    fenster = new sf::RenderWindow(video_mode,
            "Fulde", fenster_flags); // sf::Style::Fullscreen
    fenster->setFramerateLimit(FPS);
    fenster->setVerticalSyncEnabled(cfg.get_int<int>("vsync") == 1);
    fenster->setTitle("Fulde");
    ImGui::SFML::Init(*fenster, false);

    // Schriftarten einrichten
    Log::debug() << "Gui::init() ImGui Schriften initialisieren..." << Log::endl;
    auto& font_atlas = ImGui::GetIO().Fonts;
    font_atlas->AddFontFromMemoryTTF(font_standard_data.data(), 16 * font_scale, 16 * font_scale, nullptr, font_atlas->GetGlyphRangesCyrillic()); //0
    font_atlas->AddFontFromMemoryTTF(font_standard_data.data(), 22 * font_scale, 22 * font_scale, nullptr, font_atlas->GetGlyphRangesCyrillic()); //1
    font_atlas->AddFontFromMemoryTTF(font_standard_data.data(), 26 * font_scale, 26 * font_scale, nullptr, font_atlas->GetGlyphRangesCyrillic()); //2
    font_atlas->AddFontFromMemoryTTF(font_mono_data.data(),     18 * font_scale, 18 * font_scale, nullptr, font_atlas->GetGlyphRangesCyrillic()); //3
    font_atlas->AddFontFromMemoryTTF(font_standard_data.data(), 14 * font_scale, 14 * font_scale, nullptr, font_atlas->GetGlyphRangesCyrillic()); //4
    for (auto& font_cfg : font_atlas->ConfigData) font_cfg.FontDataOwnedByAtlas = false;
    ImGui::SFML::UpdateFontTexture();

    /* Verwendung:
    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[0]);
    ImGui::PopFont();
     */
}
