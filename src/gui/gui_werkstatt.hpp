#pragma once

#include "basisfenster.hpp"
#include "../welt/produktion/werkstatt.hpp"
#include "../welt/waren/lager.hpp"
#include "../welt/stadt/stadt.hpp"
#include "../welt/spieler/charakter.hpp"

/// Nutzeroberfläche zur Verwalten einer Werkstatt.
class Gui_Werkstatt final : public Basisfenster {

public:

    /// Ctor. Benötigt ID des anzuzeigenden Werks + ID der Stadt, in der es liegt.
    Gui_Werkstatt(Netzwerk::id_t id_werk, Netzwerk::id_t id_stadt);

private:

    void loop_body() override;

    /// Besorgt sich alle angezeigten Daten.
    void sync();

    /// Besorgt sich alle Arbeiter in Werkstatt und Anlagen.
    void sync_arbeiter();

    /// Verwendete Waren (`warenkeys`) aktualisieren.
    void update_filter();

    /// Rendert den Bereich 'Arbeiter'.
    bool draw_arbeiter(const std::function<void(void)>& function);

    /// Rendert den Bereich 'Lager'.
    void draw_lager();

    /// Rendert den Bereich 'Werk'. Hier werden die Anlagen angezeigt.
    bool draw_werk();

    /// Rendert den Bereich 'Ausbau'.
    bool draw_ausbau(const std::function<void(void)>& f);

private:

    /// Veranschaulichte Werkstatt.
    Werkstatt werk;

    /// Mit der Werkstatt verbundenes Lager.
    Lager lager;

    /// Stadt, in der das Werk steht.
    Stadt stadt;

    /// Benötigt für Preisniveau.
    Markt markt;

    /// Eine Liste der Waren, die in den Inputs und Outputs aller Prozesse verwendet werden.
    std::unordered_set<std::string> warenkeys;

    /// Arbeitercharaktere.
    std::unordered_map<Netzwerk::id_t, Charakter> arbeiterchars;

};
