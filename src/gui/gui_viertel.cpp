#include "gui_viertel.hpp"
#include "../netzwerk/klient.hpp"
#include "selektoren/selektor_viertel.hpp"
#include "gui_markt.hpp"
#include "gui_lager.hpp"
#include "gui_werkstatt.hpp"
#include "gui_heim.hpp"
#include "zusatz/imgui_zusatz.h"
#include "gui_rathaus.hpp"
#include <unordered_map>
#include <nada/log.hpp>

using nada::Log;

Gui_Viertel::Gui_Viertel(Netzwerk::id_t viertel_id, Netzwerk::big_id_t char_id) :
        char_id(char_id), dynastie(0),
        viertel(viertel_id, 0, "unintialisiert"),
        fenster(Gui::fenster)
{
    Log::debug() << "Gui_Viertel(" << viertel.get_id() << ")" << Log::endl;

    // Rahmen initialisieren + Sync
    viertel = Klient::request(Netzwerk::Anfrage::VIERTEL, viertel.get_id(), viertel);
    charaktere[char_id] = Klient::request(Netzwerk::Anfrage::CHARAKTER, char_id, charaktere[char_id]); // depends: viertel
    dynastie = Klient::request(Netzwerk::Anfrage::DYNASTIE, charaktere[char_id].get_dynastie().value(), dynastie); // depends: charaktere
    stadt = Klient::request(Netzwerk::Anfrage::STADT, viertel.get_id_stadt(), stadt); // depends: viertel
    land = Klient::request(Netzwerk::Anfrage::LAND, stadt.get_id_land(), land); // depends: stadt
    kultur = Klient::request(Netzwerk::Anfrage::KULTUR, land.get_id_kultur(), kultur); // depends: land
    Rahmen::init(dynastie, stadt, zeit);
    sync();

    // Umweltabhängige Hintergründe
    himmel = Parallaxe_Wandel::get_himmel(land.get_id());
    bg_stadt = Parallaxe_Wandel("data/gfx/viertel/bg_stadt/" + kultur.get_bg_stadt(stadt.get_name()));
    bg_viertel = Parallaxe_Wandel("data/gfx/viertel/bg_viertel/" + kultur.get_bg_viertel(viertel.get_name()));
    strasse = Parallaxe("data/gfx/viertel/strasse/1.png", 10); // TODO dynamisch, TODO tag/nacht

    // View konfigurieren TODO
    view.setSize(Gui::VIEW_SIZE_X, Gui::VIEW_SIZE_Y);
    //view.zoom(static_cast<float>(fenster->getSize().y - Y_PADDING) / view.getSize().y);

    // Gfx Positionieren
    wegweiser_gfx = Grafik("data/gfx/bauten/" + kultur.get_key() + "/wegweiser.png");
    wegweiser_gfx.set_y(view.getSize().y - Y_PADDING - wegweiser_gfx.size().y);
    bg_stadt.set_y(static_cast<float>(view.getSize().y - 2 * Y_PADDING - (float)bg_stadt.get_size_y()));
    bg_viertel.set_y(static_cast<float>(view.getSize().y - bg_viertel.get_size_y()
                     - strasse.get_size_y()) + Y_RAND_STRASSE);
    strasse.set_y(static_cast<float>(view.getSize().y - strasse.get_size_y()));
    for (Grafik& g : grenzen) {
        g = Grafik("data/gfx/bauten/" + kultur.get_key() + "/grenze.png");
        g.set_y(view.getSize().y - Y_PADDING - g.size().y);
    }

    sync_viertel();
    Rahmen::set(stadt, viertel);
}

Gui_Viertel::~Gui_Viertel() {
    fenster->setView(fenster->getDefaultView());
}

void Gui_Viertel::start() {
    Log::debug() << "Gui_Viertel::start(" << viertel.get_id() << ')' << Log::endl;

    // Timing
    net_clock.restart();
    for (unsigned net_counter = 0; loop;) {

        // FPS Syncs / Alle X Frames
        if (net_clock.getElapsedTime() >= sf::seconds(0.1f)) {
            net_counter++;
            if (net_counter % 5 == 0) {
                sync_dynastie(); // z.B. Geld
                sync_zeit();
                if (net_counter % 10 == 0) sync_viertel(); // wegen Zivilisten kommen/gehen
                else sync_posistionen();
                if (net_counter >= 50) { // Alle 5s
                    sync_gebaeude();
                    net_counter = 0;
                }
            }
            net_clock.restart();
        }

        // Sicht auf Char zentrieren
        const Charakter& spieler = charaktere[char_id];
        view.setCenter(spieler.get_pos().x, view.getCenter().y);
        fenster->setView(view);
        ImGui::SFML::Update(*fenster, fps_clock.restart());

        // Parallaxen mitlaufen lassen
        himmel.set_x_offset(spieler.get_pos().x);
        bg_stadt.set_x_offset(spieler.get_pos().x / 1.1f);
        bg_viertel.set_x_offset(spieler.get_pos().x / 1.25f);

        // UI
        const float maus_x_fenster = sf::Mouse::getPosition(*fenster).x;
        const float maus_x_view = maus_x_fenster * (view.getSize().x / fenster->getSize().x);
        const float maus_x = maus_x_view + view.getCenter().x - view.getSize().x/2;
        const float maus_y = sf::Mouse::getPosition(*fenster).y;

        // Tooltips: Gebäude
        for (const auto& grafik : gebaeudegrafiken) {
            if (grafik.second.is_inside(maus_x, maus_y)) {
                static Dynastie d(0);
                const Gebaeude& g = gebaeude.at(grafik.first);
                if (g.hat_besitzer() && g.get_besitzer() != d.get_id()) d = Klient::request<Dynastie>(Netzwerk::Anfrage::DYNASTIE, g.get_besitzer(), d);
                ImGui::SetNextWindowPos({maus_x_fenster, maus_y});
                ImGui::BeginTooltip();
                ImGui::TextUnformatted(g.get_typ_name().c_str());
                if (g.hat_besitzer()) {
                    ImGui::TextUnformatted("Besitzer: "); ImGui::SameLine();
                    if (g.get_besitzer() == spieler.get_dynastie()) imgui_zusatz::Farbtext("Dieses Gebäude gehört Ihnen.", imgui_zusatz::GRUEN);
                    else imgui_zusatz::Farbtext(d.get_name().c_str(), imgui_zusatz::GOLD);
                }
                ImGui::EndTooltip();
            }
        }

        // Debug-Fenster
        #ifndef NDEBUG
            ImGui::Begin("Debug Viertel");
            ImGui::Text("%s", viertel.get_name().c_str());
            ImGui::Text("Maus x,y = %f, %f", maus_x, maus_y);
            ImGui::Value("Viertel ID", viertel.get_id());
            ImGui::Value("Charakter x", charaktere[char_id].get_pos().x);
            ImGui::Value("Charakter gfx x", charaktergrafiken[char_id].pos().x);
            ImGui::End();
        #endif

        // Rendern
        fenster->clear();
        draw();
        ImGui::SFML::Render(*fenster);
        fenster->display();

        // Tastatur: Bewegung
        using Keys = sf::Keyboard;
        if (Keys::isKeyPressed(Keys::A) || Keys::isKeyPressed(Keys::Left)) {
            bewege(Position::Ausrichtung::LINKS);
        }
        else if (Keys::isKeyPressed(Keys::D) || Keys::isKeyPressed(Keys::Right)) {
            bewege(Position::Ausrichtung::RECHTS);
        }

        // Events
        sf::Event event;
        while (fenster->pollEvent(event)) {
            ImGui::SFML::ProcessEvent(event);
            if (event.type == sf::Event::Closed) fenster->close(); //macht Fenster schließen mögich

            // Tastatur: Einmalig gedrückt
            if (event.type == sf::Event::KeyPressed)  {
                switch (event.key.code) {

                    // Beispiel
                    case Keys::Escape: loop = false;            break;
                    case Keys::W: case Keys::Up: betreten();    break;
                    default: break;
                }
            }
        }
    }
}

void Gui_Viertel::draw() {
    // Parallax Hintergrund
    himmel.draw(fenster, zeit);
    bg_stadt.draw(fenster, zeit);
    bg_viertel.draw(fenster, zeit);
    strasse.draw(fenster);

    // Gebäude BG
    for (const auto& g_paar : gebaeude) gebaeudegrafiken[g_paar.first].render_bg();
    for (const auto& g : grenzen) g.draw(fenster);
    wegweiser_gfx.draw(fenster);

    // Dekoration TODO

    // Entitäten rendern
    for (const auto& z : zivilisten) if (z.second.get_pos().sichtbar) zivilistengrafiken[z.first].render();
    for (const auto& c : charaktere) if (c.second.get_pos().sichtbar) charaktergrafiken[c.first].render();
    for (const auto& g_paar : gebaeude) gebaeudegrafiken[g_paar.first].render_vg(); // TODO überflüssig?

    // Rahmen
    Rahmen::draw();
}

void Gui_Viertel::sync() {
    sync_zeit();
    sync_viertel(); // auch Personen/Positionen
    sync_gebaeude();
}

void Gui_Viertel::sync_zeit() {
    using namespace Netzwerk;
    zeit = Klient::request<Zeit>(Anfrage::ZEIT, 0, zeit);
    Rahmen::set(zeit);
}

void Gui_Viertel::sync_viertel() {
    using Netzwerk::Anfrage;
    if (charaktere.count(char_id) != 0) {
        Klient::update<Position>(Anfrage::POSITION, charaktere[char_id].get_pos(), char_id);
    }
    viertel = Klient::request<Viertel>(Anfrage::VIERTEL, viertel.get_id(), viertel);

    // Update Charaktere + Zivilisten
    charaktere.clear();
    zivilisten.clear();
    for (auto person_id : viertel.get_charaktere()) {
        charaktere[person_id] = Klient::request(Anfrage::CHARAKTER, person_id, charaktere[person_id]);
    }
    for (const auto& z : viertel.get_zivilisten()) zivilisten[z.get_id()] = z;

    // Neue Personen als Grafik hinzufügen, weggegangene von den Grafiken löschen
    auto refresh_gfx = [&](auto& menschen_map, std::unordered_map<Netzwerk::big_id_t, Charaktergrafik>& grafiken) {
        for (const auto& m : menschen_map) {
            if (grafiken.count(m.first) == 0) {
                grafiken[m.first] = Charaktergrafik(m.second);
                if (charaktergrafiken.count(char_id) != 0 && (&grafiken[m.first] == &charaktergrafiken[char_id])) {
                    grafiken[m.first].set_interpolieren(false); // Spielergrafik nicht interpolieren
                }
            }
            grafiken[m.first].set_x(m.second.get_pos().x);
        }
        for (auto iter = grafiken.begin(); iter != grafiken.end(); ) {
            if (menschen_map.count(iter->first) == 0) grafiken.erase(iter++);
            else ++iter;
        }
    };
    refresh_gfx(charaktere, charaktergrafiken);
    refresh_gfx(zivilisten, zivilistengrafiken);
    grenzen[0].set_x(static_cast<float>(viertel.get_grenze(Position::LINKS))  * (X_PLATZ+X_PADDING));
    grenzen[1].set_x(static_cast<float>(viertel.get_grenze(Position::RECHTS)) * (X_PLATZ+X_PADDING));
}

void Gui_Viertel::sync_gebaeude() {
    gebaeude.clear();
    for (const auto& gebaeude_id : viertel.get_gebaeude_map()) {
        const auto& id = gebaeude_id.second;
        gebaeude[id] = Klient::request<Gebaeude>(Netzwerk::Anfrage::GEBAEUDE, gebaeude_id.second, Gebaeude());

        // Nicht vorhandene Grafiken laden
        // Werkstätten rufen einen anderen Konstruktor auf, der je nach Werkstatt eine andere Grafik initialisiert.
        const Gebaeude& g = gebaeude[id];
        if (g.get_typ() == Gebaeude::Typ::WERKSTATT) {
            Werkstatt werk = Klient::request<Werkstatt>(Netzwerk::Anfrage::WERKSTATT, g.get_id_link(), werk);
            const auto& temp_grafik = Gebaeudegrafik(g, kultur.get_key(), werk);
            if (gebaeudegrafiken.count(id) == 0 || gebaeudegrafiken[id] != temp_grafik) gebaeudegrafiken[id] = temp_grafik;
        }
        else if (gebaeudegrafiken.count(id) == 0) gebaeudegrafiken[id] = Gebaeudegrafik(g, kultur.get_key());
    }
}

void Gui_Viertel::sync_posistionen() {
    auto pos_sync = [&](auto& chars, auto& grafiken, const auto& positionen) {
        for (auto& paar : chars) {
            if (positionen.count(paar.first) == 0) continue; // PC/NPC nicht mehr da
            const Position& p = positionen.at(paar.first);
            if (&grafiken[paar.first] == &charaktergrafiken[char_id]) continue; // eigenen Char nicht syncen
            // Check: Viertel verlassen?
            if (p.viertel != viertel.get_id()) { grafiken[paar.first].pos().sichtbar = false; continue; }
            paar.second.set_pos().x = p.x;
        }
    };
    using Positionen = std::unordered_map<Netzwerk::big_id_t, Position>;
    const Positionen& pos_pcs  = Klient::request(Netzwerk::Anfrage::POSITIONEN_PC,  viertel.get_id());
    const Positionen& pos_npcs = Klient::request(Netzwerk::Anfrage::POSITIONEN_NPC, viertel.get_id());
    pos_sync(charaktere, charaktergrafiken, pos_pcs);
    pos_sync(zivilisten, zivilistengrafiken, pos_npcs);
}

void Gui_Viertel::sync_dynastie() {
    dynastie = Klient::request(Netzwerk::Anfrage::DYNASTIE, charaktere[char_id].get_dynastie().value(), dynastie);
    Rahmen::set(dynastie);
}

void Gui_Viertel::bewege(Position::Ausrichtung richtung) {
    Charakter& c = charaktere[char_id];
    const float x_neu = c.get_pos().x + ((richtung == Position::RECHTS) ? c.get_speed() : -c.get_speed());
    c.set_pos().x = x_neu;
    c.set_pos().ausrichtung = richtung;
    charaktergrafiken[char_id].set_x(x_neu);

    // Viertel verlassen über Stadttore?
    if (c.get_pos().x < grenzen[0].x()) { c.set_pos().x += 100; reisen(); }
    else if (c.get_pos().x > grenzen[1].x() + grenzen[1].size().x) { c.set_pos().x -= 100; reisen(); }
}

void Gui_Viertel::betreten() {
    const float char_x = charaktere[char_id].get_pos().x;

    // Wegweiser?
    if (char_x > 0 && char_x < 200) {
        Log::debug() << "Gui_Viertel::" << __func__ << ' ' << " Wegweiser" << Log::endl;
        reisen();
    }

    // Gebäude?
    for (const auto& g_paar : gebaeudegrafiken) {
        if (gebaeude.count(g_paar.first) == 0) continue;
        if (std::abs(g_paar.second.get_pos().x + g_paar.second.get_size().x / 2.f - char_x) < 100.f) {

            Gebaeude& g_temp = gebaeude[g_paar.first];
            Log::debug() << "Gui_Viertel::" << __func__ << ' ' << g_temp.get_typ_name() << " betreten" << Log::endl;
            const bool is_besitzer = g_temp.get_besitzer() == charaktere[char_id].get_dynastie();

            switch (g_temp.get_typ()) {
                case Gebaeude::Typ::WOHNHAUS:
                    if (is_besitzer) {
                        Log::debug() << "\tEigenes Wohnhaus betreten..." << Log::endl;
                        Gui_Heim gui_heim(g_temp);
                        gui_heim.start();
                    }
                    else Log::debug() << "\tFremdes Wohnhaus..." << Log::endl;
                    return;
                case Gebaeude::Typ::WERKSTATT:
                    if (is_besitzer) {
                        Log::debug() << "\tEigene Werkstatt betreten..." << Log::endl;
                        Gui_Werkstatt gui_werk(g_temp.get_id_link(), stadt.get_id());
                        gui_werk.start();
                    }
                    else Log::debug() << "\tFremde Werkstatt..." << Log::endl;
                    return;
                case Gebaeude::Typ::LAGER:
                    if (is_besitzer) {
                        Log::debug() << "\tEigenes Lager betreten..." << Log::endl;
                        Gui_Lager gui_lager(stadt.get_id(), g_temp.get_id_link(), *fenster);
                        gui_lager.start();
                    }
                    else Log::debug() << "\tFremdes Lager..." << Log::endl;
                    return;
                case Gebaeude::Typ::MARKT: {
                    Log::debug() << "\tMarkt "<< g_temp.get_id_link() << " betreten..." << Log::endl;
                    Dynastie d = Klient::request(Netzwerk::Anfrage::DYNASTIE, Klient::id(), d);
                    int lager_id = -1;
                    if (d.hat_lager(stadt.get_id())) lager_id = d.get_lager(stadt.get_id());
                    Gui_Markt gui_markt(g_temp.get_id_link(), viertel.get_id_stadt(), lager_id, *fenster);
                    gui_markt.start();
                } return;
                case Gebaeude::Typ::RATHAUS: {
                    Log::debug() << "\tRathaus " << stadt.get_id() << " betreten..." << Log::endl;
                    Gui_Rathaus gui_rathaus(stadt.get_id());
                    gui_rathaus.start();
                } return;
                default: Log::debug() << "\tTyp " << (Netzwerk::enum_t) g_temp.get_typ() << " nicht implementiert" << Log::endl; return;
            }
        }
    }
}

void Gui_Viertel::reisen() {
    Selektor_Viertel vs(viertel.get_id(), charaktere[char_id].get_dynastie().value());
    const auto vs_auswahl = vs.start();
    Log::debug() << "Auswahl: " << vs_auswahl << Log::endl;
    if (vs_auswahl >= 0) {
        // Reise antreten
        charaktere[char_id].set_pos().viertel = vs_auswahl;
        charaktere[char_id].set_pos().x = 0;
        Klient::update<Position>(Netzwerk::Anfrage::POSITION, charaktere[char_id].get_pos(), char_id);
        *this = Gui_Viertel((Netzwerk::id_t) vs_auswahl, char_id); // Gui_Viertel reinitialisieren
        Stadt temp_stadt = Klient::request(Netzwerk::Anfrage::STADT, viertel.get_id_stadt(), temp_stadt);
        Rahmen::set(temp_stadt, viertel);
        return;
    }
}
