#pragma once

#include "selektor_generic.hpp"
#include "../../gfx/grafik.hpp"
#include "../../welt/waren/lager.hpp"

using Selektor_Ware = Selektor_Generic<std::string>;

/**
 * Liefert einen Selektor_Generic, der schon mit Waren und deren Icons befüllt ist.
 * Eine Blacklist kann gegeben werden, falls Waren ausgeblendet werden sollen.
 *
 * @param blacklist Im Selektor nicht anzuzeigende Waren (warenkeys).
 * @return Einen Selektor_Generic, der zur Auswahl von Waren geeignet ist.
 */
static Selektor_Ware get_selektor_ware(const std::vector<std::string>& blacklist = {}) {
    std::vector<std::tuple<std::string, std::string, const Grafik*>> temp_liste;
    temp_liste.reserve(Lager::alle_waren_keys().size());
    for (const auto& warenkey : Lager::alle_waren_keys_sortiert()) {
        if (std::find(blacklist.begin(), blacklist.end(), warenkey) != blacklist.end()) continue;
        temp_liste.emplace_back(TXT::get(warenkey), warenkey, &Ware::get(warenkey).get_icon());
    }
    Selektor_Ware selektor(temp_liste);
    return selektor;
}
