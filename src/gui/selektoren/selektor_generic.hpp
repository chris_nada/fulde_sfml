#pragma once

#include "../basisfenster.hpp"
#include "../gui.hpp"
#include "../../lingua/lingua.hpp"

/**
 * @brief Generisches Auswahlfenster.
 * Liefert aus gegebener Liste das ausgewählte `std::tuple::second`.
 * Falls eine `Grafik*` gegeben ist, wird diese neben dem Element angezeigt.
 * Falls kein Icon gewünscht / vorhanden ist, `nullptr` übergeben.
 * @note Mit `is_abgebrochen()` wird geprüft, ob die Auswahl abgebrochen wurde.
 * @tparam T Rückgabetyp.
 */
template <typename T>
class Selektor_Generic final : public Basisfenster {

    /// Interner Datentyp der Auswahloptionen. Im Paar befinden sich Keys und Werte der auswählbaren Objekte.
    using liste_t = std::vector<std::tuple<std::string, T, const Grafik*>>;

public:

    /** 
     * Ctor. Als Liste wird ein Array von Paaren gegeben.
     * Der String dient als Key (zur Auswahl), und T ist der Datentyp der auszuwählenden Objekte.
     * Falls eine `Grafik*` gegeben ist, wird diese neben dem Element angezeigt.
     * Falls kein Icon gewünscht / vorhanden ist, `nullptr` übergeben.
     */
    Selektor_Generic(liste_t liste) : liste(liste), abgebrochen(true) {}

    /// Liefert das ausgewählte Objekt.
    T get_auswahl() const { return std::get<1>(auswahl); }

    /// Wurde die Auswahl abgebrochen?
    bool is_abgebrochen() const { return abgebrochen; }

private:

    liste_t liste;

    typename liste_t::value_type auswahl;

    bool abgebrochen;

    void loop_body() override {
        // Events
        sf::Event event;
        while (fenster.pollEvent(event)) {
            ImGui::SFML::ProcessEvent(event);
            if (event.type == sf::Event::Closed) fenster.close(); //macht Fenster schließen mögich
            if (event.type == sf::Event::KeyPressed) {
                switch (event.key.code) {
                    case sf::Keyboard::Escape: loop = false; break;
                    default: break;
                }
            }
        }

        ImGui::SetNextWindowPos({fenster.getSize().x*0.3f, fenster.getSize().y * 0.1f});
        ImGui::SetNextWindowSize({fenster.getSize().x*0.4f, fenster.getSize().y *0.8f});
        ImGui::Begin("Auswahl", nullptr,
                     ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

        // Liste schreiben
        for (const auto& tuple : liste) {
            if (const Grafik* g = std::get<2>(tuple)) {
                ImGui::Image(*g->data(), g->size_f());
                ImGui::SameLine();
            }
            if (ImGui::Button(std::get<0>(tuple).c_str())) {
                auswahl = tuple;
                abgebrochen = false;
                loop = false;
            }
        }

        ImGui::NewLine();
        if (ImGui::Button(TXT::get("abbrechen").c_str())) {
            abgebrochen = true;
            loop = false;
        }

        ImGui::End();
        fenster.clear();
    }

};
