#include "selektor_arbeiter.hpp"
#include "../../netzwerk/klient.hpp"
#include "../../gfx/rahmen.hpp"
#include "../../lingua/lingua.hpp"
#include "../zusatz/imgui_zusatz.h"

Selektor_Arbeiter::Selektor_Arbeiter(Netzwerk::id_t id_stadt) :
    stadt(id_stadt, 0, 0, "none"),
    abgebrochen(true) // bis etwas ausgewählt wird
{
    stadt = Klient::request(Netzwerk::Anfrage::STADT, id_stadt, stadt);
    sync_arbeiter();
}

void Selektor_Arbeiter::loop_body() {

    // Events
    sf::Event event;
    while (fenster.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(event);
        if (event.type == sf::Event::Closed) fenster.close(); //macht Fenster schließen mögich
        if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Escape: loop = false; break;
                default: break;
            }
        }
    }

    // Abbrechen
    ImGui::SetNextWindowPos({20, fenster.getSize().y / 2.f});
    //ImGui::SetNextWindowSize({80, 40});
    ImGui::Begin("Arbeiter##ArbeiterSelektorAbbrechen", nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove |
                 ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize);
    if (ImGui::Button(TXT::get("auswaehlen").c_str())) {
        abgebrochen = false;
        loop = false;
    }
    if (ImGui::Button(TXT::get("abbrechen").c_str())) {
        abgebrochen = true;
        loop = false;
    }
    ImGui::End();

    // Hauptfenster
    ImGui::SetNextWindowPos({fenster.getSize().x * 0.15f, Rahmen::ABSTAND_TOP * 2.f});
    ImGui::SetNextWindowSize({INFO_BREITE * 2.1f, fenster.getSize().y - 2.f * Rahmen::ABSTAND_TOP});
    ImGui::Begin("Arbeiter##ArbeiterSelektor", nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

    // Auflisten
    ImGui::Columns(2, nullptr, false);
    for (const auto& arbeiter : arbeiter_chars) {
        bool enthalten = auswahl.count(arbeiter.get_id()); // In der Auswahl enthalten?
        if (enthalten) { // Löschen?
            arbeiter_info(arbeiter, &enthalten);
            if (!enthalten) auswahl.erase(arbeiter.get_id());
        }
        else if (!enthalten) { // Hinzufügen?
            arbeiter_info(arbeiter, &enthalten);
            if (enthalten) auswahl.insert(arbeiter.get_id());
        }
        ImGui::NextColumn();
    }
    ImGui::End();
    fenster.clear();
}

void Selektor_Arbeiter::sync() {
    if (net_clock.getElapsedTime() >= sf::milliseconds(3000)) {
        stadt = Klient::request(Netzwerk::Anfrage::STADT, stadt.get_id(), stadt);
        sync_arbeiter();
        net_clock.restart();
        // Aus der Stadt verschwundene Arbeiter aus der Auswahl entfernen
        for (auto it = auswahl.begin(); it != auswahl.end(); ) {
            if (stadt.get_arbeiter().count(*it) == 0) it = auswahl.erase(it);
            else it++;
        }
    }
}

void Selektor_Arbeiter::sync_arbeiter() {
    const std::unordered_set<Netzwerk::big_id_t> arbeiter_ids(stadt.get_arbeiter().begin(), stadt.get_arbeiter().end());
    arbeiter_chars = Klient::request_alle<Charakter>(Netzwerk::Anfrage::CHARAKTER, arbeiter_ids);
}

Selektor_Arbeiter::Auswahl Selektor_Arbeiter::arbeiter_info(const Charakter& arbeiter, bool* ausgewaehlt, bool entlassen) {
    const std::string child_id(std::to_string(arbeiter.get_id()) + "_a_child_win");
    ImGui::BeginChild(child_id.c_str(), {INFO_BREITE,250}, true);
    ImGui::Columns(2, nullptr, false);
    //ImGui::SetColumnWidth(-1, )
    auto end = []() { // Muss vor return ausgeführt werden
        ImGui::Columns();
        ImGui::EndChild();
    };

    // Name, Portrait
    ImGui::Image(*arbeiter.get_grafik().data());
    std::string name(arbeiter.get_voller_name());
    name.append(", ").append(std::to_string(arbeiter.get_alter(Rahmen::get_zeit())));
    name.append("##name" + std::to_string(arbeiter.get_id()));

    // Ausgewählt?
    if (ausgewaehlt != nullptr) ImGui::Checkbox(name.c_str(), ausgewaehlt);
    else if (ImGui::Button(name.c_str())) {
        end();
        return Auswahl::NAME;
    }

    // Geschlecht
    static Grafik icon_m = Grafik("data/gfx/icons/s_male.png");
    static Grafik icon_f = Grafik("data/gfx/icons/s_female.png");
    ImGui::SameLine();
    if (arbeiter.is_maennlich()) ImGui::Image(*icon_m.data());
    else ImGui::Image(*icon_f.data());

    // Gehalt
    ImGui::TextColored(sf::Color{Gui::FARBE1},"%s: %d", TXT::get("tageslohn").c_str(), arbeiter.get_arbeiter().value().get_tageslohn());
    ImGui::SameLine();
    if (std::string btn_id(TXT::get("entlassen") + "##entlassen" + std::to_string(arbeiter.get_id()));
        entlassen && ImGui::Button(btn_id.c_str()))
    {
        end();
        return Auswahl::ENTLASSEN;
    }
    ImGui::NextColumn();

    // Skills
    if (!arbeiter.get_arbeiter()->get_skills().empty()) ImGui::Text("%s:", TXT::get("fertigkeiten").c_str());
    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[4]);
    for (const auto& paar : arbeiter.get_arbeiter()->get_skills()) {
        ImGui::Image(*Arbeiter::get_skill_icon(paar.first).data());
        imgui_zusatz::Tooltip(TXT::get(paar.first).c_str());
        ImGui::SameLine(); // INFO_BREITE * 0.25f
        ImGui::ProgressBar(paar.second / 100.f, {-1, 32});
    }
    ImGui::PopFont();
    end();
    return Auswahl::NICHTS;
}
