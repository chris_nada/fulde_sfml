#pragma once

#include "../basisfenster.hpp"
#include "../../welt/land.hpp"
#include "../../welt/stadt/stadt.hpp"

/// UI zur Auswahl einer Stadt.
class Selektor_Stadt final : public Basisfenster {

public:

    /// Ctor.
    Selektor_Stadt();

    /// Nach Schließen des Fensters liefert diese Methode die gewählte Stadt via ID. -1 wenn nichts ausgewählt.
    int get_auswahl() const { return auswahl; }

    /// Entfernt eine Stadt aus der Auswahl.
    void erase(Netzwerk::id_t id_stadt);

private:

    /// Rendering-Loop.
    void loop_body() override;

    /// Zwischenspeicher für Länder.
    std::vector<Land> laender;

    /// Zwischenspeicher für Städte.
    std::vector<Stadt> staedte;

    /// Ausgewählte Stadt.
    int auswahl;

};
