#pragma once

#include "../basisfenster.hpp"
#include "../../welt/waren/prozess.hpp"

class Selektor_Prozess final : public Basisfenster {

public:

    /// Schreibt ImGui-Elemente mit Informationen zu gegebenem Prozess.
    static void prozessinfo(const Prozess& prozess);

    /// Nur Prozesse aus angegebener auswählbar; leer lassen für alle.
    explicit Selektor_Prozess(const std::string& prozess_gruppe = "");

    /// Liefert den Key des ausgewählten Prozesses. "", wenn abgebrochen.
    const std::string& get_prozess_key() const { return prozess_key; }

private:

    /// Rendering-Schleife.
    void loop_body() override;

    /// Alle Prozesse.
    std::vector<std::pair<std::string, Prozess>> prozesse;

    /// Alle Prozessgruppen als key-Liste
    std::vector<std::string> prozessgruppen;

    /// Key: Produkt, Values: Liste von Prozessen.
    std::unordered_map<std::string, std::vector<Prozess>> prozesse_nach_produkt;

    /// Ausgewählter key vom Prozess.
    std::string prozess_key;

    /// Nur Prozesse dieser Gruppe auswähltbar. Leer = alle.
    const std::string prozess_gruppe;

};
