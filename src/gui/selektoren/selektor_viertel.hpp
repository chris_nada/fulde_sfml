#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include "../../welt/land.hpp"
#include "../../netzwerk/klient.hpp"

/**
 * Auswahldialog für ein Viertel.
 */
class Selektor_Viertel final {

public:

    /// Ctor.
    explicit Selektor_Viertel(int auswahl = -1, int dynastie = -1);

    /**
     * Liefert die Ausgewählte Viertel-ID.
     * -1 bedeutet Abbruch.
     */
    int start();

private:

    // Zwischenspeicher für weitere Infos eines Viertels. Für Tooltips etc.
    struct Vierteldaten {
        bool gebaeude; // Hat Spieler hier Gebäude?
    };

    /// Lädt die Daten aus dem Netzwerk neu.
    void sync();

    /// Rendert das Fenster.
    void draw_viertelauswahl();

    // Netzwerkdaten
    std::vector<Land> laender;
    std::vector<Stadt> staedte;
    std::vector<Viertel> viertel;
    std::unordered_map<Netzwerk::id_t, std::vector<const Viertel*>> staedte_viertel;
    std::unordered_map<Netzwerk::id_t, Vierteldaten> viertel_daten;
    std::unordered_map<Netzwerk::id_t, bool> stadt_eigentum;
    std::optional<Dynastie> dynastie_instanz;

    // Temp Daten
    int auswahl; // ausgewähltes Viertel
    int dynastie; // Dynastie des Spielers
    bool loop; // true, solange das Fenster angezeigt werden soll

    // UI
    sf::RenderWindow* fenster;
    sf::Clock clock;

    void draw_infos();

};
