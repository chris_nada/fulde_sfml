#include "selektor_prozess.hpp"
#include "../../welt/waren/prozess.hpp"
#include "../../gfx/rahmen.hpp"
#include "../../lingua/lingua.hpp"

Selektor_Prozess::Selektor_Prozess(const std::string& prozess_gruppe) :
    Basisfenster(),
    prozesse(Prozess::alle().begin(), Prozess::alle().end()),
    prozess_key(),
    prozess_gruppe(prozess_gruppe)
{
    // Prozesse vorab sortieren
    std::sort(prozesse.begin(), prozesse.end());

    // Prozesse nach Output in Liste schreiben
    for (const auto& paar : prozesse) {
        if (!prozess_gruppe.empty() && paar.second.get_gruppe() != prozess_gruppe) continue; // falsche Gruppe
        for (const auto& output : paar.second.get_outputs()) {

            // Ware noch nicht in Liste, Vektor initialisieren
            if (prozesse_nach_produkt.count(output.first) == 0) {
                prozesse_nach_produkt.emplace(output.first, std::vector<Prozess>());
            }
            prozesse_nach_produkt.at(output.first).push_back(paar.second);
        }
    }
    // Alle Prozessgruppen als key-Liste
    for (const auto& gruppe : Prozess::alle_gruppen()) prozessgruppen.push_back(gruppe);
}

void Selektor_Prozess::loop_body() {

    // Events
    sf::Event event;
    while (fenster.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(event);
        if (event.type == sf::Event::Closed) fenster.close(); //macht Fenster schließen mögich
        if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Escape: loop = false; break;
                default: break;
            }
        }
    }

    ImGui::SetNextWindowPos({fenster.getSize().x / 4.f, Rahmen::ABSTAND_TOP + 8.f});
    ImGui::SetNextWindowSize({fenster.getSize().x / 2.f, fenster.getSize().y - Rahmen::ABSTAND_TOP});
    ImGui::Begin("Prozess##ProzessSelektor", nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

    // Tree nach Gruppen
    for (const auto& gruppe : prozessgruppen) {
        if (ImGui::TreeNode(TXT::get(gruppe).c_str())) {

            // Prozesse auflisten nach Output (Ware)
            for (const auto& prozessliste : prozesse_nach_produkt) {
                if (std::none_of(std::begin(prozessliste.second), std::end(prozessliste.second),
                        [&] (const Prozess& p) { return p.get_gruppe() == gruppe; })) {
                    continue;
                }

                // Tree nach Ware
                if (ImGui::TreeNode(TXT::get(prozessliste.first).c_str())) {
                    // Prozessauswahl
                    for (const Prozess& prozess : prozessliste.second) {
                        // Prozess wurde ausgewählt
                        ImGui::NewLine();
                        if (ImGui::Button(TXT::get(prozess.get_key()).c_str())) {
                            prozess_key = prozess.get_key();
                            loop = false;
                        }
                        prozessinfo(prozess);
                    }
                    ImGui::TreePop();
                }
            }
            ImGui::TreePop();
        }
    }

    // Ende + Abbruch
    if (ImGui::Button(TXT::get("zurueck").c_str())) loop = false;
    ImGui::End();
    fenster.clear();
}

void Selektor_Prozess::prozessinfo(const Prozess& prozess) {
    ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[3]);
    ImGui::BulletText("%d %s", prozess.get_kosten(), TXT::get("aufbaukosten").c_str());
    ImGui::BulletText("%d %s", prozess.get_techlevel(), TXT::get("technologiestufe").c_str());
    ImGui::BulletText("%d %s", prozess.get_arbeitsaufwand(), TXT::get("arbeitsaufwand").c_str());
    ImGui::BulletText("%d %s", prozess.get_arbeiter(), TXT::get("arbeiter").c_str());
    ImGui::BulletText("%d %s", prozess.get_platzverbrauch(), TXT::get("platzverbrauch").c_str());
    ImGui::BulletText("%.1f %s", prozess.get_basisquali(), TXT::get("basisqualitaet").c_str());
    ImGui::PopFont();
    for (const auto& input : prozess.get_inputs()) {
        ImGui::BulletText("%s: %d %s", TXT::get("verbraucht3sg").c_str(), input.second, TXT::get(input.first).c_str());
        ImGui::SameLine();
        ImGui::Image(*Ware::get(input.first).get_icon().data(), Ware::get(input.first).get_icon().size_f());
    }
    for (const auto& output : prozess.get_outputs()) {
        ImGui::BulletText("%s: %d %s", TXT::get("produziert3sg").c_str(), output.second, TXT::get(output.first).c_str());
        ImGui::SameLine();
        ImGui::Image(*Ware::get(output.first).get_icon().data(), Ware::get(output.first).get_icon().size_f());
    }
}
