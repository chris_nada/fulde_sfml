#include "selektor_stadt.hpp"
#include "../gui.hpp"
#include "../../netzwerk/klient.hpp"
#include "../../lingua/lingua.hpp"
#include "../zusatz/imgui_zusatz.h"

#include <nada/log.hpp>

using nada::Log;

Selektor_Stadt::Selektor_Stadt() : auswahl(-1) {
    Log::debug() << "Selektor_Stadt()" << Log::endl;
    laender = Klient::request_alle<Land>(Netzwerk::Anfrage::LAND);
    staedte = Klient::request_alle<Stadt>(Netzwerk::Anfrage::STADT);
}

void Selektor_Stadt::loop_body() {

    // Events
    sf::Event event;
    while (fenster.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(event);
        if (event.type == sf::Event::Closed) fenster.close(); //macht Fenster schließen mögich
        if (event.type == sf::Event::KeyPressed) {
            switch (event.key.code) {
                case sf::Keyboard::Escape: loop = false; break;
                default: break;
            }
        }
    }

    ImGui::SetNextWindowPos({fenster.getSize().x*0.1f, fenster.getSize().y * 0.1f});
    ImGui::SetNextWindowSize({fenster.getSize().x*0.4f, fenster.getSize().y *0.8f});
    ImGui::Begin(TXT::get("reisen").c_str(), nullptr,
            ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

    // Baumstruktur aufbauen
    for (const auto& land : laender) {
        ImGui::TextColored(sf::Color(Gui::FARBE1), "%s", land.get_name().c_str());
        for (const auto& stadt : staedte) {
            if (std::find(land.get_staedte().begin(), land.get_staedte().end(),
                stadt.get_id()) != land.get_staedte().end())
            {
                // Stadtauswahl
                ImGui::Dummy({});
                ImGui::SameLine(28);
                if (ImGui::Button(stadt.get_name().c_str())) {
                    auswahl = stadt.get_id();
                    loop = false;
                }
                if (dynastie.hat_lager(stadt.get_id())) {
                    ImGui::SameLine();
                    static Grafik icon_haus("data/gfx/icons/haus.png");
                    ImGui::Image(*icon_haus.data());
                    imgui_zusatz::Tooltip(TXT::get("eigenes_lager_hier").c_str());
                }
            }
        }
        ImGui::NewLine();
    }
    if (ImGui::Button(TXT::get("abbrechen").c_str())) loop = false;
    ImGui::End();
    fenster.clear();
}

void Selektor_Stadt::erase(Netzwerk::id_t id_stadt) {
    staedte.erase(std::remove_if(staedte.begin(), staedte.end(), [=](const Stadt& s) {
        return s.get_id() == id_stadt;
    }));
}
