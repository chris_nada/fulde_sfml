#include "selektor_viertel.hpp"
#include "../gui.hpp"
#include "../../welt/stadt/stadt.hpp"
#include "../../netzwerk/klient.hpp"
#include "../../welt/land.hpp"
#include "../../welt/bauten/gebaeude.hpp"
#include "../../lingua/lingua.hpp"
#include "../zusatz/imgui_zusatz.h"
#include <imgui.h>
#include <imgui-SFML.h>

#include <nada/log.hpp>

using nada::Log;

Selektor_Viertel::Selektor_Viertel(int auswahl, int dynastie) :
    auswahl(auswahl), dynastie(dynastie), loop(true), fenster(Gui::fenster)
{
    Log::debug() << "Selektor_Viertel()" << Log::endl;
    sync();
}

int Selektor_Viertel::start() {
    Log::debug() << "Selektor_Viertel::" << __func__ << Log::endl;
    while (loop) {

        // Events
        sf::Event event;
        while (fenster->pollEvent(event)) {
            ImGui::SFML::ProcessEvent(event);
            if (event.type == sf::Event::Closed) fenster->close(); //macht Fenster schließen mögich

            // Tastatur: Einmalig gedrückt
            if (event.type == sf::Event::KeyPressed)  {
                switch (event.key.code) {
                    case sf::Keyboard::Escape: loop = false; break;
                    default: break;
                }
            }
        }

        ImGui::SFML::Update(*fenster, clock.restart());

        // UI Quelltext verteilt auf:
        draw_viertelauswahl();
        draw_infos();

        // Rendern
        fenster->clear();
        ImGui::SFML::Render(*fenster);
        fenster->display();
    }
    Log::debug() << "\tAuswahl = " << auswahl << Log::endl;
    return auswahl;
}

void Selektor_Viertel::sync() {
    // Daten beschaffen
    laender = Klient::request_alle<Land>(Netzwerk::Anfrage::LAND);
    staedte = Klient::request_alle<Stadt>(Netzwerk::Anfrage::STADT);
    viertel = Klient::request_alle<Viertel>(Netzwerk::Anfrage::VIERTEL);
    if (dynastie >= 0) {
        Dynastie temp = Klient::request<Dynastie>(Netzwerk::Anfrage::DYNASTIE, dynastie, Dynastie(0, ""));
        if (!temp.get_name().empty()) dynastie_instanz = temp;
    }

    // Viertel auf Städte mappen
    staedte_viertel.clear();
    for (const auto& stadt : staedte) {
        staedte_viertel[stadt.get_id()] = std::vector<const Viertel*>();
        for (const auto& v : viertel) {
            if (std::find(stadt.get_viertel().begin(), stadt.get_viertel().end(), v.get_id()) != stadt.get_viertel().end()) {
                staedte_viertel[stadt.get_id()].push_back(&v);
            }
        }
    }

    viertel_daten.clear();
    for (const auto& v : viertel) {
        viertel_daten[v.get_id()] = Vierteldaten { .gebaeude = false };

        // Hat Spieler hier Gebäude?
        if (dynastie < 0) continue;
        const auto& g_ids_temp = v.get_gebaeude();
        for (auto gid : g_ids_temp) {
            Gebaeude g = Klient::request<Gebaeude>(Netzwerk::Anfrage::GEBAEUDE, gid, g);
            if (g.get_besitzer() == Klient::id()) {
                if (g.get_typ() == Gebaeude::Typ::LAGER ||
                    g.get_typ() == Gebaeude::Typ::WERKSTATT ||
                    g.get_typ() == Gebaeude::Typ::WOHNHAUS) {
                    viertel_daten[v.get_id()].gebaeude = true;
                    stadt_eigentum[v.get_id_stadt()] = true;
                    break;
                }
            }
        }
    }
}

void Selektor_Viertel::draw_viertelauswahl() {
    // 0.1 * Fenstergröße = Padding
    ImGui::SetNextWindowPos({ fenster->getSize().x*0.1f, fenster->getSize().y * 0.1f});
    ImGui::SetNextWindowSize({fenster->getSize().x*0.4f, fenster->getSize().y *0.8f});
    ImGui::Begin("Reisen", nullptr,
            ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

    imgui_zusatz::Ueberschrift("viertel_auswaehlen"_txtc);

    // Hilfsfunktionen: Erstellt ein Icon mit Tooltip zur Markierung von ...
    static auto marker = [](const sf::Texture& icon, const std::string& tooltip_text_id) {
        ImGui::SameLine(); ImGui::Image(icon); imgui_zusatz::Tooltip(TXT::get(tooltip_text_id).c_str());
    }; /* Icons */
    static Grafik icon_haus(         "data/gfx/icons/haus.png");
    static Grafik icon_markt(        "data/gfx/icons/markt.png");
    static Grafik icon_bank(         "data/gfx/icons/bank.png");
    static Grafik icon_rathaus(      "data/gfx/icons/rathaus.png");
    static Grafik icon_kirche(       "data/gfx/icons/kirche.png");
    static Grafik icon_schattengilde("data/gfx/icons/schattengilde.png");
    static auto marker_eigentum      = []() { marker(*icon_haus.data(),          "eigentum_vorhanden"); };
    static auto marker_markt         = []() { marker(*icon_markt.data(),         "markt_vorhanden"); };
    static auto marker_bank          = []() { marker(*icon_bank.data(),          "bank_vorhanden"); };
    static auto marker_rathaus       = []() { marker(*icon_rathaus.data(),       "rathaus_vorhanden"); };
    static auto marker_kirche        = []() { marker(*icon_kirche.data(),        "kirche_vorhanden"); };
    static auto marker_schattengilde = []() { marker(*icon_schattengilde.data(), "schattengilde_vorhanden"); };

    // Tree bauen
    for (const auto& land : laender) {
        // Länder
        const bool land_selected = ImGui::TreeNode(land.get_name().c_str());
        if (dynastie_instanz.has_value() && dynastie_instanz->get_id_land() == land.get_id()) marker_eigentum();
        if (land_selected) {
            for (const auto& stadt : staedte) {

                if (std::find(land.get_staedte().begin(), land.get_staedte().end(), stadt.get_id()) != land.get_staedte().end()) {

                    // Stadt
                    const bool stadt_selected = ImGui::TreeNodeEx(stadt.get_name().c_str());
                    if (stadt_eigentum[stadt.get_id()]) marker_eigentum();
                    if (stadt_selected) {

                        // Viertel
                        for (const Viertel* v : staedte_viertel[stadt.get_id()]) {

                            // Spieler hier?
                            const bool heim = (int) v->get_id() == auswahl;
                            if (heim) {
                                ImGui::PushStyleColor(ImGuiCol_Button,        sf::Color(0x40'80'40'FF));
                                ImGui::PushStyleColor(ImGuiCol_ButtonHovered, sf::Color(0x60'A0'60'FF));
                            }
                            // Auswahlknopf
                            if (ImGui::Button(v->get_name().c_str(), {200, 24})) {
                                auswahl = v->get_id();
                                loop = false;
                            }
                            // Ursprungsort?
                            if (heim) {
                                imgui_zusatz::Tooltip(TXT::get("du_bist_hier").c_str());
                                ImGui::PopStyleColor(2);
                            }
                            // Marker
                            if (viertel_daten[v->get_id()].gebaeude) marker_eigentum();
                            if (stadt.get_id_viertel_markt()         == v->get_id()) marker_markt();
                            if (stadt.get_id_viertel_schattengilde() == v->get_id()) marker_schattengilde();
                            if (stadt.get_id_viertel_bank()          == v->get_id()) marker_bank();
                            if (stadt.get_id_viertel_rathaus()       == v->get_id()) marker_rathaus();
                            if (stadt.get_id_viertel_kirche()        == v->get_id()) marker_kirche();
                        }
                        ImGui::TreePop();
                    }
                }
            }
            ImGui::TreePop();
            ImGui::Separator();
        }
    }
    ImGui::NewLine();
    if (ImGui::Button(TXT::get("abbrechen").c_str())) {
        auswahl = -1;
        loop = false;
    }
    ImGui::End();
}

void Selektor_Viertel::draw_infos() {
    static Grafik icon_haus("data/gfx/icons/haus.png");

    // 0.1 * Fenstergröße = Padding
    ImGui::SetNextWindowPos({ fenster->getSize().x * 0.5f, fenster->getSize().y * 0.1f});
    ImGui::SetNextWindowSize({fenster->getSize().x * 0.4f, fenster->getSize().y * 0.8f});
    ImGui::Begin("Liste_Viertel", nullptr,
                 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);
    imgui_zusatz::Ueberschrift("laenderuebersicht"_txtc);
    for (const auto& land : laender) {
        ImGui::TextUnformatted(land.get_name().c_str());

        // Länderinfos
        ImGui::Bullet(); ImGui::SameLine();

        static const float textabstand = 100.f + std::max(
                ImGui::CalcTextSize(TXT::get("grundsteuer").c_str()).x,
                ImGui::CalcTextSize(TXT::get("ertragssteuer").c_str()).x);

        ImGui::Text("%d%% %s", land.get_steuern(Steuertyp::ERTRAG), TXT::get("ertragssteuer").c_str());
        ImGui::SameLine(textabstand);
        ImGui::DrawRectFilled(sf::Rect<float>(0, 4, land.get_steuern(Steuertyp::ERTRAG) * 2, 8), sf::Color(Gui::FARBE1));
        ImGui::Dummy({8,8});

        ImGui::Bullet(); ImGui::SameLine();
        ImGui::Text("%d%% %s", land.get_steuern(Steuertyp::MEHRWERT), TXT::get("mehrwertsteuer").c_str());
        ImGui::SameLine(textabstand);
        ImGui::DrawRectFilled(sf::Rect<float>(0, 4, land.get_steuern(Steuertyp::MEHRWERT) * 2, 8), sf::Color(Gui::FARBE1));
        ImGui::Dummy({8,8});

        ImGui::Bullet(); ImGui::SameLine();
        ImGui::Text("%d%% %s", land.get_steuern(Steuertyp::GRUND), TXT::get("grundsteuer").c_str());
        ImGui::SameLine(textabstand);
        ImGui::DrawRectFilled(sf::Rect<float>(0, 4, land.get_steuern(Steuertyp::GRUND) * 10, 8), sf::Color(Gui::FARBE1));
        ImGui::Dummy({8,8});

        for (const auto& stadt : staedte) {
            if (std::find(land.get_staedte().begin(), land.get_staedte().end(),
                    stadt.get_id()) != land.get_staedte().end())
            {
                // Städteinfos
                ImGui::Bullet();                 ImGui::SameLine();
                ImGui::Image(*icon_haus.data()); ImGui::SameLine();
                ImGui::Text("%s, %d %s, %d %s",
                        stadt.get_name().c_str(), stadt.get_einwohnerzahl(), TXT::get("einwohner").c_str(),
                        (uint32_t) stadt.get_viertel().size(), TXT::get("viertel_pl").c_str()
                );
            }
        }
        ImGui::Separator();
    }
    ImGui::End();
}
