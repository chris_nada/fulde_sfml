#pragma once

#include "../basisfenster.hpp"
#include "../../welt/stadt/stadt.hpp"
#include "../../welt/spieler/charakter.hpp"

class Selektor_Arbeiter final : public Basisfenster {

public:

    /// Feste Breite  eines Arbeiterinfo-ImGui::BeginChild.
    static constexpr float INFO_BREITE = 600;

    /**
     * Enum welche Auswahl getroffen wurde via `arbeiter_info`.
     */
    enum class Auswahl {
        NICHTS, NAME, ENTLASSEN
    };

    /**
     * Erzeugt UI-Elemente mit Infos und einem Button oder Checkbox für einen Arbeiter.
     * @param arbeiter Darzustellender Arbeiter.
     * @param ausgewaehlt Manipuliert eine Flag, ob der Arbeiter ausgewaehlt ist via Checkbox. Kann `nullptr` sein,
     *                    dann wird die Funktion genutzt um einen einzelnen Arbeiter auszusuchen und es wird
     *                    keine Checkbox gezeigt.
     * @param entlassen Entlassen Button zeigen?
     * @return true, falls der Arbeiter ausgewählt wurde via Button.
     */
    static Auswahl arbeiter_info(const Charakter& arbeiter, bool* ausgewaehlt = nullptr, bool entlassen = false);

    /// Ctor. Aus gegebener Stadt wird eine Liste von Arbeitern geladen.
    explicit Selektor_Arbeiter(Netzwerk::id_t id_stadt);

    /// Wurde die Auswahl abgebrochen?
    bool is_abgebrochen() const { return abgebrochen; }

    /// Liefert die IDs der ausgewählten Arbeiter.
    const std::unordered_set<Netzwerk::id_t>& get_auswahl() const { return auswahl; }

private:

    void loop_body() override;

    void sync();

private:

    Stadt stadt;

    bool abgebrochen;

    std::vector<Charakter> arbeiter_chars;

    std::unordered_set<Netzwerk::id_t> auswahl;

    void sync_arbeiter();
};
