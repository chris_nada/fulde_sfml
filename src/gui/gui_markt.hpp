#pragma once

#include "../welt/waren/markt.hpp"
#include "../welt/spieler/dynastie.hpp"
#include "../welt/zeit.hpp"
#include "gui.hpp"
#include "../welt/stadt/stadt.hpp"
#include "../welt/land.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

/**
 * Beinhaltet das Gui-Fenster für das Handeln am Markt.
 */
class Gui_Markt final {

public:

    /// Konstruktor.
    Gui_Markt(Netzwerk::id_t markt_id, Netzwerk::id_t stadt_id, int lager_id, sf::RenderWindow& window);

    /// Öffnet das Fenster
    void start();

private:

    /// Auswahl
    static inline std::string auswahl_ware;

    /// Synchronisiert genutzte Daten vom Server
    void sync();

    /// Zeichnet das die linke Fensterseite.
    bool draw_linke_seite();

    /// Zeichnet die Kauf-GUI-Elemente.
    void draw_kauf();

    /// Zeichnet die Verkauf-GUI-Elemente.
    void draw_verkauf();

    /* Simulationsdaten / Zwischenspeicher */
    int         lager_id; // Lager des Spielers
    Land        land;
    Stadt       stadt;
    Markt       markt;
    Lager       lager; // Lager des Spielers, kann default-ctort sein
    Dynastie    dynastie;
    Zeit        zeit;

    /* Gui_hauptmenu Elemente */
    bool loop = true;
    sf::Clock net_clock;
    sf::Clock fps_clock;
    sf::RenderWindow& window;

};
