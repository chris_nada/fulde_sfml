#pragma once

#include "../welt/zeit.hpp"
#include "../welt/land.hpp"
#include "../welt/kultur.hpp"
#include "../welt/stadt/viertel.hpp"
#include "../welt/bauten/gebaeude.hpp"
#include "../welt/spieler/charakter.hpp"
#include "../gfx/sprites/gebaeudegrafik.hpp"
#include "../gfx/sprites/charaktergrafik.hpp"
#include "../gfx/rahmen.hpp"
#include "../gfx/parallaxe.hpp"
#include "../gfx/parallaxe_wandel.hpp"

/// 2D-Side-Scrolling Nutzeroberfläche für das Viertel.
class Gui_Viertel final {

public:

    /// Abstand zwischen Gebaeuden untereinander.
    static constexpr float X_PADDING = 0;

    /// (Maximale) Größe, die eine Gebäudegrafik einnehmen kann. Wird nicht skaliert.
    static constexpr float X_PLATZ = 400;

    /// Abstand von Gebäuden zum unteren Bildrand.
    static constexpr float Y_PADDING = 120; // TODO resize Probleme

    /**
     * Überschneidung von Straße und Viertelparallaxe zum Sanfteren Übergang.
     * Straße oder Viertel sollten am oberen bzw. unteren Rand 24 Pixel Transparenz haben.
     */
    static constexpr float Y_RAND_STRASSE = 24; // TODO resize Probleme

    /// Ctor.
    explicit Gui_Viertel(Netzwerk::id_t viertel_id, Netzwerk::big_id_t char_id);

    /// Eintrittspunkt zum Anzeigen.
    void start();

    /// Dtor.
    virtual ~Gui_Viertel();

private:

    /// Rendert alles.
    void draw();

    /// Führt einen kompletten Netzwerk-Sync durch.
    void sync();

    /// Synchronisiert die Zeit mit dem Server.
    void sync_zeit();

    /// Synchronisiert das Viertel mit dem Server.
    void sync_viertel();

    /// Synchronisiert die Gebäude mit dem Server.
    void sync_gebaeude();

    /// Synchronisiert die Positionen der im Viertel befindlichen Spieler mit dem Server.
    void sync_posistionen();

    /// Synchronisiert die Dynastiedaten des Spielers.
    void sync_dynastie();

    /// Schickt eine Anfrage und bewegt den SpierlerCharakter.
    void bewege(Position::Ausrichtung richtung);

    /// Spieler hat Taste zum Betreten gedrückt; führt zu anderen Fenstern wie z.B. Markt.
    void betreten();

    /// Öffnet einen Dialog zum Wechseln des Viertels.
    void reisen();

    /* Simulationsdaten */
    Netzwerk::big_id_t char_id; // Charakter-ID des Klienten
    Dynastie dynastie;
    Land land;
    Kultur kultur;
    Stadt stadt;
    Viertel viertel;
    Zeit zeit;

    /* GFX */
    /// 0 linke Grenze, 1 rechte Grenze.
    std::array<Grafik, 2> grenzen;
    /// first entspricht Gebäude-ID
    std::unordered_map<Netzwerk::id_t, Gebaeude> gebaeude;
    /// first entspricht Gebäude-ID bzw. gebaeude.first
    std::unordered_map<Netzwerk::id_t, Gebaeudegrafik> gebaeudegrafiken;
    std::unordered_map<Netzwerk::big_id_t, Zivilist> zivilisten;
    std::unordered_map<Netzwerk::big_id_t, Charakter> charaktere;
    std::unordered_map<Netzwerk::big_id_t, Charaktergrafik> charaktergrafiken;
    std::unordered_map<Netzwerk::big_id_t, Charaktergrafik> zivilistengrafiken;
    Grafik wegweiser_gfx;

    // Hintergrund
    Parallaxe_Wandel himmel;
    Parallaxe_Wandel bg_stadt;
    Parallaxe_Wandel bg_viertel;
    Parallaxe strasse; // bis ymax-120

    /* GFX Backend */
    sf::RenderWindow* fenster;
    sf::View view;
    bool loop = true;
    sf::Clock net_clock;
    sf::Clock fps_clock;

};
