#include "lingua.hpp"
#include "nada/ini.hpp"
#include <filesystem>

#include <nada/log.hpp>
#include <nada/str.hpp>
#include <fstream>
#include <vector>

using nada::Log;

TXT::speicher_t TXT::einlesen() {
    Log::debug() << "TXT::einlesen()" << Log::endl;
    TXT::speicher_t temp; // return

    // Alle CSVs einlesen
    for (const auto& eintrag : std::filesystem::directory_iterator("data/lingua")) {
        if (!eintrag.is_regular_file()) continue;
        std::string datei = eintrag.path().string();
        if (datei.find(".csv") == std::string::npos) {
            Log::debug() << "\t" << datei << ": keine CSV\n";
            continue;
        }
        if (std::ifstream in(datei, std::ios::binary); in.good()) {
            Log::debug() << "\tStrings werden gelesen aus " << datei << Log::endl;

            // Zeile für Zeile auslesen
            for (std::string zeile; std::getline(in, zeile);) {
                if (!zeile.empty() && zeile[0] != '#' &&
                    zeile[0] != ';') { // '#' sind Kommentare, ';' sind leere Zeilen

                    // Zeilen am ';' teilen, Fehler prüfen
                    const std::vector<std::string>& tokens = nada::str::tokenize(zeile, ';');
                    if (tokens.size() < N_SPRACHEN) {
                        if (tokens.size() > 1) {
                            Log::err() << datei << " Zeile enthaelt zu wenige Token. Zeile = " << zeile << '\n';
                        }
                        continue;
                    }
                    if (tokens[0].empty()) {
                        Log::err() << datei << " Zeile enthaelt leeres Key-Token. Zeile = " << zeile << '\n';
                        continue;
                    }

                    // Zeile gültig, einlesen
                    TXT::string_array_t strings;
                    for (uint8_t sprache = 0; sprache < N_SPRACHEN; ++sprache) {
                        try { strings[sprache] = tokens[sprache + 1]; }
                        catch (std::exception& e) {
                            Log::err() << datei << " Fehler in Zeile =  " << zeile << '\n';
                            strings[sprache] = "???";
                        }
                    }
                    if (temp.count(tokens[0])) Log::err() << datei << " enthaelt Key-Duplikat: " << tokens[0] << '\n';
                    else temp[tokens[0]] = strings;
                }
            }
        } else Log::err() << datei << " konnte nicht eingelesen werden." << Log::endl;
    }
    return temp;
}

const std::string& TXT::get(const std::string& key) {
    try { return translationen.at(key)[lingua]; }
    catch (const std::exception& e) {
        Log::err() << "TXT::" << __func__ << " key nicht vorhanden: " << key << '\n';
        translationen[key] = string_array_t();
        for (uint8_t sprache = 0; sprache < N_SPRACHEN; ++sprache) translationen[key][sprache] = "???";
        return TXT::get(key);
    }
}

TXT::Lingua TXT::get_sprache() {
    nada::Ini cfg{"config.ini"};
    const std::string& sprache = cfg.get("language");
    if (sprache == "DE") return DE;
    if (sprache == "EN") return EN;
    if (sprache == "RU") return RU;
    if (sprache == "UA") return UA;
    return EN;
}
