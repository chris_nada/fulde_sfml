#pragma once

#include <string>
#include <array>
#include <unordered_map>

class TXT final {

public:

    /// Implementierte Sprachen
    enum Lingua {
        DE = 0,
        EN = 1,
        RU = 2,
        UA = 3,
        ANZAHL = 4
    };

    /// Legt die von der Laufzeit zu verwendene Sprache fest.
    static void set_lingua(Lingua lingua) { TXT::lingua = lingua; }

    /// Gibt einen String wieder in der eingestellten Sprache, die sich über `set_lingua` ändern lässt.
    static const std::string& get(const std::string& key);

private:

    /// Anzahl Sprachen.
    static constexpr unsigned int N_SPRACHEN = ANZAHL;

    /// Typ für die Speicherzuweisung von lokalisierten Strings.
    typedef std::array<std::string, N_SPRACHEN> string_array_t;

    /// Speichertyp der lokalisierten Strings.
    typedef std::unordered_map<std::string, string_array_t> speicher_t;

    /// Liest den Speicher der übersetzten Strings aus einer csv Datei.
    static speicher_t einlesen();

    /// Liefert die in der Konfiguration eingestellte Sprache.
    static Lingua get_sprache();

    /// Von der Laufzeit verwendete Sprache.
    static inline Lingua lingua = get_sprache();

    /// Speicher der lokalisierten Strings.
    static inline speicher_t translationen = einlesen();

};

/// Abkürzung für TXT::get
inline const std::string& operator"" _txt(const char* key, size_t) { return TXT::get(key); }

/// Abkürzung für TXT::get::c_str
inline const char* operator"" _txtc(const char* key, size_t) { return TXT::get(key).c_str(); }
