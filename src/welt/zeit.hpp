#pragma once

#include <ostream>

/**
 * Klasse zum Speichern der Zeit im Spiel.
 */
class Zeit final {

public:

    /// Jahr zu Beginn des Spiels.
    static constexpr uint16_t START_JAHR = 1850;

    /// Aktuelles Jahr. (Wird Server-seitig aktualisiert).
    static inline uint16_t SERVER_JAHR = START_JAHR;

    /// Konstruktor. Zeit startet um 8:00, im Jahr `START_JAHR`.
    Zeit() : h(8), min(0), jahreszeit(1), jahr(START_JAHR) {}

    /// Konstruktor. Nur mit Jahreszeit & Jahr.
    Zeit(uint8_t jahreszeit, uint16_t jahr);

    /// Konstruktor. Setzt Zeit auf gegebenen Wert.
    Zeit(uint8_t h, uint8_t min, uint8_t jahreszeit, uint16_t jahr);

    /// Deserialisiert ein Zeitobjekt aus einem `std::string` von Daten.
    explicit Zeit(std::stringstream& daten);

    /// Getter: Stunde (0 - 23).
    uint8_t get_h() const { return h; }

    /// Getter: Minute (0 - 59).
    uint8_t get_min() const { return min; }

    /// Getter: Jahreszeit (1 bis 4).
    uint8_t get_jahreszeit() const { return jahreszeit; }

    /// Getter: Name der aktuelllen Jahreszeit.
    const std::string& get_jahreszeit_name() const;

    /// Getter: Jahr.
    uint16_t get_jahr() const { return jahr; }

    /// Setter: Setzt die Zeit auf den gegebenen Zeitpunkt.
    void set(uint8_t min, uint8_t h, uint8_t jahreszeit, uint16_t jahr);

    /// Lässt eine gewisse Zahl Minuten verstreichen. Gibt `true` zurück, falls der Tag dadurch endete.
    bool add_min(uint8_t minuten);

    /// Berechnet das Alter zwischen gegebener Zeit und dieser Zeit in Jahren (jahreszeitengenau).
    uint16_t get_alter(const Zeit& datum_jetzt) const;

    /// Addiert oder Subtrahiert Jahre.
    Zeit operator+(uint16_t jahre) const;
    Zeit operator-(uint16_t jahre) const;
    Zeit& operator+=(uint16_t jahre);
    Zeit& operator-=(uint16_t jahre);

    /// Vergleich zweier Datum. Älter < Jünger.
    bool operator<(const Zeit& rhs) const;

    /// Vergleich zweier Datum. Älter < Jünger.
    bool operator>(const Zeit& rhs) const;

    /// Vergleich zweier Datum. Älter < Jünger.
    bool operator<=(const Zeit& rhs) const;

    /// Vergleich zweier Datum. Älter < Jünger.
    bool operator>=(const Zeit& rhs) const;

    /// Vergleicht zwei Datum auf minutengenau gleich.
    bool operator==(const Zeit& rhs) const;

    /// Vergleicht zwei Datum auf minutengenau gleich.
    bool operator!=(const Zeit& rhs) const;

    /// Serialisiert Objekt in einen Stream.
    friend std::ostream& operator<<(std::ostream& os, const Zeit& zeit);

    /// Serialisierung via Cereal.
    template <class Archive> void serialize(Archive& ar) {
        ar(min, h, jahreszeit, jahr);
    }

private:

    /// Stunde.
    uint8_t  h;

    /// Minute.
    uint8_t  min;

    /// Jahreszeit.
    uint8_t  jahreszeit;

    /// Jahr.
    uint16_t jahr;

};
