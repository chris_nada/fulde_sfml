#pragma once

#include "../zeit.hpp"
#include "../../netzwerk/netzwerk.hpp"
#include <list>
#include <unordered_map>

/// Prädeklarationen.
class Dynastie;

/**
 * Nachrichten, die Spielern angezeigt werden.
 */
class Nachricht final {

public:

    /// Maximale Anzahl Nachrichten je Spieler. Wird das Limit überschritten, wird die älteste gelöscht.
    static constexpr unsigned LIMIT = 20;

    /// Bestimmt den Typ und Inhalt der Nachricht.
    enum Inhalt : uint8_t {
        WILLKOMMEN    = 0,
        TEXTNACHRICHT = 1,
        HANDEL        = 2,
    };

    /**
     * Hilfsfunktion, die dem Nachrichtenspeicher (in `Welt`) eine neue Nachricht hinzufügt.
     * @note Neue Nachrichten werden vorn angefügt, alte am Ende der Liste gelöscht.
     * @param nachrichten
     * @param dynastie
     * @param zeit
     * @param inhalt
     */
    static void add(std::unordered_map<Netzwerk::id_t, std::list<Nachricht>>& nachrichten, const Dynastie& dynastie, const Zeit& zeit, Inhalt inhalt);

    /// Ctor.
    Nachricht() = default;

    /// Ist eine Antwort in Form von *Akzeptiert* / *Abgelehnt* nötig?
    bool beantwortbar() const;

    /**
     * Wird aus Lokalisation als `text#` ausgelesen, wobei # für die Zahl aus `enum Inhalt` steht.
     * @return Lokalisierter Inhalt.
     */
    const std::string& get_text() const;

    /// Getter: Hash für die nachricht. Z.b. zum Wiederauffinden.
    std::string get_hash() const;

    /// Getter: Zu welchem Zeitpunkt wurde die Nachricht erstellt.
    const Zeit& get_zeit() const { return zeit; }

    /// Serialisierung via Cereal.
    template <class Archive> void serialize(Archive& ar) {
        ar(hash, inhalt, gelesen, geantwortet, zeit);
    }

private:

    /**
     *
     * @param zeit
     * @param dynastie
     * @param inhalt
     */
    Nachricht(const Zeit& zeit, Inhalt inhalt);

    ///
    uint16_t hash{};

    ///
    uint8_t inhalt;

    /// Hat der Spieler die Nachricht bereits gelesen?
    bool gelesen;

    /// Hat der Spieler die Nachricht bereits beantwortet?
    bool geantwortet;

    /// Zu welcher Zeit die Nachricht erstellt wurde.
    Zeit zeit;

};

/// Wrapper zur Serialisierung von Listen von Nachrichten.
struct Nachrichten {

    /// Default Ctor.
    Nachrichten() = default;

    /// Erzeugt den Wrapper aus einer Liste von Nachrichten.
    explicit Nachrichten(const std::list<Nachricht>& nachrichten);

    /// Deserialisierender Ctor.
    explicit Nachrichten(std::stringstream& ss);

    /// Serialisiert in einen Stream.
    friend std::ostream& operator<<(std::ostream& os, const Nachrichten& nachrichten);

    /// Serialisierung via Cereal.
    template <class Archive> void serialize(Archive& ar) { ar(nachrichten); }

    /// Wrapper-Inhalt.
    std::list<Nachricht> nachrichten;

};
