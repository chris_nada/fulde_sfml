#pragma once

#include "../../netzwerk/netzwerk.hpp"
#include "nachricht.hpp"
#include "../statistik.hpp"

#include <cstdint>
#include <unordered_map>
#include <vector>
#include <string>
#include <ostream>
#include <set>
#include <unordered_set>

class Stadt;
class Land;
class Nachricht;

/**
 * Beschreibt den gesamten Besitz und weitere Informationen jedes Spielers.
 */
class Dynastie final {

    friend class Welt;
    friend class Gegner;
    friend class Nachricht;

public:

    Dynastie() = default;

    /// Konstruktor bloß über id. Alle andern Feldern bleiben uninitialisiert.
    explicit Dynastie(Netzwerk::id_t id) : id(id) {}

    /// Konstruktor zum erstellen neuer Spieler.
    Dynastie(Netzwerk::id_t id, const std::string& nachname);

    /// Baut aus einem zuvor durch `<<` serialisiertem String wieder eine Instanz dieser Klasse auf.
    explicit Dynastie(std::stringstream& daten);

    /// Legt ein (neues) Oberhaupt fest.
    void set_oberhaupt(Netzwerk::big_id_t id_oberhaupt) { Dynastie::id_oberhaupt = id_oberhaupt; }

    /// Fügt gegebene Menge zum Geld hinzu.
    void geld_erhalten(uint64_t menge, Statistik::Einnahme zweck, Statistik& statistik);

    /// Zieht gegebene Menge vom Geld ab.
    void geld_bezahlen(uint64_t menge, Statistik::Ausgabe zweck, Statistik& statistik);

    /// Getter: ID.
    Netzwerk::id_t get_id() const { return id; }

    /// Getter: Geld.
    int64_t get_geld() const { return geld; }

    /// Getter: Lager in Stadt vorhanden?
    bool hat_lager(Netzwerk::id_t stadt_id) const { return lager.count(stadt_id) > 0; }

    /// Getter: Lager, in Stadt.
    Netzwerk::id_t get_lager(Netzwerk::id_t stadt_id) const { return lager.at(stadt_id); }

    /// Getter: Familienname.
    const std::string& get_name() const { return familienname; }

    /// Getter: Oberhaupt.
    Netzwerk::big_id_t get_oberhaupt() const { return id_oberhaupt; }

    /// Getter: ID des Landes, in dem die Dynastie ihren Wohnsitz hat.
    Netzwerk::id_t get_id_land() const { return id_land; }

    /// Getter: Der Dynastie zugehörige Gebäude.
    const std::unordered_set<Netzwerk::id_t>& get_gebaeude() const { return gebaeude; }

    /// Getter: Ist vom Computer gesteuert?
    bool is_computergesteuert() const { return computergesteuert; }

    /// Serialisiert eine Instanz.
    friend std::ostream& operator<<(std::ostream& os, const Dynastie& dynastie);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(id, id_land, geld, familienname, lager,
           id_oberhaupt, charaktere, gebaeude
        );
    }

private:

    /// Quartalstick: Steuern zahlen, Zinsen, sonstige Ein/Ausgaben
    void tick(Statistik& statistik, Land& land);

private:

    /// Der Computer übernimmt die Aktionen für diesen Spieler.
    bool computergesteuert = false;

    /// ID. Wichtig für Verlinkungen insb. beim Netzwerkkode, da dort Zeiger nicht so ohne weiteres funktionieren.
    Netzwerk::id_t id = 0;

    /// ID, in welchem Land die Dynastie ihren Wohnsitz hat.
    Netzwerk::id_t id_land = 0;

    /// Derzeitiges Oberhaupt der Dynastie.
    Netzwerk::big_id_t id_oberhaupt = 0;

    /// Barvermögen der Dynastie.
    int64_t geld = 0;

    /// Nachname
    std::string familienname;

    /// Lager in Besitz. 1. ID der Stadt; 2. ID des Lagers. 0 bzw. kein Eintrag bedeutet, dass dort keins vorhanden ist.
    std::unordered_map<Netzwerk::id_t, Netzwerk::id_t> lager;

    /// Charaktere, die der Dynastie angehören.
    std::unordered_set<Netzwerk::big_id_t> charaktere;

    /// Gebäude, die der Dynastie gehören.
    std::unordered_set<Netzwerk::id_t> gebaeude;

};
