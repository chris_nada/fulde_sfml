#include "person.hpp"
#include "../../gfx/grafik.hpp"
#include "../../werkzeuge/etc.hpp"
#include "../kultur.hpp"

#include <nada/log.hpp>
#include <nada/fs.hpp>

using nada::Log;

Person::Person() : Person(0, 0, false, "???", "???", Zeit(), {})
{
    //
}

Person::Person(Netzwerk::big_id_t id, uint8_t id_kultur, bool maennlich,
               const std::string& vorname, const std::string& nachname,
               const Zeit& geboren, const Position& pos) :
        id(id), id_kultur(id_kultur),
        maennlich(maennlich), vorname(vorname),
        familienname(nachname), geboren(geboren),
        pos(pos),
        speed(SPEED), lebendig(true), computergesteuert(true)
{
    // nichts weiter zu tun
}

Position& Person::set_pos() {
    return pos;
}

void Person::gehe(Position::Ausrichtung richtung) {
    pos.ausrichtung = richtung;
    if (richtung == Position::LINKS) pos.x -= get_speed();
    else pos.x += get_speed();
}

Grafik& Person::get_grafik() const {
    static std::unordered_map<Netzwerk::big_id_t, Grafik> id_pfad; // key: char_id, value: grafikpfad
    static std::unordered_map<decltype(id_kultur), std::unordered_map<char, std::vector<std::string>>> kultur_mf_grafiken;

    // Pfade für alle Kulturen laden
    if (kultur_mf_grafiken.empty()) {
        for (const Kultur& kultur : Kultur::alle()) {
            for (std::string mf : {"m", "f"}) {
                const auto& alle_pngs = nada::fs::all_files(
                        "data/gfx/portraits/" + Kultur::alle().at(id_kultur).get_key() + "/" + mf, "png");
                kultur_mf_grafiken[kultur.get_id()][mf[0]].reserve(alle_pngs.size());
                for (const auto& png : alle_pngs) kultur_mf_grafiken[id_kultur][mf[0]].push_back(png);
            }
            for (char mf : {'m', 'f'}) if (kultur_mf_grafiken[kultur.get_id()].at(mf).empty()) {
                Log::err() << "Keine Portraits " << kultur.get_key() << ' ' << mf << Log::endl;
            }
        }
    }
    // Grafikpfad cachen
    if (id_pfad.count(id) == 0) {
        const std::vector<std::string>& pfade = kultur_mf_grafiken.at(id_kultur).at(maennlich ? 'm' : 'f');
        id_pfad[id] = Grafik(pfade.at(id % pfade.size()));
    }
    return id_pfad[id];
}

uint16_t Person::get_alter(const Zeit& jetzt) const {
    return geboren.get_alter(jetzt);
}

std::string Person::get_voller_name() const {
    return get_vorname() + " " + get_nachname();
}
