#include "nachricht.hpp"
#include "dynastie.hpp"
#include "../../lingua/lingua.hpp"
#include "../../werkzeuge/etc.hpp"
#include <cereal/types/list.hpp>

#include <nada/random.hpp>

Nachricht::Nachricht(const Zeit& zeit, Inhalt inhalt) :
    hash(nada::random::uid<decltype(hash)>()),
    inhalt(inhalt),
    gelesen(false),
    geantwortet(false),
    zeit(zeit)
{
    //
}

const std::string& Nachricht::get_text() const {
    return TXT::get("n_" + std::to_string(inhalt));
}

void Nachricht::add(std::unordered_map<Netzwerk::id_t, std::list<Nachricht>>& nachrichten, const Dynastie& dynastie, const Zeit& zeit, Nachricht::Inhalt inhalt) {
    if (dynastie.is_computergesteuert()) return;
    Nachricht n(zeit, inhalt);
    rehash: n.hash = nada::random::uid<decltype(n.hash)>();
    for (const auto& nachrichtenset : nachrichten) {
        for (const auto& nachricht : nachrichtenset.second) if (nachricht.hash == n.hash) goto rehash; // falls Hash schon vorhanden - neu hashen
    }
    while (nachrichten[dynastie.get_id()].size() > LIMIT) nachrichten[dynastie.get_id()].pop_back();
    nachrichten[dynastie.get_id()].push_front(n);
}

std::string Nachricht::get_hash() const {
    return std::to_string(hash);
}

bool Nachricht::beantwortbar() const {
    const auto typ = (Nachricht::Inhalt) inhalt;
    switch (typ) {
        case WILLKOMMEN:    return false;
        case TEXTNACHRICHT: return false;
        case HANDEL:        return true;
    };
    return false;
}

/* Nachrichten-Wrapper */
Nachrichten::Nachrichten(const std::list<Nachricht>& nachrichten) : nachrichten(nachrichten)
{
    //
}

Nachrichten::Nachrichten(std::stringstream& ss) {
    etc::serial_in in(ss);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Nachrichten& nachrichten) {
    etc::serial_out out(os);
    out << nachrichten;
    return os;
}
