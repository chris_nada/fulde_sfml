#pragma once

#include "person.hpp"
#include "../../netzwerk/netzwerk.hpp"
#include "../produktion/arbeiter.hpp"

#include <cereal/types/polymorphic.hpp>
#include <cereal/types/optional.hpp>

class Welt;

/**
 * Eine erweiterte Person. Wird geboren und stirbt.
 * + Kann: Gehört zu einer Dynastie (einer Familie), die nach seinem Tod weiterexistiert und die einen Nachfolger liefert (wenn vorhanden).
 * + Kann Arbeiter (nicht-Spieler) sein.
 * + Kann von der KI gesteuert sein, kann von Spielern im Netzwerk gesteuert sein.
 */
class Charakter final : public Person {

    friend class Welt;
    friend class Anlage;

public:

    /// Ctor.
    Charakter();

    /// Ctor zum alles Initialisieren.
    Charakter(Netzwerk::big_id_t id, uint8_t id_kultur,
              const std::string& vorname, const std::string& nachname,
              const Zeit& geboren, bool maennlich, const Position& pos,
              std::optional<Arbeiter> arbeiter = std::nullopt,
              std::optional<Netzwerk::id_t> id_dynastie = std::nullopt);

    /// Ctor zum Deserialisieren.
    explicit Charakter(std::stringstream& ss);

    /// ID der Dynastie, zu der der C gehört.
    const std::optional<Netzwerk::id_t>& get_dynastie() const { return id_dynastie; }

    /// Arbeitereigenschaft des Characters.
    const std::optional<Arbeiter>& get_arbeiter() const { return arbeiter; }

    /// Serialisiert den C in gg. Stream.
    friend std::ostream& operator<<(std::ostream& os, const Charakter& charakter);

    /// Serialisierung via Cereal.
    template <class Archive> void serialize(Archive& ar) {
        ar(cereal::base_class<Person>(this), id_dynastie, arbeiter);
    }

private:

    /// Täglich von der Welt ausgeführte Ticks.
    void tick_tag(Welt* welt);

    /// Dieser Char bewirbt sich auf ein Amt.
    void amt_bewerben(Welt* welt);

    /// Dieser Char gibt seine Stimme bei der Wahl ab.
    void amt_waehlen(Welt* welt);

private:

    /// ID der Dynastie, zu der der C gehört.
    std::optional<Netzwerk::id_t> id_dynastie;

    /// Arbeitereigenschaften des Charakters.
    std::optional<Arbeiter> arbeiter;

};
