#pragma once

#include "position.hpp"
#include "../zeit.hpp"

class Grafik;

/**
 * Basisklasse zur Darstellung von Personen, NPSs, Spielern.
 */
class Person {

    /// Bewegungsgeschwindigkeit pro Simulationstick.
    static constexpr float SPEED = 10;

public:

    /// Ctor.
    Person();

    /// Alles initialisierender Ctor.
    Person(Netzwerk::big_id_t id, uint8_t id_kultur, bool maennlich,
           const std::string& vorname, const std::string& nachname,
           const Zeit& geboren, const Position& pos);

    /// Liefert die Posistion manipulierbar.
    Position& set_pos();

    /// Bewegung ausführen in gg. Richtung.
    void gehe(Position::Ausrichtung richtung);

    /// ID.
    Netzwerk::big_id_t get_id() const { return id; }

    /// True bedeutet M; false bedeutet W.
    bool is_maennlich() const { return maennlich; }

    /// Lebt (noch)?
    bool is_lebendig() const { return lebendig; }

    /// Is Computergesteuert?
    bool is_computergesteuert() const { return computergesteuert; }

    /// Liefert die Geschwindigkeit beim laufen.
    float get_speed() const { return speed; }

    /// Liefert die Posistion als konstant.
    const Position& get_pos() const { return pos; }

    /// Liefert den Vornamen.
    const std::string& get_vorname() const { return vorname; }

    /// Liefert den Familiennamen.
    const std::string& get_nachname() const { return familienname; }

    /// Liefert den vollen Namen.
    std::string get_voller_name() const;

    /// Liefert das Alter in Jahren.
    uint16_t get_alter(const Zeit& jetzt) const;

    /// Liefert das Portrait für diese Person.
    Grafik& get_grafik() const;

    /// Serialisierung via Cereal.
    template <class Archive> void serialize(Archive& ar) {
        ar(id, id_kultur,
           vorname, familienname,
           geboren, pos, speed,
           stimmrecht, lebendig, maennlich, computergesteuert); // bools
    }

protected:

    /// ID.
    Netzwerk::big_id_t id;

    /// Kultur, der der Charakter angehört.
    uint8_t id_kultur;

    /// Geschlecht. True = Männlich, False = Weiblich.
    bool maennlich;

    /// Vorname.
    std::string vorname;

    /// Nachname.
    std::string familienname;

    /// Geburtsdatum.
    Zeit geboren;

    /// Aktuelle Aufenthaltsposition.
    Position pos;

    /// Bewegungsgeschwindigkeit in x-Pixeln pro tick.
    float speed;

    /// Darf wählen?
    bool stimmrecht;

    /// Lebt (noch)?
    bool lebendig;

    /// Computergesteuert?
    bool computergesteuert;

    // TODO: Eigenschaften

    // TODO: Beziehungen

};
