#include "position.hpp"
#include "../../werkzeuge/etc.hpp"

Position::Position(Netzwerk::id_t land, Netzwerk::id_t stadt, Netzwerk::id_t viertel) :
    land(land), stadt(stadt), viertel(viertel),
    x(0), ausrichtung(LINKS), sichtbar(true)
{
    // Alles initialisiert.
}

std::ostream& operator<<(std::ostream& os, const Position& position) {
    etc::serial_out out(os);
    out(position);
    return os;
}

Position::Position(std::stringstream& ss) {
    etc::serial_in in(ss);
    in(*this);
}
