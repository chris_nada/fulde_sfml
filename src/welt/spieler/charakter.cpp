#include "charakter.hpp"
#include "../../werkzeuge/etc.hpp"
#include "../welt.hpp"

#include <cereal/types/string.hpp>
#include <utility>

Charakter::Charakter() :
        Person(0, 0, false, "???", "???", Zeit(), {}),
        id_dynastie(0)
{
    //
}

Charakter::Charakter(Netzwerk::big_id_t id, uint8_t id_kultur,
                     const std::string& vorname, const std::string& nachname,
                     const Zeit& geboren, bool maennlich, const Position& pos,
                     std::optional<Arbeiter> arbeiter,
                     std::optional<Netzwerk::id_t> id_dynastie) :
        Person(id, id_kultur, maennlich, vorname, nachname, geboren, pos),
        id_dynastie(id_dynastie),
        arbeiter(std::move(arbeiter))
{
    //
}

Charakter::Charakter(std::stringstream& ss) {
    etc::serial_in ia(ss);
    ia(*this);
}

std::ostream& operator<<(std::ostream& os, const Charakter& charakter) {
    etc::serial_out oa(os);
    oa(charakter);
    return os;
}

void Charakter::tick_tag(Welt* welt) {

    // KI-Logik
    if (!computergesteuert) return;

    // Ämter
    if (welt->get_zeit().get_jahreszeit() == 1) amt_bewerben(welt);
    if (welt->get_zeit().get_jahreszeit() == 4) amt_waehlen(welt);
}

void Charakter::amt_bewerben(Welt* welt) {
    // Jetzige Amtstufe bestimmen
    unsigned stufe = 0;
    if (Amt* jetziges_amt = Amt::get_amt(this->id, *welt); jetziges_amt) stufe = jetziges_amt->get_stufe();

    // Alle gültigen Ämter bestimmen, 1 auswählen
    std::vector<Amt*> aemter;
    aemter.reserve(8);
    auto add_amt = [&](Amt::Ebene ebene, auto id_ort) {
        for (auto& amt : welt->aemter.at(ebene).at(id_ort)) {
            if (amt.get_stufe() > stufe && amt.darf_bewerben(this->id, stufe, true)) aemter.push_back(&amt);
        }
    };
    add_amt(Amt::Ebene::LAND,  this->pos.land);
    add_amt(Amt::Ebene::STADT, this->pos.stadt);

    // Als Bewerber eintragen
    if (!aemter.empty()) {
        Amt* amt = nada::random::choice(aemter);
        if (amt->get_bewerber().size() < Amt::MAX_KI_BEWERBER || Amt::MAX_KI_BEWERBER == 0) amt->bewerben(this->id);
    }
}

void Charakter::amt_waehlen(Welt* welt) {
    // Alle gültigen Wahlämter bestimmen
    std::vector<Amt*> aemter;
    for (Amt& amt : welt->aemter[Amt::Ebene::LAND].at(this->pos.land))   aemter.push_back(&amt);
    for (Amt& amt : welt->aemter[Amt::Ebene::STADT].at(this->pos.stadt)) aemter.push_back(&amt);

    // Für jedes Amt eine Stimme abgeben
    for (Amt* amt : aemter) {
        if (amt->is_bewerber(this->id)) amt->waehlen(this->id, this->id); // sich selbst wählen
        else if (!amt->get_bewerber().empty()) {
            if (nada::random::b(75)) {
                // zufällig jmd. wählen
                amt->waehlen(nada::random::choice(amt->get_bewerber()).first, this->id);
                // TODO wählen, wer gemocht wird
            }
        }
    }
}
