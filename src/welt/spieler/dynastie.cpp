#include "dynastie.hpp"
#include "../land.hpp"
#include "../stadt/stadt.hpp"
#include "../../werkzeuge/etc.hpp"

#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/set.hpp>
#include <cereal/types/unordered_set.hpp>

Dynastie::Dynastie(Netzwerk::id_t id, const std::string& nachname) :
        id(id),
        familienname(nachname) {
}

std::ostream& operator<<(std::ostream& os, const Dynastie& dynastie) {
    etc::serial_out out(os);
    out(dynastie);
    return os;
}

Dynastie::Dynastie(std::stringstream& daten) {
    etc::serial_in in(daten);
    in(*this);
}

void Dynastie::geld_erhalten(uint64_t menge, Statistik::Einnahme zweck, Statistik& statistik) {
    geld += menge;
    statistik.add_einnahme(zweck, menge);
}

void Dynastie::geld_bezahlen(uint64_t menge, Statistik::Ausgabe zweck, Statistik& statistik) {
    geld -= menge;
    statistik.add_ausgabe(zweck, menge);
}

void Dynastie::tick(Statistik& statistik, Land& land) {
    const auto plus  = statistik.get_aktuelle_einnahmen();
    const auto minus = statistik.get_aktuelle_ausgaben();
    auto paar_plus = [](auto z, const auto& paar) { return z + paar.second; };
    const uint64_t einnahmen = summieren(plus, paar_plus);
    const uint64_t ausgaben  = summieren(minus, paar_plus);

    // Ertragssteuer zahlen
    if (einnahmen > ausgaben) {
        const uint64_t ertrag = einnahmen - ausgaben;
        const uint64_t ertragssteuer = (ertrag * land.get_steuern(Steuertyp::ERTRAG)) / (100 * 4); // 1/4 des Jahresbetrags
        land.geld_einnehmen(ertragssteuer);
        geld_bezahlen(ertragssteuer, Statistik::Ausgabe::STEUER_ERTRAG, statistik);
        // TODO Grundsteuer
    }
}
