#pragma once

#include "../../netzwerk/netzwerk.hpp"
#include <cstdint>
#include <ostream>

/// Gibt an, wo sich eine Person befindet.
struct Position final {

    enum Ausrichtung : uint8_t {
        LINKS, RECHTS
    };

    /// Standardkonstruktor.
    Position() = default;

    /// Initialisiert die Position in das gegebene Viertel.
    Position(Netzwerk::id_t land, Netzwerk::id_t stadt, Netzwerk::id_t viertel);

    /// Deserialisiert aus einem Stringstream.
    explicit Position(std::stringstream& ss);

    /// In welcher Region sich die Person aufhält.
    Netzwerk::id_t land, stadt, viertel;

    /// Horizontale Position im Viertel.
    float x;

    /// Blickrichtung.
    Ausrichtung ausrichtung;

    /// Im einem Gebaeude?
    bool sichtbar;

    /// Deserialisiert die Position.
    friend std::ostream& operator<<(std::ostream& os, const Position& position);

    /// Serialisierungsfunktion für Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(land, stadt, viertel, x, ausrichtung, sichtbar);
    }

};
