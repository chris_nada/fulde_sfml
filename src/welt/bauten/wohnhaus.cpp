#include "wohnhaus.hpp"
#include "../../werkzeuge/etc.hpp"

Wohnhaus::Wohnhaus(Netzwerk::id_t id) :
    id(id)
{
    //
}

std::ostream& operator<<(std::ostream& os, const Wohnhaus& wohnhaus) {
    etc::serial_out boa(os);
    boa(wohnhaus);
    return os;
}

Wohnhaus::Wohnhaus(std::stringstream& ss) {
    etc::serial_in in(ss);
    in(*this);
}
