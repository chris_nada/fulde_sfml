#pragma once

#include <string>
#include <ostream>
#include "../../netzwerk/netzwerk.hpp"

/**
 * Representation von einem Gebäude im Backend.
 */
class Gebaeude final {

    friend class Welt;

public:

    /// Gebäudetypen.
    enum Typ : Netzwerk::enum_t {
        UNGUELTIG     = -1,
        WOHNHAUS      =  0, // Link = Wohnhaus
        WERKSTATT     =  1, // Link = Werkstatt
        MARKT         =  2, // Link = Markt
        LAGER         =  3, // Link = Lager
        RATHAUS       =  4,
        KIRCHE        =  5,
        BANK          =  6,
        SCHATTENGILDE =  7,
    };

    /// Ctor. Initialisiert nichts.
    Gebaeude() : typ(UNGUELTIG) {}

    /// Ctor, initialisiert alles auf 0.
    explicit Gebaeude(Typ typ) : typ(typ), id(0), id_besitzer(0), id_link(0), position(0) {}

    /// Deserialisierender Ctor.
    explicit Gebaeude(std::stringstream& ss);

    /// Getter: Id.
    Netzwerk::id_t get_id() const { return id; }

    /// Getter: Verlinkte Instanz des Gebäudes.
    Netzwerk::id_t get_id_link() const { return id_link; }

    /// Getter: Übersetzter Name des Gebäudetyps.
    const std::string& get_typ_name() const;

    /// Getter: Gebäudetyp.
    Typ get_typ() const { return typ; }

    /// Getter: Position im Viertel.
    int8_t get_position() const { return position; }

    /// Getter: Hat das Haus einen Besitzer? Wenn nicht, ist es staatlich oder so.
    bool hat_besitzer() const;

    /// Getter: Besitzende Dynastie.
    Netzwerk::big_id_t get_besitzer() const { return id_besitzer; }

    /// Setter: Id.
    void set_id(Netzwerk::id_t id) { Gebaeude::id = id; }

    /// Setter: Besitzende Dynastie.
    void set_besitzer(Netzwerk::big_id_t id_besitzer) { Gebaeude::id_besitzer = id_besitzer; }

    /// Setter: ID der verlinkten Gebäudefunktion.
    void set_link(Netzwerk::id_t id_link) { Gebaeude::id_link = id_link; }

    /// Setter: Ändert den Standort (im Viertel).
    void set_position(int8_t position) { Gebaeude::position = position; }

    /// Serialisieren.
    friend std::ostream& operator<<(std::ostream& os, const Gebaeude& gebaeude);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(id, id_besitzer, id_link, typ, position);
    }

private:

    /// Gebäudetyp.
    Typ typ;

    /// Id in der Welt.
    Netzwerk::id_t id;

    /// Dynastie ID.
    Netzwerk::big_id_t id_besitzer;

    /// Link zur Gebäudefunktion (Werkstatt...).
    Netzwerk::id_t id_link;

    /// Position im Viertel.
    int8_t position;

};
