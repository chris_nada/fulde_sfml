#include "gebaeude.hpp"
#include "../../lingua/lingua.hpp"
#include "../../werkzeuge/etc.hpp"
#include <nada/log.hpp>

using nada::Log;

Gebaeude::Gebaeude(std::stringstream& ss) {
    etc::serial_in in(ss);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Gebaeude& gebaeude) {
    etc::serial_out out(os);
    out(gebaeude);
    return os;
}

const std::string& Gebaeude::get_typ_name() const {
    static const std::string ungueltig = "UNGUELTIG";
    static const std::string fehler = "FEHLER GEBAEUDETYP";
    switch (typ) {
        case WOHNHAUS:      return TXT::get("wohnhaus");
        case WERKSTATT:     return TXT::get("werkstatt");
        case LAGER:         return TXT::get("lager");
        case MARKT:         return TXT::get("markt");
        case KIRCHE:        return TXT::get("kirche");
        case BANK:          return TXT::get("bank");
        case SCHATTENGILDE: return TXT::get("schattenlager");
        case RATHAUS:       return TXT::get("rathaus");
        case UNGUELTIG:     return ungueltig;
        default: {
            Log::err() << "Fehlerhafter Gebaeudetyp: " << (int) typ << '\n';
            return fehler;
        }
    }
}

bool Gebaeude::hat_besitzer() const {
    switch (typ) {
        case WOHNHAUS:      return true;
        case WERKSTATT:     return true;
        case LAGER:         return true;
        case UNGUELTIG:     return false;
        case MARKT:         return false;
        case RATHAUS:       return false;
        case KIRCHE:        return false;
        case BANK:          return false;
        case SCHATTENGILDE: return false;
        default: return false;
    }
}
