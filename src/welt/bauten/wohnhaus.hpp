#pragma once

#include <ostream>
#include "../../netzwerk/netzwerk.hpp"

/// TODO unbenutzt
class Wohnhaus final {

public:

    /// Ctor.
    Wohnhaus() = default;

    /// Ctor.
    explicit Wohnhaus(Netzwerk::id_t id);

    /// Deserialisierender Ctor.
    explicit Wohnhaus(std::stringstream& ss);

    /// Getter: Id.
    Netzwerk::id_t get_id() const { return id; }

    /// Serialisierung in einen Stream.
    friend std::ostream& operator<<(std::ostream& os, const Wohnhaus& wohnhaus);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(id);
    }

private:

    /// Id.
    Netzwerk::id_t id;

};
