#include "markt.hpp"
#include "../land.hpp"
#include <nada/log.hpp>

using nada::Log;

Markt::Markt(Netzwerk::id_t id) : Lager(id) {
    Log::debug() << "Markt() id=" << id << '\n';
    for (const std::string& waren_key : alle_waren_keys()) {
        const auto& ware = Ware::get(waren_key);
        vorraete[waren_key] = Vorrat(true); // zufällige Inhalte
        vorraete[waren_key].preis = ware.get_zufallspreis(); // TODO besser gleiche Preisniveaus nach Ware?
    }
}

Markt::Markt(std::stringstream& daten) : Lager(daten) {
    // erweitert Lager nur um Methoden
}

std::ostream& operator<<(std::ostream& os, const Markt& markt) {
    os << static_cast<Lager>(markt); // erweitert Lager nur um Methoden
    return os;
}

bool Markt::kauf(const std::string& waren_key, uint32_t anzahl) {
    return get_vorrat(waren_key).entnehmen(anzahl);
}

void Markt::verkauf(const std::string& waren_key, uint32_t anzahl, uint8_t qualitaet) {
    get_vorrat(waren_key).einlagern(anzahl, qualitaet);
}

float Markt::get_preisniveau() const {
    float faktoren = 0.f;
    for (const auto& paar : vorraete) {
        const float basispreis = Ware::get(paar.first).get_basispreis();
        const float preis = paar.second.preis;
        faktoren += preis / basispreis;
    }
    return faktoren / alle_waren_keys().size();
}

uint32_t Markt::get_preis(const std::string& waren_key, const uint32_t anzahl) {
    return (get_vorrat(waren_key).get_preis(waren_key, anzahl));
}

unsigned Markt::get_bedarf(const std::string& auswahl_ware) {
    const Vorrat& vorrat = get_vorrat(auswahl_ware);
    return vorrat.max() > vorrat.menge ? vorrat.max() - vorrat.menge : 0;
}

bool Markt::kauf(const std::string& waren_key, uint32_t anzahl, Dynastie* dynastie, Lager* lager, Land* land, Stadt* stadt, Statistik* statistik) {
    if (get_vorrat(waren_key).menge < anzahl) return false; // Nicht genug im Markt
    const auto preis = get_preis(waren_key, anzahl);
    const auto steuern = (preis * land->get_steuern(Steuertyp::MEHRWERT)) / 100;
    if (dynastie->get_geld() < preis + steuern) return false; // zu wenig Geld
    if (kauf(waren_key, anzahl)) { // Transaktion
        dynastie->geld_bezahlen(preis, Statistik::Ausgabe::WARENKAUF, *statistik);
        dynastie->geld_bezahlen(steuern, Statistik::Ausgabe::STEUER_MEHRWERT, *statistik);
        lager->get_vorrat(waren_key).einlagern(anzahl, get_vorrat(waren_key).quali);
        stadt->geld_einnehmen(steuern);
        return true;
    }
    return false;
}
