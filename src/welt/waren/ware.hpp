#pragma once

#include <unordered_map>
#include <unordered_set>
#include "../../gfx/grafik.hpp"

class Ware final {

    friend class Lager;
    friend class Prozess;

public:

    /// Liefert alle Prototypen von Waren. Führt beim ersten Aufruf Import durch.
    static const std::unordered_map<std::string, Ware>& alle();

    /// Liefert einen Prototyp einer Ware.
    static const Ware& get(const std::string& waren_key);

    /// Liefert alle Warengruppen.
    static const std::unordered_set<std::string>& get_gruppen();

    /// Default-Konstruktor.
    Ware() = default;

    /// Deserialisiert einen Warenprototypen aus einem JSON-Stream.
    explicit Ware(std::istream& stream);

    /// Gruppe zu der die Ware gehört.
    const std::string& get_gruppe() const { return gruppe; }

    /// Liefert einen Preis, der einen bestimmten Prozentsatz um den Basispreis herum schwanken kann.
    uint32_t get_zufallspreis() const;

    /// Liefert eine Qualität, die irgendwo um die Basisqualität herum liegt.
    float get_zufallsquali() const;

    // TODO doc + zufällige Schwankung
    uint16_t get_gewichtung_verbrauch() const;

    // TODO doc + zufällige Schwankung
    uint16_t get_gewichtung_produktion() const;

    /// Wie stark wirkt sich die Qualität auf den Preis aus? 1.0 = Normal, < 1 schwächer, > 1 stärker.
    float get_preis_qualifaktor() const { return qualifaktor_preis; }

    /// Liefert den Basispreis der Ware.
    uint32_t get_basispreis() const { return basispreis; }

    /// Liefert das Icon dieser Ware.
    const Grafik& get_icon() const { return icon; }

private:

    static std::unordered_map<std::string, Ware> einlesen();

    static std::unordered_map<std::string, Ware> waren;

    std::string key;

    std::string gruppe;

    Grafik icon;

    uint32_t basispreis;

    uint16_t gewichtung_verbrauch;

    uint16_t gewichtung_produktion;

    float basisquali;

    /// Wie stark wirkt sich die Qualität auf den Preis aus? 1.0 = Normal, < 1 schwächer, > 1 stärker.
    float qualifaktor_preis;

    uint8_t chance_verbot;

    uint8_t chance_lizenz;

};
