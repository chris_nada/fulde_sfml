#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <unordered_set>

class Prozess final {

    friend class Host;
    friend class Anlage;

public:

    /**
     * Liefert alle Prototypen von Prozessen. Führt beim ersten Aufruf Import durch.
     */
    static const std::unordered_map<std::string, Prozess>& alle();

    /// Liefert alle Keys von Prozessen.
    static const std::unordered_set<std::string>& alle_keys();

    /// Liefert alle Keys von Prozessgruppen.
    static const std::unordered_set<std::string>& alle_gruppen();

    /// Liefert einen Prototyp eines Prozesses.
    static const Prozess& get(const std::string& key);

    /// Default Ctor.
    Prozess() = default;

    /// Deserialisierender Konstruktor (JSON).
    explicit Prozess(std::istream& stream);

    /// Prozesse werden verglichen nach `techlevel`, `kosten`, `platzverbrauch` und zuletzt `arbeitsaufwand`.
    bool operator<(const Prozess& rhs) const;

    /// Prozesse werden verglichen nach `techlevel`, `kosten`, `platzverbrauch` und zuletzt `arbeitsaufwand`.
    bool operator>(const Prozess& rhs) const;

    /// Prozesse werden verglichen nach `techlevel`, `kosten`, `platzverbrauch` und zuletzt `arbeitsaufwand`.
    bool operator<=(const Prozess& rhs) const;

    /// Prozesse werden verglichen nach `techlevel`, `kosten`, `platzverbrauch` und zuletzt `arbeitsaufwand`.
    bool operator>=(const Prozess& rhs) const;

    /// ID.
    const std::string& get_key() const;

    /// (Technologie-)Gruppe.
    const std::string& get_gruppe() const;

    /// Input: Ware, Anzahl.
    const std::unordered_map<std::string, uint16_t>& get_inputs() const;

    /// Output: Ware, Anzahl.
    const std::unordered_map<std::string, uint16_t>& get_outputs() const;

    /// Kosten der Ersteinrichtung (Maschinen, Einrichtung etc.).
    uint32_t get_kosten() const;

    /// Benötiger Raum in der Werkstatt.
    uint16_t get_platzverbrauch() const;

    /// Technologiestufe. 0 = zu Beginn verfügbar.
    uint16_t get_techlevel() const;

    /// Benötigter Gesamtarbeitsaufwand zur Fertigstellung in Stunden.
    uint16_t get_arbeitsaufwand() const;

    /// Optimale Anzahl Arbeiter.
    uint16_t get_arbeiter() const;

    /// Arbeitsaufwandminderung durch jeden Arbeiter zu viel.
    float get_arbeiterbonus() const;

    /// Arbeitsaufwandsteigerung durch zu wenige Arbeiter.
    float get_arbeitermalus() const;

    /// Bonusproduktivität für jede Einheit Platz extra.
    float get_platzbonus() const;

    /// Basisqualität der Endprodukte.
    float get_basisquali() const;

private:

    /// Liest alle JSONs ein im Ordner Prozesse.
    static std::unordered_map<std::string, Prozess> einlesen();

    /// ID.
    std::string key;

    /// (Technologie-)Gruppe.
    std::string gruppe;

    /// Input: Ware, Anzahl.
    std::unordered_map<std::string, uint16_t> inputs;

    /// Output: Ware, Anzahl.
    std::unordered_map<std::string, uint16_t> outputs;

    /// Kosten der Ersteinrichtung (Maschinen, Einrichtung etc.).
    uint32_t kosten;

    /// Benötiger Raum in der Werkstatt.
    uint16_t platzverbrauch;

    /// Technologiestufe. 0 = zu Beginn verfügbar.
    uint16_t techlevel;

    /// Benötigter Gesamtarbeitsaufwand zur Fertigstellung in Stunden.
    uint16_t arbeitsaufwand;

    /// Optimale Anzahl Arbeiter.
    uint16_t arbeiter;

    /// Arbeitsaufwandminderung durch jeden Arbeiter zu viel.
    float arbeiterbonus;

    /// Arbeitsaufwandsteigerung durch zu wenige Arbeiter.  // TODO ungenutzt
    float arbeitermalus;

    /// Bonusproduktivität für jede Einheit Platz extra.
    float platzbonus;

    /// Basisqualität der Endprodukte.
    float basisquali;

};
