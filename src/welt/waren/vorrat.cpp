#include "vorrat.hpp"
#include "../../werkzeuge/etc.hpp"
#include "ware.hpp"
#include <cereal/types/vector.hpp>
#include <nada/random.hpp>
#include <nada/log.hpp>

using nada::Log;

Vorrat::Vorrat(bool inhalt) :
    menge(inhalt ? nada::random::ui(0, 50) : 0),
    preis(inhalt ? nada::random::ui(5, 30) : 0),
    quali(inhalt ? nada::random::f(2.5, 7.5) : 5)
{
    //
}

Vorrat::Vorrat(uint32_t menge,
               uint32_t preis,
               float quali,
               std::vector<float> menge_v,
               std::vector<float> preis_v,
               std::vector<float> quali_v) :
        menge(menge),
        preis(preis),
        quali(quali),
        menge_v(std::move(menge_v)),
        preis_v(std::move(preis_v)),
        quali_v(std::move(quali_v))
{
    //
}

std::ostream& operator<<(std::ostream& os, const Vorrat& vorrat) {
    etc::serial_out out(os);
    out(vorrat.menge, vorrat.menge_v, vorrat.preis, vorrat.preis_v, vorrat.quali, vorrat.quali_v);
    return os;
}

bool Vorrat::entnehmen(uint32_t k_menge) {
    if (k_menge == 0) return false;
    if (menge < k_menge) return false; // nicht genug im Lager
    menge -= k_menge;
    return true;
}

void Vorrat::einlagern(uint32_t k_menge, float k_quali) {
    if (menge + k_menge > 0) {
        quali = (float)(((float)k_menge * k_quali + (float)menge * quali)) /
                (float)(menge + k_menge);
    }
    else quali = k_quali;
    menge += k_menge;
    quali = std::max( 1.f, quali); // Quali muss 1 bis 10 sein
    quali = std::min(10.f, quali);
}

void Vorrat::tick(const std::string& warenkey) {
    static auto update = [] (std::vector<float>& vektor, float wert) {
        vektor.push_back(wert);
        while (vektor.size() > WERTELIMIT) vektor.erase(vektor.begin());
    };
    update(menge_v, menge);
    update(quali_v, quali);
    update(preis_v, get_preis(warenkey, 1));
    // TODO max überschritten
}

uint32_t Vorrat::get_preis(const std::string& warenkey, uint32_t anzahl) const {
    return get_preis(warenkey, anzahl, quali);
}

uint32_t Vorrat::get_preis(const std::string& warenkey, uint32_t anzahl, float t_quali) const {
    const float faktor = (0.5f + (t_quali / 10.f)) * Ware::get(warenkey).get_preis_qualifaktor();
    return std::max(1u, static_cast<uint32_t>((faktor * anzahl * preis) / get_verfuegbarkeit()));
}

float Vorrat::get_verfuegbarkeit() const {
    const float kehrwert_max = 1.0f / static_cast<float>(max());
    const float halb_max = max() / 2.f;
    const float historienfaktor = 0.25f / menge_v.size();
            float vfbk = 1.0f;
    for (unsigned i = 1; i < menge_v.size(); ++i) {
        vfbk += (menge_v[i] - menge_v[i-1]) * kehrwert_max;
        if (menge_v[i] >= halb_max) vfbk += historienfaktor;
        else                        vfbk -= historienfaktor;
    }
    if (menge > max()) vfbk *= 2.f;
    return vfbk;
}

Vorrat Vorrat::entnehmen_als_vorrat(uint32_t k_menge) {
    Vorrat v = *this;
    v.preis_v.clear();
    v.menge_v.clear();
    v.quali_v.clear();
    if (menge >= k_menge) {
        v.menge = k_menge;
        this->menge -= k_menge;
    }
    return v;
}

void Vorrat::einlagern(Vorrat& vorrat, uint32_t k_menge) {
    if (vorrat.entnehmen(k_menge)) {
        preis = (k_menge * vorrat.preis + menge * preis) / (k_menge + menge);
        einlagern(k_menge, vorrat.quali);
    }
}
