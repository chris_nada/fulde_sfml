#include "lagertransport.hpp"
#include "lager.hpp"
#include "markt.hpp"
#include "../land.hpp"
#include "../../werkzeuge/etc_cereal.hpp"

#include <cereal/types/unordered_map.hpp>
#include <cereal/types/string.hpp>
#include <nada/log.hpp>

using nada::Log;

Lagertransport::Lagertransport(const std::string& daten) {
    std::stringstream ss(daten);
    etc::serial_in in(ss);
    in(waren_hin, waren_zurueck, reisedauer, restdauer, transport, hinfahrt, id_spieler, id_ziel);
}

std::ostream& operator<<(std::ostream& os, const Lagertransport& l) {
    etc::serial_out out(os);
    out(l.waren_hin, l.waren_zurueck, l.reisedauer, l.restdauer, l.transport, l.hinfahrt, l.id_spieler, l.id_ziel);
    return os;
}

float Lagertransport::fortschritt() const {
    const float f = 1.f - (((float) restdauer) / (float) reisedauer);
    return f;
}

Lagertransport::Ereignis Lagertransport::tick() {
    if (restdauer > 0) {
        restdauer--;

        // Ankunft
        if (restdauer == 0) {
            if (hinfahrt) {
                hinfahrt = false;
                restdauer = reisedauer;
                return Ereignis::ANKUNFT_ZIEL;
            }
            return Ereignis::ANKUNFT_HEIM;
        }
    }
    return Ereignis::KEIN;
}

void Lagertransport::tick(Dynastie* spieler, Lager* ursprung, Lager* ziel_lager, Land* land, Statistik* statistik) {
    switch (tick()) {
        case KEIN: break;
        case ANKUNFT_ZIEL: ankunft_ziel(spieler, ziel_lager, land, statistik); break;
        case ANKUNFT_HEIM: ankunft_zuhause(spieler, ursprung); break;
    }
}

void Lagertransport::ankunft_ziel(Dynastie* spieler, Lager* ziel_lager, Land* land, Statistik* statistik) {
    Log::debug() << "Lagertransport::" << __func__ << ' ' << spieler->get_name() << Log::endl;

    // Verkauf bzw. Einlagern
    for (auto& p : waren_hin) {
        Vorrat& zielvorrat = ziel_lager->get_vorrat(p.first);
        if (!transport) {
            Log::debug() << '\t' << p.second.menge << ' ' << p.first << " wird verkauft" << Log::flush;
            Markt* markt = (Markt*) ziel_lager;
            const uint32_t vkpreis = markt->get_vorrat(p.first).get_preis(p.first, p.second.menge, p.second.quali);
            markt->verkauf(p.first, p.second.menge, p.second.quali);
            spieler->geld_erhalten(vkpreis, Statistik::Einnahme::WARENVERKAUF, *statistik);
            Log::debug() << " fuer " << vkpreis << Log::endl;
        }
        else {
            Log::debug() << '\t' << p.second.menge << ' ' << p.first << " wird eingelagert" << Log::endl;
            zielvorrat.einlagern(p.second, p.second.menge);
        }
    }

    // Einkauf bzw. Abholung
    for (auto& p : waren_zurueck) {
        Vorrat& zielvorrat = ziel_lager->get_vorrat(p.first);

        // Einkauf -> Geld bezahlen
        if (!transport) {
            const unsigned preis = zielvorrat.get_preis(p.first, zielvorrat.menge);
            const unsigned mwst  = (preis * land->get_steuern(Steuertyp::MEHRWERT)) / 100;
            if (preis + mwst > spieler ->get_geld()) continue; // nicht genug Geld // TODO Nachricht
            spieler->geld_bezahlen(preis, Statistik::Ausgabe::WARENKAUF, *statistik);
            spieler->geld_bezahlen(mwst, Statistik::Ausgabe::STEUER_MEHRWERT, *statistik);
            Log::debug() << "\tExport: " << preis << " bezahlt und " << mwst << " Steuern.\n";
        }

        // Zu wenig vorhanden?
        if (zielvorrat.menge < p.second.menge) {
            Log::debug() << '\t' << p.first << " nicht genug auf Vorrat: ";
            Log::debug() << zielvorrat.menge << '/' << p.second.menge << Log::endl;
            p.second.menge = zielvorrat.menge;
        }
        // Gar nichts vorhanden?
        if (p.second.menge == 0) {
            Log::debug() << '\t' << p.first << " nicht auf Vorrat" << Log::endl;
            waren_zurueck.erase(p.first); // Löschen
            continue;
        }
        Log::debug() << '\t' << p.second.menge << ' ' << p.first << " wird eingeladen" << Log::endl;
        p.second = zielvorrat.entnehmen_als_vorrat(p.second.menge);
    }
}

void Lagertransport::ankunft_zuhause(Dynastie* spieler, Lager* heim_lager) {
    Log::debug() << "Lagertransport::" << __func__ << ' ' << spieler->get_name() << Log::endl;
    (void) spieler;
    for (auto& p : waren_zurueck) {
        Log::debug() << '\t' << p.first << " wird einlagert" << Log::endl;
        heim_lager->get_vorrat(p.first).einlagern(p.second, p.second.menge);
    }
}
