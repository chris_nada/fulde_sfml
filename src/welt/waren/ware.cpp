#include "ware.hpp"
#include <filesystem>
#include <fstream>
#include <cereal/archives/json.hpp>
#include <cereal/types/string.hpp>
#include <nada/random.hpp>
#include <nada/log.hpp>

using nada::Log;

decltype(Ware::waren) Ware::waren = einlesen();

Ware::Ware(std::istream& stream) {
    cereal::JSONInputArchive json(stream);
    std::string temp;
    json(cereal::make_nvp("key", key),
         cereal::make_nvp("gruppe", gruppe),
         cereal::make_nvp("icon", temp),
         cereal::make_nvp("basispreis", basispreis),
         cereal::make_nvp("gewichtung_produktion", gewichtung_verbrauch),
         cereal::make_nvp("gewichtung_verbrauch", gewichtung_produktion),
         cereal::make_nvp("basisquali", basisquali),
         cereal::make_nvp("qualifaktor_preis", qualifaktor_preis),
         cereal::make_nvp("chance_lizenz", chance_lizenz),
         cereal::make_nvp("chance_verbot", chance_verbot)
    );
    icon = Grafik(temp);
}

const std::unordered_map<std::string, Ware>& Ware::alle() {
    return waren;
}

const Ware& Ware::get(const std::string& waren_key) {
    try { return alle().at(waren_key); }
    catch (const std::out_of_range& e) { throw std::out_of_range("FEHLER Keine Ware geladen mit key " + waren_key); }
}

uint32_t Ware::get_zufallspreis() const {
    return static_cast<uint32_t>(nada::random::f(0.75, 1.25) * basispreis);
}

float Ware::get_zufallsquali() const {
    float q = nada::random::f(0.5, 1.5) * basisquali;
    if      (q < 1) q = 1;
    else if (q > 9) q = 9;
    return q;
}

std::unordered_map<std::string, Ware> Ware::einlesen() {
    Log::debug() << "Ware::" << __func__ << Log::endl;
    std::unordered_map<std::string, Ware> temp;
    for (const auto& d : std::filesystem::recursive_directory_iterator("data/waren")) {
        if (!d.is_regular_file()) continue;
        const std::string json_datei(d.path().string());
        if (json_datei.find(".json") == std::string::npos) continue; // keine JSON
        if (std::ifstream in(json_datei); in.good()) {
            Log::debug() << "\tLese " << json_datei << Log::endl;
            try {
                Ware w(in);
                temp[w.key] = w;
            } catch (std::exception& e) {
                Log::err() << "\tWare konnte nicht eingelesen werden: " << json_datei << " ";
                Log::err() << e.what() << '\n';
            }
        }
    }
    Log::debug() << '\t' << temp.size() << " Waren eingelesen" << Log::endl;
    return temp;
}

uint16_t Ware::get_gewichtung_verbrauch() const {
    if (gewichtung_verbrauch == 0) return gewichtung_verbrauch;
    const uint16_t g = static_cast<uint16_t>(gewichtung_verbrauch * nada::random::f(0.25f, 4.f));
    return (uint16_t) (g > 1 ? g : 1); // Gewichtung nicht auf 0 absinken lassen
}

uint16_t Ware::get_gewichtung_produktion() const {
    if (gewichtung_produktion == 0) return gewichtung_produktion;
    const uint16_t g = static_cast<uint16_t>(gewichtung_produktion * nada::random::f(0.25f, 4.f));
    return (uint16_t) (g > 1 ? g : 1); // Gewichtung nicht auf 0 absinken lassen
}

const std::unordered_set<std::string>& Ware::get_gruppen() {
    static std::unordered_set<std::string> warengruppen;
    if (warengruppen.empty()) for (const auto& ware : alle()) warengruppen.insert(ware.second.get_gruppe());
    return warengruppen;
}
