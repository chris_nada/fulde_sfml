#include "prozess.hpp"
#include "../../werkzeuge/etc.hpp"
#include "ware.hpp"
#include "nada/str.hpp"
#include <filesystem>
#include <cereal/archives/json.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <fstream>
#include <cmath>
#include <nada/log.hpp>

using nada::Log;

Prozess::Prozess(std::istream& stream) {
    cereal::JSONInputArchive json(stream);
    json(
            cereal::make_nvp("key", key),
            cereal::make_nvp("gruppe", gruppe),
            cereal::make_nvp("inputs", inputs),
            cereal::make_nvp("outputs", outputs),
            cereal::make_nvp("kosten", kosten),
            cereal::make_nvp("platzverbrauch", platzverbrauch),
            cereal::make_nvp("platzbonus", platzbonus),
            cereal::make_nvp("techlevel", techlevel),
            cereal::make_nvp("arbeitsaufwand", arbeitsaufwand),
            cereal::make_nvp("arbeiter", arbeiter),
            cereal::make_nvp("arbeiterbonus", arbeiterbonus),
            cereal::make_nvp("arbeitermalus", arbeitermalus),
            cereal::make_nvp("basisquali", basisquali)
            );
}

const std::unordered_map<std::string, Prozess>& Prozess::alle() {
    static const std::unordered_map<std::string, Prozess> prozesse = einlesen();
    return prozesse;
}

const Prozess& Prozess::get(const std::string& key) {
    return alle().at(key);
}

const std::unordered_set<std::string>& Prozess::alle_keys() {
    static std::unordered_set<std::string> prozess_keys;
    if (prozess_keys.empty()) {
        const auto& alle_map = alle();
        prozess_keys.reserve(alle_map.size());
        for (const auto& paar : alle_map) prozess_keys.insert(paar.first);
    }
    return prozess_keys;
}

const std::unordered_set<std::string>& Prozess::alle_gruppen() {
    static std::unordered_set<std::string> prozess_gruppen;
    if (prozess_gruppen.empty()) {
        const auto& alle_map = alle();
        for (const auto& paar : alle_map) prozess_gruppen.insert(paar.second.get_gruppe());
    }
    return prozess_gruppen;
}

std::unordered_map<std::string, Prozess> Prozess::einlesen() {
    Log::debug() << "Prozess::" << __func__ << Log::endl;
    static std::unordered_map<std::string, Prozess> prozesse;
    if (!prozesse.empty()) return prozesse;
    if (std::ifstream csv("data/prozesse/prozesse.csv"); csv.good()) {
        for (std::string zeile; std::getline(csv, zeile); ) {
            nada::str::remove_whitespace(zeile);
            try {
                std::vector<std::string> v = nada::str::tokenize(zeile, ';');
                if (v.size() < 24) { Log::err() << "Prozesszeile zu Kurz: " << zeile << '\n'; continue; }

                Prozess p;
                p.key = v[0] + v[1];
                p.outputs[v[0]] = std::stoul(v[2]);
                Ware::waren[v[0]].basispreis = std::round(std::stof(v[3]));

                // Inputs parsen
                auto add_input = [&](std::string input, std::string anzahl) {
                    if (input.empty() || anzahl.empty()) return;
                    p.inputs[input] = std::stoul(anzahl);
                };
                add_input(v[4], v[5]);
                add_input(v[7], v[8]);
                add_input(v[10], v[11]);

                // Restliche Werte ausparsen
                p.arbeitsaufwand = std::stoul(v[13]);
                p.arbeiter       = std::stoul(v[15]);
                p.platzverbrauch = std::stoul(v[17]);
                p.gruppe         = v[18];
                p.kosten         = std::stoul(v[19]);
                p.basisquali     = std::stof(v[20]);
                p.techlevel      = std::stoul(v[21]);
                p.platzbonus     = std::stof(v[22]);
                p.arbeiterbonus  = std::stof(v[23]);
                p.arbeitermalus  = 1.0f;
                prozesse[p.key] = p;
            } catch (const std::exception& e) {
                Log::err() << "Prozess konnte nicht gelesen werden: " << e.what() << ' ' << zeile << '\n';
            }
        }
    }
    Log::debug() << '\t' << prozesse.size() << " Prozesse eingelesen." << Log::endl;
    return prozesse;
}

const std::string& Prozess::get_key() const {
    return key;
}

const std::string& Prozess::get_gruppe() const {
    return gruppe;
}

const std::unordered_map<std::string, uint16_t>& Prozess::get_inputs() const {
    return inputs;
}

const std::unordered_map<std::string, uint16_t>& Prozess::get_outputs() const {
    return outputs;
}

uint32_t Prozess::get_kosten() const {
    return kosten;
}

uint16_t Prozess::get_platzverbrauch() const {
    return platzverbrauch;
}

uint16_t Prozess::get_techlevel() const {
    return techlevel;
}

uint16_t Prozess::get_arbeitsaufwand() const {
    return arbeitsaufwand;
}

uint16_t Prozess::get_arbeiter() const {
    return arbeiter;
}

float Prozess::get_arbeiterbonus() const {
    return arbeiterbonus;
}

float Prozess::get_arbeitermalus() const {
    return arbeitermalus;
}

float Prozess::get_platzbonus() const {
    return platzbonus;
}

float Prozess::get_basisquali() const {
    return basisquali;
}

bool Prozess::operator<(const Prozess& rhs) const {
    if (techlevel < rhs.techlevel) return true;
    if (rhs.techlevel < techlevel) return false;
    if (kosten < rhs.kosten) return true;
    if (rhs.kosten < kosten) return false;
    if (platzverbrauch < rhs.platzverbrauch) return true;
    if (rhs.platzverbrauch < platzverbrauch) return false;
    return arbeitsaufwand < rhs.arbeitsaufwand;
}

bool Prozess::operator>(const Prozess& rhs) const {
    return rhs < *this;
}

bool Prozess::operator<=(const Prozess& rhs) const {
    return !(rhs < *this);
}

bool Prozess::operator>=(const Prozess& rhs) const {
    return !(*this < rhs);
}
