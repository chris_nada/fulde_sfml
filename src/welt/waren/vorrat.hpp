#pragma once

#include <utility>
#include <vector>
#include <cstdint>
#include <ostream>
#include <cereal/types/vector.hpp>

/**
 * Enthält alle Information für die Speicherung des Vorrats *einer* Ware.
 */
class Vorrat final {

public:

    /// Wieviele Schritte die Statistik speichert (maximal).
    static const unsigned WERTELIMIT = 20;

    /// Erzeugt einen Vorrat mit initialem Vorrat, wenn `inhalt = true`. Sonst Nullen-Konstruktor.
    explicit Vorrat(bool inhalt = false);

    /// Alles-initialisierender Konstruktor.
    Vorrat(uint32_t menge,
           uint32_t preis,
           float    quali,
           std::vector<float> menge_v,
           std::vector<float> preis_v,
           std::vector<float> quali_v);

    /// Gibt den Preis für die hiesige Qualität an.
    uint32_t get_preis(const std::string& warenkey, uint32_t anzahl) const;

    /// Gibt den Preis für die gewünschte Qualität an.
    uint32_t get_preis(const std::string& warenkey, uint32_t anzahl, float t_quali) const;

    /**
     * @brief Berechnet einen Verfügbarkeitswert.
     * < 1.0 bedeutet niedrige Verfügbarkeit, > 1.0 hoch.
     * Wert liegt um 1.0 herum.
     * @return
     */
    float get_verfuegbarkeit() const;

    /// Maximal einlagerbare Menge.
    uint32_t max() const { return 1000; } // TODO momentan sehr willkürlich

    /**
     * Entnimmt dem Vorrat eine gegebene Menge.
     * @return true, wenn erfolgreich; false, wenn Entnahme nicht durchgeführt werden konnte.
     */
    bool entnehmen(uint32_t k_menge);

    /**
     * Entnimmt dem Vorrat eine gegebene Menge.
     * Gibt einen neuen (Teil-)Vorrat zurück mit gleichen Informationen wie dieser Vorrat.
     * @return true, wenn erfolgreich; false, wenn Entnahme nicht durchgeführt werden konnte.
     */
    Vorrat entnehmen_als_vorrat(uint32_t k_menge);

    /**
     * Fügt dem Vorrat eine gegebene Menge gegebener Qualität hinzu.
     * Berechnet die resultierende Qualität des Vorrats.
     */
    void einlagern(uint32_t k_menge, float k_quali);

    /**
     * Nimmt aus gegebenem Vorrat k_menge und fügt sie diesem Vorrat hinzu.
     * @param vorrat Hinzuzufügender Vorrat.
     */
    void einlagern(Vorrat& vorrat, uint32_t k_menge);

    /// Aktualisiert die Statistik(-Vektoren).
    void tick(const std::string& warenkey);

    /// Serialisierung via cereal.
    template <class Archive>
    void serialize(Archive& ar) { ar(menge, preis, quali, menge_v, preis_v, quali_v); }

    /* Felder */

    /// Die Menge, die der Vorrat enthält.
    uint32_t menge;

    /// Der Kaufpreis für 1 Objekt des Vorrats durchschnittlicher Qualität (=Feld `quali`).
    uint32_t preis;

    /// Durchschnittliche Qualität der Warenmenge. Wert 0.0 bis 10.0 (1 = echt mies, 5 = standard, 10 = makellos).
    float quali;

    /// Historie: Menge (0 = alt -> neu).
    std::vector<float> menge_v;

    /// Historie: Preis (0 = alt -> neu).
    std::vector<float> preis_v;

    /// Historie: Qualität (0 = alt -> neu).
    std::vector<float> quali_v;

};
