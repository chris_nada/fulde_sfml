#pragma once

#include "lager.hpp"

/**
 * Jede Stadt hat einen Markt. Markt ist ein Lager mit erweiterten Funktionen zum Kauf/Verkauf von Waren
 * und der Simulation von wirtschaftlichem Austausch mit den Stadtbewohnern.
 */
class Markt final : public Lager {

public:

    /// Leerer Konstruktor.
    Markt() : Lager() {}

    /// Konstruktor Erzeugt einen Markt mit fester ID.
    explicit Markt(Netzwerk::id_t id);

    /// Konstruktor: Erzeugt einen Markt aus serialisierten Daten.
    explicit Markt(std::stringstream& daten);

    /**
     * Führt den Kauf einer bestimmten Ware vom Markt aus.
     * @return `true`, wenn die Transaktion erfolgreich war. `false`, wenn keine Transaktion möglich war.
     */
    bool kauf(const std::string& waren_key, uint32_t anzahl);

    /**
     * Führt den Kauf einer bestimmten Ware vom Markt aus.
     * @param dynastie Dieser Dynastie werden Steuern und Kaufpreis abgezogen.
     * @param lager In dieses Lager wird die Ware transferiert.
     * @param land Diesem Land werden Steuern gezahlt.
     * @return Erfolg, wenn Geld der Dynastie ausreicht und Waren im Markt ausreichen.
     */
    bool kauf(const std::string& waren_key, uint32_t anzahl, Dynastie* dynastie, Lager* lager, Land* land, Stadt* stadt, Statistik* statistik);

    /// Führt einen Verkauf einer Ware an den Markt aus.
    void verkauf(const std::string& waren_key, uint32_t anzahl, uint8_t qualitaet);

    /// Getter: Wieviel Stück der gegebenen Ware kauft der Markt maximal an?
    unsigned get_bedarf(const std::string& auswahl_ware);

    /// Getter: Preis für gegebene (enum) Ware mit gegebener Qualität und Quantität.
    uint32_t get_preis(const std::string& waren_key, uint32_t anzahl);

    /// Liefert den aktuellen, durchschnittlichen Preisfaktor aller Waren.
    float get_preisniveau() const;

    /// Serialisiert einen Markt beispielsweise in einen `std::stringstream`.
    friend std::ostream& operator<<(std::ostream& os, const Markt& markt);

};
