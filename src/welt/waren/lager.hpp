#pragma once

#include "vorrat.hpp"
#include "../../netzwerk/netzwerk.hpp"
#include "ware.hpp"
#include "lagertransport.hpp"

#include <cstdint>
#include <sstream>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/string.hpp>

class Markt;

/**
 * Ein Lager ist der Speicherort für Waren, deren Art und Anzahl.
 * Wird auch vom Markt genutzt, welcher der Einfachheit halber vom Lager erbt.
 */
class Lager {

    friend class Welt;

public:

    /// Verwaltet den automatischen Einkauf/Verkauf von waren im Lager.
    struct Automatik_Kauf {
        bool aktiv = false;
        std::unordered_map<std::string, std::tuple<uint16_t, uint16_t>> ware_menge_preis; // Warenkey, Menge, Preis
        template<class Archive> void serialize(Archive& ar) { ar(aktiv, ware_menge_preis); }
    };

    /// Maximale Anzahl jeder Ware.
    static const unsigned MAX = 100;

    /// Liefert eine Liste aller geladenen Keys von Waren. Wird beim ersten Aufruf gefüllt.
    static const std::vector<std::string>& alle_waren_keys();

    /// Liefert eine Liste aller geladenen Keys von Waren. Wird beim ersten Aufruf gefüllt. Sortiert.
    static const std::vector<std::string>& alle_waren_keys_sortiert();

    /// Liefert den Namen der Ware als String aus dem `enum Ware`.
    static const std::string& get_warenname(const std::string& waren_key);

    /**
     * Konstruktor erzeugt Lager mit gegebener ID.
     * Vergibt an Waren einen Basispreis je nach Typ.
     * @param id
     */
    explicit Lager(Netzwerk::id_t id = 0);

    /// Konstruktor erzeugt ein Lager aus (mit <<) serialisierten Daten.
    explicit Lager(std::stringstream& daten);

    /// Getter: ID.
    Netzwerk::id_t get_id() const { return id; }

    /// Getter mit Schreibzugriff: Vorrat (über den key der Ware).
    Vorrat& get_vorrat(const std::string& waren_key);

    /// Getter: Alle Vorräte `const`.
    const std::unordered_map<std::string, Vorrat>& get_vorraete() const { return vorraete; }

    /// Getter: Transport, der vom Lager aus ausgeführt wird.
    const Lagertransport& get_transport() const { return lagertransport; }

    /// Leert alle Vorräte.
    void leeren();

    /// Startet einen neuen Transport. False, wenn nicht genügend Waren zum losschicken.
    bool start_transport(Netzwerk::id_t id_dynastie, Netzwerk::id_t ziel, bool transport,
                         const std::unordered_map<std::string, uint16_t>& waren_hin,
                         const std::unordered_map<std::string, uint16_t>& waren_zurueck);

    /// Serialisiert ein Lager.
    friend std::ostream& operator<<(std::ostream& os, const Lager& lager);

    /// Serialisierung via cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(id, vorraete, lagertransport, auto_einkauf, auto_verkauf);
    }

protected:

    /// Aktualisiert die Statistik. Tagestick.
    void tick_statistik();

    /// Führt einen Simulationsschritt durch. Tagestick.
    void tick_lagerverwaltung(Markt* markt, Land* land, Stadt* stadt, Dynastie* dynastie, Statistik* statistik);

protected:

    Netzwerk::id_t id;

    /// Key = Waren-Key; Value = Vorrat der Ware.
    std::unordered_map<std::string, Vorrat> vorraete;

    /// Transport, der vom Lager aus geht.
    Lagertransport lagertransport;

public:

    Automatik_Kauf auto_einkauf{};
    Automatik_Kauf auto_verkauf{};

};
