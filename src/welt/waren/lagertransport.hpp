#pragma once

#include "vorrat.hpp"
#include "../../netzwerk/netzwerk.hpp"
#include "../spieler/dynastie.hpp"
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/string.hpp>
#include <ostream>

// Prädeklarationen.
class Lager;
class Land;

/// Wird verwendet um Waren von Lager <-> Lager oder Lager <-> Markt zu transportieren/exportieren/importieren.
class Lagertransport final {

    friend class Lager;
    friend class Welt;

    /// Mögliche Ereignisse bei Ticks.
    enum Ereignis { KEIN, ANKUNFT_ZIEL, ANKUNFT_HEIM };

public:

    /// Default-Ctor.
    Lagertransport() = default;

    /// Deserialisierender Ctor.
    explicit Lagertransport(const std::string& daten);

    /// Ist der Transport gerade unterwegs?
    bool unterwegs() const { return restdauer > 0; }

    /// Liefert den Fortschritt als Bruch.
    float fortschritt() const;

    /// Befindet sich der Transport auf der Hinreise?
    bool is_hinfahrt() const { return hinfahrt; }

    /// Serialisierung.
    friend std::ostream& operator<<(std::ostream& os, const Lagertransport& lagertransport);

    /// Serialisierung via cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(waren_hin, waren_zurueck, reisedauer, restdauer, transport, hinfahrt, id_spieler, id_ziel);
    }

private:

    /// Verringert die Restdauer um 1. Ist etwas passiert? Bestimmt durch Restreisedauer.
    Ereignis tick();

    /// Miniticks.
    void tick(Dynastie* spieler, Lager* ursprung, Lager* ziel_lager, Land* land, Statistik* statistik);

    /// Wird bei halber Reisedauer ausgeführt.
    void ankunft_ziel(Dynastie* spieler, Lager* ziel_lager, Land* land, Statistik* statistik);

    /// Wird bei Ankunft daheim ausgeführt.
    void ankunft_zuhause(Dynastie* spieler, Lager* heim);

    /// Warenkeys + Anzahl Hinfahrt.
    std::unordered_map<std::string, Vorrat> waren_hin;

    /// Warenkeys + Anzahl Rückfahrt.
    std::unordered_map<std::string, Vorrat> waren_zurueck;

    /// Gesamtdauer der Reise.
    int16_t reisedauer;

    /// Verbleibende Reisedauer. < 0 = Inaktiv.
    int16_t restdauer;

    /// Wenn true, Ziel = eigenes Lager, sonst: Markt.
    bool transport;

    /// Wenn true = Hinfahrt, sonst Rückfahrt.
    bool hinfahrt;

    /// ID des versendenden Spielers.
    Netzwerk::id_t id_spieler;

    /// ID des Ziellagers (oder Marktes).
    Netzwerk::id_t id_ziel;

};
