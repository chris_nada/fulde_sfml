#include "lager.hpp"
#include "../../lingua/lingua.hpp"
#include "markt.hpp"
#include "../../werkzeuge/etc_cereal.hpp"
#include <nada/log.hpp>

using nada::Log;

Lager::Lager(Netzwerk::id_t id) :
    id(id), lagertransport()
{
    for (const std::string& waren_key : alle_waren_keys()) {
        vorraete[waren_key] = Vorrat();
    }
}

Lager::Lager(std::stringstream& daten) {
    etc::serial_in in(daten);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Lager& lager) {
    etc::serial_out boa(os);
    boa(lager);
    return os;
}

Vorrat& Lager::get_vorrat(const std::string& waren_key) {
    if (vorraete.count(waren_key) == 0) vorraete[waren_key] = Vorrat(false);
    return vorraete[waren_key];
}

const std::string& Lager::get_warenname(const std::string& key) {
    return TXT::get(key);
}

void Lager::leeren() {
    for (auto& v : vorraete) {
        v.second.menge = 0;
    }
}

const std::vector<std::string>& Lager::alle_waren_keys() {
    static std::vector<std::string> waren; // Cache
    if (waren.empty()) for (const auto& ware : Ware::alle()) waren.push_back(ware.second.key);
    return waren;
}

void Lager::tick_statistik() {
    /// Vorräte tick, Statistik
    for (auto& v : vorraete) v.second.tick(v.first);
}

void Lager::tick_lagerverwaltung(Markt* markt, Land* land, Stadt* stadt, Dynastie* dynastie, Statistik* statistik) {
    tick_statistik();
    // Automatisch Einkaufen
    if (auto_einkauf.aktiv) {
        for (const std::pair<const std::string, std::tuple<uint16_t, uint16_t>>& ware_menge_preis : auto_einkauf.ware_menge_preis) {
            const std::string& warenkey = ware_menge_preis.first;
            const auto menge = std::get<0>(ware_menge_preis.second);
            const auto preis = std::get<1>(ware_menge_preis.second);

            // Einkauf vornehmen?
            Vorrat& vorrat = get_vorrat(warenkey);
            if (vorrat.menge < menge && markt->get_preis(warenkey, 1) <= preis) {
                const unsigned menge_kaufen = std::min(menge - vorrat.menge, markt->get_vorrat(warenkey).menge);
                if (menge_kaufen > 0 && markt->kauf(warenkey, menge_kaufen, dynastie, this, land, stadt, statistik)) {
                    vorrat.einlagern(menge_kaufen, markt->get_vorrat(warenkey).quali); // erfolg
                }
            }
        }
    }
    // Automatisches Verkaufen
    if (auto_verkauf.aktiv) {
        for (const std::pair<const std::string, std::tuple<uint16_t, uint16_t>>& ware_menge_preis : auto_verkauf.ware_menge_preis) {
            const std::string& warenkey = ware_menge_preis.first;
            const auto menge = std::get<0>(ware_menge_preis.second);
            const auto preis = std::get<1>(ware_menge_preis.second);
            Vorrat& vorrat = get_vorrat(warenkey);

            // Bedarf vorhanden? Preis in Ordnung? Dann verkaufen.
            if (vorrat.menge > menge && markt->get_vorrat(warenkey).get_preis(warenkey, 1, vorrat.quali) >= preis) {
                const unsigned bedarf = markt->get_bedarf(warenkey);
                const unsigned menge_verkauf = std::min(bedarf, vorrat.menge - menge);
                if (menge_verkauf > 0) {
                    if (vorrat.entnehmen(menge_verkauf)) {
                        markt->verkauf(warenkey, menge_verkauf, vorrat.quali);
                        const uint32_t gesamtpreis = markt->get_vorrat(warenkey).get_preis(warenkey, menge_verkauf, vorrat.quali);
                        dynastie->geld_erhalten(gesamtpreis, Statistik::Einnahme::WARENVERKAUF, *statistik);
                    }
                }
            }
        }
    }
}

const std::vector<std::string>& Lager::alle_waren_keys_sortiert() {
    static std::vector<std::string> waren;
    if (waren.empty()) {
        waren = alle_waren_keys();
        std::sort(waren.begin(), waren.end(), [](const std::string& lhs, const std::string& rhs) {
            const Ware& ware1 = Ware::get(lhs);
            const Ware& ware2 = Ware::get(rhs);
            return ware1.get_basispreis() < ware2.get_basispreis();
        });
    }
    return waren;
}

bool Lager::start_transport(Netzwerk::id_t id_dynastie, Netzwerk::id_t ziel, bool transport,
                            const std::unordered_map<std::string, uint16_t>& waren_hin,
                            const std::unordered_map<std::string, uint16_t>& waren_zurueck) {
    Log::debug() << "Lager::" << __func__ << Log::endl;

    // Genug vorhanden?
    for (const auto& paar : waren_hin) {
        const Vorrat& v = get_vorrat(paar.first);
        if (v.menge < paar.second) return false;
    }

    // Transport beladen
    Lagertransport trans = Lagertransport();
    trans.id_spieler = id_dynastie;
    for (const auto& paar : waren_hin) {
        Vorrat vorrat = get_vorrat(paar.first).entnehmen_als_vorrat(paar.second);
        trans.waren_hin[paar.first] = vorrat;
    }

    // Zu verkaufendes eintragen (als Platzhalter Vorrat)
    for (const auto& paar : waren_zurueck) {
        Vorrat vorrat(false);
        vorrat.menge = paar.second;
        trans.waren_zurueck[paar.first] = vorrat;
    }

    trans.hinfahrt = true;
    trans.reisedauer = 10; // TODO berechnen
    trans.restdauer = trans.reisedauer;
    trans.transport = transport;
    trans.id_ziel = ziel;
    lagertransport = trans;
    return true;
}
