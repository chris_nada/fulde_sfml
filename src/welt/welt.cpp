#include "welt.hpp"
#include "gegner/gegner.hpp"
#include <cereal/types/vector.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/unordered_map.hpp>
#include <nada/log.hpp>

using nada::Log;

Welt::Welt() {
    Log::debug() << "Welt()" << Log::endl;

    // Pro Kultur 1 Land erzeugen
    kulturen = Kultur::alle();
    for (Kultur& kultur : kulturen) {
        Log::debug() << "\tKultur " << kultur.get_key() << " erzeugt Nation... " << Log::flush;
        Land& land = laender.emplace_back(laender.size(), kultur.get_id(), kultur.get_random_nation());
        Log::debug() << land.get_name() << Log::endl;

        // Ländern Städte hinzufügen
        for (unsigned int i = 0; i < N_STAEDTE; ++i) {

            // Jeder Stadt einen Markt hinzufügen
            Log::debug() << "\tMarkt " << i << "... " << Log::flush;
            Markt& markt = maerkte.emplace_back(maerkte.size());

            // Stadt erzeugen
            Log::debug() << " Stadt " << i << "... " << Log::endl;
            Stadt& stadt = staedte.emplace_back(staedte.size(), markt.get_id(), land.get_id(), kultur.get_random_ortschaft());
            land.staedte.push_back(stadt.get_id());

            // Jeder Stadt Viertel hinzufügen
            stadt.id_viertel_kirche        = nada::random::ui(0, N_VIERTEL - 1);
            stadt.id_viertel_rathaus       = nada::random::ui(0, N_VIERTEL - 1);
            stadt.id_viertel_bank          = nada::random::ui(0, N_VIERTEL - 1);
            stadt.id_viertel_schattengilde = nada::random::ui(0, N_VIERTEL - 1);
            for (unsigned j = 0; j < N_VIERTEL; ++j) {
                Log::debug() << "\t\tViertel " << j << "..." << Log::endl;
                Viertel& neues_viertel = viertel.emplace_back(viertel.size(), stadt.get_id(), kultur.get_random_viertel());
                stadt.add_viertel(neues_viertel.get_id());

                // Viertel bebauen
                auto add_gebaeude = [&](Gebaeude::Typ typ, Netzwerk::id_t& link) {
                    Gebaeude& g = gebaeude.emplace_back(typ);
                    g.set_id(gebaeude.size()-1);
                    g.set_position(neues_viertel.add_gebaeude(g.get_id()));
                    link = neues_viertel.get_id();
                    return g;
                };
                if (j == stadt.get_id_viertel_kirche())        add_gebaeude(Gebaeude::Typ::KIRCHE,        stadt.id_viertel_kirche);
                if (j == stadt.get_id_viertel_rathaus())       add_gebaeude(Gebaeude::Typ::RATHAUS,       stadt.id_viertel_rathaus);
                if (j == stadt.get_id_viertel_bank())          add_gebaeude(Gebaeude::Typ::BANK,          stadt.id_viertel_bank);
                if (j == stadt.get_id_viertel_schattengilde()) add_gebaeude(Gebaeude::Typ::SCHATTENGILDE, stadt.id_viertel_schattengilde);
            }

            // Marktgebäude einrichten
            Viertel& marktviertel = viertel.at(*stadt.get_viertel().begin());
            Gebaeude& neues_gebaeude = gebaeude.emplace_back(Gebaeude::Typ::MARKT);
            neues_gebaeude.set_id(gebaeude.size()-1);
            neues_gebaeude.id_link = markt.get_id();
            neues_gebaeude.set_position(marktviertel.add_gebaeude(neues_gebaeude.id));
            marktviertel.id_markt = markt.get_id();
            stadt.id_viertel_markt  = marktviertel.get_id();
            stadt.id_markt_gebaeude = neues_gebaeude.get_id();

            // In jeder Stadt Gegner erzeugen
            for (const auto& warengruppe : Ware::get_gruppen()) {
                gegner.emplace_back(gegner.size(), *this, kultur.get_id(), stadt.id, warengruppe);
            }

        }
    }
    // Ämter einlesen + auf Städte / Länder verteilen
    for (const Amt& amt : Amt::alle()) {
        switch (amt.get_ebene()) {
            case Amt::STADT:
                for (const Stadt& stadt : staedte) aemter[Amt::STADT][stadt.get_id()].push_back(amt);
                break;
            case Amt::LAND:
                for (const Land& land : laender) aemter[Amt::LAND][land.get_id()].push_back(amt);
                break;
        }
    }
    Log::debug() << '\t' << aemter[Amt::STADT].size() << " Aemter auf Ebene Stadt und\n";
    Log::debug() << '\t' << aemter[Amt::LAND].size()  << " Aemter auf Ebene Land erzeugt." << Log::endl;
}

Welt::~Welt() {
    //Log::debug() << "~Welt()" << Log::endl;
}

Welt::Welt(std::stringstream& ss) {
    etc::serial_in in(ss);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Welt& welt) {
    etc::serial_out out(os);
    out(welt);
    return os;
}

Netzwerk::id_t Welt::neuer_spieler(const std::string& vorname, const std::string& nachname, bool maennlich, int viertel_id, bool add_wohnhaus, bool computergesteuert) {

    // Dynastie generieren
    Netzwerk::id_t id = dynastien.size();
    dynastien.emplace_back(id, nachname);
    Dynastie& temp_dynastie = dynastien.back();
    statistiken.emplace_back(statistiken.size());
    temp_dynastie.computergesteuert = computergesteuert;

    // Viertel auswählen
    Viertel& temp_viertel = (viertel_id < 0 || (unsigned) viertel_id >= viertel.size()) ?
                            nada::random::choice(viertel) : viertel[viertel_id];
    const auto& stadt = staedte.at(temp_viertel.get_id_stadt());
    const auto& land  = laender.at(stadt.get_id_land());
    temp_dynastie.id_land = land.get_id();

    // Charakter generieren
    Charakter& neuer_char = charaktere.emplace_back(
            charaktere.size(), laender.at(temp_dynastie.get_id_land()).get_id_kultur(),
            vorname, temp_dynastie.familienname,
            Zeit(0, 8, 1, zeit.get_jahr() - 18), maennlich, Position(land.get_id(), stadt.get_id(), temp_viertel.get_id()),
            std::nullopt, // Arbeiter? nein
            temp_dynastie.get_id()
            );
    neuer_char.computergesteuert = computergesteuert;
    temp_viertel.add_person(neuer_char.get_id());

    // Hilfsmethode für Gebäude // TODO verschieben als Methode
    auto add_gebaeude = [&] (Gebaeude::Typ typ) {
        Gebaeude* temp_gebaeude = &gebaeude.emplace_back(typ);
        const Stadt& stadt = staedte.at(temp_viertel.get_id_stadt());
        temp_gebaeude->set_id(gebaeude.size()-1);
        temp_gebaeude->set_besitzer(temp_dynastie.get_id());
        auto g_pos_x = viertel.at(neuer_char.get_pos().viertel).add_gebaeude(temp_gebaeude->get_id());
        if (g_pos_x == 0) { // Ausweichviertel finden
            for (auto idv : stadt.get_viertel()) {
                Viertel& v = viertel.at(idv);
                g_pos_x = v.add_gebaeude(temp_gebaeude->get_id());
                if (g_pos_x != 0) {
                    Log::debug() << "Gebaeude musste in Viertel " << v.get_name();
                    Log::debug() << " gebaut werden, weil kein Platz in " << temp_viertel.get_name() << '\n';
                    break;
                }
            }
            Log::err() << "Kein Platz fuer " << temp_gebaeude->get_typ_name() << " in Stadt " << stadt.get_name() << '\n';
        }
        temp_gebaeude->set_position(g_pos_x);
        return temp_gebaeude;
    };

    // Gebäude errichten und zuweisen
    if (add_wohnhaus) {
        Gebaeude* g_heim = add_gebaeude(Gebaeude::Typ::WOHNHAUS);
        temp_dynastie.gebaeude.insert(g_heim->get_id());
    }

    // Lager verknüpfen
    Gebaeude* g_lager = add_gebaeude(Gebaeude::Typ::LAGER);
    Lager& temp_lager = lager.emplace_back(lager.size());
    g_lager->id_besitzer = temp_dynastie.get_id();
    temp_lager.leeren();
    temp_dynastie.lager[temp_viertel.get_id_stadt()] = temp_lager.get_id();
    temp_dynastie.gebaeude.insert(g_lager->get_id());
    g_lager->set_link(temp_lager.get_id());

    // Werkstatt verknüpfen
    Gebaeude* g_werk = add_gebaeude(Gebaeude::Typ::WERKSTATT);
    Werkstatt& temp_werk = werkstaetten.emplace_back(werkstaetten.size());
    g_werk->set_link(temp_werk.get_id());
    g_werk->id_besitzer = temp_dynastie.get_id();
    temp_dynastie.gebaeude.insert(g_werk->get_id());

    // Konfigurieren
    temp_dynastie.set_oberhaupt(neuer_char.get_id());
    temp_dynastie.geld_erhalten(2000, Statistik::Einnahme::SPENDEN, statistiken.at(temp_dynastie.get_id()));

    // Willkommensnachricht schreiben
    Nachricht::add(nachrichten, temp_dynastie, zeit, Nachricht::Inhalt::WILLKOMMEN);
    Nachricht::add(nachrichten, temp_dynastie, zeit, Nachricht::Inhalt::WILLKOMMEN);
    Nachricht::add(nachrichten, temp_dynastie, zeit, Nachricht::Inhalt::WILLKOMMEN);
    return temp_dynastie.get_id();
}

void Welt::set_size(unsigned n_staedte, unsigned n_viertel) {
    N_STAEDTE = n_staedte;
    N_VIERTEL = n_viertel;
}

void Welt::tick(uint8_t minuten) {
    const bool neuer_tag  = zeit.add_min(minuten);
    const bool neues_jahr = zeit.get_jahreszeit() == 1;
    Zeit::SERVER_JAHR = zeit.get_jahr();

    // Ticks Dynastien
    for (Dynastie& dynastie : dynastien) {
        for (auto id_stadt_id_lager : dynastie.lager) {

            // Lager/Transport
            Lager& lag = lager[id_stadt_id_lager.second];
            Lager& temp_ziel = lag.lagertransport.transport ? lager[lag.lagertransport.id_ziel] : maerkte[lag.lagertransport.id_ziel];
            Stadt& stadt = staedte[id_stadt_id_lager.first];
            lag.lagertransport.tick(&dynastie, &lag, &temp_ziel, &laender.at(stadt.get_id_land()), &statistiken.at(dynastie.get_id()));

            // Viertel
            for (auto viertel_id : stadt.get_viertel()) {
                Viertel* vier = &viertel[viertel_id];

                // Werkstätten ticks
                for (auto gebaeude_id : vier->get_gebaeude()) {
                    Gebaeude* geb = &gebaeude[gebaeude_id];
                    if (geb->get_besitzer() != dynastie.get_id()) continue;
                    if (geb->get_typ() != Gebaeude::Typ::WERKSTATT) continue;
                    Werkstatt* werk = &werkstaetten[geb->get_id_link()];
                    werk->tick(this, &dynastie, &lag, &maerkte.at(stadt.get_id_markt()), &laender.at(stadt.get_id_land()), &stadt,
                               minuten, neuer_tag, zeit, statistiken.at(dynastie.get_id()));
                }
            }
        }
        // Dynastie Quartalsticks (=neues Jahr)
        if (neuer_tag) dynastie.tick(statistiken.at(dynastie.get_id()), laender.at(dynastie.get_id_land()));
    }

    // Jahreszeitenticks
    if (neuer_tag) {
        Log::debug() << "Welt::" << __func__ << ' ' << (int) zeit.get_jahreszeit() << '/' << zeit.get_jahr() << Log::endl;

        // Neues Jahr
        if (neues_jahr) {
            for (Statistik& ds : statistiken) ds.tick_jahr();

            // Ämter: Wahlen
            std::vector<Amt*> temp_aemter = Amt::get_aemter_aus_welt(*this);
            for (Amt* amt : temp_aemter) amt->wahl(*this);
        }

        // Computergegner Tagesticks
        for (auto& g : gegner)     g.tick_tag(*this);
        for (auto& c : charaktere) c.tick_tag(this); // muss nach der Wahl sein, wegen Neubewerbungen
    }

    // "Schnelle" Ticks (mehrfach pro Sekunde)
    for (Land& land : laender) {
        for (auto stadt_id : land.get_staedte()) {
            Stadt* stadt = &staedte[stadt_id];
            Markt* markt = &maerkte[stadt->get_id_markt()];
            if (neuer_tag) {
                stadt->tick(this, &kulturen[land.get_id_kultur()], &land);
                markt->tick_statistik();
                // Zufällige Produktion gegen kompletten Ausverkauf
                for (auto& paar : markt->vorraete) if (paar.second.menge < 10) paar.second.menge += nada::random::ui(0, 10);
            }

            // Viertel
            for (auto viertel_id : stadt->get_viertel()) {
                Viertel* vier = &viertel[viertel_id];

                // Zivilisten: spawnen
                for (unsigned i = 0; i < minuten; ++i) if (stadt->zivilist_erzeugen(zeit)) {
                    Position start(land.get_id(), stadt_id, viertel_id);
                    Position ziel(land.get_id(),  stadt_id, stadt->id_viertel_markt);
                    start.x = nada::random::b(50) ? -5000.f : 5000.f;
                    //start.x = Zufall::b(50) ? vier->get_grenze(Position::LINKS) : vier->get_grenze(Position::RECHTS);
                    ziel.x = viertel[stadt->get_id_viertel_markt()].get_gebaeude_x(stadt->get_id_markt_gebaeude());
                    vier->zivilisten.push_front(Zivilist(kulturen[land.get_id_kultur()], start, ziel, *vier));
                }
                // Zivilisten: tick
                for (auto it = vier->zivilisten.begin(); it != vier->zivilisten.end();) {
                    const auto viertel_alt = it->pos.viertel;
                    if (it->tick(*stadt, *markt, *vier)) { // Angekommen?
                        vier->zivilisten.erase(it++);
                    } else if (it->pos.viertel != viertel_alt) { // Viertel gewechselt?
                        viertel[it->pos.viertel].zivilisten.push_front(*it);
                        vier->zivilisten.erase(it++);
                    } else ++it;
                }

                /// Gebäude
                if (neuer_tag) for (auto gebaeude_id : vier->get_gebaeude()) {
                    Gebaeude& geb = gebaeude[gebaeude_id];
                    switch (geb.get_typ()) {
                        case Gebaeude::Typ::LAGER: {
                            Lager& l = lager[geb.get_id_link()];
                            l.tick_lagerverwaltung(markt, &land, stadt, &dynastien[geb.get_besitzer()],
                                                    &statistiken[geb.get_besitzer()]);
                            } break;
                        default: break;
                    }
                }
            }
        }
    }
}

void Welt::speichern(const std::string& datei) const {
    if (std::ofstream out(datei); out.good()) {
        out << *this;
        Log::out() << "Welt gepeichert in " << datei << '\n';
    }
    else Log::err() << "Speichern fehlgeschlagen, kein Dateizugriff\n";
}

void Welt::a_transfer_arbeiter(Netzwerk::id_t arbeiter_id, bool stadt_zu_werk, Netzwerk::id_t id_werk, Netzwerk::id_t id_stadt) {
    Stadt* stadt = &staedte.at(id_stadt);
    Werkstatt* werk = &werkstaetten.at(id_werk);
    auto& pool_von  = stadt_zu_werk ? stadt->arbeiter : werk->freie_arbeiter;
    auto& pool_nach = stadt_zu_werk ? werk->freie_arbeiter : stadt->arbeiter;
    if (pool_von.count(arbeiter_id) && pool_nach.count(arbeiter_id) == 0) {
        pool_nach.insert(arbeiter_id);
        pool_von.erase(arbeiter_id);
    } else Log::err() << "\tArbeiter nicht gefunden oder doppelt: " << arbeiter_id << '\n';;
}

void Welt::skip(unsigned int jahreszeiten) {
    // Vorsimulieren; x * 1440 = 60 * 24 also x Jahreszeiten in Minuten.
    const unsigned dauer_tag = 60 * 24;
    auto skip_jahreszeiten = [&] () {
        for (unsigned i = 0; i < (jahreszeiten * dauer_tag) / MIN_PRO_TICK; ++i) tick();
    };
    const std::string name = std::to_string(jahreszeiten) + " Jahreszeiten uebersprungen (Welt::skip)";
    Log::benchmark_debug(skip_jahreszeiten, name);
}
