#pragma once

#include "land.hpp"
#include "zeit.hpp"
#include "kultur.hpp"
#include "stadt/stadt.hpp"
#include "spieler/dynastie.hpp"
#include "bauten/wohnhaus.hpp"
#include "bauten/gebaeude.hpp"
#include "produktion/werkstatt.hpp"
#include "spieler/charakter.hpp"
#include "stadt/zivilist.hpp"
#include "statistik.hpp"
#include "politik/amt.hpp"
#include "gegner/gegner.hpp"

/// Prädeklaration für `friend`-Beziehung. Vermeidet so zusätzliches `include`.
class Host;

/**
 * Enthält alle simulierten Daten, die die Welt im Spiel abbilden.
 */
class Welt final {

    /* Die Welt hat viele Freunde. (Für tick()s und KI-Logik) */
    friend class Test_Welt_Generierung;
    friend class Test_Welt_Gegner;
    friend class Test_Netzwerk;
    friend class Host;
    friend class Gegner;
    friend class Charakter;
    friend class Amt;
    friend class Stadt;
    friend class Werkstatt;
    friend class Anlage;

    static inline nada::Ini cfg{"config.ini"};

    /// Minuten je `tick()`. Maximum = 190.
    static inline unsigned MIN_PRO_TICK = cfg.get_int<unsigned>("min_pro_tick", 45);

    /// Zu generierende Städte pro land.
    static inline unsigned N_STAEDTE = cfg.get_int<unsigned>("staedte_pro_land", 3);

    /// Zu generierende Viertel pro Stadt.
    static inline unsigned N_VIERTEL = cfg.get_int<unsigned>("viertel_pro_stadt", 2);

public:

    /// Bestimmt die Größe *neu erstellter* Welten.
    static void set_size(unsigned n_staedte, unsigned n_viertel);

    /**
     * Erzeugt eine zufällige neue Welt.
     */
    Welt();

    /**
     * Erstellt eine Welt-Instanz aus zuvor serialisierten Daten (in Form vom `std::string`). Unkomprimiert.
     * @param serialisiert
     */
    explicit Welt(std::stringstream& daten);

    /**
     * Erstellt einen neuen Spieler (Dynastie) mit gegebenen Parametern, fügt ihn dem Spiel hinzu
     * und gibt die zugewiesene ID zurück.
     * @param vorname Vorname des ersten Charakters.
     * @param nachname Nachname, Dynastiename, Familienname.
     * @param maennlich true = Erster Charakter männlich : sonst weiblich.
     * @return ID, die dem Spieler zugewiesen wurde.
     */
    Netzwerk::id_t neuer_spieler(const std::string& vorname, const std::string& nachname, bool maennlich, int viertel_id, bool add_wohnhaus = true, bool computergesteuert = true);

    /**
     * Getter: Zeit.
     * @note Tag == Quartal == Jahreszeit.
     */
    const Zeit& get_zeit() const { return zeit; };

    /// Destruktor.
    ~Welt();

    /**
     * Führt einen Simulationsschritt durch.
     * @param minuten Anzahl zu simulierender Minuzen. Muss <= 190 sein.
     * @note Tag == Quartal == Jahreszeit.
     */
    void tick(uint8_t minuten = MIN_PRO_TICK);

    /// Simuliert gg. Zeitraum in x Jahreszeiten weiter.
    void skip(unsigned jahreszeiten);

    /// Speichert diese Welt in eine Datei mit gegebenen Pfad ab.
    void speichern(const std::string& datei) const;

    /// Schreibt die Welt in einen Stream.
    friend std::ostream& operator<<(std::ostream& os, const Welt& welt);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(aemter, charaktere, dynastien, gebaeude, gegner, kulturen, laender, lager,
           maerkte, nachrichten,  viertel, werkstaetten, wohnhaeuser,
           staedte, statistiken,  zeit
           );
    }

private:

    /** 
     * Transferiert einen Arbeiter mit gegebenem Hash entweder von einer Stadt zu einem Werk,
     * wenn `stadt_zu_werk = true` oder von einem Werk zur Stadt, wenn `false`.
     */
    void a_transfer_arbeiter(Netzwerk::id_t arbeiter_id, bool stadt_zu_werk, Netzwerk::id_t id_werk, Netzwerk::id_t id_stadt);

    Zeit                            zeit;
    std::vector<Kultur>             kulturen;
    std::vector<Land>               laender;
    std::vector<Stadt>              staedte;
    std::vector<Dynastie>           dynastien;
    std::vector<Statistik>  statistiken;
    std::vector<Markt>              maerkte;
    std::vector<Viertel>            viertel;
    std::vector<Gebaeude>           gebaeude;
    std::vector<Werkstatt>          werkstaetten;
    std::vector<Wohnhaus>           wohnhaeuser;
    std::vector<Lager>              lager;
    std::vector<Charakter>          charaktere;
    std::vector<Gegner>             gegner; // Computergegner
    std::unordered_map<Netzwerk::id_t, std::list<Nachricht>> nachrichten;
    std::unordered_map<Amt::Ebene, std::unordered_map<Netzwerk::id_t, std::vector<Amt>>> aemter; // [Ebene][Stadt]->Ämter

};
