#pragma once

#include "../netzwerk/netzwerk.hpp"

#include <unordered_map>
#include <deque>
#include <cereal/types/deque.hpp>
#include <cereal/types/unordered_map.hpp>

///
class Statistik final {

public:

    ///
    enum class Einnahme : uint8_t {
        WARENVERKAUF = 0,
        GEHALT       = 1,
        SPENDEN      = 2,
        WERKRUECKBAU = 20,
    };

    ///
    enum class Ausgabe : uint8_t {
        WARENKAUF        = 0,
        GEHALT           = 1,
        SPENDEN          = 2,
        STEUER_ERTRAG    = 10,
        STEUER_MEHRWERT  = 11,
        WERKAUSBAU       = 20,
    };

    /// Für wieviele Schritte soll eine Statistik geschrieben werden?
    static constexpr unsigned MAX = 20;

    /// Kann zum Iterieren über die Einnahmen genutzt werden.
    static constexpr std::array<Einnahme, 4> EINNAHMEN = {
            Einnahme::WARENVERKAUF,
            Einnahme::GEHALT,
            Einnahme::SPENDEN,
            Einnahme::WERKRUECKBAU
    };

    /// Kann zum Iterieren über die Ausgaben genutzt werden.
    static constexpr std::array<Ausgabe, 6> AUSGABEN = {
            Ausgabe::WARENKAUF,
            Ausgabe::GEHALT,
            Ausgabe::SPENDEN,
            Ausgabe::STEUER_ERTRAG,
            Ausgabe::STEUER_MEHRWERT,
            Ausgabe::WERKAUSBAU
    };

    /// Liefert gegebenen enum als übersetzten Text.
    static const std::string& get_txt(Einnahme einnahme);

    /// Liefert gegebenen enum als übersetzten Text.
    static const std::string& get_txt(Ausgabe ausgabe);

public:

    /// Default-Konstruktor.
    Statistik() = default;

    /// Ctor. Fügt für alle Statistiken ein Anfangsjahr ein.
    Statistik(Netzwerk::id_t id);

    /// Deserialisierender Ctor.
    explicit Statistik(std::stringstream& daten);

    /// Tick, wenn ein Jahr vergangen ist.
    void tick_jahr();

    ///
    void add_produkt(const std::string& warenkey, uint32_t menge);

    ///
    void add_verbrauch(const std::string& warenkey, uint32_t menge);

    ///
    void add_einnahme(Einnahme typ, uint64_t menge);

    ///
    void add_ausgabe(Ausgabe typ, uint64_t menge);

    /// Getter: Anzahl gespeicherter Quartalsstatistiken.
    unsigned size() const { return einnahmen.size(); }

    /// Getter: 0 = Aktuelles Jahr. 1 = Vor einem Jahr usw. Kann nur für `jahr` < MAX Werte > 0 liefern.
    uint64_t get_einnahme(Einnahme typ, unsigned jahr) const;

    /// Getter: 0 = Aktuelles Jahr. 1 = Vor einem Jahr usw. Kann nur für `jahr` < MAX Werte > 0 liefern.
    uint64_t get_ausgabe(Ausgabe typ, unsigned jahr) const;

    /// Getter: 0 = Aktuelles Jahr. 1 = Vor einem Jahr usw. Kann nur für `jahr` < MAX Werte > 0 liefern.
    uint64_t get_produktion(const std::string& warenkey, unsigned jahr) const;

    /// Getter: 0 = Aktuelles Jahr. 1 = Vor einem Jahr usw. Kann nur für `jahr` < MAX Werte > 0 liefern.
    uint64_t get_verbrauch(const std::string& warenkey, unsigned jahr) const;

    /// Getter: Id.
    Netzwerk::id_t get_id() const { return id; }

    /// Getter: Liefert die Einnahmen des aktuellen Quartals.
    const std::unordered_map<Einnahme, uint64_t>& get_aktuelle_einnahmen() const;

    /// Getter: Liefert die Einnahmen des aktuellen Quartals.
    const std::unordered_map<Ausgabe, uint64_t>& get_aktuelle_ausgaben() const;

    /// Serialisiert eine Instanz.
    friend std::ostream& operator<<(std::ostream& os, const Statistik& dynastie);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(id, einnahmen, ausgaben, produziert, verbraucht);
    }

private:

    ///
    Netzwerk::id_t id;

    ///
    std::deque<std::unordered_map<Einnahme, uint64_t>> einnahmen;

    ///
    std::deque<std::unordered_map<Ausgabe, uint64_t>> ausgaben;

    ///
    std::deque<std::unordered_map<std::string, uint32_t>> produziert;

    ///
    std::deque<std::unordered_map<std::string, uint32_t>> verbraucht;

};