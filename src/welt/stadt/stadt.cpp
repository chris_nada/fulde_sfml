#include "stadt.hpp"
#include "../spieler/charakter.hpp"
#include "../welt.hpp"

#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/tuple.hpp>
#include <nada/log.hpp>
#include <nada/random.hpp>

using nada::Log;

Stadt::Stadt(Netzwerk::id_t id, Netzwerk::id_t id_markt, Netzwerk::id_t id_land, std::string name) :
        id(id), id_markt(id_markt), id_land(id_land), name(std::move(name)),
        einwohnerzahl(nada::random::ui(500, 2500))
{
    // Zufällige Produktion / Verbrauch
    for (const std::string& waren_key : Lager::alle_waren_keys()) {
        const Ware& ware = Ware::get(waren_key);
        gewichtung_verbrauch[waren_key]  = ware.get_gewichtung_verbrauch();
        gewichtung_produktion[waren_key] = ware.get_gewichtung_produktion();
        produktion_qualitaet[waren_key]  = ware.get_zufallsquali();
    }
    // Infrastruktur einrichten
    for (const auto& paar : Infrastruktur::alle()) {
        const Infrastruktur& i = paar.second;
        auto infra_log = [&](const std::string& name, unsigned anzahl) {
            Log::debug() << this->name << " initialisiert mit " << anzahl << ' ' << name << '\n';
        };
        if (i.get_versorgt() > 0) { // Versorgende Infrastruktur
            const unsigned min_benoetigt = 1 + einwohnerzahl / i.get_versorgt();
            const unsigned anzahl = i.is_kritisch() ? min_benoetigt : nada::random::ui(0, min_benoetigt);
            infrastruktur.insert({paar.first, anzahl}); // 0 - min
            infra_log(paar.first, anzahl);
        } else { // Sonstige Infrastruktor
            const unsigned bezahlbar = einwohnerzahl / i.get_unterhalt();
            const unsigned anzahl = nada::random::ui(0, bezahlbar / Infrastruktur::alle().size());
            infrastruktur.insert({paar.first, anzahl});
            infra_log(paar.first, anzahl);
        }
    }
}

std::ostream& operator<<(std::ostream& os, const Stadt& stadt) {
    etc::serial_out out(os);
    out(stadt);
    return os;
}

Stadt::Stadt(std::stringstream& ss) {
    etc::serial_in in(ss);
    in(*this);
}

void Stadt::tick(Welt* welt, Kultur* kultur, Land* land) {
    if (!arbeiter.empty()) nada::random::choice_erase(arbeiter); // Zufälligen Arbeiter löschen

    // Neue Arbeiter generieren: Anzahl bestimmen
    const unsigned anzahl = std::max(MAX_ARBEITER, nada::random::ui(0, einwohnerzahl / 100));
    for (unsigned i = 0; i < anzahl && arbeiter.size() < MAX_ARBEITER; ++i) generiere_arbeiter(welt, kultur);
    (void) land; // TODO Benutzen?
}

void Stadt::generiere_arbeiter(Welt* welt, Kultur* kultur) {
    const Arbeiter a(*kultur, *this);
    const bool m = nada::random::b(50);
    const Zeit geboren = welt->get_zeit() - nada::random::get<uint16_t>(16, 50);
    const Position pos(this->get_id_land(), this->get_id(), nada::random::choice(this->get_viertel()));
    const Charakter c(welt->charaktere.size(), kultur->get_id(),
                      m ? kultur->get_random_vorname_m() : kultur->get_random_vorname_f(),
                      kultur->get_random_nachname(),
                      geboren, m, pos,
                      a
    );
    welt->charaktere.push_back(c);
    arbeiter.insert(c.get_id());
}

const std::string& Stadt::get_zufallsware_produktion() const {
    static const std::vector<std::string> zufallswaren = nada::random::get_weights<std::string, unsigned>(gewichtung_produktion);
    return nada::random::choice(zufallswaren);
}

const std::string& Stadt::get_zufallsware_verbrauch() const {
    static const std::vector<std::string> zufallswaren = nada::random::get_weights<std::string, unsigned>(gewichtung_verbrauch);
    return nada::random::choice(zufallswaren);
}

float Stadt::get_quali_produktion(const std::string& waren_key) const {
    try { return produktion_qualitaet.at(waren_key); }
    catch (std::out_of_range& e) { return 1.f; }
}

bool Stadt::zivilist_erzeugen(const Zeit& zeit) const {
    return zeit.get_h() > 6 && zeit.get_h() < 18 && get_einwohnerzahl() >= nada::random::ui(1, 100'000);
}

void Stadt::starte_bau(const Infrastruktur& i, uint8_t anzahl) {
    Infrastruktur::IN_BAU neues_projekt {
            .key = i.get_key(),
            .anzahl = anzahl,
            .restbauzeit = i.get_bauzeit(),
    };
    infra_in_bau.push_back(neues_projekt);
    geld_ausgeben(neues_projekt.anzahl * i.get_baukosten());
}

void Stadt::geld_einnehmen(unsigned betrag) {
    geld += betrag;
}

void Stadt::geld_ausgeben(unsigned int betrag) {
    geld -= betrag;
}
