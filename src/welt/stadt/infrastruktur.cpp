#include "infrastruktur.hpp"
#include "../../werkzeuge/etc.hpp"

#include <numeric>
#include <cassert>
#include <nada/log.hpp>
#include <nada/fs.hpp>

using nada::Log;

const std::unordered_map<std::string, Infrastruktur>& Infrastruktur::alle() {
    static std::unordered_map<std::string, Infrastruktur> infrastruktur;
    if (infrastruktur.empty()) {
        // Einlesen
        const auto& json_dateien = nada::fs::all_files("data/infrastruktur", "json");
        assert(json_dateien.size() > 0);
        infrastruktur.reserve(json_dateien.size());
        for (const std::string& pfad : json_dateien) {
            try {
                std::ifstream in(pfad);
                cereal::JSONInputArchive json(in);
                Infrastruktur i;
                json(cereal::make_nvp("infrastruktur", i));
                infrastruktur[i.key] = i;
            } catch (const std::exception& e) {
                Log::err() << e.what() << '\n';
            }
        }
    }
    return infrastruktur;
}

const Infrastruktur& Infrastruktur::get(const std::string& key) {
    try { return Infrastruktur::alle().at(key); }
    catch (const std::exception& e) {
        Log::err() << "Keine Infrastruktur mit key " << key << Log::endl;
        return alle().at("");
    }
}

unsigned Infrastruktur::calc_unterhalt(const infra_map_t& infra) {
    return summieren(infra,
        [](unsigned x, const infra_map_t::value_type& paar) {
            return x + get(paar.first).unterhalt * paar.second;
        });
}

unsigned Infrastruktur::calc_versorgung(const infra_map_t& infra) {
    unsigned versorgung = 0;
    for (const auto& paar : infra) {
        const auto& infrastruktur = Infrastruktur::get(paar.first);
        if (infrastruktur.get_versorgt() != 0) {
            if (versorgung == 0) versorgung = infrastruktur.get_versorgt() * paar.second;
            else versorgung = std::min(versorgung, infrastruktur.get_versorgt() * paar.second);
        }
    }
    return versorgung;
}

unsigned Infrastruktur::calc_reichtum(const infra_map_t& infra) {
    return summieren(infra,
        [](unsigned x, const infra_map_t::value_type& paar) {
            return x + get(paar.first).reichtum * paar.second;
        });
}

float Infrastruktur::calc_training(const infra_map_t& infra) {
    return summieren(infra,
        [](float x, const infra_map_t::value_type& paar) {
            return x + get(paar.first).garnision_training * paar.second;
        });
}

float Infrastruktur::calc_ausruestung(const infra_map_t& infra) {
    return summieren(infra,
        [](float x, const infra_map_t::value_type& paar) {
            return x + get(paar.first).garnision_ausruestung * paar.second;
        });
}
