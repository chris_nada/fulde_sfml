#pragma once

#include "../../netzwerk/netzwerk.hpp"
#include "zivilist.hpp"
#include <utility>
#include <ostream>
#include <cereal/types/vector.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/unordered_set.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/string.hpp>

/**
 * Eine Stadt setzt sich aus Vierteln zusammen, die einzeln begehbar sind.
 */
class Viertel final {

    friend class Welt;

public:

    /// Maximale Anzahl Gebäude pro Viertel.
    static constexpr unsigned MAX_GEBAEUDE = 30;

    /// Ctor.
    Viertel();

    /// Ctor initialisiert alle Werte.
    Viertel(Netzwerk::id_t id, Netzwerk::id_t id_stadt, std::string name);

    /// Deserialisierender Ctor.
    explicit Viertel(std::stringstream& daten);

    /// Fügt ein Gebäude an gegebener Position hinzu. Gibt zurück, ob Gebäude hinzugefügt werden konnte.
    bool add_gebaeude(Netzwerk::id_t gebaeude_id, int8_t pos);

    /**
     * Fügt ein Gebäude an beliebiger Position hinzu. Gibt Gebäudeposition zurück. 0, bei Fehlschlag.
     */
    int8_t add_gebaeude(Netzwerk::id_t gebaeude_id);

    /// Gibt true zurück, wenn die Person hinzugefügt wurde, sonst false.
    bool add_person(Netzwerk::id_t person_id) { return charaktere.insert(person_id).second; }

    /// Entfernt die Person aus dem Viertel.
    void remove_person(Netzwerk::id_t person_id) { charaktere.erase(person_id); }

    /// Ändert den Reichtum des Viertels um gegebenen Wert. Beschränkt dabei das Resultat auf 0 bis 100.
    void aendere_reichtum(int8_t reichtum);

    /// Getter: ID des Viertels.
    Netzwerk::id_t get_id() const { return id; }

    /// Getter: ID der Stadt.
    Netzwerk::id_t get_id_stadt() const { return id_stadt; }

    /// Getter: Name des Viertels.
    const std::string& get_name() const { return name; }

    /// Getter: Reichtum.
    uint8_t get_reichtum() const { return reichtum; }

    /// Getter: Im Viertel befindliche Bauten. Key = Position; Wert = ID der Bauten.
    const std::unordered_map<int8_t, Netzwerk::id_t>& get_id_gebaeude() const { return gebaeude; }

    /// Getter: Im Viertel befindliche Bauten.
    const std::unordered_map<int8_t, Netzwerk::id_t>& get_gebaeude_map() const { return gebaeude; }

    /// Liefert die letzte freie Position rechts oder links.
    int8_t get_grenze(Position::Ausrichtung richtung) const;

    /// Liefert die *grafische* x-Koordinate der Mitte des gegebenen Gebäudes. 0, wenn Gebäude nicht vorhanden.
    float get_gebaeude_x(Netzwerk::id_t gebaeude_id) const;

    /// Getter: Im Viertel befindliche Bauten.
    std::vector<Netzwerk::id_t> get_gebaeude() const;

    /// Getter: Im Viertel befindliche Charaktere.
    const std::unordered_set<Netzwerk::big_id_t>& get_charaktere() const { return charaktere; }

    /// Getter: Im Viertel befindliche Zivilisten.
    const std::list<Zivilist>& get_zivilisten() const { return zivilisten; }

    /// Getter: Befindet sich im Viertel ein Markt? Liefert `-1`, wenn nicht.
    int get_id_markt() const { return id_markt; }

    /// Getter: Hat das Viertel einen Markt?
    bool hat_markt() const { return id_markt >= 0; }

    /// Serialisiert das Viertel in gg. Stream.
    friend std::ostream& operator<<(std::ostream& os, const Viertel& viertel);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(id, id_stadt, id_markt, reichtum, name, gebaeude, charaktere, zivilisten);
    }

private:

    /// ID des Viertels.
    Netzwerk::id_t id;

    /// ID der Stadt.
    Netzwerk::id_t id_stadt;

    /// ID des Markts. -1, wenn kein Markt.
    int16_t id_markt;

    /// Reichtum der Stadt. 0 - 100, (superarm - superreich). Zufällig zu Beginn.
    uint8_t reichtum;

    /// Name des Viertels.
    std::string name;

    /// Im Viertel befindliche Bauten. Key = Position; Wert = ID der Bauten.
    std::unordered_map<int8_t, Netzwerk::id_t> gebaeude;

    /// Personen, die sich im Viertel (auf der Straße oder in Gebäuden) befinden.
    std::unordered_set<Netzwerk::big_id_t> charaktere;

    /// Zivilisten, die sich im Viertel befinden.
    std::list<Zivilist> zivilisten;

};
