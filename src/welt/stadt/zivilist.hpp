#pragma once

#include "../../netzwerk/netzwerk.hpp"
#include "../spieler/position.hpp"
#include "../spieler/person.hpp"
#include "../kultur.hpp"

#include <nada/random.hpp>
#include <cereal/types/base_class.hpp>

class Stadt;
class Markt;
class Viertel;

/**
 * Kurzlebige Entitäten, die eine bestimmte Aufgabe erfüllen
 * (z.B. eine Ware kaufen/verkaufen) und danach aufhören zu existieren.
 * Sind daher wichtiger Teil der (Wirtschafts-)Simulation.
 */
class Zivilist final : public Person {

    friend class Welt; // Welt ruft tick() auf

public:

    /// Enum der möglichen Aufgaben.
    enum Aufgabe : Netzwerk::enum_t {
        WAREN_KAUF,
        WAREN_VERKAUF,
        WAREN_HANDEL
    };

    /// Ctor.
    Zivilist() = default;

    /// Ctor, alles initialisierend.
    Zivilist(const Kultur& kultur, const Position& start, const Position& ziel, const Viertel& viertel);

    /// Serialisierung via cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(cereal::base_class<Person>(this), aufgabe, geld, ziel);
    }

private:

    /**
     * Simulationsschritt. Z bewegt sich, versucht Aufgabe zu erledigen.
     * @return Liefert `true`, wenn die Aufgabe erledigt ist und der Z vernichtet werden kann.
     */
    bool tick(const Stadt& stadt, Markt& markt, Viertel& viertel);

    /**
     * Wird bei Ankunft am Zielmarkt erledigt.
     * @note Wichtige Funktion der Wirtschaftssimulation.
     */
    void handel(const Stadt& stadt, Markt& markt, Viertel& viertel);

    /// True, wenn angekommen.
    bool gehe_zu_ziel();

    /// Zur Auswahl einer zufälligen Aufgabe.
    static inline const std::vector<Aufgabe> aufgaben_gewichtet = nada::random::get_weights<Aufgabe, unsigned>(
            {
                    {WAREN_KAUF,    18},
                    {WAREN_VERKAUF, 1},
                    {WAREN_HANDEL,  1}
            }
    );

    /// Vergibt die ID für Zivilisten.
    static inline uint32_t id_counter = 0;

    /// Zu erfüllende Aufgabe.
    Aufgabe aufgabe;

    /// Geld, das Z mit sich trägt.
    uint32_t geld;

    /// Zu diesem Ziel soll Z gehen.
    Position ziel;

};
