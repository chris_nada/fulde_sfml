#include "armee.hpp"
#include <nada/random.hpp>

Armee::Armee() :
    anzahl(nada::random::get<decltype(anzahl)>(50, 150)),
    erfahrung  (nada::random::f(1,20) / 100.f),
    ausruestung(nada::random::f(1,10) / 100.f)
{
    //
}

uint32_t Armee::get_kosten_pro_jahreszeit() const {
    return anzahl + static_cast<uint32_t>((static_cast<float>
                    (anzahl) * (erfahrung / 100.f + ausruestung / 100.f)));
}
