#pragma once

#include <unordered_map>
#include <cereal/types/string.hpp>
#include <cereal/archives/json.hpp>

/**
 * \brief Daten für eine einzelne Infrastruktureinheit in einer Stadt.
 * Eine Stadt kann viele solcher Einhalten enthalten.
 */
class Infrastruktur final {

public:

    /// Datentyp, der Anzahl von Infrastruktur in der Stadt speichert.
    using infra_map_t = std::unordered_map<std::string, uint32_t>;

    /// Speichert Informationen zu in Bau befindlicher Infrastruktur für die Stadt-Klasse.
    struct IN_BAU {
        std::string key;
        uint8_t anzahl;
        uint8_t restbauzeit;
        template<class Archive> void serialize(Archive& ar) { ar(key, anzahl, restbauzeit); }
    };

    /// Default Ctor.
    Infrastruktur() = default;

    /// Liefert alle eingelesenen Gebäude.
    static const std::unordered_map<std::string, Infrastruktur>& alle();

    /// Liefert das Gebäude mit gegebenem Key.
    static const Infrastruktur& get(const std::string& key);

    /// Kalkuliert den Unterhalt der gesamten Infrastruktur.
    static unsigned calc_unterhalt(const infra_map_t& infra);

    /**
     * \breif Kalkuliert die Versorgungsschwelle der gesamten Infrastruktur.
     * \note Es zählt der Wert der kleinsten Versorgungsinfrastruktur.
     */
    static unsigned calc_versorgung(const infra_map_t& infra);

    /// Kalkuliert den Reichtumsbonus der gesamten Infrastruktur.
    static unsigned calc_reichtum(const infra_map_t& infra);

    /// Kalkuliert das Training der Garnison der gesamten Infrastruktur.
    static float calc_training(const infra_map_t& infra);

    /// Kalkuliert die Ausrüstungsverbesserung der Garnison der gesamten Infrastruktur.
    static float calc_ausruestung(const infra_map_t& infra);

    /// Getter: key.
    const std::string& get_key() const { return key; }

    /// Getter: Baukosten für 1 Element dieses Typs.
    uint16_t get_baukosten() const { return baukosten; }

    /// Getter: Unterhalt für 1 Element dieses Typs pro Jahr.
    uint16_t get_unterhalt() const { return unterhalt; }

    /// Getter: Versorgt x Einwohner.
    uint16_t get_versorgt() const { return versorgt; }

    /// Getter: Bauzeit in Anzahl Jahreszeiten.
    uint8_t get_bauzeit() const { return bauzeit; }

    /// Getter: Reichtumsteigerung.
    uint8_t get_reichtum() const { return reichtum; }

    /// Getter: Garnisionstraining pro Jahr.
    float get_garnision_training() const { return garnision_training; }

    /// Getter: Garnisionsausrüstung pro Jahr.
    float get_garnision_ausruestung() const { return garnision_ausruestung; }

    /// Getter: Pfad zum Icon.
    const std::string& get_icon() const { return icon; }

    /// Getter: Handelt es sich um kritische Infrastruktur?
    bool is_kritisch() const { return kritisch; }

    /// Serialisierung via Cereal
    template<class Archive> void serialize(Archive& ar) {
        ar(CEREAL_NVP(key), CEREAL_NVP(icon),
           CEREAL_NVP(baukosten), CEREAL_NVP(unterhalt), CEREAL_NVP(versorgt),
           CEREAL_NVP(reichtum), CEREAL_NVP(bauzeit),
           CEREAL_NVP(kritisch),
           CEREAL_NVP(garnision_training), CEREAL_NVP(garnision_ausruestung));
    }

private:

    /// ID + Übersetzungsstring.
    std::string key;

    /// Dateiname des Icons.
    std::string icon;

    /// Kosten für den Bau einer Einheit.
    uint16_t baukosten;

    /// Unterhalt, den die Stadt pro Einheit zahlen muss.
    uint16_t unterhalt;

    /// Kann x Einwohner versorgen.
    uint16_t versorgt;

    /// Bonus Reichtum für die Stadt.
    uint8_t reichtum;

    /// Dauer für den Baue in Jahreszeiten.
    uint8_t bauzeit;

    /// Gibt x Erfahrung pro Zeiteinheit.
    float garnision_training;

    /// Verbessert die Garnisionsausrüstung um x pro Zeiteinheit.
    float garnision_ausruestung;

    /// Kritische Infratstruktur?
    bool kritisch;

};
