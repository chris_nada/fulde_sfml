#pragma once

#include <cstdint>

/**
 * \brief Enthält Informationen über eine Militäreinheit.
 */
class Armee final {

public:

    /// Erzeugt eine nicht leere Armee.
    Armee();

    /// Sold pro Soldat pro Zeiteinheit.
    uint32_t get_kosten_pro_jahreszeit() const;

    /// Anzahl Soldaten in der Einheit.
    uint32_t get_anzahl() const { return anzahl; }

    /// Erfahrung (0.0 bis 1.0) der Einheit.
    float get_erfahrung() const { return erfahrung; }

    /// Ausrüstungslevel (0.0 bis 1.0) der Einheit.
    float get_ausruestung() const { return ausruestung; }

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(anzahl, erfahrung, ausruestung);
    }

private:

    /// Anzahl der Soldaten.
    uint32_t anzahl;

    /// 0.0 bis 1.0 Erfahrung der Soldaten.
    float erfahrung;

    /// 0.0 bis 1.0 Ausrüstungsstärke der Soldaten. Kann durch Infrastruktur verbessert werden.
    float ausruestung;

};
