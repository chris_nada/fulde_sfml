#pragma once

#include "../waren/markt.hpp"
#include "viertel.hpp"
#include "../produktion/arbeiter.hpp"
#include "armee.hpp"
#include "infrastruktur.hpp"

#include <utility>
#include <ostream>
#include <unordered_map>

class Land;
class Welt;

/**
 * Datenabbildung einer simulierten Stadt.
 */
class Stadt final {

    friend class Host;
    friend class Welt;
    friend class Gegner;
    friend class Test_Welt_Generierung;

public:

    /// Maximale Anzahl Arbeitsuchende.
    static constexpr unsigned MAX_ARBEITER = 20;

    /// Jährliches durchschnittliches Bevölkerungswachstum als Faktor.
    static constexpr float WACHSTUMSFAKTOR = 1.01; // TODO

public:

    /// Leerer Konstruktor.
    Stadt() = default;

    /// Konstruktor, initialisiert alle Felder.
    Stadt(Netzwerk::id_t id, Netzwerk::id_t id_markt, Netzwerk::id_t id_land, std::string name);

    /// Deserialisierender Konstruktor.
    explicit Stadt(std::stringstream& ss);

    /// Fügt der Stadt ein Viertel via ID hinzu.
    void add_viertel(Netzwerk::id_t id_viertel) { viertel.insert(id_viertel); }

    /// Name der Stadt.
    const std::string& get_name() const { return name; }

    /// ID der Stadt.
    Netzwerk::id_t get_id() const { return id; }

    /// Liefert die ID des Marktes der Stadt.
    Netzwerk::id_t get_id_markt() const { return id_markt; }

    /// Liefert die ID des Marktgebäudes (das Gebäude, das mit dem Markt verknüpft ist).
    Netzwerk::id_t get_id_markt_gebaeude() const { return id_markt_gebaeude; }

    /// Liefert die ID des Viertels, in dem sich der Markt der Stadt befindet.
    Netzwerk::id_t get_id_viertel_markt() const { return id_viertel_markt; }

    /// Liefert die ID des Viertels, in dem sich die Kirche der Stadt befindet.
    Netzwerk::id_t get_id_viertel_kirche() const { return id_viertel_kirche; }

    /// Liefert die ID des Viertels, in dem sich das Rathaus der Stadt befindet.
    Netzwerk::id_t get_id_viertel_rathaus() const { return id_viertel_rathaus; }

    /// Liefert die ID des Viertels, in dem sich die Bank der Stadt befindet.
    Netzwerk::id_t get_id_viertel_bank() const { return id_viertel_bank; }

    /// Liefert die ID des Viertels, in dem sich die Schattengilde der Stadt befindet.
    Netzwerk::id_t get_id_viertel_schattengilde() const { return id_viertel_schattengilde; }

    /// Liefert die ID des Landes, dem die Stadt angehört.
    Netzwerk::id_t get_id_land() const { return id_land; }

    /// Liefert eine Liste von IDs aller Viertel dieser Stadt.
    const std::unordered_set<Netzwerk::id_t>& get_viertel() const { return viertel; }

    /// Liefert eine Liste von IDs aller Arbeiter dieser Stadt.
    const std::unordered_set<Netzwerk::id_t>& get_arbeiter() const { return arbeiter; }

    /// Liefert die Einwohnerzahl der Stadt. // TODO Wachstum/Schrumpfen
    uint32_t get_einwohnerzahl() const { return einwohnerzahl; }

    /// Liefert wieviel Geld sich in der Stadtkasse befindet..
    int64_t get_geld() const { return geld; }

    /// Liefert eine zufällige nachgefragte Ware.
    const std::string& get_zufallsware_verbrauch() const;

    /// Liefert eine zufällige produzierte Ware.
    const std::string& get_zufallsware_produktion() const;

    /// Liefert die fertige Infrastruktur.
    const Infrastruktur::infra_map_t& get_infrastruktur() const { return infrastruktur; }

    /// Liefert die in Bau befindliche Infrastruktur.
    const std::vector<Infrastruktur::IN_BAU>& get_infrastruktur_in_bau() const { return infra_in_bau; }

    /// Liefert für gegebene Ware die Qualität, zu der die Stadt diese produziert. TODO nutzen
    [[deprecated]] float get_quali_produktion(const std::string& waren_key) const;

    /// Zahlt Geld in die Stadtkasse ein.
    void geld_einnehmen(unsigned betrag);

    /// Zahlt Geld in die Stadtkasse ein.
    void geld_ausgeben(unsigned betrag);

    /// Startet den Bau gegebener Infrastruktur.
    void starte_bau(const Infrastruktur& i, uint8_t anzahl);

    /// Bestimmt zufällig, ob ein Zivilist erzeugt werden soll.
    bool zivilist_erzeugen(const Zeit& zeit) const;

    /// Serialisierung.
    friend std::ostream& operator<<(std::ostream& os, const Stadt& stadt);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(id, id_markt, id_markt_gebaeude,
           id_viertel_markt, id_viertel_bank, id_viertel_kirche, id_viertel_rathaus, id_viertel_schattengilde,
           id_land, name, viertel, arbeiter,
           einwohnerzahl, geld,
           gewichtung_verbrauch, gewichtung_produktion, produktion_qualitaet,
           infrastruktur, infra_in_bau,
           armeen
        );
    }

private:

    /// Führt einen Simulationsschritt durch. Täglich. Generiert Arbeiter.
    void tick(Welt* welt, Kultur* kultur, Land* land);

    /// Fügt der Welt und dieser Stadt einen zufälligen Arbeiter hinzu.
    void generiere_arbeiter(Welt* welt, Kultur* kultur);

private:

    /* Verknüpfungen zu anderen Objekten in der Welt */
    Netzwerk::id_t id;                        // ID dieser Stadt
    Netzwerk::id_t id_markt;                  // ID des Marktobjekts
    Netzwerk::id_t id_markt_gebaeude;         // ID des Marktgebäudeobjekts
    Netzwerk::id_t id_viertel_markt;          // Viertel, welches den Markt hat
    Netzwerk::id_t id_viertel_kirche;         // Viertel, welches die Kirche hat
    Netzwerk::id_t id_viertel_rathaus;        // Viertel, welches das Rathaus hat
    Netzwerk::id_t id_viertel_bank;           // Viertel, welches die Bank hat
    Netzwerk::id_t id_viertel_schattengilde;  // Viertel, welches die Schattengilde hat
    Netzwerk::id_t id_land;
    /* Eigenschaften der Stadt */
    std::string name;
    uint32_t einwohnerzahl;
    int64_t geld;
    std::unordered_set<Netzwerk::id_t> viertel;
    std::unordered_set<Netzwerk::id_t> arbeiter; // Arbeiter sind Character IDs
    std::unordered_map<std::string, unsigned> gewichtung_produktion;
    std::unordered_map<std::string, unsigned> gewichtung_verbrauch;
    std::unordered_map<std::string, float>    produktion_qualitaet;
    /* Infrastruktur */
    Infrastruktur::infra_map_t infrastruktur; // Key, Anzahl
    std::vector<Infrastruktur::IN_BAU> infra_in_bau; // Infrastruktur in Bau: Key, Restbauzeit
    /* Militär */
    std::vector<Armee> armeen;

};
