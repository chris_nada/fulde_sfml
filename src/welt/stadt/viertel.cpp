#include "viertel.hpp"
#include "../../netzwerk/netzwerk.hpp"
#include "../../werkzeuge/etc.hpp"
#include "../../gui/gui_viertel.hpp"
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/unordered_set.hpp>
#include <cereal/types/list.hpp>

#include <nada/log.hpp>
#include <nada/random.hpp>

using nada::Log;

Viertel::Viertel() : id(0), id_stadt(0), id_markt(-1), name("uninitialisiert")
{
    //
}

Viertel::Viertel(Netzwerk::id_t id, Netzwerk::id_t id_stadt, std::string name) :
    id(id), id_stadt(id_stadt), id_markt(-1),
    reichtum(nada::random::get<decltype(reichtum)>(0,100)),
    name(std::move(name))
{
    //
}

Viertel::Viertel(std::stringstream& daten) {
    etc::serial_in bia(daten);
    bia(*this);
}

std::ostream& operator<<(std::ostream& os, const Viertel& viertel) {
    etc::serial_out boa(os);
    boa(viertel);
    return os;
}

int8_t Viertel::add_gebaeude(Netzwerk::id_t gebaeude_id) {
    if (gebaeude.size() < MAX_GEBAEUDE) {
        for (int8_t i = 1; i < 0.9 * std::numeric_limits<int8_t>::max(); ++i) {
            if (add_gebaeude(gebaeude_id, i)) return i;
            if (add_gebaeude(gebaeude_id, -i)) return -i;
        }
    }
    Log::err() << "Viertel::add_gebaeude() Nicht platzierbar in: " << name << " ID = " << id << Log::endl;
    return 0;
}

bool Viertel::add_gebaeude(Netzwerk::id_t gebaeude_id, int8_t pos) {
    if (gebaeude.size() >= MAX_GEBAEUDE) return false;
    if (pos != 0 && gebaeude.count(pos) == 0) {
        gebaeude[pos] = gebaeude_id;
        return true;
    }
    return false;
}

std::vector<Netzwerk::id_t> Viertel::get_gebaeude() const {
    std::vector<Netzwerk::id_t> g_vektor;
    g_vektor.reserve(gebaeude.size());
    for (auto gid_paar : gebaeude) g_vektor.push_back(gid_paar.second);
    return g_vektor;
}

void Viertel::aendere_reichtum(int8_t diff) {
    if (std::abs(diff) > reichtum && diff < 0) reichtum = 0;
    else {
        if (diff > 0) reichtum += diff;
        if (Viertel::reichtum > 100) Viertel::reichtum = 100;
    }
}

float Viertel::get_gebaeude_x(Netzwerk::id_t gebaeude_id) const {
    for (const auto& paar : gebaeude) if (paar.second == gebaeude_id) {
        return static_cast<float>(paar.first) * (Gui_Viertel::X_PADDING + Gui_Viertel::X_PLATZ) // Linker Beginn des Gebäudes
               + 0.5f * Gui_Viertel::X_PLATZ; // Mitte
    }
    Log::err() << "Viertel::get_gebaeude_x(" << gebaeude_id << ") nicht gefunden in Viertel " << id << '\n';
    return 0.f;
}

int8_t Viertel::get_grenze(Position::Ausrichtung richtung) const {
    const int8_t iterator = richtung == Position::Ausrichtung::RECHTS ? 1 : -1;
    for (int8_t i = iterator; std::abs(i) < (int) MAX_GEBAEUDE; i += iterator) if (gebaeude.count(i) == 0) return i;
    return iterator;
}
