#include "zivilist.hpp"
#include "viertel.hpp"
#include "../waren/markt.hpp"
#include "../../welt/stadt/stadt.hpp"
#include "../../werkzeuge/etc.hpp"

Zivilist::Zivilist(const Kultur& kultur, const Position& start, const Position& ziel, const Viertel& viertel) :
        Person(id_counter++, kultur.get_id(), nada::random::b(50),
               "???", "???",
               Zeit(nada::random::ui(1,4), Zeit::SERVER_JAHR - nada::random::ui(14, 70)),
               start),
        aufgabe(nada::random::choice(aufgaben_gewichtet)),
        geld(nada::random::ui(viertel.get_reichtum() * 1, viertel.get_reichtum() * 100)), // Max 10.000 (weil 100*100)
        ziel(ziel)
{
    vorname = maennlich ? kultur.get_random_vorname_m() : kultur.get_random_vorname_f();
    speed = nada::random::f(40.f, 100.f);
}

bool Zivilist::tick(const Stadt& stadt, Markt& markt, Viertel& viertel) {

    // Wenn angekommen Aufgabe erledigen
    if (gehe_zu_ziel()) switch (aufgabe) {
        case WAREN_KAUF:
        case WAREN_VERKAUF:
        case WAREN_HANDEL:
            handel(stadt, markt, viertel);
            return true; // Z fertig
    }
    return false; // Z noch nicht fertig
}

bool Zivilist::gehe_zu_ziel() {
    const float marge = 50.f; // Tolereanz in Pixeln bei der Bestimmung, ob angekommen

    // Angekommen?
    if (ziel.viertel == pos.viertel && std::abs(pos.x - ziel.x) < marge) {
        //Log::debug() << "\tZivilist " << id << " angekommen\n";
        return true;
    }

    // Bereits im Zielviertel?
    if (ziel.viertel == pos.viertel) {
        // Sich bewegen
        if (ziel.x < pos.x)  gehe(Position::LINKS);
        else gehe(Position::RECHTS);
    }
    else { // Zum Wegweiser reisen
        if (pos.x > 0) gehe(Position::LINKS);
        else gehe(Position::RECHTS);
        if (std::abs(pos.x) < marge) {
            // Ins Zielviertel reisen
            pos.viertel = ziel.viertel;
            pos.x = 5000.f * nada::random::i(-1, 1); // kann links, mitte oder rechts sein
        }
    }
    return false;
}

void Zivilist::handel(const Stadt& stadt, Markt& markt, Viertel& viertel) {

    /// Warenverkauf
    auto verkauf = [&]() {
        const auto waren_key = stadt.get_zufallsware_produktion();
        const auto erloes = markt.get_preis(waren_key, 1);
        markt.verkauf(waren_key, 1, Ware::get(waren_key).get_zufallsquali());
        geld += erloes;
    };

    /// Warenkauf
    auto kauf = [&]() {
        const auto waren_key = stadt.get_zufallsware_verbrauch();
        const auto preis = markt.get_preis(waren_key, 1);
        if (geld >= preis && markt.kauf(waren_key, 1)) {
            geld -= preis;
            if (nada::random::b(Ware::get(waren_key).get_gewichtung_verbrauch())) viertel.aendere_reichtum(1);
        }
        else if (nada::random::b(Ware::get(waren_key).get_gewichtung_verbrauch())) viertel.aendere_reichtum(-1);
    };

    switch (aufgabe) {
        case WAREN_HANDEL:
            verkauf(); // Warenverkäufe
            for (unsigned i = nada::random::ui(0, 2); i <= 3; ++i) kauf(); // Warenkäufe
            break;
        case WAREN_KAUF:
            for (unsigned i = nada::random::ui(0, 2); i <= 3; ++i) kauf(); // Warenkäufe
            break;
        case WAREN_VERKAUF:
            verkauf(); // Warenverkäufe
            break;
    }

}
