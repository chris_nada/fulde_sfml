#include "statistik.hpp"
#include "../lingua/lingua.hpp"
#include "../werkzeuge/etc.hpp"

#include <cereal/types/deque.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/string.hpp>

Statistik::Statistik(Netzwerk::id_t id) :
    id(id)
{
    tick_jahr();
}

Statistik::Statistik(std::stringstream& daten) {
    etc::serial_in in(daten);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Statistik& dynastie) {
    etc::serial_out out(os);
    out(dynastie);
    return os;
}

void Statistik::tick_jahr() {
    auto weitersetzen = [](auto& historie) {
        while (historie.size() >= MAX) historie.pop_back();
        historie.emplace_front();
    };
    weitersetzen(einnahmen);
    weitersetzen(ausgaben);
    weitersetzen(produziert);
    weitersetzen(verbraucht);
}

void Statistik::add_produkt(const std::string& warenkey, uint32_t menge) {
    if (produziert.front().count(warenkey) == 0) produziert.front()[warenkey] = menge;
    else produziert.front()[warenkey] += menge;
}

void Statistik::add_verbrauch(const std::string& warenkey, uint32_t menge) {
    if (verbraucht.front().count(warenkey) == 0) verbraucht.front()[warenkey] = menge;
    else verbraucht.front()[warenkey] += menge;
}

void Statistik::add_einnahme(Statistik::Einnahme typ, uint64_t menge) {
    if (einnahmen.front().count(typ) == 0) einnahmen.front()[typ] = menge;
    else einnahmen.front()[typ] += menge;
}

void Statistik::add_ausgabe(Statistik::Ausgabe typ, uint64_t menge) {
    if (ausgaben.front().count(typ) == 0) ausgaben.front()[typ] = menge;
    else ausgaben.front()[typ] += menge;
}

const std::string& Statistik::get_txt(Statistik::Einnahme einnahme) {
    switch (einnahme) {
        case Einnahme::WARENVERKAUF: return TXT::get("warenverkaeufe");
        case Einnahme::SPENDEN:      return TXT::get("spenden");
        case Einnahme::GEHALT:       return TXT::get("gehaelter");
        case Einnahme::WERKRUECKBAU: return TXT::get("werkrueckbau");
    }
    return TXT::get("???");
}

const std::string& Statistik::get_txt(Statistik::Ausgabe ausgabe) {
    switch (ausgabe) {
        case Ausgabe::WARENKAUF:        return TXT::get("warenkaeufe");
        case Ausgabe::GEHALT:           return TXT::get("gehaelter");
        case Ausgabe::SPENDEN:          return TXT::get("spenden");
        case Ausgabe::STEUER_ERTRAG:    return TXT::get("steuer_einkommen");
        case Ausgabe::STEUER_MEHRWERT:  return TXT::get("steuer_mehrwert");
        case Ausgabe::WERKAUSBAU:       return TXT::get("werkausbau");
    }
    return TXT::get("???");
}

uint64_t Statistik::get_einnahme(Statistik::Einnahme typ, unsigned int jahr) const {
    try { return einnahmen.at(jahr).at(typ); }
    catch (std::out_of_range& e) { return 0; }
}

uint64_t Statistik::get_ausgabe(Statistik::Ausgabe typ, unsigned int jahr) const {
    try { return ausgaben.at(jahr).at(typ); }
    catch (std::out_of_range& e) { return 0; }
}

const std::unordered_map<Statistik::Einnahme, uint64_t>& Statistik::get_aktuelle_einnahmen() const {
    static const std::unordered_map<Statistik::Einnahme, uint64_t> leer;
    if (einnahmen.empty()) return leer;
    return einnahmen[0];
}

const std::unordered_map<Statistik::Ausgabe, uint64_t>& Statistik::get_aktuelle_ausgaben() const {
    static const std::unordered_map<Statistik::Ausgabe, uint64_t> leer;
    if (ausgaben.empty()) return leer;
    return ausgaben[0];
}

uint64_t Statistik::get_produktion(const std::string& warenkey, unsigned int jahr) const {
    try { return produziert.at(jahr).at(warenkey); }
    catch (std::out_of_range& e) { return 0; }
}

uint64_t Statistik::get_verbrauch(const std::string& warenkey, unsigned int jahr) const {
    try { return verbraucht.at(jahr).at(warenkey); }
    catch (std::out_of_range& e) { return 0; }
}
