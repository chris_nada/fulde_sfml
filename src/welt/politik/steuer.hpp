#pragma once

#include <cstdint>

/// Enum für alle verwendeten Arten von Steuern.
enum class Steuertyp : uint8_t {
    MEHRWERT = 0,
    ERTRAG   = 1,
    GRUND    = 2,
};

/// Hilfs-struct für Steuern. Alle Werte sind Prozentpunkte.
class Steuer final {

public:

    /// Default Ctor.
    Steuer() = default;

    /// Ctor.
    Steuer(uint8_t mehrwert, uint8_t ertrag, uint8_t grund);

    /// Liefert den Steuersatz für die gegebene Steuerart als const.
    uint8_t get(Steuertyp steuertyp) const;

    /// Liefert den Steuersatz für die gegebene Steuerart, änderbar.
    void set(Steuertyp steuertyp, uint8_t wert);

    /// Serialisierung.
    template<class Archive> void serialize(Archive& ar) { ar(mehrwert, ertrag, grund); }

private:

    uint8_t mehrwert;
    uint8_t ertrag;
    uint8_t grund;

};
