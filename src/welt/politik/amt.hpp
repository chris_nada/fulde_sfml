#pragma once

#include "../../netzwerk/netzwerk.hpp"
#include "../../werkzeuge/etc_cereal.hpp"

#include <cereal/types/string.hpp>
#include <cereal/types/unordered_set.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/optional.hpp>
#include <cereal/archives/xml.hpp>

class Welt;

/// Ämter, die man ausüben kann, die einem bestimmte Vorteile, Rechte und ein Einkommen sichern.
class Amt final {

public:

    /// Wieviele Computergesteuerte Charaktere sich maximal auf ein Amt bewerben dürfen. [0] bedeutet kein Limit.
    static const unsigned MAX_KI_BEWERBER = 10;

    /// Rechte, die man hat, wenn man das Amt inne hält. TXT sucht nacht `recht_x`, wobei `x` dem Zahlenwert.
    enum Recht : uint8_t {
        STEUER_MWST_AENDERN     = 0, // TODO
        STEUER_ERTRAG_AENDERN   = 1,
    };

    /// Auf welcher Ebene befindet sich das Amt.
    enum Ebene : uint8_t {
        STADT = 1,
        LAND = 2
    };

    /// Liefert eine Liste alle Ämter.
    static const std::vector<Amt>& alle();

    /// Liefert eine Liste aller Ämter der gegebenen Ebene.
    static const std::vector<Amt>& alle(Ebene ebene);

    /// Liefert die lokalisierte Bezeichnung für ein Recht.
    static const std::string& get_recht_text(Recht recht);

    /// Liefert das Amt mit gegebenen Schlüssel.
    static const Amt& get(const std::string& key);

    /// Liefert das Amt, das gegebener Charakter besetzt hat oder `nullptr`.
    static Amt* get_amt(Netzwerk::big_id_t char_id, std::vector<Amt>& aemter);

    /// Liefert das Amt, das gegebener Charakter besetzt hat oder `nullptr`.
    static Amt* get_amt(Netzwerk::big_id_t char_id, Welt& welt);

    /// Extrahiert aus der Welt alle Ämter.
    static std::vector<Amt*> get_aemter_aus_welt(Welt& welt);

public:

    /// Getter: Bewerber; Key: Kandidat; Value: Liste von Wählern.
    const std::unordered_map<Netzwerk::big_id_t, std::unordered_set<Netzwerk::big_id_t>>& get_bewerber() const { return bewerber; }

    /// Schlüssel des Amts.
    const std::string& get_key() const { return key; }

    /// Getter: Jährliches Einkommen. // TODO auszahlen
    uint16_t get_jaehrliches_einkommen() const { return jaehrliches_einkommen; }

    /// Getter: Politische Ebene. (Landesebene, Stadtebene).
    Ebene get_ebene() const { return ebene; }

    /// Getter: Stufe innerhalb der Stadt/Landesebene. Höher = "besser"; 1 = Beginn.
    uint8_t get_stufe() const { return stufe; }

    /// Getter: Stufe als char A = Anfang, B, C...
    char get_stufe_as_char() const;

    /// Getter: Alle Rechte, die der Amtsinhaber besitzt. Kann leer sein.
    const std::unordered_set<Recht>& get_rechte() const { return rechte; }

    /// Getter: Wer hat dieses Amt inne. @note Ungültig, wenn `besetzt` == `false`.
    Netzwerk::big_id_t get_inhaber() const { return inhaber.value(); }

    /// Getter: Ist das Amt besetzt? Wenn false, ist inhaber ungültig.
    bool is_besetzt() const { return inhaber.has_value(); }

    /// Getter: Wieviele Stimmen hat gg. Bewerber.
    size_t get_stimmen(Netzwerk::big_id_t char_id) const;

    /// Getter: Wähler.
    std::unordered_set<Netzwerk::big_id_t> get_waehler(Netzwerk::big_id_t bewerber) const;

    /**
     * Gegebener Wähler gibt eine Stimme ab für gegebenen Bewerber.
     * @note Wählen dürfen alle Charaktere derselben Stadt/Land.
     */
    void waehlen(Netzwerk::big_id_t bewerber, Netzwerk::big_id_t waehler);

    /// Gegebener Charakter bewirbt sich auf dieses Amt.
    void bewerben(Netzwerk::big_id_t char_id);

    /**
     * Darf gegebener Charakter sich bewerben?
     * @param alte_stufe Stufe des aktuellen Amts des Charakters.
     * @param derselbe_ort Ist er am Ort (dieses Amtes hier) ansässig?
     */
    bool darf_bewerben(Netzwerk::big_id_t char_id, uint8_t alte_stufe, bool derselbe_ort) const;

    /// Ist gegebener Charakter ein Bewerber auf dieses Amt?
    bool is_bewerber(Netzwerk::big_id_t char_id) const;

    /// Hat gegebener Charakter eine Stimme auf irgendeinen Bewerber dieses Amts abgegeben?
    bool is_waehler(Netzwerk::big_id_t char_id) const;

    /// Führt eine Wahl durch. Setzt die Bewerber und Wähler zurück. Inhaber wird der Charakter mit den meisten Stimmen.
    void wahl(Welt& welt); // TODO Benachrichtigungen

    /// Vergleicht Ämter nach Stufe, Einkommen, schließlich Key.
    friend bool operator<(const Amt& lhs, const Amt& rhs);

    /// Serialisierung via Cereal
    template<class Archive> void serialize_binary(Archive& ar) {
        ar(key, jaehrliches_einkommen, ebene, stufe, rechte, inhaber, bewerber);
    }
    void serialize(etc::serial_in& ar)  { serialize_binary(ar); }
    void serialize(etc::serial_out& ar) { serialize_binary(ar); }
    void serialize(cereal::XMLInputArchive& ar) { /// Einlesen aus XML via Cereal.
        ar(CEREAL_NVP(key), CEREAL_NVP(jaehrliches_einkommen), CEREAL_NVP(ebene), CEREAL_NVP(stufe), CEREAL_NVP(rechte));
    }

private:

    /// Schlüssel zum Identifizieren.
    std::string key;

    /// Jährliches Einkommen.
    uint16_t jaehrliches_einkommen;

    /// Ebene, auf der das Amt sich befindet.
    Ebene ebene;

    /// Stufe innerhalb der Ebene.
    uint8_t stufe;

    /// Alle Rechte, Tätigkeiten, die man hat, wenn man das Amt innehat.
    std::unordered_set<Recht> rechte;

    /// Aktueller Amtsinhaber. Kann unbesetzt sein.
    std::optional<Netzwerk::big_id_t> inhaber;

    /// Key: Kandidat; Value: Liste von Wählern.
    std::unordered_map<Netzwerk::big_id_t, std::unordered_set<Netzwerk::big_id_t>> bewerber;

};
