#include "bank.hpp"
#include <nada/random.hpp>

Bank::Bank() :
    geldvorrat(1000),
    zins_kredit(8),
    zins_guthaben(4),
    zinsfaktor(nada::random::f(0.f, 5.0f))
{
    //
}

uint8_t Bank::get_zins_guthaben() const {
    return static_cast<uint8_t>(std::round(static_cast<float>(zins_guthaben) * zinsfaktor));
}

uint8_t Bank::get_zins_kredit() const {
    return static_cast<uint8_t>(std::round(static_cast<float>(zins_kredit) / zinsfaktor));
}

void Bank::tick() {
    // Neue Kredite
    while (geldvorrat >= 1000) {
        verfuegbare_kredite.emplace_back(std::make_tuple<uint32_t, uint8_t>(
                nada::random::ui(1, 10) * 100, get_zins_kredit()
        ));
    }

    // Zu viele Kredite -> Kredite auflösen
    while (verfuegbare_kredite.size() > 10) {
        geldvorrat += std::get<0>(verfuegbare_kredite.front());
        verfuegbare_kredite.pop_front();
    }

    // Geldtransfer zum Land
    // TODO
}
