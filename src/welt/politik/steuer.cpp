#include "steuer.hpp"

#include <string>
#include <stdexcept>

Steuer::Steuer(uint8_t mehrwert, uint8_t ertrag, uint8_t grund) : mehrwert(mehrwert), ertrag(ertrag), grund(grund) {
    //
}

uint8_t Steuer::get(Steuertyp steuertyp) const {
    switch (steuertyp) {
        case Steuertyp::MEHRWERT: return mehrwert;
        case Steuertyp::ERTRAG:   return ertrag;
        case Steuertyp::GRUND:    return grund;
        default: break;
    }
    throw std::runtime_error("Steuertyp nicht implementiert: " + std::to_string((uint8_t)steuertyp));
}

void Steuer::set(Steuertyp steuertyp, uint8_t wert) {
    switch (steuertyp) {
        case Steuertyp::MEHRWERT: mehrwert = wert; return;
        case Steuertyp::ERTRAG:   ertrag   = wert; return;
        case Steuertyp::GRUND:    grund    = wert; return;
        default: break;
    }
    throw std::runtime_error("Steuertyp nicht implementiert: " + std::to_string((uint8_t)steuertyp));
}
