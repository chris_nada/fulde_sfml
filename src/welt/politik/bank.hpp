#pragma once

#include <tuple>
#include <cstdint>
#include <deque>

/// Hauptaufgabe ist die Vergabe von Krediten.
class Bank final {

public:

    /// Ctor.
    Bank();

    /// Quartalsticks.
    void tick();

    ///
    uint8_t get_zins_guthaben() const;

    ///
    uint8_t get_zins_kredit() const;

private:

    /// Vorrat an Geld, das die Bank hat.
    uint32_t geldvorrat;

    /// Basiszinssatz für Kredite in Prozent.
    uint8_t zins_kredit;

    /// Basiszinssatz für Guthaben in Prozent.
    uint8_t zins_guthaben;

    /// 0...5 Multiplikator für Zinsen. Wird mit Guthabenzinsen multipliziert, Krediten dividiert.
    float zinsfaktor;

    /// Von der Bank zu vergebene Kredite Im Paar: <Kreditrahmen, Zinssatz>
    std::deque<std::tuple<uint32_t, uint8_t>> verfuegbare_kredite;

};
