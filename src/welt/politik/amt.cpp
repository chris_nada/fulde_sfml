#include "amt.hpp"
#include "../../lingua/lingua.hpp"
#include "../welt.hpp"

#include <nada/fs.hpp>
#include <nada/log.hpp>

using nada::Log;

const std::vector<Amt>& Amt::alle() {
    static std::vector<Amt> aemter;
    if (aemter.empty()) {
        Log::debug() << "Aemter werden geladen..." << Log::endl;
        const auto& dateien = nada::fs::all_files("data/aemter", "xml");
        assert(dateien.size() > 0);
        aemter.reserve(dateien.size());
        for (const std::string& datei : dateien) {
            try {
                std::ifstream in(datei);
                Amt amt;
                cereal::XMLInputArchive xml(in);
                xml(CEREAL_NVP(amt));
                aemter.push_back(amt);
                Log::debug() << "\tAmt " << datei << " eingelesen\n";
            } catch (const std::exception& e) {
                Log::err() << "Fehler beim lesen von: " << datei << " " << e.what() << Log::endl;
            }
        }
        Log::debug() << aemter.size() << " Aemter eingelesen." << Log::endl;
    }
    return aemter;
}

const std::vector<Amt>& Amt::alle(Amt::Ebene ebene) {
    static std::unordered_map<Ebene, std::vector<Amt>> map;
    if (map[ebene].empty()) for (const auto& amt : alle()) if (amt.ebene == ebene) map[ebene].push_back(amt);
    return map[ebene];
}

const Amt& Amt::get(const std::string& key) {
    static std::unordered_map<std::string, Amt> map;
    if (map.empty()) for (const auto& amt : alle()) map[amt.get_key()] = amt;
    return map.at(key);
}

std::vector<Amt*> Amt::get_aemter_aus_welt(Welt& welt) {
    std::vector<Amt*> aemter;
    aemter.reserve(welt.laender.size() * Amt::alle(Ebene::LAND).size() + welt.staedte.size() * Amt::alle(Ebene::STADT).size());
    for (auto& temp_ebene : welt.aemter) for (auto& ort_amt : temp_ebene.second) for (auto& amt : ort_amt.second) {
        aemter.push_back(&amt);
    }
    auto ptr_compare = [](const auto* o1, const auto* o2) { return *o1 < *o2; };
    std::sort(aemter.begin(), aemter.end(), ptr_compare);
    return aemter;
}

const std::string& Amt::get_recht_text(Amt::Recht recht) {
    return TXT::get("recht_" + std::to_string((unsigned)recht));
}

bool Amt::is_bewerber(Netzwerk::big_id_t char_id) const {
    return bewerber.count(char_id) != 0;
}

bool Amt::is_waehler(Netzwerk::big_id_t char_id) const {
    return std::any_of(bewerber.begin(), bewerber.end(), [=](const auto& paar) {
        return paar.second.count(char_id) != 0;
    });
}

size_t Amt::get_stimmen(Netzwerk::big_id_t char_id) const {
    if (is_bewerber(char_id)) return bewerber.at(char_id).size();
    return 0;
}

void Amt::bewerben(Netzwerk::big_id_t char_id) {
    if (bewerber.count(char_id) == 0) bewerber[char_id] = {};
}

void Amt::waehlen(Netzwerk::big_id_t bewerber_id, Netzwerk::big_id_t waehler) {
    if (Amt::bewerber.count(bewerber_id) != 0) Amt::bewerber[bewerber_id].insert(waehler);
}

std::unordered_set<Netzwerk::big_id_t> Amt::get_waehler(Netzwerk::big_id_t bewerber_id) const {
    try { return Amt::bewerber.at(bewerber_id); }
    catch (const std::exception& e) { return std::unordered_set<Netzwerk::big_id_t>(); }
}

Amt* Amt::get_amt(Netzwerk::big_id_t char_id, std::vector<Amt>& aemter) {
    for (Amt& amt : aemter) if (amt.get_inhaber() == char_id && amt.is_besetzt()) return &amt;
    return nullptr;
}

Amt* Amt::get_amt(Netzwerk::big_id_t char_id, Welt& welt) {
    auto aemter = get_aemter_aus_welt(welt);
    for (auto& amt : aemter) if (amt->is_besetzt() && amt->inhaber == char_id) return amt;
    return nullptr;
}

void Amt::wahl(Welt& welt) {
    if (bewerber.empty()) return; // Keine Bewerber
    const decltype(inhaber) inhaber_ex = is_besetzt() ? this->inhaber : 0;

    // Gewinner bestimmen
    Netzwerk::big_id_t gewinner = nada::random::choice(bewerber).first; // Zufälligen Bewerber
    for (const auto& b : bewerber) if (b.second.size() > bewerber.at(gewinner).size()) gewinner = b.first;
    if (Amt* bisheriges_amt = get_amt(gewinner, welt); bisheriges_amt) bisheriges_amt->inhaber.reset();
    this->inhaber = gewinner;

    // Teilnehmer Benachrichtigen TODO
    if (is_besetzt() && this->inhaber != inhaber_ex) {
        Log::debug() << "Amt " << key << " besetzt von " << this->inhaber.value() << " mit ";
        Log::debug() << bewerber.at(gewinner).size() << " Stimmen bei " << (bewerber.size() - 1) << " Mitbewerbern\n";
    }
    this->bewerber.clear();

    // Andere Bewerbungen des Gewinners löschen
    for (auto& temp_ebene : welt.aemter) for (auto& ort_amt : temp_ebene.second) for (auto& amt : ort_amt.second) {
        amt.bewerber.erase(gewinner);
    }
}

bool Amt::darf_bewerben(Netzwerk::big_id_t char_id, uint8_t alte_stufe, bool derselbe_ort) const {
    if (is_bewerber(char_id)) return false;
    if (this->stufe > alte_stufe + 1) return false;
    if (!derselbe_ort) return false;
    return true;
}

bool operator<(const Amt& lhs, const Amt& rhs) {
    if (lhs.stufe < rhs.stufe) return true;
    if (lhs.jaehrliches_einkommen < rhs.jaehrliches_einkommen) return true;
    return rhs.key < lhs.key;
}

char Amt::get_stufe_as_char() const {
    return static_cast<char>('A' + get_stufe() - 1);
}
