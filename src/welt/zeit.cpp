#include "zeit.hpp"
#include "../lingua/lingua.hpp"
#include "../werkzeuge/etc.hpp"

Zeit::Zeit(uint8_t jahreszeit, uint16_t jahr) : Zeit(8, 0, jahreszeit, jahr) {}

Zeit::Zeit(uint8_t h, uint8_t min, uint8_t jahreszeit, uint16_t jahr) :
    h(h), min(min), jahreszeit(jahreszeit), jahr(jahr) {}

Zeit::Zeit(std::stringstream& daten) {
    etc::serial_in in(daten);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Zeit& zeit) {
    etc::serial_out out(os);
    out(zeit);
    return os;
}

void Zeit::set(uint8_t min, uint8_t h, uint8_t jahreszeit, uint16_t jahr) {
    Zeit::min        = min;
    Zeit::h          = h;
    Zeit::jahreszeit = jahreszeit;
    Zeit::jahr       = jahr;
}

bool Zeit::add_min(uint8_t minuten) {
    min += minuten;
    while (min >= 60) {
        min -= 60;
        h++;
    }
    if (h >= 24) {
        h -= 24;
        jahreszeit += 1;
        if (jahreszeit > 4) {
            jahreszeit = 1;
            jahr++;
        }
        return true;
    }
    return false;
}

uint16_t Zeit::get_alter(const Zeit& datum_jetzt) const {
    const uint16_t alter = datum_jetzt.jahr - jahr;
    if (jahreszeit <= datum_jetzt.jahreszeit) return alter;
    else if (alter > 0) return alter - 1;
    else return 0;
}

const std::string& Zeit::get_jahreszeit_name() const {
    switch (jahreszeit) {
        case 1: return TXT::get("fruehling");
        case 2: return TXT::get("sommer");
        case 3: return TXT::get("herbst");
        case 4: return TXT::get("winter");
        default: return TXT::get("???");
    }
}

Zeit& Zeit::operator+=(uint16_t jahre) {
    this->jahr += jahre;
    return *this;
}

Zeit& Zeit::operator-=(uint16_t jahre) {
    this->jahr += jahre;
    return *this;
}

Zeit Zeit::operator+(uint16_t jahre) const {
    Zeit z = *this;
    z.jahr += jahre;
    return z;
}

Zeit Zeit::operator-(uint16_t jahre) const {
    Zeit z = *this;
    z.jahr -= jahre;
    return z;
}

bool Zeit::operator<(const Zeit& rhs) const {
    if (jahr < rhs.jahr) return true;
    if (jahr > rhs.jahr) return false;
    if (jahreszeit < rhs.jahreszeit) return true;
    if (rhs.jahreszeit < jahreszeit) return false;
    if (h < rhs.h) return true;
    if (rhs.h < h) return false;
    return min < rhs.min;
}

bool Zeit::operator>(const Zeit& rhs) const {
    return rhs < *this;
}

bool Zeit::operator<=(const Zeit& rhs) const {
    return !(rhs < *this);
}

bool Zeit::operator>=(const Zeit& rhs) const {
    return !(*this < rhs);
}

bool Zeit::operator==(const Zeit& rhs) const {
    return min == rhs.min && h == rhs.h && jahreszeit == rhs.jahreszeit && jahr == rhs.jahr;
}

bool Zeit::operator!=(const Zeit& rhs) const {
    return !(rhs == *this);
}
