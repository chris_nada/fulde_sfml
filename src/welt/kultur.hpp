#pragma once

#include "../netzwerk/netzwerk.hpp"
#include <unordered_map>
#include <ostream>
#include <vector>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>

/**
 * Repräsentiert die den Ordnern von `data/kulturen/...` enthaltenen Daten zu den im Spiel
 * vorkommenden Kulturen. Jeder generierten Nation wird eine Kultur assoziiert.
 */
class Kultur final {

public:

    /// Liest die Kulturen ein und liefert diese als Vektor.
    static const std::vector<Kultur>& alle();

    /// Konstruktor.
    Kultur() : id(0) {};

    /// Deserialisierender Ctor.
    explicit Kultur(std::stringstream& ss);

    /// Konstruktor, Pfad zum Verzeichnis der Kultur wird gegeben und ID der Kultur.
    explicit Kultur(const std::string& pfad, Netzwerk::id_t id);

    /// Getter für einen zufälligen Nationsnamen und löscht diesen.
    std::string get_random_nation();

    /// Getter für einen zufälligen Ortsnamen und löscht diesen.
    std::string get_random_ortschaft();

    /// Getter für einen zufälligen Ortsnamen (für Stadtviertel) und löscht diesen.
    std::string get_random_viertel();

    /// Getter für einen zufälligen männlichen Vornamen.
    std::string get_random_vorname_m() const;

    /// Getter für einen zufälligen weiblichen Vornamen.
    std::string get_random_vorname_f() const;

    /// Getter für einen zufälligen Nachnamen.
    std::string get_random_nachname() const;

    /// Liefert für gegebenes Viertel die Hintergrunddatei (nicht als kompletter Pfad, nur z.B. "1.png").
    std::string get_bg_viertel(const std::string& viertelname);

    /// Liefert für gegebene Stadt die Landschaftsdatei (nicht als kompletter Pfad, nur z.B. "1.png").
    std::string get_bg_stadt(const std::string& stadtname);

    /// Getter für den Key der Kultur.
    const std::string& get_key() const { return key; }

    /// Getter: Id der Kultur.
    Netzwerk::id_t get_id() const { return id; }

    /// Serialisiert die Kultur in einen Stream.
    friend std::ostream& operator<<(std::ostream& os, const Kultur& kultur);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(key, id, vornamen_m, vornamen_f, nachnamen, nationen, ortschaften, viertel);
    }

private:

    std::string              key;
    Netzwerk::id_t           id;
    std::vector<std::string> vornamen_m;
    std::vector<std::string> vornamen_f;
    std::vector<std::string> nachnamen;
    std::vector<std::string> nationen;
    std::vector<std::string> ortschaften;
    std::vector<std::string> viertel;

};
