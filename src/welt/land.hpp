#pragma once

#include "../netzwerk/netzwerk.hpp"
#include "stadt/stadt.hpp"
#include "politik/steuer.hpp"
#include <cereal/types/array.hpp>
#include <cereal/types/vector.hpp>

/**
 * Objekte repräsentieren eine Nation im Spiel.
 */
class Land final {

    friend class Welt;

public:

    /// Ctor.
    Land() = default;

    /// Konstruktor.
    explicit Land(Netzwerk::id_t id, Netzwerk::id_t id_kultur, std::string name);

    /// Deserialisierender Konstruktor.
    explicit Land(std::stringstream& ss);

    /// Getter: Name des Landes.
    const std::string& get_name() const { return name; }

    /// Getter: Id des Landes.
    Netzwerk::id_t get_id() const { return id; }

    /// Getter: Id der Kultur des Landes.
    Netzwerk::id_t get_id_kultur() const { return id_kultur; }

    /// Getter: Liste mit Ids der Städte des Landes.
    const std::vector<Netzwerk::id_t>& get_staedte() const { return staedte; }

    /// Getter: Liefert gegebene Steuer in %.
    uint8_t get_steuern(Steuertyp steuertyp) const { return steuern.get(steuertyp); }

    /// Fügt der Staatskasse einen Betrag hinzu.
    void geld_einnehmen(unsigned betrag);

    /// Gibt Geld aus der Staatskasse aus.
    void geld_ausgeben(unsigned betrag);

    /// Setzt gegebenen Steuertypen auf den gegebenen neuen Satz.
    void set_steuer(Steuertyp steuertyp, uint8_t prozentsatz);

    /// Serialisiert das Land in einen Stream.
    friend std::ostream& operator<<(std::ostream& os, const Land& land);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(id, id_kultur, name, staedte, staatskasse, steuern);
    }

private:

    /// Id.
    Netzwerk::id_t id;

    /// Id der Kultur des Landes.
    Netzwerk::id_t id_kultur;

    /// Name des Landes.
    std::string name;

    /// Zugehörige Städte als Ids.
    std::vector<Netzwerk::id_t> staedte;

    /// Geldvorrat.
    int64_t staatskasse;

     /// Steuersätze in diesem Land.
    Steuer steuern;

};
