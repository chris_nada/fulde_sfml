#include "kultur.hpp"
#include "../werkzeuge/etc.hpp"
#include <filesystem>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cassert>
#include <nada/log.hpp>
#include <nada/fs.hpp>
#include <nada/ini.hpp>
#include <nada/misc.hpp>
#include <nada/random.hpp>

using nada::Log;

Kultur::Kultur(const std::string& pfad, Netzwerk::id_t id) : id(id) {
    // Daten aus Ordner laden
    std::string ini_pfad(pfad + "/kultur.ini");
    nada::Ini ini{ini_pfad};
    key = ini.get("key");
    Log::debug() << "Kultur geladen: " << key << '\n';
    nada::fs::read_lines(pfad + "/namen_m.dat", vornamen_m, 2);
    nada::fs::read_lines(pfad + "/namen_f.dat", vornamen_f, 2);
    nada::fs::read_lines(pfad + "/nachnamen.dat", nachnamen, 2);
    nada::fs::read_lines(pfad + "/nationen.dat", nationen, 2);
    nada::fs::read_lines(pfad + "/ortschaften.dat", ortschaften, 2);
    nada::fs::read_lines(pfad + "/viertel.dat", viertel, 2);
}

Kultur::Kultur(std::stringstream& ss) {
    etc::serial_in in(ss);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Kultur& kultur) {
    etc::serial_out out(os);
    out(kultur);
    return os;
}

std::string Kultur::get_random_nation() { return nada::random::choice_erase(nationen); }

std::string Kultur::get_random_ortschaft() { return nada::random::choice_erase(ortschaften); }

std::string Kultur::get_random_viertel() { return nada::random::choice_erase(viertel); }

std::string Kultur::get_random_vorname_m() const { return vornamen_m[nada::random::ul(0, vornamen_m.size() - 1)]; }

std::string Kultur::get_random_vorname_f() const { return vornamen_f[nada::random::ul(0, vornamen_f.size() - 1)]; }

std::string Kultur::get_random_nachname() const { return nachnamen[nada::random::ul(0, nachnamen.size() - 1)]; }

std::string Kultur::get_bg_viertel(const std::string& viertelname) {
    const std::vector<std::string> hintergruende = nada::Ini::get_vector_from_file( // TODO static map
            "data/kulturen/"+key+"/kultur.ini", "viertel",','
    );
    const size_t hash = std::hash<std::string>{}(viertelname);
    return hintergruende.at(hash % hintergruende.size());
}

std::string Kultur::get_bg_stadt(const std::string& stadtname) {
    const std::vector<std::string> landschaften = nada::Ini::get_vector_from_file( // TODO static map
            "data/kulturen/"+key+"/kultur.ini", "stadt",','
    );
    const size_t hash = std::hash<std::string>{}(stadtname);
    return landschaften.at(hash % landschaften.size());
}

const std::vector<Kultur>& Kultur::alle() {
    static std::vector<Kultur> kulturen_temp;
    // Kulturen laden
    if (kulturen_temp.empty()) {
        const std::string kulturen_dir("data/kulturen");
        for (const auto& dir : std::filesystem::directory_iterator(kulturen_dir)) {
            kulturen_temp.emplace_back(dir.path().string(), kulturen_temp.size());
        }
        if (kulturen_temp.empty()) Log::err() << "Es konnten keine Kulturen geladen werden\n";
        std::sort(kulturen_temp.begin(), kulturen_temp.end(),
                  [](const Kultur& k1, const Kultur& k2) { return k1.get_key() < k2.get_key(); });
        for (unsigned i = 0; i < kulturen_temp.size(); ++i) kulturen_temp[i].id = i;
    }
    return kulturen_temp;
}
