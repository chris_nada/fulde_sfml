#pragma once

#include <string>
#include "../../netzwerk/netzwerk.hpp"
#include "../produktion/werkstatt.hpp"

// Prädekls
class Welt;
class Land;
class Markt;

/**
 * @brief Vom Computer gesteuerte Spieler.
 */
class Gegner final {

    /// Tests hier.
    friend class Test_Welt_Gegner;

    /// Nach wievielen Zügen darf ein Gegner wieder eine Anlage bauen/abreißen?
    static constexpr uint16_t AUSBAUINTERVALL = 5;

    /// Das wieviel-fache der benötigten Inputs soll auf Vorrat beschafft werden?
    static constexpr uint16_t VORRATMULTI = 10;

    /// Wieviele Anlagen sollen initial errichtet werden, wenn der Prozess Output-only ist?
    static constexpr uint16_t NUR_OUTPUT_ANLAGEN = 4;

public:

    /// Ctor.
    Gegner() = default;

    /**
     * Erstellt einen neuen Gegner und fügt ihn in gegebene Welt ein.
     * @note Es wird auch eine Dynastie und mit dieser verbundene Gebäude usw. erzeugt.
     * @param kultur Id der Kultur, zu der der Gegner gehören soll.
     * @param welt Wird in diese Welt eingefügt.
     * @param stadt Id der Stadt, aus der sich der Gegner einen Wohnsitz sucht.
     * @param warengruppe Schlüssel der Warengruppe, die der Gegner produzieren wird.
     */
    explicit Gegner(Netzwerk::id_t id, Welt& welt, Netzwerk::id_t kultur, Netzwerk::id_t stadt, const std::string& warengruppe);

    /// Deserialisierender Ctor.
    explicit Gegner(std::stringstream& daten);

    /// Tägliche Ticks. Verwaltet Werkstatt, Waren, Produktion etc.
    void tick_tag(Welt& welt);

    /// Getter: ID.
    Netzwerk::id_t get_id() const { return id; }

    /// Serialisiert eine Instanz.
    friend std::ostream& operator<<(std::ostream& os, const Gegner& gegner);

    /// Serialisierungsfunktion für Cereal.
    template <class Archive> void serialize(Archive& ar) {
        ar(id, id_dynastie, id_char, id_land, id_werk, id_lager, id_markt, id_viertel, id_stadt, warengruppe, ausbaualter);
    }

private:

    /// Baut _eine_ neue (zur Warengruppe des Gegners passende) Anlage in gegebener Werkstatt.
    void a_anlage_zu_jeder_ware_bauen(Werkstatt& werk) const;

    /// Versucht  _einen_ Arbeiter der Anlage hinzuzufügen.
    void a_add_arbeiter(Welt& welt, Stadt& stadt, Werkstatt& werk, uint8_t index_anlage) const;

    /// Versucht einen Wareneinkauf zu tätigen.
    void a_ware_kaufen(Dynastie& dyn, const std::string& warenkey, unsigned int anzahl, Lager& lager, Markt& markt, Land& land, Stadt& stadt, Statistik& statistik) const;

    /**
     * Versucht eine Warenverkauf zu tätigen. Liefert nur dann `true`, wenn `anzahl` verkauft wurde;
     * `false`, wenn gar keine oder auch weniger als `anzahl`.
     */
    bool a_ware_verkaufen(Dynastie& dyn, const std::string& warenkey, unsigned int anzahl, Lager& lager, Markt& markt, Statistik& statistik) const;

    /// Baut eine Anlage ab, die gegebene Ware produziert.
    bool a_produktion_abbauen(const std::string& warenkey, Werkstatt& werkstatt);

private:

    /// ID des Gegners.
    Netzwerk::id_t id;

    /// ID der verknüpften Dynastie.
    Netzwerk::id_t id_dynastie;

    /// ID des aktuellen Dynastieoberhaupts, das 'gesteuert' wird.
    Netzwerk::big_id_t id_char;

    /// Land, in dem der Gegner / die Dynastie spielt.
    Netzwerk::id_t id_land;

    /// Werk, das der Gegner verwaltet.
    Netzwerk::id_t id_werk;

    /// Lager, das der Gegner nutzt.
    Netzwerk::id_t id_lager;

    /// Markt, an dem der Gegner kauft/verkauft.
    Netzwerk::id_t id_markt;

    /// Viertel, in dem der Gegner wohnt.
    Netzwerk::id_t id_viertel;

    /// Stadt, in dem der Gegner wohnt.
    Netzwerk::id_t id_stadt;

    /// Warengruppe, die produziert werden kann.
    std::string warengruppe;

    /// Wie viele Tage ist der letzte Ausbau zurück?
    uint16_t ausbaualter;

};
