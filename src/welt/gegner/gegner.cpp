#include "gegner.hpp"
#include "../welt.hpp"
#include "../../lingua/lingua.hpp"

#include <nada/log.hpp>

using nada::Log;

Gegner::Gegner(Netzwerk::id_t id, Welt& welt, Netzwerk::id_t kultur_id, Netzwerk::id_t stadt_id, const std::string& warengruppe) :
    id(id),
    id_stadt(stadt_id),
    warengruppe(warengruppe),
    ausbaualter(0)
{
    // Daten sammeln
    const Kultur& kultur = welt.kulturen.at(kultur_id);
    const Stadt& stadt = welt.staedte.at(stadt_id);
    id_land = stadt.get_id_land();
    id_viertel = nada::random::choice(stadt.get_viertel());
    Viertel& viertel = welt.viertel.at(id_viertel);
    const bool maennlich = nada::random::b(50);

    // Neuen Spieler erstellen
    id_dynastie = welt.neuer_spieler(
            maennlich ? kultur.get_random_vorname_m() : kultur.get_random_vorname_f(),
            kultur.get_random_nachname(), maennlich, viertel.get_id(), false
    );
    bool werk_zugeordnet = false;
    Dynastie& dynastie = welt.dynastien.at(id_dynastie);
    id_char  = dynastie.get_oberhaupt();
    id_lager = dynastie.get_lager(stadt.get_id());
    id_markt = stadt.get_id_markt();
    for (auto gebaeude_id : dynastie.get_gebaeude()) {
        Gebaeude& gebaeude = welt.gebaeude.at(gebaeude_id);
        if (gebaeude.get_typ() == Gebaeude::WERKSTATT) {
            id_werk = gebaeude.get_id_link();
            werk_zugeordnet = true;
            break;
        }
    }
    if (!werk_zugeordnet) Log::err() << "Neuem Gegner konnte kein Werkstattgebaeude zugeordnet werden" << Log::endl; // TODO
}

Gegner::Gegner(std::stringstream& daten) {
    etc::serial_in in(daten);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Gegner& gegner) {
    etc::serial_out out(os);
    out << gegner;
    return os;
}

void Gegner::tick_tag(Welt& welt) {
    ++ausbaualter;
    // Daten
    Statistik& statistik = welt.statistiken.at(id_dynastie);
    Land&      land  = welt.laender.at(id_land);
    Dynastie&  dyn   = welt.dynastien.at(id_dynastie);
    Werkstatt& werk  = welt.werkstaetten.at(id_werk);
    Stadt&     stadt = welt.staedte.at(id_stadt);
    //Viertel&   vier  = *welt.viertel.at(id_viertel); // ungenutzt
    Lager&     lager = welt.lager.at(id_lager);
    Markt&     markt = welt.maerkte.at(id_markt);

    // Keine Anlage -> Anlage bauen.
    if (werk.get_anlagen().empty()) a_anlage_zu_jeder_ware_bauen(werk);

    // Anlagen verwalten
    std::unordered_map<std::string, unsigned short> benoetigte_waren; // Prüfen ob benötigte Waren ausreichend vorhanden
    for (unsigned i = 0; i < werk.get_anlagen().size(); ++i) {
        Anlage& anlage = werk.anlagen[i];

        // Arbeiter einteilen
        if (anlage.get_arbeiter().size() < anlage.get_prozess().get_arbeiter()) {
            a_add_arbeiter(welt, stadt, werk, i);
        }

        // Benötigte Waren vorhanden?
        bool inputs_verfuegbar = true;
        for (const auto& input : anlage.get_prozess().get_inputs()) {
            const unsigned short anzahl_benoetigt = VORRATMULTI * input.second;
            if (benoetigte_waren.count(input.first) == 0) benoetigte_waren[input.first] = anzahl_benoetigt;
            else benoetigte_waren[input.first] += anzahl_benoetigt;
            const Vorrat& vorrat = lager.get_vorrat(input.first);
            if (vorrat.menge < anzahl_benoetigt) {
                a_ware_kaufen(dyn, input.first, anzahl_benoetigt - vorrat.menge, lager, markt, land, stadt, statistik);
            }
            if (vorrat.menge < anzahl_benoetigt) inputs_verfuegbar = false;
        }

        // Anlage ausbauen?
        if (ausbaualter > AUSBAUINTERVALL) for (const auto& output : anlage.get_prozess().get_outputs()) {
            const Vorrat& vorrat = markt.get_vorrat(output.first);
            int mengendelta = 0;
            for (int n = 0; n < (int)(vorrat.menge_v.size()) - 2; ++n) {
                mengendelta += static_cast<int>(vorrat.menge_v.at(n+1) - vorrat.menge_v.at(n));
            }
            if (inputs_verfuegbar && mengendelta < 0 && anlage.get_zaehler() > 0) {
                Log::debug() << "\tGegner " << id << " baut Anlage aus: " << TXT::get(output.first);
                Log::debug() << " Delta = " << mengendelta << '\n';
                werk.anlagen.emplace_back(anlage.get_prozess().get_key());
                ausbaualter = 0;
                break;
            }
        }
    }

    // Waren verkaufen?
    for (const auto& ware : Ware::alle()) {
        const unsigned anzahl_behalten = benoetigte_waren.count(ware.first) == 0 ? 0 : benoetigte_waren[ware.first];
        const unsigned vorhanden = lager.get_vorrat(ware.first).menge;
        if (vorhanden > anzahl_behalten) { // Überschuss verkaufen
            if (!a_ware_verkaufen(dyn, ware.first, vorhanden - anzahl_behalten, lager, markt, statistik)) {
                //if (ausbaualter > AUSBAUINTERVALL) a_produktion_abbauen(ware.first, werk); // Waren nicht losgeworden -> Produktion zurückfahren
            }
        }
    }
}

void Gegner::a_anlage_zu_jeder_ware_bauen(Werkstatt& werk) const {
    if (werk.get_anlagen().size() >= Werkstatt::MAX_ANLAGEN) {
        Log::err() << "\tGegner " << id << " maximale Zahl Anlagen erreicht in Werk " << id_werk << '\n';
        return;
    }
    auto anlage_bauen = [&](const std::string& warenkey) {
        // Prozess finden.
        for (const auto& prozess : Prozess::alle()) {
            if (std::find_if(prozess.second.get_outputs().begin(), prozess.second.get_outputs().end(),
                             [&](const auto& pair) { return pair.first == warenkey; }) !=
                prozess.second.get_outputs().end()) {
                unsigned anzahl = 1;
                if (prozess.second.get_inputs().empty()) anzahl = NUR_OUTPUT_ANLAGEN; // Für Output-Only Prozesse mehrere Werkstätten bauen.
                for (unsigned i = 0; i < anzahl; ++i) werk.anlagen.emplace_back(prozess.first);
                return true; // erledigt
            }
        }
        Log::err() << "\tGegner::a_anlage_zu_jeder_ware_bauen() Kein Prozess gefunden fuer Ware " << warenkey << '\n';
        return false;
    };
    // Alle Waren der Gruppe bestimmen
    std::vector<std::string> waren;
    waren.reserve(Ware::alle().size());
    for (const auto& paar : Ware::alle()) if (paar.second.get_gruppe() == warengruppe) waren.push_back(paar.first);

    // Für alle Waren der Gruppe eine Anlage einrichten
    for (const auto& warenkey : waren) {
        Log::debug() << "\tGegner " << id << " baut Anlage: " << TXT::get(warenkey) << '\n';
        anlage_bauen(warenkey);
    }
}

void Gegner::a_add_arbeiter(Welt& welt, Stadt& stadt, Werkstatt& werk, uint8_t index_anlage) const {

    // Werk hat einen Arbeiter -> Anlage
    if (!werk.freie_arbeiter.empty()) {
        werk.transfer_arbeiter(*werk.get_freie_arbeiter().begin(), index_anlage);
        return;
    }

    // Neuen Arbeiter einstellen
    if (!stadt.get_arbeiter().empty()) {
        // Stadt -> Werk
        welt.a_transfer_arbeiter(*stadt.get_arbeiter().begin(), true, werk.get_id(), stadt.get_id());
        werk.transfer_arbeiter(*werk.get_freie_arbeiter().begin(), index_anlage);
        return;
    }

    // TODO: Cheat abschaffen
    // Cheat: Keine Arbeiter verfügbar -> Arbeiter erschaffen + einstellen + in Anlage transferieren
    Anlage& anlage = werk.anlagen[index_anlage];
    Kultur& kultur = welt.kulturen.at(welt.laender.at(stadt.get_id_land()).get_id_kultur());
    while (anlage.get_arbeiter().size() < anlage.get_prozess().get_arbeiter()) {
        stadt.generiere_arbeiter(&welt, &kultur);
        welt.a_transfer_arbeiter(*stadt.get_arbeiter().begin(), true, werk.get_id(), stadt.get_id());
        werk.transfer_arbeiter(*werk.get_freie_arbeiter().begin(), index_anlage);
    }
}

void Gegner::a_ware_kaufen(Dynastie& dyn, const std::string& warenkey, unsigned int anzahl, Lager& lager, Markt& markt, Land& land, Stadt& stadt, Statistik& statistik) const {
    const Vorrat& marktvorrat = markt.get_vorrat(warenkey);
    const unsigned verfuegbar = marktvorrat.menge;
    if (verfuegbar < anzahl) {
        // Cheat, wenn Markt nichts hat
        anzahl = std::max(1u, anzahl / 4u); // Divisor = "Mangelsimulation"
        lager.get_vorrat(warenkey).menge += anzahl;
        dyn.geld_bezahlen(markt.get_preis(warenkey, 1), Statistik::Ausgabe::WARENKAUF, statistik);
        return;
    }
    if (anzahl == 0) return; // Warenvorrat im Markt leer
    const unsigned preis = markt.get_preis(warenkey, anzahl);
    const unsigned steuern = (preis * land.get_steuern(Steuertyp::MEHRWERT)) / 100;
    if (markt.kauf(warenkey, anzahl)) { // Kauf erfolgreich
        lager.get_vorrat(warenkey).einlagern(anzahl, marktvorrat.quali);
        dyn.geld_bezahlen(preis, Statistik::Ausgabe::WARENKAUF, statistik);
        dyn.geld_bezahlen(steuern, Statistik::Ausgabe::STEUER_MEHRWERT, statistik);
        stadt.geld_einnehmen(steuern); // Steuern bezahlen
    }
}

bool Gegner::a_ware_verkaufen(Dynastie& dyn, const std::string& warenkey, unsigned int anzahl, Lager& lager, Markt& markt, Statistik& statistik) const {
    //Log::debug() << "\tGegner " << id << ' ' << __func__ << ' ' << anzahl << '*' << warenkey << '\n';
    Vorrat& vorrat = lager.get_vorrat(warenkey);
    //if (markt.get_vorrat(warenkey).menge < 10) vorrat.menge += 100; // Cheat gegen Mangel am Markt
    if (anzahl > vorrat.menge) {
        Log::err() << "\tGegner hat versucht mehr zu verkaufen als vorhanden: " << anzahl << '/' << vorrat.menge << '\n';
        return false;
    }
    bool erfolg = true;
    if (anzahl > markt.get_bedarf(warenkey)) {
        anzahl = markt.get_bedarf(warenkey);
        //erfolg = false; // nicht alles losgeworden
    }
    if (anzahl == 0) return false; // Markt kauft nichts mehr an.
    const int erloes = markt.get_preis(warenkey, anzahl);
    vorrat.entnehmen(anzahl);
    markt.verkauf(warenkey, anzahl, vorrat.quali);
    dyn.geld_erhalten(erloes, Statistik::Einnahme::WARENVERKAUF, statistik);
    return erfolg;
}

bool Gegner::a_produktion_abbauen(const std::string& warenkey, Werkstatt& werkstatt) {
    const auto n_anlagen = std::count_if(werkstatt.get_anlagen().begin(), werkstatt.get_anlagen().end(),
            [&](const Anlage& a) { return a.is_produkt(warenkey); }
    );
    if (n_anlagen <= 1) {
        Log::debug() << "Produktion " << warenkey << " kann nicht zurueckgefahren werden, Anzahl Anlagen: " << n_anlagen << '\n';
        return false;
    }

    for (unsigned i = 0; i < werkstatt.get_anlagen().size(); ++i) {
        Anlage& anlage = werkstatt.anlagen[i];
        if (anlage.is_produkt(warenkey)) { // Abbauen
            for (const auto& arbeiter_id : anlage.get_arbeiter()) werkstatt.transfer_arbeiter(arbeiter_id, i);
            werkstatt.anlagen.erase(werkstatt.anlagen.begin() + i);
            ausbaualter = 0;
            return true;
        }
    }
    return false;
}
