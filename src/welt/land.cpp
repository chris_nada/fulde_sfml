#include "land.hpp"
#include "../werkzeuge/etc_cereal.hpp"

#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/array.hpp>
#include <nada/log.hpp>
#include <nada/random.hpp>

using nada::Log;

Land::Land(Netzwerk::id_t id, Netzwerk::id_t id_kultur, std::string name) :
        id(id), id_kultur(id_kultur), name(std::move(name)),
        staatskasse(10'000),
        steuern((nada::random::get<uint8_t>(10, 30)), // Mwst
                (nada::random::get<uint8_t>(5, 35)), // Ertrag
                (nada::random::get<uint8_t>(0, 50))) // Grund (10tel)
{
    //
}

Land::Land(std::stringstream& ss) {
    etc::serial_in in(ss);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Land& land) {
    etc::serial_out out(os);
    out(land);
    return os;
}

void Land::geld_einnehmen(unsigned betrag) {
    //Log::debug() << "\tLand " << name << " nimmt " << betrag << " ein\n";
    staatskasse += betrag;
}

void Land::geld_ausgeben(unsigned betrag) {
    Log::debug() << "\tLand " << name << " gibt " << betrag << " aus\n";
    staatskasse -= betrag;
}

void Land::set_steuer(Steuertyp steuertyp, uint8_t prozentsatz) {
    steuern.set(steuertyp, prozentsatz);
}
