#pragma once

#include "anlage.hpp"
#include "arbeiter.hpp"
#include "../../netzwerk/netzwerk.hpp"
#include "../zeit.hpp"
#include "../statistik.hpp"
#include "../waren/markt.hpp"

class Werkstatt final {

    friend class Host;
    friend class Welt;
    friend class Gegner;

public:

    /// Wieviele Anlagen darf eine Werkstatt maximal haben?
    static constexpr uint8_t MAX_ANLAGEN = 10;

    /// Um wieviel Uhr beginnen Arbeiter?
    static constexpr uint8_t ARBEIT_START = 8;

    /// Um wieviel Uhr ist für die Arbeiter Feierabend?
    static constexpr uint8_t ARBEIT_ENDE = 18;

    Werkstatt() = default;

    explicit Werkstatt(Netzwerk::id_t id);

    /// Deserialisierender Konstruktor.
    explicit Werkstatt(std::stringstream& ss);

    Netzwerk::id_t get_id() const { return id; }

    /// Wieviel Kosten werden täglich insgesamt verursacht?
    uint32_t get_taegliche_kosten() const { return cache_tageskosten; }
    uint32_t get_taegliche_kosten(Welt* welt) const;

    /// Was kostet es, das Werk um eine Einheit zu vergrößern?
    int32_t get_preis_erweitern() const;

    /// Wieviel Platz ist noch verfügbar für Anlagen?
    uint32_t get_freier_platz() const { return freier_platz; }

    /// Wieviel Platz wird benutzt von Anlagen?
    uint32_t get_platz_anlagen() const;

    /// Liefert eine Liste aller hier beschäftigten, aber unzugewiesenen Arbeiter.
    const std::unordered_set<Netzwerk::id_t>& get_freie_arbeiter() const { return freie_arbeiter; }

    /// Liefert eine Liste aller hier aufgebauten Anlagen.
    const std::vector<Anlage>& get_anlagen() const { return anlagen; }

    /// Liefert die Prozessgruppe, die in dieser Werkstatt genutzt werden kann. Leer, wenn noch keine Anlagen.
    std::string get_prozessgruppe() const;

    /// Automatisches Einkaufen der Bedarfsgüter aktiviert?
    bool hat_auto_kauf() const { return auto_kauf; } // TODO benutzen?

    /// Transferiert einen Arbeiter zwischen Anlage und dem freien Arbeiter Pool.
    void transfer_arbeiter(Netzwerk::id_t char_id, uint8_t index_anlage);

    /// Serialisierung.
    friend std::ostream& operator<<(std::ostream& os, const Werkstatt& werkstatt);

    /// Serialisierung via Cereal.
    template<class Archive> void serialize(Archive& ar) {
        ar(id, freier_platz, auto_kauf, freie_arbeiter, anlagen,
           cache_tageskosten);
    }

private:

    /**
     * Führt einen Simulationsschritt durch für x minuten, der Arbeiter bezahlt, Produktion berechnet usw...
     * @param welt Benötigt die Weltdaten für die Simulations.
     * @param dynastie Verknüpfte Dynastie.
     * @param lager Verknüpftes Lager (in derselben Stadt).
     * @param minuten Anzahl zu arbeitende Minuten.
     * @param neuer_tag Hat ein neuer Tag begonnen? Dann werden Arbeiter bezahlt.
     * @param zeit Aktuelle Zeit. Es wird von 8-18 Uhr gearbeitet.
     */
    void tick(Welt* welt, Dynastie* dynastie, Lager* lager, Markt* markt, Land* land, Stadt* stadt,
              unsigned minuten, bool neuer_tag, const Zeit& zeit, Statistik& statistik);

private:

    /// Id.
    Netzwerk::id_t id;

    /// Frei zur Verfügung stehender Platz.
    uint32_t freier_platz;

    /// Sollen Bedarfsgüter automatisch eingekauft werden?
    bool auto_kauf;

    /// Freie Arbeiter Pool
    std::unordered_set<Netzwerk::id_t> freie_arbeiter;

    /// Anlagen im Werk.
    std::vector<Anlage> anlagen;

    /// Bei der Simulation werden hier die letzten Tageskosten festgehalten.
    mutable uint32_t cache_tageskosten = 0.f;

};
