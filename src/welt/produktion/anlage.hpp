#pragma once

#include "arbeiter.hpp"
#include "../waren/prozess.hpp"
#include "../../netzwerk/netzwerk.hpp"
#include "../spieler/charakter.hpp"
#include <cereal/types/vector.hpp>
#include <nada/ini.hpp>

/// Für Simulations-Ticks.
class Welt;
class Dynastie;
class Lager;

/**
 * @brief Speichert Informationen über den Status einer Anlage in der Werkstatt
 */
class Anlage final {

    friend class Host;
    friend class Werkstatt;
    static inline nada::Ini cfg{"config.ini"};

public:

    /// 50% Produktivität ohne Boni (bei Arbeitern ohne Erfahrung, ohne zusätzlichen Platz, ohne Massenproduktionsbonus).
    static const inline float BASISPRODUKTIVITAET = cfg.get_float<float>("basisproduktivitaet", 0.5f);

    /// Produktionsmultiplikator; hiermit kann das Spiel (die Produktion) beschleunigt werden. 1 = Normal.
    static const inline float PROD_MULTI = cfg.get_float<float>("prod_multi", 1.0f);

public:

    /// Ctor.
    Anlage() = default;

    /// Erzeugt eine Anlage mit passender Größe für gegebenen Prozess.
    explicit Anlage(const std::string& prozess_key);

    /// Liefert den hiesigen Prozess.
    const Prozess& get_prozess() const { return Prozess::get(prozess_key); }

    /// Liefert alle hiesigen Arbeiter.
    const std::unordered_set<Netzwerk::id_t>& get_arbeiter() const { return arbeiter; }

    /// Wieviel Raum nimmt die Anlage ein?
    uint16_t get_groesse() const;

    /// Wie oft wurde der Prozess abgeschlossen?
    uint32_t get_zaehler() const;

    /// Tägliche Kosten.
    uint32_t get_taegliche_kosten() const { return cache_tageskosten; }

    /// Wie teuer war die Produktion bisher? TODO benutzen
    float get_zaehler_kosten() const { return zaehler_kosten; }

    /// Relativer Anteil der getanen Arbeit. Als Dezimalbruch.
    float get_fortschritt() const;

    /// Ist die Produktion aktiviert (ausgeschaltet/abgeschaltet)?
    bool is_aktiviert() const;

    /// Läuft die Produktion (alle Voraussetzungen erfüllt)?
    bool is_in_betrieb() const;

    /// Ist alles nötige zur Produktion vorhanden?
    bool is_hat_inputs() const { return hat_inputs; }

    /// Ist die gegebene Ware ein Produkt dieser Anlage?
    bool is_produkt(const std::string& warenkey) const;

    /// Wird von Host/Welt über Werkstatt durchgeführt.
    void tick(Welt* welt, Lager* lager, unsigned minuten);

    /// Berechnet den Produktivitätsbonus durch zusätzlichen Platz.
    float get_bonus_platz() const;

    /// Berechnet den Produktivitätsbonus durch Massenproduktion.
    float get_bonus_massenproduktion() const;

    /// Berechnet den Produktivitätsbonus durch alle Arbeiter (Erfahrungsbasiert).
    float get_bonus_arbeiter() const { return cache_arbeiterbonus; }
    float get_bonus_arbeiter(Welt* welt) const;

    /// Berechnet die Gesamtproduktivität. Negativ beeinfluss durch zuviele Arbeiter.
    float get_produktivitaet() const { return cache_produktivitaet; }
    float get_produktivitaet(Welt* welt) const;

    /// Serialisierung via cereal.
    template <class Archive> void serialize(Archive& ar) {
        ar(prozess_key, groesse,
           arbeit_getan, zaehler, zaehler_kosten,
           quali_inputs, hat_inputs, aktiviert,
           arbeiter,
           cache_produktivitaet, cache_arbeiterbonus, cache_tageskosten
        );
    }

private:

    /// Führt einen Arbeitsschritt aus.
    void arbeiten(Welt* welt, unsigned minuten);

private:

    /// Verwendeter Prozess.
    std::string prozess_key;

    /// Liste aller hiesigen Arbeiter.
    std::unordered_set<Netzwerk::id_t> arbeiter;

    /// Größe der Anlage.
    uint16_t groesse;

    /// Wie oft wurde der Prozess abgeschlossen?
    uint32_t zaehler;

    /* Aktueller Fortschritt */

    /// Absoluter Teil der getanen Arbeit in Minuten.
    uint32_t arbeit_getan;

    /// Qualität der aktuellen Inputs.
    float quali_inputs;

    /// Wie teuer war die Produktion bisher?
    float zaehler_kosten;

    /// Ist die Produktion aktiv?
    bool aktiviert;

    /// Ist alles nötige zur Produktion vorhanden?
    bool hat_inputs;

    mutable float cache_produktivitaet = 0; // Produktivität, die bei der Simulation berechnet wird und danach nur zur Anzeige dient.
    mutable float cache_arbeiterbonus  = 0; // Arbeiterbonus, der bei der Simulation berechnet wird und danach nur zur Anzeige dient.
    mutable uint32_t cache_tageskosten = 0; // Tageskosten,   was bei der Simulation berechnet wird und danach nur zur Anzeige dient.

};
