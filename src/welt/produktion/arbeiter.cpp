#include "arbeiter.hpp"
#include "../waren/prozess.hpp"
#include "../kultur.hpp"
#include "../stadt/stadt.hpp"
#include <nada/random.hpp>

Arbeiter::Arbeiter(const Kultur& kultur, const Stadt& stadt) :
        tageslohn(Arbeiter::BASISLOHN)
{
    (void) kultur;

    // Zufällige Fertigkeiten geben
    const auto& prozessgruppen = Prozess::alle_gruppen();
    for (const auto& prozessgruppe : prozessgruppen) {
        if      (nada::random::b( 1)) skills[prozessgruppe] = nada::random::f(25.f, 100.f);
        else if (nada::random::b( 5)) skills[prozessgruppe] = nada::random::f(10.f,  25.f);
        else if (nada::random::b(10)) skills[prozessgruppe] = nada::random::f( 1.f,  10.f);
    }

    // Gehalt nach Reichtum der Stadt + Skills
    (void) stadt; // TODO Gehalt nach Reichtum der Stadt
    for (const auto& paar : skills) tageslohn += std::round(0.1f * paar.second);
    tageslohn += nada::random::i(-std::min(3, tageslohn - 1), 3);
}

void Arbeiter::train(const std::string& skill_key, unsigned minuten) {
    skills[skill_key] += 0.0001f * static_cast<float>(minuten);
    if (skills[skill_key] > 100.f) skills[skill_key] = 100.f;
}

float Arbeiter::get_skill(const std::string &key) const {
    try { return skills.at(key); }
    catch (std::exception& e) { return 0.f; } // Skill nicht vorhanden => 0
}

bool Arbeiter::hat_skill(const std::string& key) const {
    return skills.count(key) != 0;
}

Grafik& Arbeiter::get_skill_icon(const std::string& key) {
    static std::unordered_map<std::string, Grafik> icons;
    if (icons.count(key) == 0) icons[key] = Grafik("data/gfx/prozessgruppen/" + key + ".png");
    return icons.at(key);
}
