#include "anlage.hpp"
#include "../waren/lager.hpp"
#include "../waren/prozess.hpp"
#include "../spieler/dynastie.hpp"
#include "../welt.hpp"

Anlage::Anlage(const std::string& prozess_key) :
        prozess_key(prozess_key),
        groesse(get_prozess().platzverbrauch),
        zaehler(0),
        arbeit_getan(0),
        quali_inputs(0.f),
        zaehler_kosten(0.f),
        aktiviert(true),
        hat_inputs(false)
{
    //
}

uint16_t Anlage::get_groesse() const {
    return groesse;
}

float Anlage::get_fortschritt() const {
    return static_cast<float>(arbeit_getan) /
          (static_cast<float>(get_prozess().get_arbeitsaufwand()) * 60.f);
}

bool Anlage::is_aktiviert() const {
    return aktiviert;
}

uint32_t Anlage::get_zaehler() const {
    return zaehler;
}

void Anlage::tick(Welt* welt, Lager* lager, unsigned minuten) {
    if (arbeiter.empty() || !is_aktiviert()) return;
    const auto& prozess = get_prozess();

    // Benötigte Waren beschaffen
    if (!hat_inputs) {
        bool hat_lager_inputs = true;
        for (const auto& input : prozess.get_inputs()) {
            if (lager->get_vorrat(input.first).menge < input.second) {
                hat_lager_inputs = false;
                break;
            }
        }
        // Neue Produktion initialisieren
        if (hat_lager_inputs) {
            zaehler_kosten = 0.f; // Kosten neu berechnen
            quali_inputs   = 0.f;
            unsigned anzahl = 0; // Input Waren Anzahl
            for (const auto& input : prozess.get_inputs()) {
                Vorrat& vorrat = lager->get_vorrat(input.first);
                vorrat.entnehmen(input.second);
                anzahl += input.second;
                quali_inputs   += vorrat.quali * static_cast<float>(input.second);
                zaehler_kosten += vorrat.preis * static_cast<float>(input.second);
            }
            quali_inputs /= static_cast<float>(anzahl);
            hat_inputs = true;
        }
    }

    // Arbeit ausführen
    if (hat_inputs) arbeiten(welt, minuten);

    // Erledigt?
    if (arbeit_getan >= prozess.get_arbeitsaufwand() * 60) {

        // Reset
        hat_inputs = false; // verbraucht
        arbeit_getan = 0;
        zaehler++;

        // Outputs zählen
        unsigned anzahl_outputs = 0;
        for (const auto& output : prozess.get_outputs()) {
            anzahl_outputs += output.second;
        }
        // Vorräte einlagern
        for (const auto& output : prozess.get_outputs()) {
            Vorrat vorrat(false);
            vorrat.menge = output.second;
            // Quali berechnen
            float skill = 0;
            for (const auto& a : arbeiter) skill += welt->charaktere.at(a).get_arbeiter().value().get_skill(prozess_key);
            skill = (skill / arbeiter.size()) / 100.f;
            vorrat.quali = (quali_inputs + prozess.get_basisquali() + skill) / 2.f;
            vorrat.preis = (zaehler_kosten * static_cast<float>(output.second)) / static_cast<float>(anzahl_outputs);
            lager->get_vorrat(output.first).einlagern(vorrat, vorrat.menge);
        }
    }
}

void Anlage::arbeiten(Welt* welt, unsigned minuten) {
    arbeit_getan += get_produktivitaet(welt) * static_cast<float>(minuten) * PROD_MULTI * static_cast<float>(arbeiter.size());
    for (auto& a : arbeiter) {
        Charakter& c = welt->charaktere.at(a);
        c.arbeiter.value().train(get_prozess().get_gruppe(), minuten);

        // Kosten durch Arbeiter berechnen
        const float min_pro_tag = 10.f * 60.f; // Minuten pro Tag, die ein Arbeiter arbeitet
        zaehler_kosten += static_cast<float>(c.get_arbeiter().value().get_tageslohn()) /
                         (static_cast<float>(minuten) * min_pro_tag);
    }
}

bool Anlage::is_in_betrieb() const {
    return is_aktiviert() && get_fortschritt() > 0 && !get_arbeiter().empty();
}

float Anlage::get_bonus_platz() const {
    const Prozess& prozess = get_prozess();
    const float platzbonus = static_cast<float>(get_groesse() - prozess.get_platzverbrauch()) * prozess.get_platzbonus();
    return platzbonus;
}

float Anlage::get_bonus_massenproduktion() const {
    const Prozess& prozess = get_prozess();
    const float massenbonus = std::min(static_cast<float>(zaehler * prozess.get_arbeitsaufwand()) / 1000.f, 1.0f);
    return massenbonus;
}

float Anlage::get_bonus_arbeiter(Welt* welt) const {
    if (arbeiter.empty()) return 0.f;
    const Prozess& prozess = get_prozess();
    float arbeiterbonus = 0.f;
    (void) prozess;
    for (const auto& a : arbeiter) arbeiterbonus += welt->charaktere.at(a).get_arbeiter().value().get_skill(prozess.get_gruppe()) / 100.f;
    arbeiterbonus /= static_cast<float>(arbeiter.size());
    cache_arbeiterbonus = arbeiterbonus;
    return cache_arbeiterbonus;
}

float Anlage::get_produktivitaet(Welt* welt) const {
    float p = BASISPRODUKTIVITAET + get_bonus_platz() + get_bonus_massenproduktion() + get_bonus_arbeiter(welt);
    for (unsigned i = get_prozess().arbeiter; i < arbeiter.size(); ++i) p *= get_prozess().get_arbeiterbonus();
    cache_produktivitaet = std::clamp(p, 0.f, 1.0f);
    return cache_produktivitaet;
}

bool Anlage::is_produkt(const std::string& warenkey) const {
    return (std::find_if(get_prozess().get_outputs().begin(), get_prozess().get_outputs().end(),
            [&](const auto& pair) { return pair.first == warenkey; }) != get_prozess().get_outputs().end());
}
