#pragma once

#include "../../gfx/grafik.hpp"

#include <unordered_map>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/string.hpp>

class Kultur;
class Stadt;

class Arbeiter final {

public:

    /// Basislohnniveau.
    static constexpr unsigned BASISLOHN = 20;

    /// Liefert für gg. Fertigkeit den Icon.
    static Grafik& get_skill_icon(const std::string& key);

public:

    Arbeiter() = default;

    /// Erzeugt einen zufälligen neuen Arbeiter.
    Arbeiter(const Kultur& kultur, const Stadt& stadt);

    /// Verbessert angegebenen Skill 1 Mal.
    void train(const std::string& skill_key, unsigned minuten);

    /// Maximum: 100. Nicht vorhandene Skills fehlen auch in der Map.
    const std::unordered_map<std::string, float>& get_skills() const { return skills; }

    /// Maximum: 100.
    float get_skill(const std::string& key) const;

    /// Hat der Arbeiter gg. Skill?
    bool hat_skill(const std::string& key) const;

    /// Täglicher Lohn des Arbeiters.
    uint16_t get_tageslohn() const { return tageslohn; }

    /// Cereal (De-)Serialisierung.
    template <class Archive> void serialize(Archive& ar) {
        ar(tageslohn, skills);
    }

private:

    /// Aktueller Tageslohn.
    uint16_t tageslohn;

    /// Maximum jedes Skills: 100.
    std::unordered_map<std::string, float> skills;

};
