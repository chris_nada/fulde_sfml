#include "werkstatt.hpp"
#include "../../werkzeuge/etc.hpp"
#include "../spieler/dynastie.hpp"
#include "../zeit.hpp"
#include "../land.hpp"
#include "../welt.hpp"

#include <nada/log.hpp>

using nada::Log;

Werkstatt::Werkstatt(Netzwerk::id_t id) : id(id), freier_platz(3), auto_kauf(false)
{
    //
}

Werkstatt::Werkstatt(std::stringstream& ss) {
    etc::serial_in in(ss);
    in(*this);
}

std::ostream& operator<<(std::ostream& os, const Werkstatt& werk) {
    etc::serial_out boa(os);
    boa(werk);
    return os;
}

void Werkstatt::tick(Welt* welt, Dynastie* dynastie, Lager* lager, Markt* markt, Land* land, Stadt* stadt, unsigned int minuten, bool neuer_tag, const Zeit& zeit, Statistik& statistik) {
    if (neuer_tag) dynastie->geld_bezahlen(get_taegliche_kosten(welt), Statistik::Ausgabe::GEHALT, statistik);

    // Automatisches Einkaufen
    if (auto_kauf) {
        std::unordered_map<std::string, unsigned> bedarfe;
        for (const Anlage& anlage : anlagen) { // Bedarf ermitteln
            for (const auto& input : anlage.get_prozess().get_inputs()) bedarfe[input.first] += input.second;
        }
        for (const auto& bedarf : bedarfe) { // Jeden Bedarf Einkaufen
            if (lager->get_vorrat(bedarf.first).menge < bedarf.second) {
                const unsigned fehlzahl = bedarf.second - lager->get_vorrat(bedarf.first).menge;
                markt->kauf(bedarf.first, fehlzahl, dynastie, lager, land, stadt, &statistik);
            }
        }
    }
    // Anlagen simulieren
    if (zeit.get_h() >= ARBEIT_START && zeit.get_h() <= ARBEIT_ENDE) for (Anlage& anlage : anlagen) anlage.tick(welt, lager, minuten);
}

uint32_t Werkstatt::get_taegliche_kosten(Welt* welt) const {
    uint32_t kosten_anlagen        = 0;
    uint32_t kosten_freie_arbeiter = 0;
    for (const Anlage& anlage : anlagen) {
        uint32_t kosten_anlage = 0;
        for (const auto& arbeiter : anlage.get_arbeiter()) kosten_anlage += welt->charaktere.at(arbeiter).get_arbeiter().value().get_tageslohn();
        anlage.cache_tageskosten = kosten_anlage;
        kosten_anlagen += kosten_anlage;
    }
    for (const auto& arbeiter : get_freie_arbeiter()) {
        kosten_freie_arbeiter += welt->charaktere.at(arbeiter).get_arbeiter().value().get_tageslohn() / 2;
    }
    cache_tageskosten = kosten_anlagen + kosten_freie_arbeiter;
    return kosten_anlagen;
}

uint32_t Werkstatt::get_platz_anlagen() const {
    unsigned platz = 0;
    for (const Anlage& anlage : anlagen) platz += anlage.get_groesse();
    return platz;
}

std::string Werkstatt::get_prozessgruppe() const {
    return anlagen.empty() ? "" : anlagen[0].get_prozess().get_gruppe();
}

void Werkstatt::transfer_arbeiter(Netzwerk::id_t char_id, uint8_t index_anlage) {
    // True, wenn Transfer erfolgreich.
    auto transfer = [=](decltype(freie_arbeiter)& v1, decltype(freie_arbeiter)& v2) {
        if (v1.count(char_id) && !v2.count(char_id)) {
            v2.insert(char_id);
            v1.erase(char_id);
            return true;
        }
        return false;
    };
    Anlage& anlage = anlagen.at(index_anlage);
    if (transfer(freie_arbeiter, anlage.arbeiter)) return;
    if (transfer(anlage.arbeiter, freie_arbeiter)) return;
    Log::err() << "\tWerkstatt::transfer_arbeiter Arbeiter " << char_id << " nicht gefunden (oder doppelt)\n";
}

int32_t Werkstatt::get_preis_erweitern() const {
    return (get_platz_anlagen() + get_freier_platz()) * 100; // TODO Preis dynamisch
}
