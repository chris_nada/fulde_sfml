#pragma once

#include <doctest.h>
#include "../welt/welt.hpp"
#include "../welt/gegner/gegner.hpp"

class Test_Welt_Gegner {

protected:

    static inline std::stringstream console{};

    // Testet die Gegner und deren Wirtschaft
    TEST_CASE_CLASS("gegner") {
        static Welt welt;
        if (welt.get_zeit().get_jahr() == Zeit::START_JAHR) welt.skip(15 * 4); // x * 4 (x Jahre)
        SUBCASE("Ware im Markt nie vorhanden?") {
            for (const Markt& markt : welt.maerkte) {
                for (const auto& paar : markt.get_vorraete()) {
                    const unsigned lagermenge = static_cast<unsigned>(std::round(
                            std::accumulate(paar.second.menge_v.begin(), paar.second.menge_v.end(), 0.f)
                    ));
                    SUBCASE(paar.first.c_str()) {
                        CHECK_GT(lagermenge, 0);
                    }
                }
            }
        }
        SUBCASE("Arbeiter vorhanden?") {
            for (const Werkstatt& werk : welt.werkstaetten) {
                for (const Anlage& anlage : werk.get_anlagen()) {
                    if (&anlage == &werk.get_anlagen().at(werk.get_anlagen().size()-1)) {
                        continue; // Letzte Anlage könnte neu sein, daher nicht geprüft
                    }
                    CHECK_GT(anlage.get_arbeiter().size(), 0);
                }
            }
        }
        SUBCASE("Prozesse alle vorhanden?") {
            std::unordered_set<std::string> waren_outputs;
            for (const Werkstatt& werk : welt.werkstaetten) {
                for (const Anlage& anlage : werk.get_anlagen()) {
                    for (const auto& paar : anlage.get_prozess().get_outputs()) waren_outputs.insert(paar.first);
                }
            }
            for (const auto& paar : Ware::alle()) CHECK_NE(waren_outputs.count(paar.first), 0);
        }
        SUBCASE("Aemter gueltig besetzt?") {
            unsigned besetzte_aemter = 0;
            const auto& aemter = Amt::get_aemter_aus_welt(welt);
            for (const auto& amt : aemter) {
                if (amt->is_besetzt()) besetzte_aemter++;
            }
            CHECK_GE(besetzte_aemter, aemter.size() / 2); // Mindestens die Hälfte ist besetzt
            console << (besetzte_aemter * 100u) / aemter.size() << "% Aemter besetzt (" << besetzte_aemter << '/' << aemter.size() << ")\n";
        }
        SUBCASE("Amtsinhaber haben nur 1 Amt") {
            const auto& aemter = Amt::get_aemter_aus_welt(welt);
            for (const auto& amt : aemter) {
                if (amt->is_besetzt()) {
                    for (const auto& amt2 : aemter) {
                        if (&amt == &amt2) continue;
                        if (amt2->is_besetzt()) CHECK_NE(amt->get_inhaber(), amt2->get_inhaber());
                    }
                }
            }
        }
    }

};
