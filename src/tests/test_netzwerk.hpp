#pragma once

#include "../netzwerk/netzwerkpaket.hpp"
#include "../welt/welt.hpp"
#include "../welt/gegner/gegner.hpp"

#include <doctest.h>

class Test_Netzwerk {

protected:

    /// Logger für Benchmarks.
    static inline std::stringstream console{};

    TEST_CASE_CLASS("netzwerk") {
        SUBCASE("paket") {
            for (unsigned size = 50; size <= 500; size += 50) {
                Netzwerkpaket paket;
                std::string data_in;
                data_in.reserve(size);
                for (unsigned i = 0; i < size; ++i) data_in += ('a' + nada::random::get<char>(0, 3));
                paket << data_in << size; // TODO reale Daten testen

                // Daten "senden"
                console << "Paket unkomprimiert>komprimiert = " << paket.getDataSize() << '>';
                size_t s = paket.getDataSize();
                const void* data_ptr = paket.onSend(s);
                console << s << '\n';

                Netzwerkpaket paket2;
                paket2.onReceive(data_ptr, s);
                std::string data_out;
                unsigned tsize;
                paket2 >> data_out >> tsize;
                REQUIRE_EQ(data_in, data_out);
                REQUIRE_EQ(tsize, size);
            }
        }
        SUBCASE("serialisierung") {
            static Welt welt;
            static auto serialisieren = [](const auto& objekt) {
                Netzwerkpaket paket;
                std::stringstream ss;
                etc::serial_out out(ss);
                out(objekt);
                std::string data(ss.str());
                paket << data;
                size_t size_data = data.size();
                size_t size_paket_unkomp = paket.getDataSize();
                size_t size_paket_komp   = paket.getDataSize();
                paket.onSend(size_paket_komp);
                std::stringstream logger;
                logger << "Paket " << " ss.str() / paket / paket.onSend() = ";
                logger << size_data << ", " << size_paket_unkomp << ", " << size_paket_komp;
                if (logger.str().size() < 70) {
                    for (unsigned i = 0; i < 70 - logger.str().size(); ++i) logger << ' ';
                }
                logger << typeid(objekt).name() << '\n';
                console << logger.str();
                REQUIRE_LE(size_data + 4, Netzwerkpaket::PUFFER_BYTES); // Muss in den Paketpuffer "passen"
                REQUIRE_EQ(size_data + 4, size_paket_unkomp); // Paket hat 4 byte "Overhead".
            };
            serialisieren(welt.get_zeit());
            serialisieren(welt.kulturen.at(0));
            serialisieren(welt.laender.at(0));
            serialisieren(welt.staedte.at(0));
            serialisieren(welt.dynastien.at(0));
            serialisieren(welt.statistiken.at(0));
            serialisieren(welt.maerkte.at(0));
            serialisieren(welt.viertel.at(0));
            serialisieren(welt.gebaeude.at(0));
            serialisieren(welt.werkstaetten.at(0));
            //serialisieren(*welt.wohnhaeuser.at(0));
            serialisieren(welt.lager.at(0));
            //serialisieren(welt.charaktere.at(0)); // TODO
            serialisieren(welt.gegner.at(0));
            serialisieren(*welt.aemter.begin()->second.begin()->second.begin()); // SIGSEGV wenn 0 Ämter
            serialisieren(welt);
        }
    }

};
