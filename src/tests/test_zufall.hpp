#pragma once

#include <doctest.h>
#include <unordered_set>
#include <unordered_map>

#include <nada/random.hpp>

using namespace nada;

class Test_Zufall {

protected:

    TEST_CASE_CLASS("zufall") {
        SUBCASE("int") {
            for (int test_min = -10; test_min <= 10; test_min++) {
                for (int test_max = test_min + 1; test_max <= test_min + 20; test_max++) {
                    std::map<int, int> ergebnisse;
                    for (unsigned i = 0; i < (unsigned)((test_max - test_min) * 100); ++i) ergebnisse[random::i(test_min, test_max)] += 1;
                    CHECK_EQ(ergebnisse.size(), test_max - test_min + 1);
                    for (int i = test_min; i <= test_max; ++i) {
                        if (ergebnisse[i] == 0) {
                            std::stringstream ss;
                            ss << "Wert " << i << " kommt nicht vor fuer min=" << test_min << " max=" << test_max;
                            REQUIRE_MESSAGE(false, ss.str().c_str());
                        }
                    }
                }
            }
        }
        SUBCASE("float") {
            float f;
            for (unsigned i = 0; i < 1000; ++i) {
                f = random::f(-100.f, 100.f);
                REQUIRE_GE(f, -100.f);
                REQUIRE_LE(f,  100.f);
                f = random::f(-0.1f, 0.1f);
                REQUIRE_GE(f, -0.1f);
                REQUIRE_LE(f,  0.1f);
                f = random::f(0.009f, 0.011f);
                REQUIRE_EQ(f, doctest::Approx(0.01f).epsilon(0.1f));
            }
        }
        SUBCASE("bool") {
            std::map<bool, unsigned> ergebnisse;
            for (unsigned i = 0; i < 1000; ++i) ergebnisse[random::b(50)] += 1;
            REQUIRE_EQ(ergebnisse.size(), 2); // true und false kommt vor
            REQUIRE_LT(ergebnisse.at(0), ergebnisse.at(1) * 2); // nicht wesentlich mehr
            REQUIRE_LT(ergebnisse.at(1), ergebnisse.at(0) * 2); // true oder false
        }
    }

};
