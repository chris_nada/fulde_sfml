#pragma once

#include <doctest.h>
#include <nada/fs.hpp>
#include "../welt/welt.hpp"
#include "../welt/gegner/gegner.hpp"

class Test_Welt_Generierung {

protected:

    // Testet die Welt
    TEST_CASE_CLASS("welt") {

        SUBCASE("dateien vorhanden") {
            SUBCASE("aemter_xmls") {
                const auto& aemter_xmls = nada::fs::all_files("data/aemter", "xml");
                REQUIRE_GT(aemter_xmls.size(), 0);
            }
        }

        SUBCASE("generierung") {
            static Welt welt;
            SUBCASE("bestandteile valide") {
                auto vektorcheck = [](const auto& v) {
                    const std::string fallname = std::string(typeid(v).name()) + " vektorcheck";
                    SUBCASE(fallname.c_str()) {
                    REQUIRE(!v.empty());
                    for (unsigned i = 0; i < v.size(); ++i) {
                        REQUIRE_NOTHROW(v.at(i));
                        REQUIRE_EQ(v.at(i).get_id(), i);
                    }
                }
                };
                vektorcheck(welt.kulturen);
                vektorcheck(welt.laender);
                vektorcheck(welt.staedte);
                vektorcheck(welt.viertel);
                vektorcheck(welt.dynastien);
                vektorcheck(welt.gebaeude);
                vektorcheck(welt.statistiken);
                vektorcheck(welt.gegner);
                vektorcheck(welt.lager);
                vektorcheck(welt.maerkte);
                vektorcheck(welt.werkstaetten);
                // vektorcheck(welt.wohnhaeuser); // TODO, noch unbenutzt
                vektorcheck(welt.charaktere);
            }
            SUBCASE("bestandteile anzahl logik") {
                REQUIRE_GT(Amt::alle().size(), 0);
                REQUIRE_GT(Infrastruktur::alle().size(), 0);
                REQUIRE_LE(welt.laender.size(), welt.staedte.size());
                REQUIRE_LE(welt.staedte.size(), welt.viertel.size());
                REQUIRE_EQ(welt.gegner.size(), welt.werkstaetten.size());
                REQUIRE_EQ(welt.gegner.size(), welt.dynastien.size());
                REQUIRE_EQ(welt.dynastien.size(), welt.statistiken.size());
                REQUIRE_EQ(welt.dynastien.size(), welt.lager.size());
                REQUIRE_EQ(welt.dynastien.size(), welt.werkstaetten.size());
                REQUIRE_LE(welt.dynastien.size(), welt.charaktere.size());
                REQUIRE_EQ(welt.dynastien.size(),
                           welt.wohnhaeuser.size() + welt.gegner.size()); // Gegner haben kein Wohnhaus
                REQUIRE_EQ(welt.gebaeude.size(), welt.werkstaetten.size() + welt.wohnhaeuser.size() +
                                                 welt.maerkte.size() + welt.lager.size() +
                                                 welt.staedte.size() * 4 // Kirche, Rathaus, Bank, Schattengilde
                );
            }
            SUBCASE("stadt infrastruktur") {
                for (const auto& infra : Infrastruktur::alle()) {
                    REQUIRE_FALSE(infra.second.get_key().empty());
                    REQUIRE_MESSAGE(infra.second.get_icon().find('.') != std::string::npos, infra.second.get_key());
                    REQUIRE_MESSAGE((!infra.second.is_kritisch() || (infra.second.is_kritisch() && infra.second.get_versorgt() > 0)), infra.second.get_key());
                    for (const auto& stadt : welt.staedte) {
                        REQUIRE_NOTHROW(stadt.infrastruktur.at(infra.first)); // Jede Infrastruktur eingetragen
                    }
                }
            }
        }
    }

};
