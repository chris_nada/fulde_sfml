#pragma once

#define DOCTEST_CONFIG_IMPLEMENT
#include <doctest.h>
#include <rang.hpp>

#include "test_zufall.hpp"
#include "test_welt_generierung.hpp"
#include "test_welt_gegner.hpp"
#include "test_netzwerk.hpp"

/**
 * Startet / verwaltet Tests.
 *
 * Um nur bestimmte Testfällle (TEST_CASE) aufzurufen:
 * --test-case=<name>
 * als Parameter hinzufügen.
 * Beispiel:
 * --test --test-case=netzwerk --subcase=serialisierung
 * https://github.com/onqtam/doctest/blob/master/doc/markdown/commandline.md
 */
class Test final :
        private Test_Zufall,
        private Test_Welt_Generierung,
        private Test_Welt_Gegner,
        private Test_Netzwerk
{

public:

    /// Startet alle Testfälle.
    static int run_tests(int argc, char** argv) {
        doctest::Context context;
        context.applyCommandLine(argc, argv);
        int result = 0;
        auto test_runner = [&]() { result = context.run(); };

        Log::benchmark(test_runner, "test_runner");
        Log::out() << rang::fg::yellow << "= Weitere Statistiken =========================================================\n";
        Log::out() << rang::fg::reset;
        //Log::out() << rang::style::bold << Test_Welt_Gegner::console.str() << rang::style::reset; // TODO
        Log::out() << Test_Netzwerk::console.str();
        Log::out() << rang::fg::yellow << "===============================================================================\n";
        return result;
    }

};
