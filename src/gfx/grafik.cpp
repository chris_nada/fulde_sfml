#include "grafik.hpp"
#include <nada/log.hpp>
#include "../werkzeuge/assets.hpp"

using nada::Log;

Grafik::Grafik(const std::string& textur_pfad, bool is_shared) :
    is_shared(is_shared),
    is_flipped(false)
{
    // Textur noch nicht vorhanden
    if (shared_texturen.count(textur_pfad) == 0) {
        textur = new sf::Texture;
        if (Assets::good()) {
            const auto& asset = Assets::get(textur_pfad);
            if (!asset.empty() && textur->loadFromMemory(asset.data(), asset.size())) {
                Log::debug() << "\t\tGrafik erzeugt: " << textur_pfad << '\n';
            } else Log::err() << "Grafik() Textur " << textur_pfad << " konnte nicht geladen werden.\n";
        } else Log::err() << "Grafik() assets.pack konnte nicht geladen werden.\n";
        shared_texturen[textur_pfad] = textur;
    }
    else textur = shared_texturen[textur_pfad]; // Textur aus Cache laden

    // Textur setzen
    if (textur) sprite.setTexture(*textur);
}

Grafik::~Grafik() {
    if (!is_shared && textur) delete textur;
}

void Grafik::set_flip(bool flip) {
    if (flip == is_flipped) return;
    const auto& r = sprite.getTextureRect();
    if (flip) {
        sprite.setTextureRect(sf::IntRect(std::abs(r.width), 0, -std::abs(r.width), r.height));
    }
    else {
        sprite.setTextureRect(sf::IntRect(0, 0, std::abs(r.width), r.height));
    }
    is_flipped = flip;
}

void Grafik::set_size(const sf::Vector2f& size) {
    sprite.setScale(size.x / textur->getSize().x, size.y / textur->getSize().y);
}

sf::Vector2f Grafik::size_f() const {
    return sf::Vector2f(size().x, size().y);
}

bool Grafik::is_inside(float x, float y) const {
    return sprite.getGlobalBounds().contains(x, y);
}
