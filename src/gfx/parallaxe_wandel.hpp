#pragma once

#include "parallaxe.hpp"
#include "../welt/zeit.hpp"

/**
 * Eine Parallaxe, die aus 2 Parallaxen für Tag und Nacht besteht.
 */
class Parallaxe_Wandel final {

public:

    static Parallaxe_Wandel get_himmel(Netzwerk::id_t id_land);

    /// Ctor.
    Parallaxe_Wandel() = default;

    /**
     * Konstruktor.
     * @mod_tag Wenn != 0, wandert mit der Zeit von selbst um gegebenen Wert in Pixeln pro sekunde.
     * @mod_nacht Wenn != 0, wandert mit der Zeit von selbst um gegebenen Wert in Pixeln pro sekunde.
     * @param grafik_pfad Pfad zum Ordner, in dem sich `tag.png` und `nacht.png` befindet.
     * @param anzahl Wie oft die Grafik in jede Richtung wiederholt werden soll.
     */
    explicit Parallaxe_Wandel(const std::string& grafik_pfad, float mod_tag = 0, float mod_nacht = 0, int anzahl = 2);

    /// Rendert die Grafiken im angegenem Fenster.
    void draw(sf::RenderWindow* fenster, const Zeit& zeit);

    /// Liefert die X-Größe der Parallaxen.
    int get_size_x() const;

    /// Liefert die Y-Größe der Parallaxen.
    int get_size_y() const;

    /// Setzt die x-Koordinate der Grafik.
    void set_x_offset(float x);

    /// Legt die y-Koordinate für die Grafik fest.
    void set_y(float y);

    /// Ändert die Mitlaufgeschwindigkeit der Tagesparallaxe. Kann z.B. schnelleren/langsameren Wind simulieren.
    void set_mod_tag(float mod_tag);

private:

    sf::Clock timer;

    Parallaxe tag;
    Parallaxe nacht;

    float x_offset;
    float x_tag;
    float x_nacht;
    float mod_tag;
    float mod_nacht;

};
