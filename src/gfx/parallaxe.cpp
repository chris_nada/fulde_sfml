#include "parallaxe.hpp"

Parallaxe::Parallaxe(const std::string &grafik_pfad, int anzahl) : x(0), y(0), grafik_pfad(grafik_pfad) {
    const Grafik g(grafik_pfad);
    size_x = g.size().x;
    size_y = g.size().y;
    set_anzahl(anzahl);
    set_x(x);
}

void Parallaxe::draw(sf::RenderWindow *fenster) {
    for (const auto& paar : grafiken) paar.second.draw(fenster);
}

void Parallaxe::set_x(float x) {
    Parallaxe::x = x;
    //const sf::RenderWindow* fenster = Gui::fenster;
    //const auto res_x = fenster->getSize().x;

    for (auto& paar : grafiken) {
        paar.second.set_x((float)(paar.first * size_x) + x);
    }
}

void Parallaxe::set_y(float y) {
    Parallaxe::y = y;
    for (auto& paar : grafiken) paar.second.set_y(y);
}

void Parallaxe::set_anzahl(int anzahl) {
    if ((anzahl * 2 + 1) < (int)grafiken.size()) grafiken.clear(); // Schrumpfen
    for (int i = -anzahl; i <= anzahl; ++i) {
        // Grafik einfügen?
        if (grafiken.count(i) == 0) grafiken[i] = Grafik(grafik_pfad);
    }
}

void Parallaxe::set_color(const sf::Color &farbe) {
    for (auto& paar : grafiken) paar.second.set_color(farbe);
}
