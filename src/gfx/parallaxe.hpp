#pragma once

#include <utility>
#include <unordered_map>

#include "grafik.hpp"
#include "../gui/gui.hpp"

/**
 * Zum Anzeigen sich horizontal wiederholender Grafiken,
 * z.B. sog. parallaxe Hintergründe.
 */
class Parallaxe final {

public:

    /// Default Ctor.
    Parallaxe() = default;

    /**
     * Konstruktor.
     * @param grafik_pfad Anzuzeigende, sich wiederholende Grafik(en).
     * @param anzahl Wie oft die Grafik in jede Richtung wiederholt werden soll.
     */
    explicit Parallaxe(const std::string&  grafik_pfad, int anzahl = 10);

    /// Rendert die Grafiken im angegenem Fenster.
    void draw(sf::RenderWindow* fenster);

    /// Liefert die aktuelle x-Koordinate der Parallaxe.
    float get_x() const { return x; }

    /// Liefert die aktuelle y-Koordinate der Parallaxe.
    float get_y() const { return y; }

    /// Liefert die x-Größe einer einzelnen verwendeten Grafik.
    int get_size_x() const { return size_x; }

    /// Liefert die y-Größe einer einzelnen verwendeten Grafik.
    int get_size_y() const { return size_y; }

    /// Setzt die x-Koordinate der Grafik.
    void set_x(float x);

    /// Legt die y-Koordinate für die Grafik fest.
    void set_y(float y);

    /// Reguliert die Anzahl an Grafiken nach *jeweils* links und rechts.
    void set_anzahl(int anzahl);

    /// Manipuliert (multipliziert) die Farbe der Grafik.
    void set_color(const sf::Color& farbe);

protected:

    float x, y;
    int size_x, size_y;
    std::string grafik_pfad;
    std::unordered_map<int, Grafik> grafiken;

};
