#include <cmath>
#include <nada/log.hpp>
#include <filesystem>
#include "parallaxe_wandel.hpp"

Parallaxe_Wandel Parallaxe_Wandel::get_himmel(Netzwerk::id_t id_land) {
    static const std::string pfad("data/gfx/viertel/himmel/");
    static std::vector<std::string> ordner;
    if (ordner.empty()) for (const auto& f : std::filesystem::directory_iterator(pfad)) {
        if (f.is_directory()) ordner.push_back(f.path().filename().string());
    }
    return Parallaxe_Wandel(pfad + ordner.at(id_land % ordner.size()),
            24.f, 8.f, 2);
}

Parallaxe_Wandel::Parallaxe_Wandel(const std::string& grafik_pfad, float mod_tag, float mod_nacht, int anzahl) :
        tag(grafik_pfad + "/tag.png", anzahl),
        nacht(grafik_pfad + "/nacht.png", anzahl),
        x_offset(0), x_tag(0), x_nacht(0),
        mod_tag(mod_tag), mod_nacht(mod_nacht)
{
    (void) x_tag;
    (void) x_nacht;
    set_y(0);
}

void Parallaxe_Wandel::draw(sf::RenderWindow* fenster, const Zeit& zeit) {
    const float zenit = 13.f; // Uhr
    const float hdiff = std::abs(zenit - (float) zeit.get_h()) / zenit;

    // Tag/Nacht
    tag.set_color({
        static_cast<sf::Uint8>(0xFF),
        static_cast<sf::Uint8>(0xFF),
        static_cast<sf::Uint8>(0xFF),
        static_cast<sf::Uint8>(0xFF * hdiff)
    });
    nacht.set_color({
        static_cast<sf::Uint8>(0xFF),
        static_cast<sf::Uint8>(0xFF),
        static_cast<sf::Uint8>(0xFF),
        static_cast<sf::Uint8>(0xFF - 0xFF * hdiff)
    });

    tag.set_x(  x_offset + std::fmod(mod_tag   * timer.getElapsedTime().asSeconds(), static_cast<float>(tag.get_size_x())));
    nacht.set_x(x_offset + std::fmod(mod_nacht * timer.getElapsedTime().asSeconds(), static_cast<float>(nacht.get_size_x())));

    tag.draw(fenster);
    nacht.draw(fenster);
}

void Parallaxe_Wandel::set_x_offset(float x) {
    x_offset = x;
}

void Parallaxe_Wandel::set_y(float y) {
    tag.set_y(y);
    nacht.set_y(y);
}

void Parallaxe_Wandel::set_mod_tag(float mod_tag) {
    Parallaxe_Wandel::mod_tag = mod_tag;
}

int Parallaxe_Wandel::get_size_x() const {
    return tag.get_size_x();
}

int Parallaxe_Wandel::get_size_y() const {
    return tag.get_size_y();
}
