#pragma once

#include "grafik.hpp"
#include "uhr.hpp"
#include "../welt/spieler/dynastie.hpp"
#include "../welt/stadt/stadt.hpp"

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

/**
 * Gui-Element, das den Rahmen mit Informationen wie Spielergeld, Spielerposition, Zeit usw. rendert.
 */
class Rahmen final {

public:

    /// Y-Größe des Infobalkens.
    static constexpr float ABSTAND_TOP = 28;

    /// *Muss* ausgeführt werden, bevor `draw` aufgerufen wird.
    static void init(const Dynastie& dynastie, const Stadt& stadt, const Zeit& zeit);

    /// Zeichnet das komplette Rahmenelement auf das Fenster der UI.
    static void draw();

    /// Aktualisiert Dynastiedaten, darunter auch Geld.
    static void set(const Dynastie& dynastie);

    /// Aktualisiert Informationen von Stadt und Viertel.
    static void set(const Stadt& stadt, const Viertel& viertel);

    /// Aktualisiert die Zeit.
    static void set(const Zeit& zeit);

    /// Getter: Zeit.
    static const Zeit& get_zeit();

private:

    /// Ausnahmsweise macht mal ein Singleton Sinn: es kann dieses UI Element nur 1x geben.
    static inline Rahmen* singleton = nullptr;

    /**
     * Erzeugt ein neues Rahmenobjekt, nutzt die Daten von der gegebenen Dynastie.
     * @param dynastie
     */
    Rahmen(const Dynastie& dynastie, const Stadt& stadt, const Zeit& zeit);

    /// Privat genutzter Render-Aufruf.
    void render();

    Dynastie  dynastie;
    Stadt     stadt;
    Viertel   viertel;
    Uhr       uhr;
    Grafik    top;
    sf::Text  t_geld;
    sf::Text  t_spielername;
    sf::Text  t_stadt;
    sf::Text  t_uhr;
    sf::Text  t_datum;

};
