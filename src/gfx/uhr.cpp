#include "uhr.hpp"
#include "grafik.hpp"

#include <nada/log.hpp>
#include <cmath>

Uhr::Uhr(const Zeit& zeit, const float x, const float y) :
        bg(RADIUS), zeit(zeit), mx(x + RADIUS), my(y + RADIUS) {
    // Hintergrund
    bg.setFillColor(sf::Color(0xBE'BE'BE'FF));
    bg.setPosition(x, y);
    if (Grafik uhr_grafik("data/gfx/uhr.png"); uhr_grafik) {
        textur.setSmooth(true);
        bg.setTexture(&textur);
        textur = *uhr_grafik.data();
    }
}

void Uhr::zeig(sf::RenderWindow* window) {
    // Zeigerwinkel berechnen
    const float deg_h   = zeit.get_h()   * 30 + zeit.get_min() / 2.f;
    const float deg_min = zeit.get_min() * 6;

    // Stundenzeiger
    sf::Vertex zeiger_h[] = {
            sf::Vertex(sf::Vector2(mx, my), sf::Color::Black),
            sf::Vertex(sf::Vector2((float)(mx + (sin((540.f - deg_h) * (M_PI / 180.f)) * 0.5f * RADIUS)),
                                   (float)(my + (cos((540.f - deg_h) * (M_PI / 180.f)) * 0.5f * RADIUS))), sf::Color::Black)
    };

    // Minutenzeiger
    sf::Vertex zeiger_min[] = {
            sf::Vertex(sf::Vector2(mx, my), sf::Color::Black),
            sf::Vertex(sf::Vector2((float)(mx + (sin((540.f - deg_min) * (M_PI / 180.f)) * 0.8f * RADIUS)),
                                   (float)(my + (cos((540.f - deg_min) * (M_PI / 180.f)) * 0.8f * RADIUS))), sf::Color::Black)
    };

    // Rendern
    window->draw(bg);
    window->draw(zeiger_h,   2, sf::Lines);
    window->draw(zeiger_min, 2, sf::Lines);
}

void Uhr::set(float x, float y) {
    bg.setPosition(x, y);
    mx = x + RADIUS;
    my = y + RADIUS;
}

void Uhr::set(const Zeit& zeit) {
    Uhr::zeit = zeit;
}
