#pragma once

#include "../welt/zeit.hpp"
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>

/**
 * Klasse zur grafischen Darstellung einer klassischen analogen Uhr.
 */
class Uhr final {

public:

    /// Konstruktor mit Pointer zur aktuellen Zeitklasse im Spiel.
    Uhr(const Zeit& zeit, float x, float y);

    /// Zeichnet die Uhr ins gegebene Fenster.
    void zeig(sf::RenderWindow* window);

    /// Aktualisiert die anzuzeigende Zeit.
    void set(const Zeit& zeit);

    /// Legt die Position fest.
    void set(float x, float y);

    /// Getter: Zeit.
    const Zeit& get_zeit() const { return zeit; }

private:

    static constexpr float RADIUS = 48.f;
    sf::Texture textur;       // Uhrtextur
    sf::CircleShape bg;       // Kreis, der mit textur gefüllt wird
    Zeit zeit;                // Spielzeit
    float mx;
    float my;

};
