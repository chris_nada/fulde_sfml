#include "partikel.hpp"
#include <nada/random.hpp>
#include "../gui/gui.hpp"

Partikel::Partikel() :
    Partikel(
            {0.f, 0.f},
            20, 2,
            sf::Color(0xFFFFFFFF), 0.999f, 0.95f,
            4.f, 60.f, 0.5f, 0.001f,
            1.f,
            2.f, true)
{
    //
}

Partikel::Partikel(
        const sf::Vector2f& position,
        size_t aufloesung, size_t erzeugung,
        const sf::Color& farbe_start, float fade_farbe, float fade,
        float radius_start, float radius_ende, float radius_zufall, float radius_faktor,
        float geschwindigkeit,
        float jitter, bool respawn) :
        position(position),
        aufloesung(aufloesung),
        max_anzahl(erzeugung),
        farbe_start(farbe_start),
        fade_farbe(fade_farbe),
        radius_start(radius_start),
        radius_ende(radius_ende),
        radius_faktor(radius_faktor),
        radius_zufall(radius_zufall),
        geschwindigkeit(geschwindigkeit),
        fade(fade),
        jitter(jitter), respawn(respawn)
{
    partikel.reserve(100);
    neue_erzeugen();
}

void Partikel::draw() {

    // Alte Partikel entfernen
    partikel.erase(
            std::remove_if(partikel.begin(), partikel.end(),
                    [this](const sf::CircleShape& k) {
                            // Tod durch Radius
                            if (radius_ende > radius_start && k.getRadius() > radius_ende) return true;
                            if (radius_ende < radius_start && k.getRadius() < radius_ende) return true;
                            return false;
                    }
                    ),
            partikel.end()
    );

    // Neue hinzufügen
    if (respawn) neue_erzeugen();

    // Alle zeichnen
    for (const auto& k : partikel) Gui::fenster->draw(k);

    // Altern lassen
    for (sf::CircleShape& k : partikel) {
        k.setPosition(
                k.getPosition().x + nada::random::f(-jitter, jitter),
                k.getPosition().y - geschwindigkeit
                );
        k.setFillColor(sf::Color(
                (float)k.getFillColor().r * fade_farbe,
                (float)k.getFillColor().g * fade_farbe,
                (float)k.getFillColor().b * fade_farbe,
                (float)k.getFillColor().a * fade
                ));
        k.setRadius(k.getRadius() + radius_faktor * ((radius_ende - radius_start)));
    }
}

void Partikel::neue_erzeugen() {
    const unsigned neue = nada::random::ui(0, max_anzahl );
    for (unsigned i = 0; i < neue; ++i) {
        sf::CircleShape k(
                radius_start * nada::random::f(1.0f - radius_zufall, 1.0f + radius_zufall),
                aufloesung
        );
        k.setPosition(position);
        k.setFillColor(farbe_start);
        k.setOutlineColor(farbe_start);
        partikel.push_back(k);
    }
}

void Partikel::set_position(const sf::Vector2f& position) {
    Partikel::position = position;
}

void Partikel::set_max_anzahl(size_t max_anzahl) {
    Partikel::max_anzahl = max_anzahl;
}

void Partikel::set_aufloesung(size_t aufloesung) {
    Partikel::aufloesung = aufloesung;
}

void Partikel::set_farbe_start(const sf::Color& farbe_start) {
    Partikel::farbe_start = farbe_start;
}

void Partikel::set_radius_start(float radius_start) {
    Partikel::radius_start = radius_start;
}

void Partikel::set_radius_ende(float radius_ende) {
    Partikel::radius_ende = radius_ende;
}

void Partikel::set_start_geschwindigkeit(float geschwindigkeit) {
    Partikel::geschwindigkeit = geschwindigkeit;
}

void Partikel::set_fade_farbe(float fade_farbe) {
    Partikel::fade_farbe = fade_farbe;
}

void Partikel::set_fade_geschwindigkeit(float fade_geschwindigkeit) {
    Partikel::fade = fade_geschwindigkeit;
}

void Partikel::set_jitter(float jitter) {
    Partikel::jitter = jitter;
}

void Partikel::set_respawn(bool respawn) {
    Partikel::respawn = respawn;
}

Partikel Partikel::rauch() {
    return Partikel(
            {0.f, 0.f},
            20, 2,
            sf::Color(0xFFFFFFFF), 0.999f, 0.999f,
            4.f, 60.f, 0.5f, 0.001f,
            1.f,
            2.f, true
    );
}

Partikel Partikel::feuer() {
    return Partikel(
            {0.f, 0.f},
            20, 2,
            sf::Color(0xFFAA22FF), 0.995f, 0.9999f,
            16.f, 1.f, 0.5f, 0.01f,
            3.f,
            1.f, true
    );
}
