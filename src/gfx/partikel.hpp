#pragma once

#include <SFML/Graphics/CircleShape.hpp>

/// Einfaches, auf Kreisen basierendes Partikelsystem.
class Partikel final {

public:

    /// Liefert einen vorgefertigten Rauch-Emitter.
    static Partikel rauch();

    /// Liefert einen vorgefertigten Feuer-Emitter.
    static Partikel feuer();

    /// Erzeugt einen Partikelemitter. Sollte anschließend konfiguriert werden.
    Partikel();

    /**
     * @brief Erzeugt einen Partikelemitter.
     *
     * @param position          Bildschirmposition.
     * @param aufloesung        Anzahl Kanten pro Kreis. Weniger => bessere Performanz.
     * @param erzeugung         Wieviele pro draw() maximal erzeugt werden dürfen.
     * @param farbe_start       Anfängliche Farbe.
     * @param fade_farbe        Faktor, mit dem die Farbe abnimmt. 1.0 = bleibt.
     * @param radius_start      Anfänglicher Radius.
     * @param radius_ende       Radius, dessen Über/Unterschreitung zur Auslöschung führt.
     * @param radius_zufall     Faktor, um den der Anfangsradius variieren darf. Z.B. 0.5 = 50%.
     * @param radius_faktor     Faktor, mit dem der Radius abnimmt. 0.1 = sehr schnell, 0.0001 langsam.
     * @param geschwindigkeit   Pixel pro draw(), die sich die Partikel bewegen.
     * @param fade              Faktor, mit dem der Alphawert der Partikel abnimmt (z.B. 0.99 pro draw()).
     * @param jitter            Pixel pro draw(), die sich ein Partikel zufällig horizontal bewegt.
     * @param respawn           True
     */
    Partikel(const sf::Vector2f& position,
             size_t aufloesung, size_t erzeugung,
             const sf::Color& farbe_start, float fade_farbe,
             float radius_start, float radius_ende,
             float radius_zufall = 0.5f, float radius_faktor = 0.001f,
             float geschwindigkeit = 1.5f, float fade = 0.999f,
             float jitter = 2.f, bool respawn = true);

    void draw();

    void set_position(const sf::Vector2f& position);

    void set_aufloesung(size_t aufloesung);

    void set_max_anzahl(size_t max_anzahl);

    void set_farbe_start(const sf::Color& farbe_start);

    void set_radius_start(float radius_start);

    void set_radius_ende(float radius_ende);

    void set_start_geschwindigkeit(float start_geschwindigkeit);

    void set_fade_farbe(float fade_farbe);

    void set_fade_geschwindigkeit(float fade_geschwindigkeit);

    void set_jitter(float jitter);

    void set_respawn(bool respawn);

private:

    void neue_erzeugen();

    /// Partikelspeicher.
    std::vector<sf::CircleShape> partikel;

    /// Startposition der Partikel.
    sf::Vector2f position;

    /// Anzahl Kanten. Mehr Kanten => weniger eckig.
    std::size_t aufloesung;

    /// Anzahl zu Beginn auszusendender Partikel.
    std::size_t max_anzahl;

    /// Farbe, die neue Partikel bekommen.
    sf::Color farbe_start;

    /// Farbe zu der ausgefaded wird.
    float fade_farbe;

    /// Radius zu Beginn der Aussendung.
    float radius_start;

    /// Radius bei Ende der Aussendung.
    float radius_ende;

    /// Wachstumsfaktor Radius. Muss zwischen 0 und 1 liegen.
    float radius_faktor;

    /// Zufällige Startabweichung beim Radius.
    float radius_zufall;

    /// Anfängliche Geschwindigkeit in Pixel pro Frame.
    float geschwindigkeit;

    /// Faktor wie schnell der Partikel schwindet.
    float fade;

    /// Zufällige horizontale Bewegung je Frame.
    float jitter;

    /// Sollen statig neue Partikel ausgesendet werden?
    bool respawn;

};
