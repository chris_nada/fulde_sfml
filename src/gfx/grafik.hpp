#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <memory>
#include <unordered_map>

/**
 * Wurzelklasse für alle Dinge, die vom Fenster gerendert werden.
 * Enthält zum Anzeigen eine Methode `draw`.
 */
class Grafik final {

public:

    /**
     *  Konstruktor.
     *  @note `is_shared` = true bedeutet, dass die Grafik nicht gelöscht wird nach ableben der Instanz.
     */
    Grafik() : is_shared(true), is_flipped(false) {};

    /// ctor. Erzeugt eine Grafik aus gegebener Datei (`jpg`, `png` ...). Formate: vgl SFML Doku.
    explicit Grafik(const std::string& textur_pfad, bool is_shared = true);

    /// dtor.
    ~Grafik();

    /// Zeichnet die Textur auf das gegebene Fenster.
    void draw(sf::RenderWindow* fenster) const { fenster->draw(sprite); };

    /// Getter: X-Koordinate.
    float x() const { return sprite.getPosition().x; }

    /// Getter: Y-Koordinate.
    float y() const { return sprite.getPosition().y; }

    /// Getter: Kompletter Positionsvektor.
    const sf::Vector2f& pos() const { return sprite.getPosition(); }

    /// Liegt gegebener Punkt in der Grafik?
    bool is_inside(float x, float y) const;

    /// Liefert einen Pointer zur verwendeten Textur.
    sf::Texture* data() const { return textur; }

    /// Liefert die Dimensionen der Grafik.
    sf::Vector2u size() const { return textur->getSize(); }

    /// Liefert die Dimensionen der Grafik zu float konvertiert.
    sf::Vector2f size_f() const;

    /// Manipuliert (multipliziert) gegebene Farbe mit der Grafik.
    void set_color(const sf::Color& farbe) { sprite.setColor(farbe); }

    /// Skaliert die Grafik auf gegebene Größe in Pixel.
    void set_size(const sf::Vector2f& size);

    /// Setter: X-Koordinate.
    void set_x(float x) { sprite.setPosition(x, sprite.getPosition().y); }

    /// Setter: Y-Koordinate.
    void set_y(float y) { sprite.setPosition(sprite.getPosition().x, y); }

    /// Setter: Position als Vektor.
    void set_pos(const sf::Vector2f& pos) { sprite.setPosition(pos); }

    /// Setter: Horizontal spiegeln.
    void set_flip(bool flip);

    /// Kann verwendet werden, um zu prüfen, ob eine Grafik geladen ist.
    explicit operator bool() const { return textur != nullptr; }

    /// Überprüft, ob die Grafiken dieselbe Textur besitzen.
    bool operator==(const Grafik& rhs) const { return textur == rhs.textur; }

    /// Überprüft, ob die Grafiken unterschiedliche Texturen besitzen.
    bool operator!=(const Grafik& rhs) const { return !(rhs == *this); }

private:

    static inline std::unordered_map<std::string, sf::Texture*> shared_texturen;

    bool is_shared;

    bool is_flipped;

    sf::Texture* textur = nullptr;

    sf::Sprite sprite;

};
