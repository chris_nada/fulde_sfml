#include "rahmen.hpp"
#include "../gui/gui_hauptmenu.hpp"
#include "../gui/gui.hpp"

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Font.hpp>

Rahmen::Rahmen(const Dynastie& dynastie, const Stadt& stadt, const Zeit& zeit) :
        dynastie(dynastie),
        stadt(stadt),
        uhr(zeit, Gui::fenster->getSize().x - 100, 4),
        top("data/gfx/bg_top.png")
{
    // Texte konfigurieren
    sf::Text* texte[] = {&t_spielername, &t_stadt, &t_geld, &t_datum, &t_uhr};
    for (sf::Text* text : texte) {
        text->setFont(Gui::font_standard);
        text->setCharacterSize(20);
        text->setFillColor(sf::Color::White);
        text->setOutlineColor(sf::Color(Gui::FARBE_DUNKEL));
        text->setOutlineThickness(1);
    }

    t_geld.setPosition(        Gui::fenster->getSize().x * 0.10f, 8);
    t_spielername.setPosition( Gui::fenster->getSize().x * 0.20f, 8);
    t_stadt.setPosition(       Gui::fenster->getSize().x * 0.30f, 8);
    t_datum.setPosition(       Gui::fenster->getSize().x * 0.60f, 8);
    t_stadt.setString(stadt.get_name());
    t_spielername.setString(dynastie.get_name()); // TODO Vorname + Nachname
}

void Rahmen::init(const Dynastie& dynastie, const Stadt& stadt, const Zeit& zeit) {
    if (!singleton) singleton = new Rahmen(dynastie, stadt, zeit);
    set(dynastie);
    set(zeit);
    singleton->stadt = stadt;
}

void Rahmen::draw() {
    singleton->render();
}

void Rahmen::render() {
    const sf::View& view_old = Gui::fenster->getView();
    static sf::View view = Gui::fenster->getDefaultView();
    Gui::fenster->setView(view);
    uhr.set(view.getSize().x * 0.9f, 4.f);
    top.draw(Gui::fenster);
    Gui::fenster->draw(t_geld);
    Gui::fenster->draw(t_spielername);
    Gui::fenster->draw(t_stadt);
    Gui::fenster->draw(t_datum);
    uhr.zeig(Gui::fenster);
    Gui::fenster->setView(view_old);

}

void Rahmen::set(const Stadt& stadt, const Viertel& viertel) {
    singleton->stadt = stadt;
    singleton->viertel = viertel;
    std::string temp = stadt.get_name() + ", " + viertel.get_name();
    singleton->t_stadt.setString(sf::String::fromUtf8(temp.begin(), temp.end()));
}

void Rahmen::set(const Dynastie& dynastie) {
    singleton->dynastie = dynastie;
    singleton->t_spielername.setString(dynastie.get_name());
    singleton->t_geld.setString(std::to_string(dynastie.get_geld()));
}

void Rahmen::set(const Zeit& zeit) {
    static std::stringstream ss;
    ss.str(std::string());
    ss.clear();
    ss << zeit.get_jahreszeit_name() << ' ' << zeit.get_jahr();
    const std::string& str = ss.str();
    singleton->t_datum.setString(sf::String::fromUtf8(str.begin(), str.end()));
    singleton->uhr.set(zeit);
}

const Zeit& Rahmen::get_zeit() {
    return singleton->uhr.get_zeit();
}
