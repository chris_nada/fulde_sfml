#pragma once

#include "../grafik.hpp"
#include "../../welt/bauten/gebaeude.hpp"
#include "../../welt/produktion/werkstatt.hpp"

class Gebaeudegrafik final {

public:

    Gebaeudegrafik() = default;

    Gebaeudegrafik(const Gebaeude& gebaeude, const std::string& kultur);

    Gebaeudegrafik(const Gebaeude& gebaeude, const std::string& kultur, const Werkstatt& werk);

    Netzwerk::id_t get_id() const { return id; }

    void render_bg();

    void render_vg();

    const sf::Vector2f& get_pos() const { return hintergrund.pos(); }

    sf::Vector2u get_size() const { return hintergrund.size(); }

    bool is_inside(float x, float y) const { return hintergrund.is_inside(x, y); }

    bool operator==(const Gebaeudegrafik& rhs) const;

    bool operator!=(const Gebaeudegrafik& rhs) const;

private:

    /// Setzt die Gebäudegrafik auf ihre Position im Viertel.
    void reset(const Gebaeude& gebaeude);

    /// ID = Gebäude-ID.
    Netzwerk::id_t id;

    bool hat_vordergrund = false;

    Grafik hintergrund;

    Grafik vordergrund;

};
