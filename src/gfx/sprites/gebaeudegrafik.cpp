#include "gebaeudegrafik.hpp"
#include "../../gui/gui.hpp"
#include "../../gui/gui_viertel.hpp"

#include <nada/log.hpp>

using nada::Log;

Gebaeudegrafik::Gebaeudegrafik(const Gebaeude& gebaeude, const std::string& kultur) : id(gebaeude.get_id()) {
    Log::debug() << "Gebaeudegrafik(" << gebaeude.get_typ_name() << ")\n";
    std::string grafikpfad = "data/gfx/bauten/" + kultur + "/";
    switch (gebaeude.get_typ()) {
        case Gebaeude::WOHNHAUS:      grafikpfad += "g_wohn.png";          break;
        case Gebaeude::MARKT:         grafikpfad += "g_markt.png";         break;
        case Gebaeude::LAGER:         grafikpfad += "g_lager.png";         break;
        case Gebaeude::RATHAUS:       grafikpfad += "g_rathaus.png";       break;
        case Gebaeude::KIRCHE:        grafikpfad += "g_kirche.png";        break;
        case Gebaeude::BANK:          grafikpfad += "g_bank.png";          break;
        case Gebaeude::SCHATTENGILDE: grafikpfad += "g_schattengilde.png"; break;
        case Gebaeude::WERKSTATT:
            Log::err() << "Fehler: Falscher Ctor fuer Werkstatt aufgerufen.";
            grafikpfad += "g_werkstatt.png";
            break;
        default: Log::debug() << "\tUngueltiger Typ = " << (Netzwerk::enum_t) gebaeude.get_typ() << Log::endl;
    }
    hintergrund = Grafik(grafikpfad);
    reset(gebaeude);
}

Gebaeudegrafik::Gebaeudegrafik(const Gebaeude& gebaeude, const std::string& kultur, const Werkstatt& werk) {
    Log::debug() << "Gebaeudegrafik(" << gebaeude.get_typ_name() << ", " << werk.get_prozessgruppe() << ")\n";
    std::string grafikpfad = "data/gfx/bauten/" + kultur + "/g_"; // g_werkstatt.png, wenn noch kein spezifischer Typ
    grafikpfad += werk.get_prozessgruppe().empty() ? "werkstatt.png" : werk.get_prozessgruppe() + ".png";
    hintergrund = Grafik(grafikpfad);
    reset(gebaeude);
}

void Gebaeudegrafik::reset(const Gebaeude& gebaeude) {
    using gui = Gui_Viertel;
    hintergrund.set_x(gebaeude.get_position() * gui::X_PLATZ + gebaeude.get_position() * gui::X_PADDING);
    hintergrund.set_y(Gui::VIEW_SIZE_Y - gui::Y_PADDING - hintergrund.size().y);
}

void Gebaeudegrafik::render_bg() {
    hintergrund.draw(Gui::fenster);
}

void Gebaeudegrafik::render_vg() {
    if (hat_vordergrund) vordergrund.draw(Gui::fenster);
}

bool Gebaeudegrafik::operator==(const Gebaeudegrafik& rhs) const {
    return hintergrund == rhs.hintergrund && vordergrund == rhs.vordergrund;
}

bool Gebaeudegrafik::operator!=(const Gebaeudegrafik& rhs) const {
    return !(rhs == *this);
}
