#pragma once

#include "../../welt/spieler/person.hpp"
#include "../grafik.hpp"

#include <array>

/// Repräsentiert Personen in der Viertel-Ansicht.
class Charaktergrafik final {

public:

    /// Ctor.
    Charaktergrafik() = default;

    /// Ctor. Initialisiert Position.
    explicit Charaktergrafik(const Person& mensch);

    /// Rendert die Grafik auf das übliche UI-Fenster.
    void render();

    /// Liefert die Position der Grafik, manipulierbar.
    Position& pos() { return position; }

    /// Bewegt die Grafik auf gegebene X-Koordinate.
    void set_x(float x_neu);

    /// Soll die Grafik zur Bewegung interpoliert werden?
    void set_interpolieren(bool interpolieren);

    /// Getter: ID zum Wiederauffinden.
    Netzwerk::big_id_t get_id() const { return id; }

    /// Prüft auf Gleichheit via ID.
    bool operator==(const Charaktergrafik& rhs) const { return id == rhs.id; }

    /// Prüft auf Gleichheit via ID.
    bool operator!=(const Charaktergrafik& rhs) const { return !(rhs == *this); }

private:

    /// Grober Umrechnungsfaktor Sim-speed -> Grafik/FPS-speed
    static constexpr float SPEED_FAKTOR = 1.2f;

    /// ID zum Wiederauffinden.
    Netzwerk::big_id_t id;

    /// Letzte gesetze Position.
    Position position;

    /// Abstand vom unteren Bildschirmrand zur unteren Kande der Grafik.
    unsigned y_padding; // Abstand von y-unten

    /// Hilfszähler für die Interpolation.
    uint8_t counter_update;

    /// Befindet sich in Bewegung?
    bool in_bewegung;

    /// Soll die Grafik interpoliert werden? Ungünstig für lokalen Spieler.
    bool interpolieren;

    /// Geschwindigkeit in px pro ms.
    float speed;

    /// Simulationsgeschwindigkeit
    float speed_sim;

    /**
     * Im Paar gespeichert: 1 = Zeit vom timer; 2 = x-Koordinate.
     * [0] neuer Wert, [1] alter Wert.
     */
    std::array<std::pair<float ,float>, 2> tx;

    /// Timer für
    sf::Clock timer;

    /* Gfx */
    Grafik mittelgrund;

};
