#include "charaktergrafik.hpp"
#include "../../gui/gui.hpp"

#include <nada/log.hpp>
#include <nada/random.hpp>
#include <cmath>

Charaktergrafik::Charaktergrafik(const Person& mensch) :
        position(mensch.get_pos()), y_padding(100), counter_update(0),
        in_bewegung(false), interpolieren(true), speed(mensch.get_speed() * SPEED_FAKTOR), speed_sim(mensch.get_speed()),
        tx{std::make_pair(0.f, 0.f), std::make_pair(0.f, 0.f)}
{
    //Log::debug() << "Charaktergrafik(" << mensch.get_name() << ")\n";
    static const std::string grafikpfad = "data/gfx/char/";
    mittelgrund = Grafik(grafikpfad + "c_test.png");
    mittelgrund.set_y(Gui::VIEW_SIZE_Y - y_padding - mittelgrund.size().y); // Höhe
}

void Charaktergrafik::render() {
    if (interpolieren && counter_update != 2) return;
    if (interpolieren && in_bewegung && counter_update == 2) {
        mittelgrund.set_x(static_cast<float>(
                position.x + speed * (timer.getElapsedTime().asSeconds() - tx[0].first))
        );
    }
    else mittelgrund.set_x(position.x);
    mittelgrund.set_flip(position.ausrichtung == Position::Ausrichtung::LINKS);
    mittelgrund.draw(Gui::fenster);
}

void Charaktergrafik::set_x(float x_neu) {
    constexpr float intervall = 0.f;
    constexpr float margin = 0.001f; // Erst ab dieser x-Änderung gilt es als Bewegung.
    if (interpolieren && timer.getElapsedTime().asSeconds() - tx[0].first > intervall) {
        std::swap(tx[0], tx[1]);
        tx[0] = std::make_pair(timer.getElapsedTime().asSeconds(), x_neu);
        speed = (tx[0].second - tx[1].second) / (tx[0].first - tx[1].first);
        if (std::isfinite(speed)) in_bewegung = std::abs(tx[0].second - tx[1].second) > margin;
        else {
            in_bewegung = false;
            speed = 0;
        }
        if (counter_update != 2) ++counter_update;
    }
    if (x_neu != position.x) position.ausrichtung = x_neu < position.x ? Position::Ausrichtung::LINKS : Position::Ausrichtung::RECHTS;
    position.x = x_neu;
}

void Charaktergrafik::set_interpolieren(bool interpolieren) {
    Charaktergrafik::interpolieren = interpolieren;
}
