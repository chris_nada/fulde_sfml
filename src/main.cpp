#include "netzwerk/host.hpp"
#include "werkzeuge/debug.hpp"
#include "gui/gui.hpp"
#include "tests/test.hpp"

#include <memory>
#include <thread>

void start_server(Host*& host) {
    host = new Host("data/save/last.dat");
    host->start();
}

int main(int argc, char** argv) {
    rang::setControlMode(rang::control::Force);
    Log::debug() << "main()" << Log::endl;
    Werkzeuge::debug();
    Log::out() << "  --h Startet einen dedizierten Host.\n";
    Log::out() << "  --p Startet einen parallelen Host.\n\n";

    Host* host = nullptr;
    std::thread* server = nullptr;

    for (int i = 0; i < argc; ++i) {
        std::string arg(argv[i]);
        Log::debug() << "\targ" << i << " = " << arg << Log::endl;
        if (arg.find("-h") != std::string::npos && !host) {
            start_server(host);
            return 0;
        }
        else if (arg.find("-p") != std::string::npos && !server) {
            server = new std::thread(start_server, std::ref(host));
            break;
        }
        else if (arg.find("-test") != std::string::npos) return Test::run_tests(argc, argv);
    }
    Log::debug() << '\n';

    #if defined(__linux__)
        if (server) std::this_thread::sleep_for(std::chrono::seconds(1)); // X11 workaround
    #endif

    // GUI starten
    const int ui = Gui::start();

    // ggf. Server stoppen
    if (host && server) {
        Log::debug() << "Server stop\n";
        host->stop();
        server->join();
        delete host;
        delete server;
    }
    return ui;
}
