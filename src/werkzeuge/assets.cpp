#include "assets.hpp"

#ifdef NADA_ASSETS_AKTIV
#include <nada_assets.hpp>
std::unique_ptr<nada_assets> Assets::assets;

std::string Assets::get(const std::string& pfad) {
    if (!assets) assets = std::make_unique<nada_assets>("data/assets.pack");
    return assets->get_asset_str(pfad, true);
}

bool Assets::good() {
    return assets && assets->good();
}
#else

#include <iostream>
#include <fstream>
#include <filesystem>

std::string Assets::get(const std::string& pfad) {
    std::ifstream in(pfad);
    try {
        const auto dateigroesse = std::filesystem::file_size(pfad);
        std::string data;
        data.resize(dateigroesse);
        in.read(data.data(), static_cast<std::streamsize>(dateigroesse));
        return data;
    } catch (const std::exception& e) {
        std::cerr << "Assets::get " << pfad << ' ' << e.what() << '\n';
        return {};
    }
}

bool Assets::good() {
    return true;
}

#endif
