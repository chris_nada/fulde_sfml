#pragma once

#ifndef NDEBUG
    #include "../welt/waren/vorrat.hpp"
    #include "../welt/waren/prozess.hpp"

    #include <iostream>
    #include <cereal/archives/json.hpp>
    #include <cereal/types/string.hpp>
    #include <cereal/types/unordered_map.hpp>
#endif

#include <nada/log.hpp>

using nada::Log;

namespace Werkzeuge {

    /// Debugmethode zum Rumprobieren. Wird von main() gleich als erstes ausgeführt.
    static void debug() {
        #ifndef NDEBUG
        Log::debug() << "Werkzeuge::debug()" << Log::endl;
        //Hier Kode zum Testen schreiben...

/*
        float a = 1.4f;
        int b = -5;
        std::string c = "JSON Test Serialisierung...";
        std::unordered_map<std::string, unsigned> map;
        map["key1"] = 5;
        map["key2"] = 10;
        map["key3"] = 15;

        std::ofstream out("test.json");
        {
            cereal::JSONOutputArchive oa(out);
            oa(CEREAL_NVP(a), CEREAL_NVP(b), CEREAL_NVP(c), CEREAL_NVP(map));
        }
        {
            cereal::JSONOutputArchive oa(out);
            oa(CEREAL_NVP(a), CEREAL_NVP(b), CEREAL_NVP(c), CEREAL_NVP(map));
        }
*/
        //exit(0);
        #endif
    }

};
