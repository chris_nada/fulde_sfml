#pragma once

#include <cereal/archives/binary.hpp>

namespace etc {

    /// Verwendete Klasse zum Deserialisieren.
    using serial_in  = cereal::BinaryInputArchive;

    /// Verwendete Klasse zum Serialisieren.
    using serial_out = cereal::BinaryOutputArchive;

}
