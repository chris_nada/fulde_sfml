#pragma once

#include "etc_cereal.hpp"

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

/**
 * Ersetzt automatisch std::reduce durch std::accumulate dort,
 * wo es nicht verfügbar ist (z.B. GCC 8.3, Clang 8 auf Debian).
 */
#if __has_include(<execution>)
    #include <execution>
    #define summieren(c, f) std::reduce(std::execution::par, c.begin(), c.end(), 0, f);
#else
    #include <algorithm>
    #define summieren(c, f) std::accumulate(c.begin(), c.end(), 0, f);
#endif

/// Helfermethoden, die sonst in keine Kategorie passen.
namespace etc {

    /// Wendet auf das gegebene Objekt den `<<` - Operator an und gibt das ergebnis als `std::string` wieder.
    template<typename T>
    std::string serialisiere(const T& objekt) {
        std::stringstream ss;
        ss << objekt;
        return ss.str();
    }

    /// Serialisiert via `serial_out` gegebenes Objekt direkt in den gegebenen Stream.
    template<typename T>
    void serialisiere(std::stringstream& ss, const T& objekt) {
        etc::serial_out out(ss);
        out(objekt);
    }

}
