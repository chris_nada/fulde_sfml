#pragma once

#include <memory>

//#define NADA_ASSETS_AKTIV

#ifdef NADA_ASSETS_AKTIV
class nada_assets;
#endif

/**
 * Verwaltet Dateien aus dem Asset-Paket.
 */
class Assets final {

public:

    /**
     * @brief Liefert den Inhalt einer Datei aus dem Asset-Paket.
     * @param pfad Regulärer (relativer) Pfad zur Datei.
     * @return Inhalt der Datei als `std::string`.
     */
    static std::string get(const std::string& pfad);

    /**
     * @brief Ist das Asset-Paket vorhanden und lesbar?
     */
    static bool good();

private:

#ifdef NADA_ASSETS_AKTIV
    /// Pointer zu dem Assetpaket.
    static std::unique_ptr<nada_assets> assets;
#endif

};
