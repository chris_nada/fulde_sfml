#include "host.hpp"
#include "klient.hpp"
#include "../welt/waren/prozess.hpp"

#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/string.hpp>
#include <nada/log.hpp>

using nada::Log;

void Host::a_neuer_spieler(Netzwerkpaket& request, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << "(...)" << Log::endl;
    // Daten auslesen
    sf::String vorname;
    sf::String nachname;
    bool maennlich;
    int viertel_id;
    request >> vorname;
    request >> nachname;
    request >> maennlich;
    request >> viertel_id;
    Log::debug() << "\tHost::a_neuer_spieler(" << (maennlich ? "M)" : "W)") << Log::endl;

    // Spieler erstellen
    Netzwerk::id_t id = welt.neuer_spieler(vorname, nachname, maennlich, viertel_id, true, false);

    // Antworten
    request.clear();
    request << id;
    socket->send(request);
}

void Host::a_markt_kauf(Netzwerkpaket& packet, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << "(...)" << Log::endl;
    (void) socket;

    // Daten Auslesen
    Netzwerk::id_t markt_id, lager_id, dynastie_id, stadt_id;
    uint32_t anzahl, preis;
    uint16_t steuern;
    float quali;
    std::string waren_key;
    packet >> dynastie_id >> stadt_id >> markt_id >> lager_id >> waren_key >> anzahl >> preis >> steuern >> quali;

    // Genug Geld? Sonst abbruch
    if (welt.dynastien.at(dynastie_id).get_geld() < preis + steuern) return;

    // Transaktion durchführen; Wenn geklappt: bezahlen.
    Markt& markt = welt.maerkte.at(markt_id);
    if (markt.kauf(waren_key, anzahl)) { // möglich?
        welt.dynastien.at(dynastie_id).geld_bezahlen(preis-steuern, Statistik::Ausgabe::WARENKAUF, welt.statistiken.at(dynastie_id));
        welt.dynastien.at(dynastie_id).geld_bezahlen(steuern, Statistik::Ausgabe::STEUER_MEHRWERT, welt.statistiken.at(dynastie_id));

        // Ware erhalten
        Lager& lager = welt.lager.at(lager_id);
        lager.get_vorrat(waren_key).einlagern(anzahl, quali);

        // Steuern an Land abführen
        Stadt& stadt = welt.staedte.at(stadt_id);
        stadt.geld_einnehmen(steuern);
    }
    else Log::err() << "Host::" << __func__ << " von " << waren_key << " fehlgeschlagen\n";
}

void Host::a_markt_verkauf(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << "(...)" << Log::endl;
    (void) socket;
    Netzwerk::id_t dynastie_id, markt_id, lager_id;
    std::string waren_key;
    uint32_t anzahl, gesamtpreis;
    float quali;
    paket >> dynastie_id >> markt_id >> lager_id >> waren_key >> anzahl >> gesamtpreis >> quali;

    // Aktion durchführen
    Vorrat& vorrat = welt.lager.at(lager_id).get_vorrat(waren_key);
    if (vorrat.entnehmen(anzahl)) {
        welt.maerkte.at(markt_id).verkauf(waren_key, anzahl, quali);
        welt.dynastien.at(dynastie_id).geld_erhalten(gesamtpreis, Statistik::Einnahme::WARENVERKAUF, welt.statistiken.at(dynastie_id));
    }
    else {
        Log::err() << "Host::" << __func__ << " Verkauf fehlgeschlagen ";
        Log::err() << anzahl << '/' << vorrat.menge << ' ' << waren_key << '\n';
    }
}

void Host::a_lager_transport(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;

    // Informationen
    Netzwerk::id_t id_spieler, id_lager, id_zielstadt;
    bool transport;
    std::unordered_map<std::string, uint16_t> waren_hin;
    std::unordered_map<std::string, uint16_t> waren_zurueck;

    // Auslesen
    {
        std::string temp;
        paket >> id_spieler >> id_lager >> id_zielstadt >> transport >> temp;
        std::stringstream ss(temp);
        etc::serial_in in(ss);
        in(waren_hin, waren_zurueck);
    }

    // Ziel bestimmen + Los
    Dynastie& dynastie = welt.dynastien.at(id_spieler);
    if (transport) { // Lagertransfer?
        if (dynastie.hat_lager(id_zielstadt)) {
            auto ziellager = dynastie.get_lager(id_zielstadt);
            welt.lager.at(id_lager).start_transport(
                    id_spieler, ziellager, true, waren_hin, waren_zurueck
            );
        }
        else {
            Log::err() << "\tSpieler hat kein Lager in Zielstadt.\n";
        }
    }
    else { // Markthandel?
        welt.lager.at(id_lager).start_transport(
                id_spieler,
                welt.staedte.at(id_zielstadt).get_id_markt(),
                false, waren_hin, waren_zurueck
        );
    }
}

void Host::a_werk_stadt_arbeitertransfer(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;
    Netzwerk::id_t id_werk, id_stadt, id_char;
    bool stadt_zu_werk;

    // Auslesen
    paket >> id_werk >> id_stadt >> id_char >> stadt_zu_werk;
    welt.a_transfer_arbeiter(id_char, stadt_zu_werk, id_werk, id_stadt);
}

void Host::a_werk_werk_arbeitertransfer(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;

    // Auslesen
    Netzwerk::id_t id_werk, id_char;
    uint8_t index_anlage;
    paket >> id_werk >> id_char >> index_anlage;

    // Transfer ausführen
    Werkstatt& werk = welt.werkstaetten.at(id_werk);
    werk.transfer_arbeiter(id_char, index_anlage);
}

void Host::a_werk_neue_anlage(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;

    // Auslesen
    Netzwerk::id_t id_dynastie, id_werk;
    uint16_t kosten; // Kosten nach Stadt Reichtum
    std::string prozess_key;
    paket >> id_dynastie >> id_werk >> kosten >> prozess_key;

    // Anlage einrichten, wenn Platz da
    Dynastie& dynastie = welt.dynastien.at(id_dynastie);
    Werkstatt& werk = welt.werkstaetten.at(id_werk);
    const Prozess& prozess = Prozess::get(prozess_key);
    if (werk.freier_platz >= prozess.platzverbrauch) {
        werk.freier_platz -= prozess.platzverbrauch;
        werk.anlagen.emplace_back(prozess_key);
        dynastie.geld_bezahlen(kosten, Statistik::Ausgabe::WERKAUSBAU, welt.statistiken.at(id_dynastie));
    }
    else Log::err() << "\tPlatz reicht nicht aus, " << werk.freier_platz << '/' << prozess.platzverbrauch << '\n';
}

void Host::a_werk_anlage_verkaufen(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;

    // Auslesen
    Netzwerk::id_t id_dynastie, id_werk;
    uint8_t index_anlage;
    uint16_t preis; // VK-Preis nach Stadt Reichtum
    paket >> id_dynastie >> id_werk >> index_anlage >> preis;

    // Anlage abbauen, Geld erstatten
    Dynastie& dynastie = welt.dynastien.at(id_dynastie);
    Werkstatt& werk = welt.werkstaetten.at(id_werk);
    Anlage& anlage = werk.anlagen.at(index_anlage);

    // Platz & Arbeiter zurückgeben
    werk.freier_platz += anlage.get_groesse();
    for (const auto& arbeiter : anlage.get_arbeiter()) {
        werk.freie_arbeiter.insert(arbeiter);
    }
    werk.anlagen.erase(werk.anlagen.begin() + index_anlage);
    dynastie.geld_erhalten(preis, Statistik::Einnahme::WERKRUECKBAU, welt.statistiken.at(id_dynastie));
}

void Host::a_werk_erweitern_verkleinern(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;

    // Auslesen
    Netzwerk::id_t id_dynastie, id_werk;
    int16_t diff_raum; // Zu erweitern / zu verkleinernder Betrag
    int32_t diff_geld; // + Geld erhalten, - Geld bezahlen
    paket >> id_dynastie >> id_werk >> diff_raum >> diff_geld;

    // Werk vergrößern / verkleinern; Geld verrrechnen
    Dynastie& dynastie = welt.dynastien.at(id_dynastie);
    Werkstatt& werk = welt.werkstaetten.at(id_werk);
    if ((int) werk.freier_platz + diff_raum >= 0) werk.freier_platz += diff_raum;
    if (diff_geld > 0) dynastie.geld_erhalten(diff_geld, Statistik::Einnahme::WERKRUECKBAU, welt.statistiken.at(id_dynastie));
    if (diff_geld < 0) dynastie.geld_bezahlen(std::abs(diff_geld), Statistik::Ausgabe::WERKAUSBAU, welt.statistiken.at(id_dynastie));
}

void Host::a_werk_anlage_erweitern_verkleinern(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;

    // Auslesen
    Netzwerk::id_t id_dynastie, id_werk;
    uint8_t index_anlage;
    int16_t diff_raum; // Zu erweitern / zu verkleinernder Betrag der *Anlage*
    int32_t diff_geld; // + Geld erhalten, - Geld bezahlen
    paket >> id_dynastie >> id_werk >> index_anlage >> diff_raum >> diff_geld;

    // Werk vergrößern / verkleinern; Geld verrrechnen
    Dynastie& dynastie = welt.dynastien.at(id_dynastie);
    Werkstatt& werk = welt.werkstaetten.at(id_werk);
    Anlage& anlage = werk.anlagen.at(index_anlage);

    // Platz der Anlage >= Min?
    if (static_cast<int>(anlage.groesse) + diff_raum >= anlage.get_prozess().platzverbrauch) {
        // Genug Raum in der Fabrik?
        if (static_cast<int>(werk.freier_platz) + diff_raum >= 0) {
            werk.freier_platz = static_cast<int>(werk.freier_platz) + diff_raum;
            anlage.groesse     = static_cast<int>(anlage.groesse)     + diff_raum;
            if (diff_geld > 0) dynastie.geld_erhalten(diff_geld, Statistik::Einnahme::WERKRUECKBAU, welt.statistiken.at(id_dynastie));
            if (diff_geld < 0) dynastie.geld_bezahlen(std::abs(diff_geld), Statistik::Ausgabe::WERKAUSBAU, welt.statistiken.at(id_dynastie));
        } else Log::debug() << "\tPlatz unterschreitet Min.: Werk\n";
    } else Log::debug() << "\tPlatz unterschreitet Min.: Anlage\n";
}

void Host::a_werk_anlage_start_stop(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;

    Netzwerk::id_t id_werk;
    uint8_t index_anlage;
    paket >> id_werk >> index_anlage;
    Werkstatt& werk = welt.werkstaetten.at(id_werk);
    Anlage& anlage = werk.anlagen.at(index_anlage);
    anlage.aktiviert = !anlage.aktiviert;
}

void Host::a_werk_anlage_auto_kauf_toggle(Netzwerkpaket& paket, sf::TcpSocket *socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;
    Netzwerk::id_t id_werk; paket >> id_werk;
    Werkstatt& werk = welt.werkstaetten.at(id_werk);
    werk.auto_kauf = !werk.auto_kauf;
}

void Host::a_lager_verwaltung(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;
    Netzwerk::id_t id_lager; paket >> id_lager;
    Lager& lager = welt.lager.at(id_lager);
    std::string data; paket >> data;
    std::stringstream ss(data);
    etc::serial_in in(ss);
    Lager::Automatik_Kauf auto_kauf;    in >> auto_kauf;
    Lager::Automatik_Kauf auto_verkauf; in >> auto_verkauf;
    std::cout << "HOST_KAUF="    << auto_kauf.ware_menge_preis.size()    << std::endl;
    std::cout << "HOST_VERKAUF=" << auto_verkauf.ware_menge_preis.size() << std::endl;
    lager.auto_einkauf = auto_kauf;
    lager.auto_verkauf = auto_verkauf;
}

void Host::a_nachricht_loeschen(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;
    Netzwerk::id_t dynastie_id;
    std::string nachrichten_hash;
    paket >> dynastie_id;
    paket >> nachrichten_hash;
    auto& nachrichten = welt.nachrichten.at(dynastie_id);
    auto it = std::find_if(nachrichten.begin(), nachrichten.end(), [&](const Nachricht& n) { return n.get_hash() == nachrichten_hash; });
    if (it != nachrichten.end()) nachrichten.erase(it);
}

void Host::a_amt_waehlen(Netzwerkpaket& paket, sf::TcpSocket* p_socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) p_socket;
    Netzwerk::id_t id_waehler, id_bewerber;
    paket >> id_waehler >> id_bewerber;
    auto aemter = Amt::get_aemter_aus_welt(welt);
    auto amt_it = std::find_if(aemter.begin(), aemter.end(), [&](const Amt* amt) { return amt->is_bewerber(id_bewerber); });
    if (amt_it != aemter.end()) (*amt_it)->waehlen(id_bewerber, id_waehler);
    else Log::err() << "Host::" << __func__ << " Kein Amt gefunden mit Bewerber " << id_bewerber << '\n';
}

void Host::a_amt_bewerben(Netzwerkpaket& paket, sf::TcpSocket* p_socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) p_socket;
    Netzwerk::id_t id_bewerber, id_stadt_oder_land;
    uint8_t amt_ebene;
    std::string amt_key;
    paket >> id_bewerber >> id_stadt_oder_land >> amt_ebene >> amt_key;
    auto& aemter = welt.aemter.at((Amt::Ebene)amt_ebene).at(id_stadt_oder_land);
    auto amt_it = std::find_if(aemter.begin(), aemter.end(), [&](const Amt& amt) { return amt.get_key() == amt_key; });
    if (amt_it != aemter.end()) amt_it->bewerben(id_bewerber);
    else Log::err() << "Host::" << __func__ << " Kein Amt gefunden in " << (unsigned)amt_ebene
                    << '/' << id_stadt_oder_land << " als " << amt_key << '\n';
}

void Host::a_rathaus_neues_bauprojekt(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;
    Netzwerk::id_t stadt_id;
    std::string infra_key;
    uint8_t anzahl;
    paket >> stadt_id >> infra_key >> anzahl;
    welt.staedte.at(stadt_id).starte_bau(Infrastruktur::get(infra_key), anzahl);
}

void Host::a_rathaus_steuer_anpassen(Netzwerkpaket& paket, sf::TcpSocket* socket) {
    Log::debug() << "Host::" << __func__ << Log::endl;
    (void) socket;
    Netzwerk::id_t land_id;
    uint8_t steuertyp, neuer_wert;
    paket >> land_id;
    paket >> steuertyp;
    paket >> neuer_wert;
    Land& land = welt.laender.at(land_id);
    land.set_steuer(static_cast<Steuertyp>(steuertyp), neuer_wert);
}
