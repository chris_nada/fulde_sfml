#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>

#include "klient.hpp"
#include "../welt/politik/steuer.hpp"
#include "../werkzeuge/etc_cereal.hpp"
#include <nada/log.hpp>

using nada::Log;

Netzwerk::id_t Klient::a_neuer_spieler(const sf::String& vorname, const sf::String& nachname, bool maennlich, int viertel_id) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket packet;
    packet << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    packet << (Netzwerk::enum_t) Netzwerk::Aktion::NEUER_SPIELER;
    packet << vorname;
    packet << nachname;
    packet << maennlich;
    packet << viertel_id;
    request(packet, true);
    packet >> dynastie_id;
    Log::debug() << "\tNeuer Spieler ID = " << dynastie_id << Log::endl;
    return dynastie_id;
}

void Klient::a_markt_kauf(Netzwerk::id_t stadt_id, Netzwerk::id_t markt_id, Netzwerk::id_t lager_id,
                          const std::string& waren_key, uint32_t anzahl, uint32_t preis, uint16_t steuern, float quali) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::MARKT_KAUF;
    paket << dynastie_id;
    paket << stadt_id;
    paket << markt_id;
    paket << lager_id;
    paket << waren_key;
    paket << anzahl;
    paket << preis;
    paket << steuern;
    paket << quali;
    request(paket);
}

void Klient::a_markt_verkauf(Netzwerk::id_t markt_id, Netzwerk::id_t lager_id,
        const std::string& waren_key, uint32_t anzahl, uint32_t gesamtpreis, float quali) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::MARKT_VERKAUF;
    paket << dynastie_id;
    paket << markt_id;
    paket << lager_id;
    paket << waren_key;
    paket << anzahl;
    paket << gesamtpreis;
    paket << quali;
    request(paket);
}

void Klient::a_lager_transport(Netzwerk::id_t id_lager, Netzwerk::id_t id_zielstadt, bool transport,
                               const std::unordered_map<std::string, uint16_t>& waren_hin,
                               const std::unordered_map<std::string, uint16_t>& waren_zurueck) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::LAGER_TRANSPORT;
    paket << id() << id_lager << id_zielstadt << transport;
    std::stringstream ss;
    {
        etc::serial_out out(ss);
        out(waren_hin, waren_zurueck);
    }
    paket << ss.str();
    request(paket);
}

void Klient::a_werk_stadt_arbeitertransfer(Netzwerk::id_t id_werk, Netzwerk::id_t id_stadt, Netzwerk::id_t id_char, bool stadt_zu_werk) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::WERK_STADT_ARBEITERTRANSFER;
    paket << id_werk << id_stadt << id_char << stadt_zu_werk;
    request(paket);
}

void Klient::a_werk_werk_arbeitertransfer(
        Netzwerk::id_t id_werk, Netzwerk::id_t id_char, uint8_t index_anlage) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::WERK_WERK_ARBEITERTRANSFER;
    paket << id_werk << id_char << index_anlage;
    request(paket);
}

void Klient::a_werk_neue_anlage(
        Netzwerk::id_t id_dynastie, Netzwerk::id_t id_werk, uint16_t kosten, const std::string& prozess_key) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::WERK_ANLAGE_NEU;
    paket << id_dynastie << id_werk << kosten << prozess_key;
    request(paket);
}

void Klient::a_werk_anlage_verkaufen(
        Netzwerk::id_t id_dynastie, Netzwerk::id_t id_werk, uint8_t index_anlage, uint16_t preis) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::WERK_ANLAGE_VK;
    paket << id_dynastie << id_werk << index_anlage << preis;
    request(paket);
}

void Klient::a_werk_erweitern_verkleinern(
        Netzwerk::id_t id_dynastie, Netzwerk::id_t id_werk, int16_t diff_raum, int32_t diff_geld) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::WERK_MODIFIZIEREN;
    paket << id_dynastie << id_werk << diff_raum << diff_geld;
    request(paket);
}

void Klient::a_werk_anlage_erweitern_verkleinern(
        Netzwerk::id_t id_dynastie, Netzwerk::id_t id_werk, uint8_t index_anlage,
        int16_t diff_raum, int32_t diff_geld) {
    Log::debug() << "Klient::" << __func__ << Log::endl;
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::WERK_ANLAGE_MODIFIZIEREN;
    paket << id_dynastie << id_werk << index_anlage << diff_raum << diff_geld;
    request(paket);
}

void Klient::a_werk_anlage_start_stop(Netzwerk::id_t id_werk, uint8_t index_anlage) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::WERK_ANLAGE_START_STOP;
    paket << id_werk << index_anlage;
    request(paket);
}

void Klient::a_werk_auto_kauf_toggle(Netzwerk::id_t id_werk) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::WERK_AUTO_KAUF_TOGGLE;
    paket << id_werk;
    request(paket);
}

void Klient::a_lager_verwaltung(const Lager& lager) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::LAGER_VERWALTUNG;
    paket << lager.get_id();
    std::stringstream ss;
    etc::serial_out out(ss);
    out << lager.auto_einkauf;
    out << lager.auto_verkauf;
    paket << ss.str();
    request(paket);
}

void Klient::a_nachricht_loeschen(const std::string& nachricht_hash) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::NACHRICHT_LOESCHEN;
    paket << dynastie_id;
    paket << nachricht_hash;
    request(paket);
}

void Klient::a_amt_waehlen(Netzwerk::id_t bewerber) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::AMT_WAEHLEN;
    paket << Klient::charakter_id << bewerber;
    request(paket);
}

void Klient::a_amt_bewerben(uint8_t ebene, Netzwerk::id_t stadt_oder_land_id, const std::string& amt_key) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::AMT_BEWERBEN;
    paket << Klient::charakter_id << stadt_oder_land_id << ebene << amt_key;
    request(paket);
}

void Klient::a_rathaus_neues_projekt(Netzwerk::id_t stadt_id, const std::string& infra_key, uint8_t anzahl) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::RATHAUS_NEUES_BAUPROJEKT;
    paket << stadt_id << infra_key << anzahl;
    request(paket);
}

void Klient::a_rathaus_steuer_anpassen(Netzwerk::id_t land_id, Steuertyp steuertyp, uint8_t neuer_wert) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::AKTION;
    paket << (Netzwerk::enum_t) Netzwerk::Aktion::RATHAUS_STEUER_ANPASSEN;
    paket << land_id << (uint8_t) steuertyp << neuer_wert;
    request(paket);
}
