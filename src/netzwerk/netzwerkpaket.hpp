#pragma once

#include <SFML/Network/Packet.hpp>

#ifdef NETZWERK_KOMPRESSION
    #include <zlib.h>
#endif

/**
 * @brief Zu nutzendes `Netzwerkpaket`.
 * @note Ist `NETZWERK_KOMPRESSION` beim Kompilieren gesetzt (siehe CMakeLists.txt),
 *       werden alle Paketinhalte beim senden komprimiert und beim
 *       Empfang dekomprimiert.
 */
class Netzwerkpaket final : public sf::Packet {

    friend class Test_Netzwerk;

public:

    /// Größe des Puffers für Pakete.
    static constexpr size_t PUFFER_BYTES = 0x7F'FF'FF;

#ifdef NETZWERK_KOMPRESSION

    /// Ctor.
    Netzwerkpaket();

    /// @inherit
    ~Netzwerkpaket() final = default;

private:

    /// Initialisiert einen zlib `z_stream`.
    static void zinit(z_stream& zs);

    /// @inherit
    const void* onSend(size_t& size) override;

    /// @inherit
    void onReceive(const void* data, std::size_t size) override;

    /// Datenspeicher.
    std::vector<uint8_t> data_puffer;

#endif

};
