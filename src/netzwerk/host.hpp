#pragma once

#include "../welt/welt.hpp"
#include "../welt/produktion/anlage.hpp"
#include "netzwerkpaket.hpp"
#include <SFML/Network.hpp>
#include <atomic>

/**
 * - Verwaltet die Simulationsdaten serverseitig.
 * - Verteilt die Daten an Teilnehmer Klasse (`Klient`).
 * - Wartet ständig auf Anfragen und schickt angeforderte Daten zurück.
 */
class Host final {

public:

    /**
     * Konstruktor, der den Host initialisiert und die Daten der Simulation initialisiert.
     * Existiert eine gespeicherte Datei mit dem Namen des Parameters, wird diese geladen, ansonsten
     * eine neue Welt erzeugt.
     */
    explicit Host(const std::string& spielstand);

    /**
     * Startet den Host und das Spiel.
     */
    void start();

    /**
     * Stoppt den Host.
     */
    void stop() { aktiv = false; }

    /**
     * Destruktor. Beendet den Host, schließt alle Verbindungen, speichert den Spielstand ab.
     */
    ~Host();

private:

    /// Führt einen Simulationsschritt durch.
    void tick();

    /**
     * Verwaltet eine Anfrage vom Klienten. Falls es sich um eine Aktion handelt,
     * wird diese weitergeleitet an handle_aktion (bloß der Übersicht halber).
     */
    void handle_request(Netzwerkpaket& request, sf::TcpSocket* socket);

    /**
     * Erhält daten von `handle_request`, falls es sich um einer Nutzeraktion (Warenkauf etc.) handelt
     * und setzt diese um.
     */
    void handle_aktion(Netzwerkpaket& request, sf::TcpSocket* socket);

    /// Führt ein Klient-seitig angefordertes Datenupdate durch.
    void handle_update(Netzwerkpaket& paket);

    /// Fügt dem Spiel einen neuen Spieler hinzu.
    void a_neuer_spieler(Netzwerkpaket& packet, sf::TcpSocket* socket);

    /// Markt: Warenkauf.
    void a_markt_kauf(Netzwerkpaket& packet, sf::TcpSocket* socket);

    /// Markt: Warenverkauf.
    void a_markt_verkauf(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Lager: Transport starten.
    void a_lager_transport(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Lager: Verwaltung (Automatischer Kauf/Verkauf).
    void a_lager_verwaltung(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Werk: Arbeiter rekrutieren.
    void a_werk_stadt_arbeitertransfer(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Werk: Weist zu oder entfernt Arbeiter zu einem Prozess.
    void a_werk_werk_arbeitertransfer(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Werk: Baut eine neue Anlage auf.
    void a_werk_neue_anlage(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Werk: Entfernt (verkauft) eine Anlage.
    void a_werk_anlage_verkaufen(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Werk: Erweitert oder verkleinert das Werk räumlich.
    void a_werk_erweitern_verkleinern(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Werk: Erweitert oder verkleinert eine Anlage räumlich.
    void a_werk_anlage_erweitern_verkleinern(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Werk: Startet oder stoppt die Produktion in einer Anlage.
    void a_werk_anlage_start_stop(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Werk: Automatischen Kauf von Bedarfsgütern ein/ausschalten.
    void a_werk_anlage_auto_kauf_toggle(Netzwerkpaket& paket, sf::TcpSocket *socket);

    /// Nachricht: Löschen.
    void a_nachricht_loeschen(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Amt: Jemanden wählen.
    void a_amt_waehlen(Netzwerkpaket& paket, sf::TcpSocket* p_socket);

    /// Amt: Sich selbst bewerben.
    void a_amt_bewerben(Netzwerkpaket& paket, sf::TcpSocket* p_socket);

    /// Rathaus: Neue Infrastruktur bauen.
    void a_rathaus_neues_bauprojekt(Netzwerkpaket& netzwerkpaket, sf::TcpSocket* socket);

    /// Rathaus: Steuer landesweit anpassen.
    void a_rathaus_steuer_anpassen(Netzwerkpaket& paket, sf::TcpSocket* socket);

    /// Liefert alle Positionen von PCs oder NPCs.
    void alle_positionen(Netzwerk::enum_t positionstyp, std::stringstream& antwort, sf::Packet& request);

    /// Serialisiert alle Objekte im gegebenem Vektor.
    template <typename T>
    std::string alle_serialisieren(Netzwerkpaket& request, const std::vector<T>& ptr_vec);

    /* Netzwerkelemente */
    sf::TcpListener             listener;
    sf::SocketSelector          selektor;
    std::vector<sf::TcpSocket*> klienten;

    /* Sonstiges */

    /// Angeschaltet?
    std::atomic_bool aktiv = true;

    /// Zähler: Bytes gesendet.
    size_t daten_gesendet = 0;

    /// Zähler: Bytes empfangen.
    size_t daten_empfangen = 0;

    /// Zähler: Pakete gesendet.
    size_t pakete_gesendet = 0;

    /// Zähler: Pakete empfangen.
    size_t pakete_empfangen = 0;

    /* Spieldaten */

    /// Pfad zum speichernden / ladenden Spielstand.
    std::string spielstand;

    /// Wurzelelement aller Simulationsdaten.
    Welt welt;

    /// Welt fertig geladen?
    std::atomic_bool bereit = false;

};
