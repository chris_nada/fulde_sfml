#pragma once

#include <SFML/System.hpp>

namespace Netzwerk {

    /// Datentyp zur Übertragung der hier definierten Enums.
    using enum_t = int16_t;

    /// Datentyp zur Übertragung von Standard-IDs.
    using id_t = uint32_t;

    /// Datentyp zur Übertragung potenziell großer IDs.
    using big_id_t = uint64_t;

    /// Vom Programm verwendeter Port.
    static constexpr uint16_t PORT = 51987;

    /// Art der Anfrage an den Server.
    enum Anfrage : enum_t {
        AKTION      = 0, // Funktion ausführen
        UPDATE      = 1, // Daten-Update
        ALLE        = 2, // Batch-Anfrage, z.B. ALLE, STADT

        // Anfragen
        ZEIT              = 101,
        KULTUR            = 102,
        DYNASTIE          = 103,
        STADT             = 104,
        LAND              = 105,
        VIERTEL           = 106,
        MARKT             = 107,
        GEBAEUDE          = 108,
        WERKSTATT         = 109,
        WOHNHAUS          = 110,
        LAGER             = 111,
        CHARAKTER         = 112,
        DYNASTIESTATISTIK = 113,
        NACHRICHTEN       = 114,
        AEMTER_LAND       = 120,
        AEMTER_STADT      = 121,
        POSITION          = 201,
        POSITIONEN_PC     = 202,
        POSITIONEN_NPC    = 203,
    };

    /// Art der Aktion, die der Spieler durchführen möchte.
    enum Aktion : enum_t {
        NEUER_SPIELER    = 0,
        MARKT_KAUF       = 100,
        MARKT_VERKAUF    = 101,
        BEWEGUNG         = 200,
        LAGER_TRANSPORT  = 300,
        LAGER_VERWALTUNG = 301,
        WERK_STADT_ARBEITERTRANSFER = 400,
        WERK_WERK_ARBEITERTRANSFER  = 401,
        WERK_ANLAGE_NEU             = 402,
        WERK_ANLAGE_VK              = 403,
        WERK_MODIFIZIEREN           = 404,
        WERK_ANLAGE_MODIFIZIEREN    = 405,
        WERK_ANLAGE_START_STOP      = 406,
        WERK_AUTO_KAUF_TOGGLE       = 407,
        NACHRICHT_LOESCHEN          = 600,
        AMT_WAEHLEN                 = 700,
        AMT_BEWERBEN                = 701,
        RATHAUS_NEUES_BAUPROJEKT    = 750,
        RATHAUS_STEUER_ANPASSEN     = 751,
    };

}
