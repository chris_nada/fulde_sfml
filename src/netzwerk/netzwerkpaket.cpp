#ifdef NETZWERK_KOMPRESSION

#include "netzwerkpaket.hpp"
#include <cstring>
#include <nada/log.hpp>

using nada::Log;

Netzwerkpaket::Netzwerkpaket() {
    //stream_puffer.resize(0x02'00);
}

const void* Netzwerkpaket::onSend(size_t& size) {
    thread_local std::vector<uint8_t> stream_puffer(PUFFER_BYTES);
    //stream_puffer.resize(getDataSize()*2);
    uLongf size_temp = stream_puffer.size();
    compress2(stream_puffer.data(), &size_temp,
              (const uint8_t*)(getData()), getDataSize(),
            Z_BEST_SPEED);
    data_puffer.resize(size_temp);
    std::copy(stream_puffer.data(), stream_puffer.data() + size_temp, data_puffer.data());
    size = size_temp;
    return (const void*)(data_puffer.data());
}

void Netzwerkpaket::onReceive(const void* data, std::size_t size) {
    thread_local std::vector<uint8_t> stream_puffer(PUFFER_BYTES);
    //stream_puffer.resize(size*4);
    uLongf puffer_size = static_cast<uLongf>(stream_puffer.size());
    uncompress(stream_puffer.data(), &puffer_size,
               (uint8_t*)(data), size);
    append((const void*)(stream_puffer.data()), puffer_size);
}

void Netzwerkpaket::zinit(z_stream& zs) {
    zs.zalloc = Z_NULL;
    zs.zfree  = Z_NULL;
    zs.opaque = Z_NULL;
}

#endif
