#pragma once

#include "netzwerk.hpp"
#include "../welt/waren/lager.hpp"
#include "../welt/spieler/position.hpp"
#include "netzwerkpaket.hpp"
#include "../welt/produktion/werkstatt.hpp"
#include "../welt/politik/steuer.hpp"

#include <nada/log.hpp>
#include <string>
#include <SFML/Network/TcpSocket.hpp>
#include <SFML/Network/Packet.hpp>
#include <unordered_map>

/**
 * Verwaltet die Netzwerkverbindung für Klienten.
 */
class Klient final {

public:

    /**
     * Stellt eine Verbindung her mit dem Server.
     * @param ip IP Adresse im Format AAA.BBB.CCC.DDD.
     * @return true, wenn erfolgreich verbunden.
     */
    static bool verbinde(const std::string& ip);

    /// Trennt die bestehende Verbindung.
    static void trennen() { socket.disconnect(); }

    /**
     * Stellt eine Anfrage an den Server her. Ist zum ständigen Synchronisieren von serverseitiger Daten gedacht.
     * @tparam T Typ des Angefragten Objekts.
     * @param anfrage Was ist angefragt?
     * @param ID ID des angefragten Objekts.
     * @param alt Das alte Objekt wird zurückgegeben bei Fehlern.
     * @return Das aktuelle angefragte Objekt.
     */
    template <typename T>
    static T request(Netzwerk::Anfrage anfrage, Netzwerk::big_id_t ID, const T& alt = T());

    /**
     * Stellt eine spezialisierte Anfrage, die für gegebenes Viertel
     * alle Positionen der Objekte liefert.
     * @param positionstyp Positionen für PCs oder NPCs liefern?
     * @param viertel_id Für welches Viertel?
     * @return Alle aktuellen Positionen der Objekte.
     */
    static std::unordered_map<Netzwerk::big_id_t, Position> request(Netzwerk::Anfrage positionstyp, Netzwerk::id_t viertel_id);

    /**
     * Stellt eine Batch-Anfrage an den Server.
     * @tparam T Typ der im Vektor enthaltenen Objekte.
     * @param anfrage Datentyp als Enum zum Auslesen.
     * @param ids Wenn nur bestimmte Objekte, dann hier IDs angeben. Wenn leer, werden alle geliefert.
     * @return Objekte vom Server.
     */
    template <typename T>
    static std::vector<T> request_alle(Netzwerk::Anfrage anfrage, const std::unordered_set<Netzwerk::big_id_t>& ids = std::unordered_set<Netzwerk::big_id_t>());

    /**
     * Führt eine Anfrage an den Host durch und gibt die Antwort als `Netzwerkpaket` zurück.
     */
    static Netzwerkpaket request(Netzwerkpaket& paket, bool antwort = false);

    /// Getter für die Spieler-ID (Dynastie-ID) des Klienten.
    static Netzwerk::id_t id() { return dynastie_id; };

    /// Getter für die Spieler-ID (Charakter-ID) des Klienten.
    static Netzwerk::id_t id_char() { return charakter_id; };

    /// Setter für die Spieler-ID (Dynastie-ID) des Klienten.
    static void id(Netzwerk::id_t dynastie_id) { Klient::dynastie_id = dynastie_id; };

    /// Setter für die Spieler-ID (Charakter-ID) des Klienten.
    static void id_char(Netzwerk::id_t charakter_id) { Klient::charakter_id = charakter_id; };

    /// Getter: Liste aller Werke von gegebener
    static std::vector<Werkstatt> get_werke(const Dynastie& dynastie);

    /// Aktion: Erstellt einen neuen Spieler beim Host und setzt `dynastie_id` auf den zugewiesenen Wert.
    static Netzwerk::id_t a_neuer_spieler(const sf::String& vorname, const sf::String& nachname, bool maennlich, int viertel_id);

    /// Aktion: Markt - Warenkauf.
    static void a_markt_kauf(Netzwerk::id_t stadt_id, Netzwerk::id_t markt_id, Netzwerk::id_t lager_id,
                             const std::string& waren_key, uint32_t anzahl, uint32_t preis, uint16_t steuern, float quali);

    /// Aktion: Markt - Warenverkauf.
    static void a_markt_verkauf(Netzwerk::id_t markt_id, Netzwerk::id_t lager_id,
            const std::string& waren_key, uint32_t anzahl, uint32_t gesamtpreis, float quali);

    /// Aktion: Lager - Transport.
    static void a_lager_transport(Netzwerk::id_t id_lager, Netzwerk::id_t id_zielstadt, bool transport,
                                  const std::unordered_map<std::string, uint16_t>& waren_hin,
                                  const std::unordered_map<std::string, uint16_t>& waren_zurueck);

    /// Aktion: Lager - Automatischer Kauf / Verkauf konfigurieren.
    static void a_lager_verwaltung(const Lager& lager);

    /// Aktion: Werkstatt - Arbeiter einstellen
    static void a_werk_stadt_arbeitertransfer(
            Netzwerk::id_t id_werk, Netzwerk::id_t id_stadt, Netzwerk::id_t id_char, bool stadt_zu_werk);

    /// Aktion: Werkstatt, Arbeiter transferieren zw. Werk <-> Anlage.
    static void a_werk_werk_arbeitertransfer(
            Netzwerk::id_t id_werk, Netzwerk::id_t id_char, uint8_t index_anlage);

    /// Aktion: Werkstatt, neue Anlage einrichten.
    static void a_werk_neue_anlage(
            Netzwerk::id_t id_dynastie, Netzwerk::id_t id_werk, uint16_t kosten, const std::string& prozess_key);

    /// Aktion: Werkstatt, bestehende Anlage verkaufen.
    static void a_werk_anlage_verkaufen(
            Netzwerk::id_t id_dynastie, Netzwerk::id_t id_werk, uint8_t index_anlage, uint16_t preis);

    /// Aktion: Werkstatt, Raum erweitern / verkleinern.
    static void a_werk_erweitern_verkleinern(
            Netzwerk::id_t id_dynastie, Netzwerk::id_t id_werk, int16_t diff_raum, int32_t diff_geld);

    /// Aktion: Werkstatt, Anlage räumlich erweitern / verkleinern.
    static void a_werk_anlage_erweitern_verkleinern(
            Netzwerk::id_t id_dynastie, Netzwerk::id_t id_werk,
            uint8_t index_anlage, int16_t diff_raum, int32_t diff_geld);

    /// Aktion: Werkstatt, Anlage starten/stoppen (toggelt).
    static void a_werk_anlage_start_stop(Netzwerk::id_t id_werk, uint8_t index_anlage);

    /// Aktion: Werkstatt: Automatischen Einkauf der Bedarfsgüter umschalten.
    static void a_werk_auto_kauf_toggle(Netzwerk::id_t id_werk);

    /// Aktion: Nachricht: Löschen.
    static void a_nachricht_loeschen(const std::string& nachricht_hash);

    /// Aktion: Stimme auf ein Amt abgeben.
    static void a_amt_waehlen(Netzwerk::id_t bewerber);

    /// Aktion: Auf ein Amt bewerben.
    static void a_amt_bewerben(uint8_t ebene, Netzwerk::id_t stadt_oder_land_id, const std::string& amt_key);

    /// Aktion: Neue Infrastruktur bauen (im Rathaus).
    static void a_rathaus_neues_projekt(Netzwerk::id_t stadt_id, const std::string& infra_key, uint8_t anzahl);

    /// Aktion: Im Rathaus eine Landessteuer anpassen.
    static void a_rathaus_steuer_anpassen(Netzwerk::id_t land_id, Steuertyp steuertyp, uint8_t neuer_wert);

    /// Aktualisiert gegebene Position.
    template <typename T>
    static void update(Netzwerk::Anfrage anfrage, const T& objekt, Netzwerk::big_id_t char_id = 0);

private:

    /// Verbindung zum Server.
    static inline sf::TcpSocket socket;

    /// ID des Spielers.
    static inline Netzwerk::id_t dynastie_id, charakter_id;

    /// Führt einen Transfer durch. Gibt zurück, ob erfolgreich.
    static bool transfer(Netzwerkpaket& paket, Netzwerk::Anfrage anfrage, Netzwerk::big_id_t wert);

};

