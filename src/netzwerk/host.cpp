#include "host.hpp"
#include "../welt/spieler/charakter.hpp"
#include "../werkzeuge/etc.hpp"

#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/unordered_map.hpp>
#include <nada/log.hpp>

using nada::Log;

// Alle unterstützten Varianten von alle_serialisieren
template std::string Host::alle_serialisieren<Land>(Netzwerkpaket& request, const std::vector<Land>& ptr_vec);
template std::string Host::alle_serialisieren<Stadt>(Netzwerkpaket& request, const std::vector<Stadt>& ptr_vec);
template std::string Host::alle_serialisieren<Viertel>(Netzwerkpaket& request, const std::vector<Viertel>& ptr_vec);
template std::string Host::alle_serialisieren<Gebaeude>(Netzwerkpaket& request, const std::vector<Gebaeude>& ptr_vec);
template std::string Host::alle_serialisieren<Dynastie>(Netzwerkpaket& request, const std::vector<Dynastie>& ptr_vec);
template std::string Host::alle_serialisieren<Charakter>(Netzwerkpaket& request, const std::vector<Charakter>& ptr_vec);

Host::Host(const std::string& spielstand)
{
    Log::debug() << "Host(" << spielstand << ")\n";

    // Datei laden
    if (std::ifstream speicherstand(spielstand); speicherstand.good()) {
        std::stringstream ss;
        ss << speicherstand.rdbuf();
        ss.flush();
        welt = *(new Welt(ss));
        Log::out() << "Spielstand " << spielstand << " geladen\n";
    }
    // Neues Spiel
    else {
        welt.skip(5 * 4); // x * 4 (x Jahre überspringen) // TODO wieviel?
        Log::out() << "Neues Spiel gestartet\n";
    }
}

void Host::start() {
    Log::debug() << "Host::start()\n";

    // Listener starten
    while (listener.listen(Netzwerk::PORT) != sf::Socket::Done) {
        Log::err() << "\tPort " << Netzwerk::PORT << " blockiert. Versuche erneut...\n";
        sf::sleep(sf::seconds(1));
    }
    selektor.add(listener);
    Log::out() << "\tServer gestartet an Port " << Netzwerk::PORT << Log::endl;

    sf::Clock timer;
    while (aktiv) {

        // Geschwindigkeit loggen
        if (timer.getElapsedTime() > sf::milliseconds(1000)) {
            const auto kb_empfangen = std::round(daten_empfangen / 1000.f);
            const auto kb_gesendet  = std::round(daten_gesendet  / 1000.f);
            Log::out() << "Host KB/s Pakete: " << kb_gesendet << '/' << kb_empfangen << ' ';
            Log::out() << pakete_gesendet << '/' << pakete_empfangen << '\n';
            daten_empfangen  = 0;
            daten_gesendet   = 0;
            pakete_gesendet  = 0;
            pakete_empfangen = 0;
            timer.restart();
            tick(); // TODO Intervall 1s?
        }

        // Der Selektor wartet bis eine Verbindung eingeht
        if (selektor.wait(sf::milliseconds(1000))) {

            // Teste den Listener
            if (selektor.isReady(listener)) {

                // Listener isReady = Neue Verbindung
                sf::TcpSocket* socket = new sf::TcpSocket;
                if (listener.accept(*socket) == sf::Socket::Done) {
                    klienten.push_back(socket);
                    selektor.add(*socket);
                    Log::debug() << "Neuer Klient=" << socket->getRemoteAddress().toString() << '\n';
                    Log::debug() << "Anzahl Klienten=" << klienten.size() << '\n';
                }
                else delete socket;
            }
            else for (sf::TcpSocket* const socket_ptr : klienten) {
                if (selektor.isReady(*socket_ptr)) {

                    // Empfangene Daten in paket schreiben und bearbeiten
                    Netzwerkpaket paket;
                    if ((*socket_ptr).receive(paket) == sf::Socket::Done) {
                        daten_empfangen += paket.getDataSize();
                        pakete_empfangen++;
                        try { handle_request(paket, socket_ptr); }
                        catch (std::exception& e) {
                            Log::err() << "Server handle_request schlug fehl: " << e.what() << '\n';
                        }
                    }
                }
            }
        }
    }
}

void Host::handle_request(Netzwerkpaket& request, sf::TcpSocket* socket) {
    //Log::debug() << "Host::handle_request() ";
    using namespace Netzwerk;

    // Anfrage extrahieren
    enum_t a;
    request >> a;
    Anfrage anfrage;
    try { anfrage = static_cast<Anfrage>(a); } // TODO Cast verifizieren
    catch (std::exception& e) {
        Log::err() << "\tUngueltige Anfrage: " << a << '\n';
        return;
    }

    // Antworten in ss schreiben
    std::stringstream ss;

    // Anfrage oder Batch-Anfrage
    try { switch (anfrage) {
        case AKTION:
            handle_aktion(request, socket);
            return;
        case UPDATE:
            handle_update(request);
            return;
        case ALLE: {
            enum_t temp;
            request >> temp;
            Anfrage anfrage_alle = (Anfrage) temp;
            switch (anfrage_alle) { // Achtung! Bei neuen Typen oben Template instanzieren
                case LAND:      ss << alle_serialisieren<Land>(request, welt.laender); break;
                case STADT:     ss << alle_serialisieren<Stadt>(request, welt.staedte); break;
                case VIERTEL:   ss << alle_serialisieren<Viertel>(request, welt.viertel); break;
                case GEBAEUDE:  ss << alle_serialisieren<Gebaeude>(request, welt.gebaeude); break;
                case DYNASTIE:  ss << alle_serialisieren<Dynastie>(request, welt.dynastien); break;
                case CHARAKTER: ss << alle_serialisieren<Charakter>(request, welt.charaktere); break;
                case POSITIONEN_PC:  alle_positionen(temp, ss, request); break;
                case POSITIONEN_NPC: alle_positionen(temp, ss, request); break;
                default: Log::err() << "\tAnfrage (alle) nicht implementiert: " << (Netzwerk::enum_t) anfrage << '\n'; break;
            }
            break;
        }
        default: {
            // Einzelanfrage
            sf::Uint64 id;
            request >> id;
            switch(anfrage) {
                    case POSITION:          etc::serialisiere(ss, welt.charaktere.at(id).get_pos()); break;
                    case CHARAKTER:         etc::serialisiere(ss, welt.charaktere.at(id)); break;
                    case ZEIT:              etc::serialisiere(ss, welt.get_zeit()); break;
                    case VIERTEL:           etc::serialisiere(ss, welt.viertel.at(id)); break;
                    case STADT:             etc::serialisiere(ss, welt.staedte.at(id)); break;
                    case KULTUR:            etc::serialisiere(ss, welt.kulturen.at(id)); break;
                    case DYNASTIE:          etc::serialisiere(ss, welt.dynastien.at(id)); break;
                    case LAGER:             etc::serialisiere(ss, welt.lager.at(id)); break;
                    case MARKT:             etc::serialisiere(ss, welt.maerkte.at(id)); break;
                    case GEBAEUDE:          etc::serialisiere(ss, welt.gebaeude.at(id)); break;
                    case WERKSTATT:         etc::serialisiere(ss, welt.werkstaetten.at(id)); break;
                    case WOHNHAUS:          etc::serialisiere(ss, welt.wohnhaeuser.at(id));  break;
                    case LAND:              etc::serialisiere(ss, welt.laender.at(id)); break;
                    case DYNASTIESTATISTIK: etc::serialisiere(ss, welt.statistiken.at(id)); break;
                    case NACHRICHTEN:       etc::serialisiere(ss, Nachrichten(welt.nachrichten.at(id))); break;
                    case AEMTER_LAND:       etc::serialisiere(ss, welt.aemter[Amt::Ebene::LAND].at(id)); break;
                    case AEMTER_STADT:      etc::serialisiere(ss, welt.aemter[Amt::Ebene::STADT].at(id)); break;
                    default: Log::err() << "\tAnfrage nicht implementiert: " << (Netzwerk::enum_t) anfrage << '\n'; return;
            }
        }
    }} catch (const std::exception& e) {
        Log::err() << "\tAnfrage fehlerhaft: " << (Netzwerk::enum_t) anfrage << ' ' << e.what() << Log::endl;
        return;
    }

    // Senden
    Netzwerkpaket antwort = request; // Paket wiederverwenden
    antwort.clear();
    antwort << ss.str();

    // Debug: Kompression loggen // Kompression nicht immer günstig. Kann sogar kleine Pakete aufblähen.
    //Log::debug() << " zip/raw : " << ss_komprimiert.str().size() << '/' << ss.str().size() << Log::endl;

    daten_gesendet += antwort.getDataSize();
    pakete_gesendet++;
    socket->send(antwort);
}

void Host::handle_aktion(Netzwerkpaket& request, sf::TcpSocket* socket) {
    // Aktion extrahieren
    Netzwerk::enum_t a;
    request >> a;
    Netzwerk::Aktion aktion = (Netzwerk::Aktion) a;

    // Dispatcher
    switch (aktion) {
        using namespace Netzwerk;
        case NEUER_SPIELER:    a_neuer_spieler(request, socket); break;
        case MARKT_KAUF:       a_markt_kauf(request, socket); break;
        case MARKT_VERKAUF:    a_markt_verkauf(request, socket); break;
        case LAGER_TRANSPORT:  a_lager_transport(request, socket); break;
        case LAGER_VERWALTUNG: a_lager_verwaltung(request, socket); break;
        case WERK_STADT_ARBEITERTRANSFER: a_werk_stadt_arbeitertransfer(request, socket); break;
        case WERK_WERK_ARBEITERTRANSFER:  a_werk_werk_arbeitertransfer(request, socket); break;
        case WERK_ANLAGE_NEU:             a_werk_neue_anlage(request, socket); break;
        case WERK_ANLAGE_VK:              a_werk_anlage_verkaufen(request, socket); break;
        case WERK_MODIFIZIEREN:           a_werk_erweitern_verkleinern(request, socket); break;
        case WERK_ANLAGE_MODIFIZIEREN:    a_werk_anlage_erweitern_verkleinern(request, socket); break;
        case WERK_ANLAGE_START_STOP:      a_werk_anlage_start_stop(request, socket); break;
        case WERK_AUTO_KAUF_TOGGLE:       a_werk_anlage_auto_kauf_toggle(request, socket); break;
        case NACHRICHT_LOESCHEN:          a_nachricht_loeschen(request, socket); break;
        case AMT_WAEHLEN:                 a_amt_waehlen(request, socket); break;
        case AMT_BEWERBEN:                a_amt_bewerben(request, socket); break;
        case RATHAUS_NEUES_BAUPROJEKT:    a_rathaus_neues_bauprojekt(request, socket); break;
        case RATHAUS_STEUER_ANPASSEN:     a_rathaus_steuer_anpassen(request, socket); break;
        default: Log::err() << "\tAktion nicht implementiert: " << a << '\n'; break;
    }
}

Host::~Host() {
    Log::debug() << "~Host()" << Log::endl;
    nada::Ini cfg{"config.ini"};
    if (cfg.get_int<int>("autosave") == 1) welt.speichern("data/save/last.dat");
    for (sf::TcpSocket* s : klienten) {
        s->disconnect();
        delete s;
    }
    listener.close();
}

template <typename T>
std::string Host::alle_serialisieren(Netzwerkpaket& request, const std::vector<T>& vec) {
    /// IDs ausparsen
    std::unordered_set<Netzwerk::big_id_t> ids;
    std::string buffer;
    request >> buffer;
    std::stringstream ss(buffer);
    etc::serial_in in(ss);
    in(ids);

    // Alle angefordert
    std::vector<T> objekte;
    const std::vector<T>* v_ptr;

    if (ids.empty()) v_ptr = &vec;
    else {
        objekte.reserve(ids.size());
        for (const auto id : ids) objekte.push_back(vec.at(id));
        v_ptr = &objekte;
    }

    // Serialisieren
    ss.str("");
    ss.clear();
    etc::serial_out out(ss);
    out(*v_ptr);
    return ss.str();
}

void Host::alle_positionen(Netzwerk::enum_t positionstyp, std::stringstream& antwort, sf::Packet& request) {
    const Netzwerk::Anfrage anfrage = (Netzwerk::Anfrage) positionstyp;
    Netzwerk::id_t viertel_id;
    request >> viertel_id;
    std::unordered_map<Netzwerk::big_id_t, Position> positionen;
    switch (anfrage) {
        case Netzwerk::POSITIONEN_PC:
            for (const auto& char_id : welt.viertel.at(viertel_id).get_charaktere()) {
                positionen[char_id] = welt.charaktere.at(char_id).get_pos();
            } break;
        case Netzwerk::POSITIONEN_NPC:
            for (const auto& ziv : welt.viertel.at(viertel_id).get_zivilisten()) {
                positionen[ziv.get_id()] = ziv.get_pos();
            } break;
        default: Log::err() << "Host::" << __func__ << " ungueltiger Positionstyp: " << positionstyp << Log::endl;
            break;
    }
    etc::serial_out boa(antwort);
    boa << positionen;
}

void Host::handle_update(Netzwerkpaket& paket) {
    // Typ auslesen
    Netzwerk::enum_t temp;
    paket >> temp;
    const Netzwerk::Anfrage update_typ = (Netzwerk::Anfrage) temp;

    // Rohdaten auslesen
    std::string buffer;
    paket >> buffer;
    std::stringstream ss(buffer);

    switch (update_typ) {
        using namespace Netzwerk;
        case POSITION: {
            Position p(ss);
            sf::Uint64 id;
            paket >> id;
            Viertel& v_alt = welt.viertel[welt.charaktere[id].get_pos().viertel];
            welt.charaktere[id].set_pos() = p;
            Viertel& v_neu = welt.viertel[p.viertel];
            if (&v_alt != &v_neu) { // In anderes Viertel gewechselt?
                v_alt.remove_person(id);
                v_neu.add_person(id);
            }
            break;
        }
        default: Log::debug() << "Host::" << __func__ << " update_typ unbekannt = " << (Netzwerk::enum_t) update_typ << Log::endl;
    }
}

void Host::tick() {
    //Log::debug() << "Host::" << __func__ << Log::endl;
    welt.tick();
}
