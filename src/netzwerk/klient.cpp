#include "klient.hpp"
#include "../welt/stadt/viertel.hpp"
#include "../welt/zeit.hpp"
#include "../welt/waren/markt.hpp"
#include "../welt/bauten/gebaeude.hpp"
#include "../welt/spieler/dynastie.hpp"
#include "../welt/stadt/stadt.hpp"
#include "../welt/spieler/charakter.hpp"
#include "../welt/statistik.hpp"
#include "../welt/land.hpp"
#include "../welt/kultur.hpp"
#include "../welt/produktion/werkstatt.hpp"
#include "../welt/politik/amt.hpp"
#include "../werkzeuge/etc.hpp"

#include <SFML/Network/IpAddress.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/unordered_map.hpp>
#include <nada/log.hpp>

using nada::Log;

// Liste aller möglicher Anfragen
template Zeit Klient::request<Zeit>           (Netzwerk::Anfrage, Netzwerk::big_id_t, const Zeit&);
template Land Klient::request<Land>           (Netzwerk::Anfrage, Netzwerk::big_id_t, const Land&);
template Kultur Klient::request<Kultur>       (Netzwerk::Anfrage, Netzwerk::big_id_t, const Kultur&);
template Markt Klient::request<Markt>         (Netzwerk::Anfrage, Netzwerk::big_id_t, const Markt&);
template Stadt Klient::request<Stadt>         (Netzwerk::Anfrage, Netzwerk::big_id_t, const Stadt&);
template Lager Klient::request<Lager>         (Netzwerk::Anfrage, Netzwerk::big_id_t, const Lager&);
template Viertel Klient::request<Viertel>     (Netzwerk::Anfrage, Netzwerk::big_id_t, const Viertel&);
template Gebaeude Klient::request<Gebaeude>   (Netzwerk::Anfrage, Netzwerk::big_id_t, const Gebaeude&);
template Dynastie Klient::request<Dynastie>   (Netzwerk::Anfrage, Netzwerk::big_id_t, const Dynastie&);
template Position Klient::request<Position>   (Netzwerk::Anfrage, Netzwerk::big_id_t, const Position&);
template Charakter Klient::request<Charakter> (Netzwerk::Anfrage, Netzwerk::big_id_t, const Charakter&);
template Werkstatt Klient::request<Werkstatt> (Netzwerk::Anfrage, Netzwerk::big_id_t, const Werkstatt&);
template Nachrichten Klient::request<Nachrichten> (Netzwerk::Anfrage, Netzwerk::big_id_t, const Nachrichten&);
template Statistik Klient::request<Statistik> (Netzwerk::Anfrage, Netzwerk::big_id_t, const Statistik&);
template std::vector<Amt> Klient::request<std::vector<Amt>> (Netzwerk::Anfrage, Netzwerk::big_id_t, const std::vector<Amt>&);

// request_alle
template std::vector<Land> Klient::request_alle<Land>           (Netzwerk::Anfrage anfrage, const std::unordered_set<Netzwerk::big_id_t>& ids);
template std::vector<Stadt> Klient::request_alle<Stadt>         (Netzwerk::Anfrage anfrage, const std::unordered_set<Netzwerk::big_id_t>& ids);
template std::vector<Viertel> Klient::request_alle<Viertel>     (Netzwerk::Anfrage anfrage, const std::unordered_set<Netzwerk::big_id_t>& ids);
template std::vector<Gebaeude> Klient::request_alle<Gebaeude>   (Netzwerk::Anfrage anfrage, const std::unordered_set<Netzwerk::big_id_t>& ids);
template std::vector<Dynastie> Klient::request_alle<Dynastie>   (Netzwerk::Anfrage anfrage, const std::unordered_set<Netzwerk::big_id_t>& ids);
template std::vector<Charakter> Klient::request_alle<Charakter> (Netzwerk::Anfrage anfrage, const std::unordered_set<Netzwerk::big_id_t>& ids);

// updates
template void Klient::update<Position> (Netzwerk::Anfrage anfrage, const Position& objekt, Netzwerk::big_id_t char_id);

bool Klient::verbinde(const std::string& ip) {
    Log::debug() << "Klient::verbinde(" << ip << ")" << Log::endl;
    if (socket.getRemoteAddress() != sf::IpAddress::None) {
        Log::debug() << "\t...bereits verbunden mit " << socket.getRemoteAddress().toString() << Log::endl;
        return true;
    }
    sf::IpAddress sf_ip(ip);
    sf::Socket::Status status = socket.connect(ip, Netzwerk::PORT);
    return (status == sf::Socket::Done);
}

Netzwerkpaket Klient::request(Netzwerkpaket& paket, bool antwort) {
    if (!(socket.send(paket) == sf::Socket::Done)) Log::err() << "Klient::request(Netzwerkpaket&) Anfrage fehlgeschlagen.\n";
    paket.clear();
    if (!antwort) return paket; // Keine Antwort nötig
    if (!(socket.receive(paket) == sf::Socket::Done)) Log::err() << "Klient::request(Netzwerkpaket&) Keine Antwort.\n";
    return paket;
}

std::unordered_map<Netzwerk::big_id_t, Position>
Klient::request(Netzwerk::Anfrage positionstyp, Netzwerk::id_t viertel_id) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::ALLE;
    paket << (Netzwerk::enum_t) positionstyp;
    paket << viertel_id;
    transfer(paket, positionstyp, viertel_id);
    std::string antwort;
    paket >> antwort;
    std::stringstream ss(antwort);
    etc::serial_in bia(ss);
    std::unordered_map<Netzwerk::big_id_t, Position> positionen;
    bia(positionen);
    return positionen;
}

template<typename T>
T Klient::request(Netzwerk::Anfrage anfrage, Netzwerk::big_id_t ID, const T& alt) {
    try {
        // Anfrage stellen
        Netzwerkpaket packet;
        for (unsigned versuch = 1; true; ++versuch) {
            packet << (Netzwerk::enum_t) anfrage;
            packet << (sf::Uint64) ID;
            if (!transfer(packet, anfrage, ID)) {
                Log::err() << "Klient::" << __func__ << ' ' << typeid(T).name() << ' ' << ID << " keine Antwort; Versuch " << versuch << '\n';
                packet.clear();
                if (versuch >= 3) return alt; // Fehlschlag, return alten Wert
            } else break; // Erfolg
        }

        // Antwort auslesen
        std::string antwort_daten;
        packet >> antwort_daten;
        std::stringstream ss_in(antwort_daten);
        etc::serial_in in(ss_in);
        T t;
        in(t);
        return t;
    } catch (const std::exception& e) {
        Log::err() << "Klient::" << __func__  << ' ' << typeid(T).name() << ' ' << ID << " Fehler beim Entpacken der Antwort: ";
        Log::err() << e.what() << Log::endl;
        return alt;
    }
}

template<typename T>
std::vector<T> Klient::request_alle(Netzwerk::Anfrage anfrage, const std::unordered_set<Netzwerk::big_id_t>& ids) {
    Netzwerkpaket paket;
    std::vector<T> objekte; // Speicher für Antwort

    // Paket mit angefragten IDs anfertigen
    {
        paket << (Netzwerk::enum_t) Netzwerk::Anfrage::ALLE;
        paket << (Netzwerk::enum_t) anfrage;
        std::stringstream buffer;
        etc::serial_out out(buffer);
        out(ids);
        paket << buffer.str();
        if (!transfer(paket, anfrage, ids.size())) return objekte;
    }

    // Antwort auslesen
    std::string antwort_daten;
    paket >> antwort_daten;
    std::stringstream ss_in(antwort_daten);
    etc::serial_in in(ss_in);
    in >> objekte;
    Log::debug() << "Klient::" << __func__ << " Angefordert/Erhalten: ";
    Log::debug() << objekte.size() << '/' << (!ids.empty() ? std::to_string(ids.size()) : "alle") << '\n';
    return objekte;
}

bool Klient::transfer(Netzwerkpaket& paket, Netzwerk::Anfrage anfrage, Netzwerk::big_id_t wert) {
    // Anfrage stellen
    if (!(socket.send(paket) == sf::Socket::Done)) {
        Log::err() << "Klient::request(" << (Netzwerk::enum_t) anfrage << ", " << wert << ") Anfrage senden fehlgeschlagen." << Log::endl;
        return false;
    }
    paket.clear();

    // Antwort empfangen
    if (!(socket.receive(paket) == sf::Socket::Done)) {
        Log::err() << "Klient::request(" << (Netzwerk::enum_t) anfrage << ", " << wert << ") Keine Antwort." << Log::endl;
        return false;
    }
    return true;
}

template<typename T>
void Klient::update(Netzwerk::Anfrage anfrage, const T& objekt, Netzwerk::big_id_t char_id) {
    Netzwerkpaket paket;
    paket << (Netzwerk::enum_t) Netzwerk::Anfrage::UPDATE;
    paket << (Netzwerk::enum_t) anfrage;
    paket << etc::serialisiere(objekt);
    if (anfrage == Netzwerk::Anfrage::POSITION) paket << (sf::Uint64) char_id; // Optional

    // Abschicken
    if (!(socket.send(paket) == sf::Socket::Done)) {
        Log::err() << "Klient::" << __func__ << "(" << (Netzwerk::enum_t) anfrage << ") senden fehlgeschlagen." << Log::endl;
    }
}

std::vector<Werkstatt> Klient::get_werke(const Dynastie& dynastie) {
    std::vector<Werkstatt> werke;
    for (const auto& id : dynastie.get_gebaeude()) {
        Gebaeude g = Klient::request<Gebaeude>(Netzwerk::Anfrage::GEBAEUDE, id);
        if (g.get_typ() == Gebaeude::Typ::WERKSTATT) {
            werke.push_back(Klient::request<Werkstatt>(Netzwerk::Anfrage::WERKSTATT, g.get_id_link()));
        }
    }
    return werke;
}
