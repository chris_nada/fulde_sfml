# Inhalt
+ 1. [Mitmachen via GitLab](#mitmachen)
+ 2. [Anleitung zum Kompilieren](#anleitung_kompilieren)
    + 2.1. Windows
    + 2.2. Linux
+ 3. [Quelltextrichtlinien](#richtlinien_cpp)

## 1. Mitmachen via GitLab <a name="mitmachen"/>

### Assets

Für das Ausführen von *Fulde* werden einige Assets (Grafiken etc.) benötigt,
die sich nicht hier im Git-Repository befinden. Link zum Download kann bei mir 
angefragt werden. Die Zip-Datei wird dann im Ordner `build` entpackt und
erzeugt dort Unterverzeichnisse, z.B. `build/gfx`. Ab und zu wird eine neue
Version der Assets benötigt, wenn Dinge hinzugekommen sind. Viele Assets sind
für das Projekt proprietär erworben und unterstehen anderen Lizenzen als der
hiesige Quelltext. Abgesehen davon wären sie recht groß für Git.

### Eigenen Kode einbringen
+ I.\* Einen [fork](https://gitlab.com/chris_nada/fulde_sfml/-/forks/new) dieses Repositorys erstellen.
+ II.\* Das **geforkte Repository** mit git clonen auf den eigenen Rechner (importieren).
+ III. Daran Programmieren.
+ IV. Commit (als Nachricht kurz beschreiben, was der geänderte Kode macht).
+ V. Push.
+ VI. In GitLab einen [Merge-Request](https://gitlab.com/chris_nada/fulde_sfml/-/merge_requests) erstellen von dem geforkten Repository in dieses.
    + Evtl. mir bescheid geben.
+ `goto III.`

<br/>\* Nur beim ersten Mal nötig.

### Check vor jedem Merge-Request
**Wichtig:** Vor jedem Merge-Request folgende Punkte durchgehen:
+ Macht mein Kode, was er soll? Ausreichend testen!
+ Ist mein Kode ordentlich geschrieben\* und kommentiert?
    + \* befolgt mein Kode die [Richtlinien](#richtlinien_cpp)?

## 2. Anleitung zum Kompilieren <a name="anleitung_kompilieren"/>

+ Dynamisch eingebundene Bibliotheken können mit dem Shell-Skript `libs/download_libraries.sh` automatisch 
  heruntergeladen und entpackt werden.
+ Der Quelltext kann mittels CMake über die vorhandene ``CMakeLists.txt`` automatisiert werden.
+ Verwendete Bibliotheken sind: 
    + [*SFML*](https://www.sfml-dev.org/) für Rendering, Netzwerk, Audio,
    + [*Cereal*](http://uscilab.github.io/cereal/) für Serialisierung,
    + [*ImGui/ImGui-SFML/ImPlot*](https://github.com/eliasdaler/imgui-sfml) für GUI.
    + [*doctest*]() für Tests.
    + Weitere Informationen auch zu den Versionen der Bibliotheken
      befinden sich in `CMakeLists.txt`
      und `libs/download_librares.sh`.

### 2.1 Windows
+ Als Entwicklungsumgebung ist [CLion](https://www.jetbrains.com/clion/) 
  mit [MSYS2](https://sourceforge.net/projects/msys2/files/Base/x86_64/)-Installation
  genutzt worden, dessen Benutzung ist jedoch nicht unbedingt notwendig,  
  um den Quelltext zu kompilieren.
    + Wichtigste Befehle für MSYS2:
    
        | **Befehl**                 | **Auswirkung**                            |
        |:---------------------------|:------------------------------------------|
        | `pacman -Syu`              | Aktualisiert *alle installierten* Pakete. |
        | `pacman -Ss 'paketname'`   | Sucht nach dem Paket 'paketname'.         |
        | `pacman -S 'paketname'`    | Installiert das Paket 'paketname'.        |
        | `pacman -Qe`               | Zeigt alle installierten Pakete an.       |
        
+ Um MSYS2 zu nutzen sind folgende Pakete (und deren Abhängigkeiten) erforderlich:
    + `mingw-w64-x86_64-gcc`
    + `mingw-w64-x86_64-gdb`
    + `mingw-w64-x86_64-lld`
    + `mingw-w64-x86_64-make`
    + `mingw-w64-x86_64-cmake`
    + `mingw-w64-x86_64-sfml`
    + (optional, automatisierte Doku-Generierung: ``mingw64/mingw-w64-x86_64-doxygen``)
    
### 2.2 Linux
Unter Linux muss ein C++ 17-kompatibler Kompiler vorhanden sein,
d.h. empfohlen wird mindestens GCC 8.3 (und GDB als Debugger).
Je nach Linux-Distribution heißt das Kompilerpaket `g++` oder `g++-8`.
Zusätzlich muss `make` und `cmake` installiert sein. Außerdem
wird bevorzugt mit `lld` gelinkt, da dieser Linker schneller als `gold` und `ld`
arbeitet.

Einige Bibliotheken werden bereits in einer projektabhängigen Version
im Ordner `/libs` dem Quelltext bereitgestellt (s.o.). Zusätzlich
wird die Bibliothek SFML (Paketname `libsfml-dev`)  benötigt. Diese kann bspw. via 
`sudo apt install 'paketname'` installiert werden.
    
## 3. Quelltextrichtlinien für C++ Kode <a key="richtlinien_cpp"/>
+ C++ 17.
+ Betriebssystem-exklusive Header wie `windows.h` sind tabu.
+ Klammer- & Einrückstil: Wie in Java bzw. an [Stroustrup](https://en.wikipedia.org/wiki/Indent_style#Variant:_Stroustrup) orientieren.
+ Zeiger- (\*) und Referenzsymbole (&) kleben am Datentypen, nicht am Variablennamen; Bsp.: `int* n;` oder `int& x;` 
   und so bitte nicht: `int *n;` oder `int &x;`
+ Namespaces werden explizit genannt; kein Datei-globales `using namespace std;` o.Ä.
    + `using namespace foo;` innerhalb eines beschränkten Scopes ist jedoch erlaubt.
+ *kleine* Klassen (<50 Zeilen) dürfen *header-only* sein.
+ *getter* und *setter* dürfen *header-only* sein, wenn sie Einzeiler sind.
+ Vor und nach Operatoren *sollte* ein Leerzeichen stehen (Bsp.: `float f = 15.f / (x * (8 + x))`).
+ Die Länge einzelner Quelltextdateien ist in Maßen zu halten (max. ~500 Zeilen).
    + Es ist erlaubt die Implementierung einer Header-Datei (hpp) auf mehrere Quelltextdateien (cpp) aufzuteilen.
+ Als *include-guard* im Präprozessor wird `#pragma once` verwendet statt `#ifndef FOO` und `#define FOO` plus `#endif`.
+ Doxygen-Kommentare befinden sich bei einer Klasse in ihrer Header-Datei.
+ Dateinamen und Identifier werden im `lower_snake_case` geschrieben; ausschließlich Klassennamen beginnen mit Großbuchstaben.
+ Es werden als erstes Funktionen der Standardbibliothek verwendet, nur falls unzureichend andere / eigene.
+ Präprozessorcode ist zu meiden.
+ Die Verwendung von `auto` ist dann erlaubt, wenn der Datentyp offensichtlich ist. 
+ Gegen eine *vernünftige* Verwendung von `goto` spricht nichts.
+ <del>Wird kein `auto` verwendet sind vorzeichenlose, primitive Datentypen mit `unsigned` - Vorzeichenmod. auszuschreiben: Also statt `unsigned` - `unsigned int`.</del>
+ Damit man Dinge in Quelltextdateien leichter findet, ist folgende Reihenfolge festgelegt für Klasseninhalte:
    + Freunde, z.B. `friend class Foo;`
    + Private Konstanten, z.B. `static constexpr int KONSTANT = 7;`
    + `public:`
        + Konstruktor(en) 
        + Methoden (& Operatoren)
        + Felder
    + `protected:`
        + Konstruktor(en) 
        + Methoden (& Operatoren)
        + Felder
    + `private:`
        + Konstruktor(en) 
        + Methoden (& Operatoren)
        + Felder 
    + `static` Methoden & Felder kommen vor lokalen Methoden und Feldern der jeweiligen Gruppe.
    
    Diese Reihenfolge hat den Hintergrund, dass man Sachen, die man häufig sucht (d.h. woanders braucht, meist `public`) möglichst weit oben findet.
+ `if`-Anweisungen mit einer einzigen Anweisung müssen entweder in einer Zeile stehen oder von `{` und `}` eingeschlossen sein.
  Folgender Quelltextstil ist daher **nicht** erlaubt:

    ```cpp
    if (bedingung)
        einzige_anweisung;
    ```
+ Bei den `#include` Anweisungen werden erst projekteigene Header eingebunden, dann alle übrigen.
+ Header haben die Dateiendung `hpp`, deren Implementierung endet mit `cpp`.
