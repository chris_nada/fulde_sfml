## 1 Nächste Schritte

### 1.1 Kurzfristig

+ Arbeiter "Fertigkeiten" umbenennen zu "Erfahrung"
+ Werkstatt
  + Neue Anlage 
    + zu Tabelle konvertieren
    + Button disable + Tooltip, falls Platz nicht ausreicht
+ Markt: Kauflimit 1000 anders lösen
+ Uhr überall

### 1.2 Bugs

+ Werkstatt verkleinern: 1 muss nicht übrig bleiben

### 1.3 Langfristig

+ Stadt
  + Geld / Steuern
    + Übersicht im Rathaus

+ Textnachricht senden
+ Handel 
  + vorschlagen
  + annehmen

+ Infrastruktur
  + Bau durch Amtsinhaber
+ Amt
  + Gehalt überweisen (?)

+ 
    
## Ideen

+ Werkstatt
    + Werbung, Nachfrage, Kaufkraft nach Städte
    + Dienstleistungen anbieten (wie Prozess in Werkstatt)
    + Erfahrung sammeln durch Arbeitsprozesse, PC+NPC
    + Produktion von Kunstobjekte?
    + Dienstleistungen?
    
+ Stadt: 
    + Simulation: 
        + Wohnungsbau, 
        + Arbeitslose, 
        + Kaufkraft
        + ...
    + Steuern an Stadt statt Land
    
+ Ämter
    + Vorsitzender ...
        + Handwerkskammer
        + Rohstoffgewerbe
    + Produktion + Handel
        + Verbote: Export, Import, Produktion
        + nur via Abgabe (Verstoß erlauben)

+ Netzwerkkode generifizieren (Idee: Template / Methoden register/dispatch)
