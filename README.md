### **Description** 
**Fulde** is going to be a business management / simulation game, inspired by [Patrician II](https://www.mobygames.com/game/patrician-ii-quest-for-power), [Fugger II](https://www.mobygames.com/game/dos/die-fugger-ii) and [Europa 1400: The Guild.](https://www.mobygames.com/game/europa-1400-the-guild) The name comes from the (german) game titles **Fu**gger + Gi**lde**.

---

**Please note**: Source code and development are primarily in German.
Though the game itself is multi-language.

**Hinweis**: Entwicklung und Quelltext ist hauptsächlich auf Deutsch.

---

### Main features will be:

-   Manage your business
    -   hire workers, 
    -   produce goods, 
    -   buy/sell them in different cities -
    -   (similar as in mentioned games, except workers will be more detailed, have skills & personality)
-   Join a political career: 
    -   adjust taxes, laws.
    -   manage cities' infrastructure and military
    -   attack and defend against other factions.
-   Life simulation aspect
    -   Cooperate or intrigue against other players,
    -   Get married & have kids,
    -   Watch your dynasty flourish or perish (pace is faster than in compared games).
-   Open-ended game play 
-   A nerdy user interface.
-   Computer opponents, and at the same time
-   Online/LAN Multiplayer*
-   Multi language support (English, German, Russian, Ukrainian)**
-   Open-Source
    -   The game is open-source, except for graphic/audio assets, which need to be acquired proprietarily as it seems.

Focus will be on open-ended gameplay and having a lot of diverse options to reach your goals. Having pretty things like graphics and a clean user interface can always be done later, if there's spare time in my opinion, but that may change depending on the feedback I get.

\* Personally I had great fun playing Patrician and Europa 1400 on LAN. That's why this feature is very important to me and I started integrating network code from the beginning (it can be pretty hard to* *impossible to do later).

** Those are the languages I am able to translate. More might be available later depending on demand.

### Status quo
Right now the game is moving forward from a pre-alpha stage:
it's playable, graphics are partially there, computer opponents exist and do something, it runs on LAN, but the interface looks like crappy and the economic balancing needs work.

Follow its progress
You can follow Fulde's progress here on [GitLab](https://gitlab.com/chris_nada/fulde_sfml/activity). You'll see there when I work on Fulde and (if you know german) also what exactly was done.

### First Preview
A first public preview release is planned for 2022.

### Support & Contribute
Contact me if you wish to support or contribute to the development of this project.
