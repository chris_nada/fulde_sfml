Ein Prozess kann man sich denken als _Rezept_.
Es gibt Waren, die Dabei entstehen und Waren die Verbraucht werden.
Das Verbrauchen von Waren ist optional (z.B. könnte in einer Erzmine etwas
produziert werden, aber nichts verbraucht werden).

Eine JSON kann z.B. so aussehen:

```json
[
  {
    "key": "pudelverfahren",
    "gruppe": "hochofen",
    "kosten": 1000,
    "platzverbrauch": 1,
    "techlevel": 0,
    "arbeitsaufwand": 20,
    "arbeiter": 3,
    "arbeiterbonus": 0.8,
    "arbeitermalus": 1.5,
    "platzbonus": 0.07,
    "basisquali": 5.0,
    "inputs": [
      {
        "key": "eisen",
        "value": 2
      },
      {
        "key": "brennstoff",
        "value": 2
      }
    ],
    "outputs": [
      {
        "key": "stahl",
        "value": 1
      }
    ]
  }
]
```

**Zusammenfassung**
+ `key` 
  + Muss einmalig sein.
  + Sollte möglichst kurz sein (wg. Performance beim Look-up).
  + Über diesen Schlüssel arbeitet auch die Übersetzungsdatei.
+ `gruppe`
  + Kann von mehreren Prozessen genutzt werden um sie zusammenzufassen.
+ `kosten`
  + Wie teuer ist das Aufbauen einer Anlage für diesen Prozess? (einmalig)
+ `platzverbrauch`
  + Wie viele Platzeinheiten braucht das Einrichten des Prozesses?
+ `techlevel`
  + Welches Technologielevel muss erreicht worden sein,
    um den Prozess zu nutzen?
+ `arbeitsaufwand`
  + Wie hoch ist der Arbeitsaufwand den Prozess einmal zu durchlaufen?
  + Hier muss experimentiert werden, was gute/gerechte Werte sind.
+ `arbeiter`
  + Wie viele Arbeiter werden für gewöhnlich gebraucht, um den Prozess
    auszuführen?
+ `arbeiterbonus`
  + Wie viel Arbeitskraft fügt jeder Arbeiter hinzu, 
  der über die Anzahl `arbeiter` hinaus geht?
  + Faktor, der < 1.0 sein sollte.
+ `arbeitermalus`
  + Wie viel Mehrarbeit müssen Arbeiter leisten, wenn
  weniger als `arbeiter` zur Verfügung stehen?
  + Faktor, der > 1.0 sein sollte. 
  <br/>1.0 hieße kein Mehraufwand;
  <br/>1.25 hieße 25% Mehraufwand.
+ `platzbonus`
  + Wie viel Produktivitätsbonus bringt jede Platzeinheit zusätzlich,
  die über den regulären Platzbedarf hinaus geht?
  + 0.05 hieße z.B. 5%.
+ `basisquali`
  + Wie hoch ist die Qualität der `output` - Waren?
  + Wird jedoch noch von der Qualität der `input` - Waren beeinflusst.
+ `inputs`
  + `key` und Anzahl (`value`) der benötigten Eingangsstoffe in das Rezept.
  + Für die korrekte Formatierung vgl. Beispiele. 
  + Auch wenn nur 1 Input, muss dieses als Array geschrieben werden.
+ `outputs`
  + `key` und Anzahl (`value`) der Produkte des Rezepts.
  + Für die korrekte Formatierung vgl. Beispiele. 
  + Auch wenn 'nur' 1 Produkt, muss dieses als Array geschrieben werden.