Waren sind unterteilt in:
  + Rohstoffe,
  + Zwischenprodukte und
  + Produkte.

Um eine Ware hinzuzufügen, reicht es eine JSON-Datei in einer
der Verzeichnisse abzulegen, der Dateiname spielt keine Rolle.

Zusätzlich sollten Übersetzungen mit dem vergebenem Schlüssel
in die Datei `data/lingua/lingua.csv` eingetragen werden.

Eine JSON kann z.B. so aussehen:

```json
{
  "key": "bier",
  "gruppe": "produkt",
  "icon": "data/gfx/icons/waren/empty.png",
  "basispreis": 15,
  "gewichtung_verbrauch": 5,
  "gewichtung_produktion": 2,
  "basisquali": 5.0,
  "qualifaktor_preis": 1.0,
  "chance_verbot": 1,
  "chance_lizenz": 25
}
```

**Zusammenfassung**
+ `key` 
  + Muss einmalig sein.
  + Sollte möglichst kurz sein (wg. Performance beim Look-up).
  + Über diesen Schlüssel arbeitet auch die Übersetzungsdatei.
+ `gruppe`
  + Hat momentan nur eine Sortierfunktion.
  + Angedachte sind engere Kategorien wie: 
      + `waffe`, `kleidung`, `nahrung`, `rohstoff` usw.
+ `icon`
  + Pfad zu einer Bilddatei (PNG, 24x24 Pixel).
+ `basispreis`
  + Gewöhnlicher Marktpreis.
+ `gewichtung_verbrauch`
  + Gewichtung, dass ein Zivilist diese Ware nachfragt.
+ `gewichtung_produktion`
  + Gewichtung, dass ein Zivilist diese Ware am Markt verkauft.
+ `basisquali`
  + Wie hoch ist "normalerweise" die Qualität der Ware,
    d.h. findet man in der Regel Exemplare hoher Qualität oder nicht.
  + Muss zwischen 0 und 10 liegen.
  + float, d.h. darf z.B. `5.25` sein.
+ `qualifaktor_preis`
  + Wie groß ist die Auswirkung der Qualität auf den Preis?
  + 1.0 bedeutet normal (100%), 1.5 wäre hoch (200%), 0.25 sehr niedrig (25%). 
+ `chance_verbot`
  + Wie hoch ist die Wahrscheinlichkeit in Prozent,
    dass die Produktion und der Handel der Ware von einem Staat verboten wird?
  + Jährliche Wahrscheinlichkeit.
  + Die Ware kann auch wieder legalisiert werden.
+ `chance_lizenz`
  + Wie hoch ist die Wahrscheinlichkeit in Prozent,
    dass man für die Produktion und den Handel mit der Ware
    eine Lizenz benötigt
  + Eine Lizenz kann vom Staat erworben werden.